package org.fenrir.launcher;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO v1.0 Documentació
 * TODO Avís d'error
 * @author Antonio Archilla Nava
 * @version v0.0.20130204
 */
public class MainProgram
{
    private static final Logger log = LoggerFactory.getLogger(MainProgram.class);

    public static void main(String... args)
    {
        // S'inicia el programa
        MainProgram mainProgram = new MainProgram();
        try{
            mainProgram.launch();
        }
        catch(Exception e){
            log.error("Error d'execució de l'aplicació: {}", e.getMessage(), e);
        }
    }

    private void update() throws Exception
    {        
        File actionsFile = new File("update/actions.xml");
        if(!actionsFile.exists()){
            if(log.isDebugEnabled()){
                log.debug("No s'ha trobat fitxer d'actualitzacions; Es continua amb l'execució normal de l'aplicació");
            }
            return;
        }

        XPath xpath = XPathFactory.newInstance().newXPath(); 
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document actionsDocument = builder.parse(actionsFile);
        // Es mira si té el flag d'actualitzable
        XPathExpression expression = xpath.compile("//updatable");
        String strUpdatable = (String)expression.evaluate(actionsDocument, XPathConstants.STRING);
        if(!Boolean.parseBoolean(strUpdatable)){
            if(log.isDebugEnabled()){
                log.debug("S'ha trobat fitxer d'actualitzacions no actualitzable; Es continua amb l'execució normal de l'aplicació");
            }
            return;
        }
        
        // Es busquen tots els artifacts actualizables i es fa l'actualització
        expression = xpath.compile("//action");
        NodeList actionNodes = (NodeList)expression.evaluate(actionsDocument, XPathConstants.NODESET);
        for(int i=0; i<actionNodes.getLength(); i++){
            Node node = actionNodes.item(i);
            NodeList childNodes = node.getChildNodes();
            String type = null;
            String sourcePath = null;
            String destinationPath = null;
            for(int j=0; j<childNodes.getLength(); j++){
                Node childNode = childNodes.item(j);
                if("type".equalsIgnoreCase(childNode.getNodeName())){
                    type = childNode.getTextContent();
                }
                else if("source-path".equalsIgnoreCase(childNode.getNodeName())){
                    sourcePath = childNode.getTextContent();
                }
                else if("destination-path".equalsIgnoreCase(childNode.getNodeName())){
                    destinationPath = childNode.getTextContent();
                }
            }
                
            // Actualitza el fitxer depenent del tipus d'acció
            if("CREATE".equals(type)){
                File updateFile = new File("update/" + destinationPath);
                File applicationFile = new File(destinationPath);
                String folderName = FilenameUtils.getFullPathNoEndSeparator(applicationFile.getAbsolutePath());
                FileUtils.forceMkdir(new File(folderName));
                if(log.isDebugEnabled()){
                    log.debug("CREATE: Copiant fitxer {} a {}", updateFile.getAbsolutePath(), applicationFile.getAbsolutePath());
                }
                FileUtils.copyFile(updateFile, applicationFile);                    
            }
            else if("UPDATE".equals(type)){
                File updateFile = new File("update/" + destinationPath);
                File applicationOldFile = new File(sourcePath);
                File applicationNewFile = new File(destinationPath);
                String folderName = FilenameUtils.getFullPathNoEndSeparator(applicationNewFile.getAbsolutePath());
                FileUtils.forceMkdir(new File(folderName));
                // S'esborra el fitxer antic
                applicationOldFile.delete();
                // Es copia el nou
                if(log.isDebugEnabled()){
                    log.debug("UPDATE: Copiant fitxer {} a {}", updateFile.getAbsolutePath(), applicationNewFile.getAbsolutePath());
                }
                FileUtils.copyFile(updateFile, applicationNewFile);
            }
            else if("DELETE".equals(type)){
                File applicationFile = new File(sourcePath);
                if(log.isDebugEnabled()){
                    log.debug("DELETE: Esborrant fitxer {}", applicationFile.getAbsolutePath());
                }
                applicationFile.delete();
            }
        }
        
        // Una vegada acabat el procés es netaja el directori d'actualitzacions
        FileUtils.cleanDirectory(new File("update"));
    }
    
    private void launch() throws Exception
    {
        // Es comprova si s'ha d'actualitzar
        update();
        
        Map<String, Object> params = new HashMap<String, Object>();
        // S'utilitzarà quan l'splash screen es pugui utilitzar desde fora
//        params.put("splash", "-splash:splash_screen.png");
        params.put("options", "-jar");
        params.put("executable", "dragonfly.jar");
        CommandLine command = CommandLine.parse("java");
        // S'utilitzarà quan l'splash screen es pugui utilitzar desde fora
//        command.addArgument("${splash}", false);
        command.addArgument("${options}", false);
        command.addArgument("${executable}", false);
        command.setSubstitutionMap(params);
        ExecuteResultHandler resultHandler = new ExecuteResultHandler()
        {
            @Override
            public void onProcessFailed(ExecuteException e)
            {
                log.error("Error en l'execució del programa exitCode={}: {}", new Object[]{e.getExitValue(), e.getMessage(), e});
            }

            @Override
            public void onProcessComplete(int exitValue)
            {
                log.info("Execució del programa extern completada amb èxit exitValue={}", exitValue);
                // Error
                if(exitValue!=0){
                    log.error("Error en l'execució del programa exitCode={}: {}", exitValue);
                }
                // Reinici
                if(restartNeeded()){
                    try{
                        launch();
                    }
                    catch(Exception e){
                        log.error("Error d'execució de l'aplicació: {}", e.getMessage(), e);
                    }
                }
            }
        };
        DefaultExecutor executor = new DefaultExecutor();
        // Al proporcionar un resultHandler s'habilita l'execució asincrona del procés
        executor.execute(command, resultHandler);
    }

    private boolean restartNeeded()
    {
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse("workspaces.xml");
            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = xpath.compile("//workspaces/workspace/@restart");
            String restart = (String)expr.evaluate(document, XPathConstants.STRING);
            return Boolean.parseBoolean(restart);
        }
        catch(Exception e){
            // Pot ser que no existeixi si es surt del programa abans d'escollir el workspace inicial
            log.error("Error en llegir el fitxer de definició de workspaces: {}", e.getMessage(), e);
            return false;
        }
    }
}
