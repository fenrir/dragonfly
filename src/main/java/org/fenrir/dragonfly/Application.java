package org.fenrir.dragonfly;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.google.inject.persist.PersistService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.TaskProgressDialog;
import org.fenrir.yggdrasil.ui.event.IWindowListener;
import org.fenrir.yggdrasil.ui.worker.AbstractWorker;
import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.dragonfly.ui.action.InitialConfigurationAction;
import org.fenrir.dragonfly.ui.action.LoginAction;
import org.fenrir.dragonfly.ui.event.ApplicationWindowListener;
import org.fenrir.dragonfly.ui.externalize.dialog.TaskProgressDialogExt;
import org.fenrir.dragonfly.core.CorePreferenceConstants;
import org.fenrir.dragonfly.core.event.IDatabaseEventListener;
import org.fenrir.dragonfly.core.service.IApplicationModeSetupService;
import org.fenrir.dragonfly.core.service.IDatabaseUpdaterService;

/**
 * TODO v1.0 Javadoc
 * TODO v0.2 Gestió d'excepcions
 * @author Antonio Archilla Nava
 * @version v0.3.20140721
 */
@EventListener(definitions={IDatabaseEventListener.class})
public class Application extends AbstractApplication implements IDatabaseEventListener
{
    private Logger log = LoggerFactory.getLogger(Application.class);
        
    @Override
    public void start() throws ApplicationException
    {
    	// S'especifica l'implementació del listener de la finestra principal de l'aplicació
    	IEventNotificationService notificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	ApplicationWindowListener listener = new ApplicationWindowListener();
    	listener.setApplicationInstance(this);
    	notificationService.addListener(IWindowListener.class, listener);
    }

    @Override
    public void stop() throws ApplicationException
    {
    	
    }
 
    @Override
    public void onBeforeWorkspaceUnload()
    {
        if(log.isDebugEnabled()){
            log.debug("Executant aturada de la persistència i l'scheduler de tasques");
        }
        
        PersistService persisteService = (PersistService)ApplicationContext.getInstance().getRegisteredComponent(PersistService.class);
        persisteService.stop();
        
        // Es comunica l'event de desconexió amb la base de dades
        IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
        try{
            eventNotificationService.notifyEvent(IDatabaseEventListener.class, IDatabaseEventListener.EVENT_DATABASE_CONNECTION_RELEASED_ID);
        }
        catch(Exception e){
            log.error("Error llançant event {}: {}", new Object[]{IDatabaseEventListener.EVENT_DATABASE_CONNECTION_RELEASED_ID, e.getMessage(), e});
        }
    }
    
    @Override
    public void databaseConnectionEstablished() throws ApplicationException
    {
        performDataUpdates();
    }

    @Override
    public void databaseConnectionReleased() throws ApplicationException
    {
        // Res a fer
    }
    
    public boolean setupApplication()
    {
    	IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
    	
    	String initialConfigurationDone = preferenceService.getProperty(CorePreferenceConstants.INITIAL_CONFIGURATION_DONE);
    	if(initialConfigurationDone!=null && !Boolean.valueOf(initialConfigurationDone)){
    		performInitialConfiguration();
    		
    		initialConfigurationDone = preferenceService.getProperty(CorePreferenceConstants.INITIAL_CONFIGURATION_DONE);
        	if(initialConfigurationDone!=null && !Boolean.valueOf(initialConfigurationDone)){
        		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
	                    "Es necessari realitzar la configuració inicial de l'aplicació",
	                    "Advertència",
	                    JOptionPane.WARNING_MESSAGE);
	        	
	        	return false;
        	}
    	}
    	
    	// Inicialització del contexte de l'aplicació. Necessari per tenir accessibles tots els components depenents del mode d'execució
    	try{
    		ApplicationContext.getInstance().configureContext(ApplicationConstants.EXECUTION_CONTEXT_APPLICATION_LOADED);
    	}
    	catch(ApplicationException e){
    		log.error("Error durant la inicialització de l'espai de treball: {}", e.getMessage(), e);
    		ApplicationWindowManager.getInstance().displayErrorMessage("Error durant la inicialització de l'espai de treball", e);
    		
    		return false;
    	}
    	
    	IApplicationModeSetupService applicationSetupService = (IApplicationModeSetupService)ApplicationContext.getInstance().getRegisteredComponent(IApplicationModeSetupService.class);
    	// Comprobació de la compatibilitat del model de l'aplicació BBDD o Servidor Vespine
		try{
			applicationSetupService.checkModelCompatibility();
		}
		catch(ApplicationException e){
			log.error("Error durant la comprovació de compatibilitat: {}", e.getMessage(), e);
			JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                    e.getMessage(),
                    "Advertència",
                    JOptionPane.WARNING_MESSAGE);
			
			return false;
		}
    	
    	// Aquesta part pot tardar bastant temps perquè implica la configuració inicial de la base de dades. Es mostra una finestra de progrés
    	AbstractWorker<Void, String> worker = new AbstractWorker<Void, String>() 
        {
            @Override
            protected Void doInBackground() throws Exception 
            {                
            	IApplicationModeSetupService applicationSetupService = (IApplicationModeSetupService)ApplicationContext.getInstance().getRegisteredComponent(IApplicationModeSetupService.class);
            	applicationSetupService.setupApplication();
                
                return null;
            }
        };            
        TaskProgressDialogExt dialog = null;
        try{
            dialog = new TaskProgressDialogExt("Inicialitzant espai de treball...", worker);
            // Es configura el tancament automàtic de la finestra en finalitzar la tasca
            dialog.setCloseOnDone(true);
            dialog.open();
        }
        catch(Exception e){
            log.error("Error durant la inicialització de l'espai de treball: {}", e.getMessage(), e);
            // Primer es tanca la finestra de progrés en cas que s'hagi quedat oberta
            if(dialog!=null && dialog.isVisible()){
                dialog.setVisible(false);
                dialog.dispose();
            }
            ApplicationWindowManager.getInstance().displayErrorMessage("Error durant la inicialització de l'espai de treball", e);
            
            return false;
        }
		
		return true;
    }
    
    public void login()
    {
        // L'actualització de la UI s'ha de fer en un Thread a part per no bloquejar l'obertura de la finestra
    	if(SwingUtilities.isEventDispatchThread()){
    		LoginAction action = new LoginAction();
    		action.actionPerformed();
    	}
    	else{
    		SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                	login();
                }
            });
    	}
    }
    
    private void performInitialConfiguration()
	{
		// L'actualització de la UI s'ha de fer en un Thread a part per no bloquejar l'obertura de la finestra
		if(SwingUtilities.isEventDispatchThread()){
			InitialConfigurationAction action = new InitialConfigurationAction();
			action.actionPerformed();
		}
		else{
			SwingUtilities.invokeLater(new Runnable()
			{
				@Override
				public void run()
				{
					performInitialConfiguration();
				}
			});
		}
	}
    
    private void performDataUpdates() throws ApplicationException
    {
    	final ApplicationContext applicationContext = ApplicationContext.getInstance();
        final IDatabaseUpdaterService databaseUpdaterService = (IDatabaseUpdaterService)applicationContext.getRegisteredComponent(IDatabaseUpdaterService.class);
        final IApplicationModeSetupService applicationSetupService = (IApplicationModeSetupService)applicationContext.getRegisteredComponent(IApplicationModeSetupService.class);
        
        // Només s'executen les actualitzacions si és necessari
        if(databaseUpdaterService.isDataUpdateNeeded()){
        	// En el cas que la finestra pricipal estigui activa, es mostra la finestra de progrés
        	if(ApplicationWindowManager.getInstance().isMainWindowActive()){
	            AbstractWorker<Void, String> worker = new AbstractWorker<Void, String>() 
	            {
	                @Override
	                protected Void doInBackground() throws Exception 
	                {                
	                    applicationSetupService.performDataUpdates();
	                    
	                    return null;
	                }
	            };            
	            TaskProgressDialog dialog = null;
	            try{
	                dialog = new TaskProgressDialog("Actualitzant dades", worker);
	                dialog.open();
	            }
	            catch(Exception e){
	                log.error("Error actualitzant les dades gestionades per l'aplicació: {}", e.getMessage(), e);
	                // Primer es tanca la finestra de progrés en cas que s'hagi quedat oberta
	                if(dialog!=null && dialog.isVisible()){
	                    dialog.setVisible(false);
	                    dialog.dispose();
	                }
	                ApplicationWindowManager.getInstance().displayErrorMessage("Error actualitzant les dades gestionades per l'aplicació", e);
	            }
        	}
        	else{
        		try{
        			applicationSetupService.performDataUpdates();
            	}
            	catch(Exception e){
            		log.error("Error actualitzant les dades gestionades per l'aplicació: {}", e.getMessage(), e);
                    ApplicationWindowManager.getInstance().displayErrorMessage("Error actualitzant les dades gestionades per l'aplicació", e);
            	}
        	}
        }        
        
        try{
        	applicationSetupService.scheduleTasks();
    	}
    	catch(Exception e){
    		log.error("Error programant les tasques automàtiques: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error programant les tasques automàtiques", e);
    	}
    }
}
