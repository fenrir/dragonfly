package org.fenrir.dragonfly;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140722
 */
public class ApplicationConstants 
{
	/*--------------------------*
	 *    EXECUTION CONTEXTS    * 
	 *--------------------------*/
	public static final String EXECUTION_CONTEXT_APPLICATION_LOADED = "application_loaded";
	
	/*--------------------------*
	 *     MODES D'EXECUCIÓ     * 
	 *--------------------------*/
	public static final String APPLICATION_MODE_STANDALONE = "STANDALONE";
	public static final String APPLICATION_MODE_DISTRIBUTED = "DISTRIBUTED";
	public static final String APPLICATION_MODE_CLIENT = "CLIENT";
	
	/*--------------------------*
	 *  UNITATS DE PERSISTENCIA * 
	 *--------------------------*/
	public static final String PERSISTENCE_UNIT_STANDALONE_MODE = "standaloneModeUnit";
    public static final String PERSISTENCE_UNIT_CLIENT_MODE = "clientModeUnit";
    
    /*--------------------------*
	 *      CONTEXTS BBDD       * 
	 *--------------------------*/
    public static final String DATABASE_SCHEMA_CONTEXT_BASE = "base";
    public static final String DATABASE_SCHEMA_CONTEXT_DEVEL = "devel";
    public static final String DATABASE_SCHEMA_CONTEXT_PROD = "prod";
    
    /*--------------------------*
	 *         WORKSPACE        * 
	 *--------------------------*/
    public static final String INDEX_FOLDER = "index";
}
