package org.fenrir.dragonfly;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.dragonfly.util.YggdrasilApplicationContextAdapter;
import org.fenrir.vespine.spi.ApplicationContextHandler;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140330
 */
public class MainProgram 
{
    public static void main(String... args) throws Exception
    {                
        Logger log = LoggerFactory.getLogger(MainProgram.class);

        boolean error = false;

        /* Es posa a disposició de la infraestructura de Vespine el context de l'aplicació a través d'un adaptador.
         * S'ha de fer en aquest punt per tal que estigui disponible a la construcció dels mòduls de Vespine dins l'inicialització de l'aplicació
         */
        ApplicationContext applicationContext = ApplicationContext.getInstance();
    	ApplicationContextHandler.getInstance().setApplicationContext(new YggdrasilApplicationContextAdapter(applicationContext));
        
        Application application = new Application();
        try{
            application.initialize();
        }
        catch(ApplicationException e){
            log.error("Error fatal inicialitzant l'aplicació: {}", e.getMessage(), e);
            ApplicationWindowManager.displayStandaloneErrorMessage("Error fatal incialitzant l'aplicació: " + e.getMessage(), e);
            // No es pot fer un exit -1 perquè es tanca la finestra de l'error
            error = true;
        }

        if(!error){
	        try{
	            application.run();
	        }
	        catch(ApplicationException e){
	            log.error("Error fatal executant l'aplicació: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error fatal executant l'aplicació: " + e.getMessage(), e);
	            // No es pot fer un exit -1 perquè es tanca la finestra de l'error
	        }
        }        
    }    
}
