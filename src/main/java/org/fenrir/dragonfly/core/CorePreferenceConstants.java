package org.fenrir.dragonfly.core;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140723
 */
public class CorePreferenceConstants 
{
    public static final String APPLICATION_MODE = "//preferences/application/@mode";
    public static final String APPLICATION_DEFAULT_USER = "//preferences/application/defaultUser";
    public static final String APPLICATION_SERVER = "//preferences/application/server";
    public static final String APPLICATION_SERVER_URL = APPLICATION_SERVER + "/url";

    public static final String INITIAL_CONFIGURATION_DONE = "//preferences/@configured";
    
    public static final String CONNECTION_PROVIDER = "//preferences/connection/provider";
    public static final String CONNECTION_DIALECT = "//preferences/connection/dialect";
    public static final String CONNECTION_PROVIDER_DEFAULT_VALUE = "org.apache.derby.jdbc.EmbeddedDriver";
    public static final String CONNECTION_PROVIDER_DEFAULT_DIALECT_VALUE = "org.hibernate.dialect.DerbyDialect";
    public static final String CONNECTION_URL = "//preferences/connection/url";
    public static final String CONNECTION_USER = "//preferences/connection/user";
    public static final String CONNECTION_PASSWORD = "//preferences/connection/password";
    
    public static final String INDEX_PROVIDER = "//preferences/index/provider";
    public static final String INDEX_PROVIDER_FILESYSTEM_VALUE = "filesystem";
    public static final String INDEX_PROVIDER_DATABASE_VALUE = "database";
    public static final String INDEX_DB_HOST = "//preferences/index/host";
    public static final String INDEX_DB_PORT = "//preferences/index/port";
    public static final String INDEX_DB_DATABASE = "//preferences/index/database";
    public static final String INDEX_DB_USER = "//preferences/index/user";
    public static final String INDEX_DB_PASSWORD = "//preferences/index/password";
    
    public static final String EXT_APPLICATION_BROWSER = "//preferences/externalApplications/browser";
}
