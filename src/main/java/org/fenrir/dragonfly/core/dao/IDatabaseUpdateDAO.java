package org.fenrir.dragonfly.core.dao;

import java.util.List;
import org.fenrir.dragonfly.core.entity.DatabaseUpdate;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20130630
 */
public interface IDatabaseUpdateDAO
{
    public List<DatabaseUpdate> findAllDatabaseUpdates();
    public long countAllDatabaseUpdates();

    public DatabaseUpdate findDatabaseUpdateById(Long id);

    public DatabaseUpdate createDatabaseUpdate(DatabaseUpdate issue);
    public DatabaseUpdate updateDatabaseUpdate(DatabaseUpdate issue);
    public void deleteDatabaseUpdate(DatabaseUpdate issue);
}
