package org.fenrir.dragonfly.core.dao;

import java.util.Date;
import java.util.List;
import org.fenrir.dragonfly.core.entity.IssueTimerRegistry;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140413
 */
public interface IIssueTimerRegistryDAO 
{
	public List<IssueTimerRegistry> findAllRegistries();
	public long countAllRegistries();
	public List<IssueTimerRegistry> findOldWorkedRegistries(Date date, List<Long> ids);
	public long countOldWorkedRegistries(Date date, List<Long> ids);
	
	public IssueTimerRegistry findRegistryById(Long id);
	
	public IssueTimerRegistry createRegistry(IssueTimerRegistry registry);
	public IssueTimerRegistry updateRegistry(IssueTimerRegistry registry);
	public void deleteRegistry(IssueTimerRegistry registry);
	public void deleteOldWorkedRegistries(Date date, List<Long> ids);
}
