package org.fenrir.dragonfly.core.dao.impl;

import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.dragonfly.core.dao.IDatabaseUpdateDAO;
import org.fenrir.dragonfly.core.entity.DatabaseUpdate;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20130630
 */
public class DatabaseUpdateDAOImpl implements IDatabaseUpdateDAO
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject
    private Provider<EntityManager> entityManagerProvider;

    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }

    @Override
    @Transactional
    public List<DatabaseUpdate> findAllDatabaseUpdates()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("from DatabaseUpdate order by id asc").getResultList();
    }
    
    @Override
    @Transactional
    public long countAllDatabaseUpdates()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(o) from DatabaseUpdate o", Long.class)
                .getSingleResult();
    }

    @Override
    @Transactional
    public DatabaseUpdate findDatabaseUpdateById(Long id) 
    {
        EntityManager em = entityManagerProvider.get();
        return em.find(DatabaseUpdate.class, id);
    }

    @Override
    @Transactional
    public DatabaseUpdate createDatabaseUpdate(DatabaseUpdate databaseUpdate)
    {
        EntityManager em = entityManagerProvider.get();
        em.persist(databaseUpdate);

        return databaseUpdate;
    }

    @Override
    @Transactional
    public DatabaseUpdate updateDatabaseUpdate(DatabaseUpdate databaseUpdate)
    {
        /* em.merge() dóna problemes i per tant s'ha de carregar primer la instancia al persistence context
         * i després modificar-la. D'aquesta manera els canvis es guardaran a bdd en fer flush / commit
         */
        DatabaseUpdate managedDatabaseUpdate = findDatabaseUpdateById(databaseUpdate.getId());
        managedDatabaseUpdate.setUpdateClass(databaseUpdate.getUpdateClass());
        managedDatabaseUpdate.setUpdateMethod(databaseUpdate.getUpdateMethod());

        return managedDatabaseUpdate;
    }

    @Override
    @Transactional
    public void deleteDatabaseUpdate(DatabaseUpdate databaseUpdate)
    {
        EntityManager em = entityManagerProvider.get();
        em.remove(databaseUpdate);
    }
}
