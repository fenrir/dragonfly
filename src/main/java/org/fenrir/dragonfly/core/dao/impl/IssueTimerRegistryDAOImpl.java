package org.fenrir.dragonfly.core.dao.impl;

import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import org.fenrir.dragonfly.core.dao.IIssueTimerRegistryDAO;
import org.fenrir.dragonfly.core.entity.IssueTimerRegistry;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140413
 */
public class IssueTimerRegistryDAOImpl implements IIssueTimerRegistryDAO 
{
	/* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
	
	@Override
	@Transactional
	public List<IssueTimerRegistry> findAllRegistries() 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueTimerRegistry", IssueTimerRegistry.class).getResultList();
	}

	@Override
	@Transactional
	public List<IssueTimerRegistry> findOldWorkedRegistries(Date date, List<Long> ids) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.createQuery("from IssueTimerRegistry where id in :idList and workingDate<:workingDate and elapsedTime>0", IssueTimerRegistry.class)
        	.setParameter("idList", ids)
            .setParameter("workingDate", date)
            .getResultList();
	}
	
	@Override
    @Transactional
    public long countAllRegistries()
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from IssueTimerRegistry i", Long.class)
                .getSingleResult();
    }
	
	@Override
    @Transactional
    public long countOldWorkedRegistries(Date date, List<Long> ids) 
    {
        EntityManager em = entityManagerProvider.get();
        return em.createQuery("select count(i) from IssueTimerRegistry i where id in :idList and workingDate<:workingDate and elapsedTime>0", Long.class)
        		.setParameter("idList", ids)
        		.setParameter("workingDate", date)
                .getSingleResult();
    }

	@Override
	@Transactional
	public IssueTimerRegistry findRegistryById(Long id) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.find(IssueTimerRegistry.class, id);
	}
	
	@Override
	@Transactional
	public IssueTimerRegistry createRegistry(IssueTimerRegistry registry) 
	{
		EntityManager em = entityManagerProvider.get();
		em.persist(registry);
		
		return registry;
	}

	@Override
	@Transactional
	public IssueTimerRegistry updateRegistry(IssueTimerRegistry registry) 
	{
		EntityManager em = entityManagerProvider.get();
        return em.merge(registry);
	}

	@Override
	@Transactional
	public void deleteRegistry(IssueTimerRegistry registry) 
	{
		EntityManager em = entityManagerProvider.get();
        em.remove(registry);
	}

	@Override
	@Transactional
	public void deleteOldWorkedRegistries(Date date, List<Long> ids) 
	{
		EntityManager em = entityManagerProvider.get();
        em.createQuery("delete from IssueTimerRegistry where id in :idList and workingDate<:workingDate and elapsedTime>0")
        	.setParameter("idList", ids)
            .setParameter("workingDate", date)
            .executeUpdate();
	}
}
