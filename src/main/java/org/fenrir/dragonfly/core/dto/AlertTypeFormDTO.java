package org.fenrir.dragonfly.core.dto;

import java.util.Date;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140422
 */
public class AlertTypeFormDTO implements IAlertTypeDTO 
{
	private String alertTypeId;
	private String name;
	private String description;
	private String icon;
	private String alertRule;
	private Date creationDate;
	private Date lastUpdated = new Date();
	
	@Override
	public String getAlertTypeId() 
	{
		return alertTypeId;
	}
	
	public void setAlertTypeId(String alertTypeId)
	{
		this.alertTypeId = alertTypeId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}

	@Override
	public String getIcon() 
	{
		return icon;
	}
	
	public void setIcon(String icon)
	{
		this.icon = icon;
	}

	@Override
	public String getAlertRule() 
	{
		return alertRule;
	}
	
	public void setAlertRule(String alertRule)
	{
		this.alertRule = alertRule;
	}

	@Override
	public Date getCreationDate() 
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
}
