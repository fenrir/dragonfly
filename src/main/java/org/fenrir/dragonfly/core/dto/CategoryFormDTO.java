package org.fenrir.dragonfly.core.dto;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.dto.IVespineCategoryDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140419
 */
public class CategoryFormDTO implements IVespineCategoryDTO 
{
	private String categoryId;
	private String name;
	private List<IProviderElementDTO> providerCategories;
	private Date lastUpdated = new Date();
	
	@Override
	public String getProvider() 
	{
		return VespineCoreConstants.PROVIDER_VESPINE;
	}

	@Override
	public String getCategoryId() 
	{
		return categoryId;
	}
	
	public void setCategoryId(String categoryId)
	{
		this.categoryId = categoryId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public IProjectDTO getProject() 
	{
		return null;
	}

	@Override
	public List<IProviderElementDTO> getProviderCategories() 
	{
		if(providerCategories==null){
			return Collections.emptyList();
		}
		
		return providerCategories;
	}
	
	public void setProviderCategories(List<IProviderElementDTO> providerCategories)
	{
		this.providerCategories = providerCategories;
	}
	
	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
}
