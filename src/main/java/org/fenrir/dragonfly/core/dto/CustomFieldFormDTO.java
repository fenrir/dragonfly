package org.fenrir.dragonfly.core.dto;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.dto.IVespineCustomFieldDTO;
import org.fenrir.vespine.core.dto.IVespineProjectDTO;
import org.fenrir.vespine.spi.dto.IFieldType;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140502
 */
public class CustomFieldFormDTO implements IVespineCustomFieldDTO 
{
	private String fieldId;
	private String name;
	private IFieldType fieldType;
	private boolean mandatory = false;
	private String dataProviderId;
	private List<IVespineProjectDTO> projects;
	private Date lastUpdated = new Date();
	
	@Override
	public String getProvider() 
	{
		return VespineCoreConstants.PROVIDER_VESPINE;
	}

	@Override
	public String getFieldId() 
	{
		return fieldId;
	}
	
	public void setFieldId(String fieldId)
	{
		this.fieldId = fieldId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public boolean isMandatory() 
	{
		return mandatory;
	}
	
	public void setMandatory(boolean mandatory)
	{
		this.mandatory = mandatory;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	@Override
	public IFieldType getFieldType() 
	{
		return fieldType;
	}
	
	public void setFieldType(IFieldType fieldType)
	{
		this.fieldType = fieldType;
	}

	@Override
	public String getDataProviderId() 
	{
		return dataProviderId;
	}
	
	public void setDataProviderId(String dataProviderId)
	{
		this.dataProviderId = dataProviderId;
	}

	@Override
	public List<IVespineProjectDTO> getProjects() 
	{
		if(projects==null){
			return Collections.emptyList();
		}
		return projects;
	}
	
	public void setProjects(List<IVespineProjectDTO> projects)
	{
		this.projects = projects;
	}
}
