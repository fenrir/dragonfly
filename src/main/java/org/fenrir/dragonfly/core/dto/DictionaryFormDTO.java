package org.fenrir.dragonfly.core.dto;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140420
 */
public class DictionaryFormDTO implements IDictionaryDTO 
{
	private String dictionaryId;
	private String name;
	private String description;
	private List<IDictionaryItemDTO> dictionaryItems;
	private Date lastUpdated = new Date();
	
	@Override
	public String getDictionaryId() 
	{
		return dictionaryId;
	}
	
	public void setDictionaryId(String dictionaryId)
	{
		this.dictionaryId = dictionaryId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}

	@Override
	public List<IDictionaryItemDTO> getDictionaryItems() 
	{
		if(dictionaryItems==null){
			return Collections.emptyList();
		}
		return dictionaryItems;
	}
	
	public void setDictionaryItems(List<IDictionaryItemDTO> dictionaryItems)
	{
		this.dictionaryItems = dictionaryItems;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
}
