package org.fenrir.dragonfly.core.dto;

import java.util.Date;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140420
 */
public class DictionaryItemFormDTO implements IDictionaryItemDTO 
{
	private String itemId;
	private String value;
	private String description;
	private Date lastUpdated = new Date();
	
	@Override
	public String getItemId() 
	{
		return itemId;
	}
	
	public void setItemId(String itemId)
	{
		this.itemId = itemId;
	}

	@Override
	public IDictionaryDTO getDictionary() 
	{
		return null;
	}

	@Override
	public String getValue() 
	{
		return value;
	}
	
	public void setValue(String value)
	{
		this.value = value;
	}

	@Override
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
}
