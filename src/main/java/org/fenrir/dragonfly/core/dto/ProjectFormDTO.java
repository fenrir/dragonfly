package org.fenrir.dragonfly.core.dto;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.dto.IVespineProjectDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140419
 */
public class ProjectFormDTO implements IVespineProjectDTO 
{
	private String projectId;
	private String name;
	private String abbreviation;
	private IWorkflowDTO workflow;
	private List<IProviderElementDTO> providerProjects;
	private Date lastUpdated = new Date();
	
	@Override
	public String getProvider() 
	{
		return VespineCoreConstants.PROVIDER_VESPINE;
	}

	@Override
	public String getProjectId() 
	{
		return projectId;
	}
	
	public void setProjectId(String projectId)
	{
		this.projectId = projectId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String getAbbreviation() 
	{
		return abbreviation;
	}
	
	public void setAbbreviation(String abbreviation)
	{
		this.abbreviation = abbreviation;
	}

	@Override
	public IProjectDTO getParentProject() 
	{
		return null;
	}

	@Override
	public List<IProjectDTO> getSubprojects() 
	{
		return Collections.emptyList();
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	@Override
	public List<IProviderElementDTO> getProviderProjects() 
	{
		if(providerProjects==null){
			return Collections.emptyList();
		}
		
		return providerProjects;
	}
	
	public void setProviderProjects(List<IProviderElementDTO> providerProjects)
	{
		this.providerProjects = providerProjects;
	}

	@Override
	public IWorkflowDTO getWorkflow() 
	{
		return workflow;
	}
	
	public void setWorkflow(IWorkflowDTO workflow)
	{
		this.workflow = workflow;
	}
}
