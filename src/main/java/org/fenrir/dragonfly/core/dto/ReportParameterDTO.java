package org.fenrir.dragonfly.core.dto;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.0.20121104
 */
public class ReportParameterDTO
{
    private String name;
    private String type;
    private String description;

    private Object value;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Object getValue()
    {
        return value;
    }

    public void setValue(Object value)
    {
        this.value = value;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }

        ReportParameterDTO other = (ReportParameterDTO) obj;
        /* Name */
        if(name==null){
            if(other.name!=null){
                return false;
            }
        }
        else if(!name.equals(other.name)){
            return false;
        }

        return true;
    }
}
