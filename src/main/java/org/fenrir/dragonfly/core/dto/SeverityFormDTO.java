package org.fenrir.dragonfly.core.dto;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.dto.IVespineSeverityDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140419
 */
public class SeverityFormDTO implements IVespineSeverityDTO 
{
	private String severityId;
	private String name;
	private String slaTerm;
	private List<IProviderElementDTO> providerSeverities;
	private Date lastUpdated = new Date();
	
	@Override
	public String getProvider() 
	{
		return VespineCoreConstants.PROVIDER_VESPINE;
	}

	@Override
	public String getSeverityId() 
	{
		return severityId;
	}
	
	public void setSeverityId(String severityId)
	{
		this.severityId = severityId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String getSlaTerm() 
	{
		return slaTerm;
	}
	
	public void setSlaTerm(String slaTerm)
	{
		this.slaTerm = slaTerm;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	@Override
	public List<IProviderElementDTO> getProviderSeverities() 
	{
		if(providerSeverities==null){
			return Collections.emptyList();
		}
		
		return providerSeverities;
	}
	
	public void setProviderSeverities(List<IProviderElementDTO> providerSeverities)
	{
		this.providerSeverities = providerSeverities;
	}
}
