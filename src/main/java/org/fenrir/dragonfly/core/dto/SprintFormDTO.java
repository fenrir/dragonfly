package org.fenrir.dragonfly.core.dto;

import java.util.Date;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140511
 */
public class SprintFormDTO implements ISprintDTO 
{
	private String sprintId;
	private IProjectDTO project;
	private Date startDate;
	private Date endDate;
	private Date lastUpdated = new Date();
	
	@Override
	public String getSprintId() 
	{
		return sprintId;
	}
	
	public void setSprintId(String sprintId)
	{
		this.sprintId = sprintId;
	}

	@Override
	public IProjectDTO getProject() 
	{
		return project;
	}
	
	public void setProject(IProjectDTO project)
	{
		this.project = project;
	}

	@Override
	public Date getStartDate() 
	{
		return startDate;
	}
	
	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	@Override
	public Date getEndDate() 
	{
		return endDate;
	}
	
	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
}
