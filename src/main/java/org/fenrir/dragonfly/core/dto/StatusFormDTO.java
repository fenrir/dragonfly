package org.fenrir.dragonfly.core.dto;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.VespineCoreConstants;
import org.fenrir.vespine.core.dto.IVespineStatusDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140419
 */
public class StatusFormDTO implements IVespineStatusDTO 
{
	private String severityId;
	private String name;
	private String colorRGB;
	private List<IProviderElementDTO> providerStatus;
	private Date lastUpdated = new Date();
	
	@Override
	public String getProvider() 
	{
		return VespineCoreConstants.PROVIDER_VESPINE;
	}

	@Override
	public String getStatusId() 
	{
		return severityId;
	}
	
	public void setStatusId(String statusId)
	{
		this.severityId = statusId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String getColorRGB() 
	{
		return colorRGB;
	}
	
	public void setColorRGB(String colorRGB)
	{
		this.colorRGB = colorRGB;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	@Override
	public List<IProviderElementDTO> getProviderStatus() 
	{
		if(providerStatus==null){
			return Collections.emptyList();
		}
		
		return providerStatus;
	}
	
	public void setProviderStatus(List<IProviderElementDTO> providerStatus)
	{
		this.providerStatus = providerStatus;
	}
}
