package org.fenrir.dragonfly.core.dto;

import java.util.Date;
import org.fenrir.vespine.spi.dto.ITagDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140421
 */
public class TagFormDTO implements ITagDTO 
{
	private String tagId;
	private String name;
	private boolean preferred = false;
	private Date creationDate;
	private Date lastUpdated = new Date();
	
	@Override
	public String getTagId() 
	{
		return tagId;
	}
	
	public void setTagId(String tagId)
	{
		this.tagId = tagId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public Boolean isPreferred() 
	{
		return preferred;
	}
	
	public void setPreferred(boolean preferred)
	{
		this.preferred = preferred;
	}

	@Override
	public Date getCreationDate() 
	{
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate)
	{
		this.creationDate = creationDate;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
}
