package org.fenrir.dragonfly.core.dto;

import java.util.Date;
import org.fenrir.vespine.core.dto.IViewFilterDTO;

public class ViewFilterFormDTO implements IViewFilterDTO 
{
	private String filterId;
	private String name;
	private String description;
	private String filterQuery;
	private String orderClause;
	private Long filterOrder;
	private Date lastUpdated = new Date();
	
	@Override
	public String getFilterId() 
	{
		return filterId;
	}
	
	public void setFilterId(String filterId)
	{
		this.filterId = filterId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public String getDescription() 
	{
		return description;
	}
	
	public void setDescription(String description)
	{
		this.description = description;
	}

	@Override
	public String getFilterQuery() 
	{
		return filterQuery;
	}
	
	public void setFilterQuery(String filterQuery)
	{
		this.filterQuery = filterQuery;
	}

	@Override
	public String getOrderClause() 
	{
		return orderClause;
	}
	
	public void setOrderClause(String orderClause)
	{
		this.orderClause = orderClause;
	}

	@Override
	public Boolean isApplicationFilter() 
	{
		return Boolean.FALSE;
	}

	@Override
	public Long getFilterOrder() 
	{
		return filterOrder;
	}
	
	public void setFilterOrder(Long filterOrder)
	{
		this.filterOrder = filterOrder;
	}

	@Override
	public Date getCreationDate() 
	{
		return lastUpdated;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}

	@Override
	public Boolean isTransient() 
	{
		return Boolean.TRUE;
	}
}
