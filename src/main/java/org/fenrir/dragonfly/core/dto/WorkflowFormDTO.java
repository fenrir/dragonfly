package org.fenrir.dragonfly.core.dto;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140419
 */
public class WorkflowFormDTO implements IWorkflowDTO 
{
	private String workflowId;
	private String name;
	private IStatusDTO initialStatus;
	private List<IWorkflowStepDTO> workflowSteps;
	private Date lastUpdated = new Date();
	
	@Override
	public String getWorkflowId() 
	{
		return workflowId;
	}
	
	public void setWorkflowId(String workflowId)
	{
		this.workflowId = workflowId;
	}

	@Override
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public IStatusDTO getInitialStatus() 
	{
		return initialStatus;
	}
	
	public void setInitialStatus(IStatusDTO initialStatus)
	{
		this.initialStatus = initialStatus;
	}

	@Override
	public List<IWorkflowStepDTO> getWorkflowSteps() 
	{
		if(workflowSteps==null){
			return Collections.emptyList();
		}
		return workflowSteps;		
	}
	
	public void setWorkflowSteps(List<IWorkflowStepDTO> workflowSteps)
	{
		this.workflowSteps = workflowSteps;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
}
