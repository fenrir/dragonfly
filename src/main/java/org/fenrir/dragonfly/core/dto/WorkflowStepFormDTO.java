package org.fenrir.dragonfly.core.dto;

import java.util.Date;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140419
 */
public class WorkflowStepFormDTO implements IWorkflowStepDTO 
{
	private String workflowStepId;
	private IWorkflowDTO workflow;
	private IStatusDTO sourceStatus;
	private IStatusDTO destinationStatus;
	private Date lastUpdated = new Date();
	
	@Override
	public String getWorkflowStepId() 
	{
		return workflowStepId;
	}
	
	public void setWorkflowStepId(String workflowStepId)
	{
		this.workflowStepId = workflowStepId;
	}

	@Override
	public IWorkflowDTO getWorkflow() 
	{
		return workflow;
	}
	
	public void setWorkflow(IWorkflowDTO workflow)
	{
		this.workflow = workflow;
	}

	@Override
	public IStatusDTO getSourceStatus() 
	{
		return sourceStatus;
	}
	
	public void setSourceStatus(IStatusDTO sourceStatus)
	{
		this.sourceStatus = sourceStatus;
	}

	@Override
	public IStatusDTO getDestinationStatus() 
	{
		return destinationStatus;
	}
	
	public void setDestinationStatus(IStatusDTO destinationStatus)
	{
		this.destinationStatus = destinationStatus;
	}

	@Override
	public Date getLastUpdated() 
	{
		return lastUpdated;
	}
}
