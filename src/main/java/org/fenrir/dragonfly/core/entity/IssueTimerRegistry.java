package org.fenrir.dragonfly.core.entity;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140321
 */
@Entity
public class IssueTimerRegistry 
{
	@Id
    private Long id;
    
    @Version
    private Long version;
    
    protected Long issueId;
    
    @Temporal(TemporalType.DATE)
    protected Date workingDate;
    
    private Integer elapsedTime;
    
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastUpdated;
    
    public Long getId() 
    {
        return id;
    }
	
    public void setId(Long id) 
    {
        this.id = id;
    }
	
    public Long getVersion()
    {
        return version;
    }
    
    public Long getIssueId()
    {
    	return issueId;
    }
    
    public void setIssueId(Long issueId)
    {
    	this.issueId = issueId;
    }
    
    public Date getWorkingDate()
    {
    	return workingDate;
    }
    
    public Integer getElapsedTime()
    {
    	return elapsedTime;
    }
    
    public void setElapsedTime(Integer elapsedTime)
    {
    	this.elapsedTime = elapsedTime;
    }
    
    public void setWorkingDate(Date workingDate)
    {
    	this.workingDate = workingDate;
    }
    
    public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
    	if(workingDate==null){
    		workingDate = new Date();
    	}
    	if(elapsedTime==null){
    		elapsedTime = 0;
    	}
        lastUpdated = new Date();        
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        IssueTimerRegistry other = (IssueTimerRegistry) obj;
        /* Id */
        if(id==null){
            if(other.id!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        
        return true;
    }
}
