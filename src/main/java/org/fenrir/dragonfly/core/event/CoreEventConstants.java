package org.fenrir.dragonfly.core.event;

public class CoreEventConstants 
{
	public static final String NOTIFICATION_ISSUES_UPDATED = "org.fenrir.dragonfly.core.notification.issuesUpdated";
	
	/* Paràmetres de les notificacions */
    /**
     * ID del paràmetre de la notificació d'actualització amb la ID indicada per {@link NotificationEventContants#NOTIFICATION_ISSUES_UPDATED}
     * de les incidències que indica la llista de registres actualitzats
     */
    public static final String NOTIFICATION_PARAMETER_ISSUES = "issues";
}
