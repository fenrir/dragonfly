package org.fenrir.dragonfly.core.event;

import org.fenrir.yggdrasil.core.event.annotation.EventMethod;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20140505
 */
public interface IAlertListener
{
    public static final String EVENT_ALERT_TYPE_CREATED_ID = "org.fenrir.dragonfly.core.event.alert.typeCreated";
    public static final String EVENT_ALERT_TYPE_UPDATED_ID = "org.fenrir.dragonfly.core.event.alert.typeUpdated";
    public static final String EVENT_ALERT_TYPE_DELETED_ID = "org.fenrir.dragonfly.core.event.alert.typeDeleted";

    @EventMethod(eventName=EVENT_ALERT_TYPE_CREATED_ID)
    public void alertTypeCreated(IAlertTypeDTO type);
    
    @EventMethod(eventName=EVENT_ALERT_TYPE_UPDATED_ID)
    public void alertTypeUpdated(IAlertTypeDTO oldType, IAlertTypeDTO newType);

    @EventMethod(eventName=EVENT_ALERT_TYPE_DELETED_ID)
    public void alertTypeDeleted(IAlertTypeDTO type);
}
