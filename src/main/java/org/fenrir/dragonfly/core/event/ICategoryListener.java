package org.fenrir.dragonfly.core.event;

import org.fenrir.yggdrasil.core.event.annotation.EventMethod;
import org.fenrir.vespine.spi.dto.ICategoryDTO;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20140815
 */
public interface ICategoryListener 
{
	public static final String EVENT_CATEGORY_CREATED_ID = "org.fenrir.dragonfly.core.event.category.created";
	public static final String EVENT_CATEGORY_UPDATED_ID = "org.fenrir.dragonfly.core.event.category.updated";
	public static final String EVENT_CATEGORY_DELETED_ID = "org.fenrir.dragonfly.core.event.category.deleted";
	
	@EventMethod(eventName=EVENT_CATEGORY_CREATED_ID)
    public void categoryCreated(ICategoryDTO category);
	
	@EventMethod(eventName=EVENT_CATEGORY_UPDATED_ID)
    public void categoryUpdated(ICategoryDTO oldCategory, ICategoryDTO newCategory);
	
	@EventMethod(eventName=EVENT_CATEGORY_DELETED_ID)
    public void categoryDeleted(ICategoryDTO category);
}
