package org.fenrir.dragonfly.core.event;

import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.201418
 */
public interface ICustomFieldListener 
{
	public static final String EVENT_CUSTOM_FIELD_CREATED_ID = "org.fenrir.dragonfly.core.event.customField.created";
	public static final String EVENT_CUSTOM_FIELD_UPDATED_ID = "org.fenrir.dragonfly.core.event.customField.updated";
	public static final String EVENT_CUSTOM_FIELD_DELETED_ID = "org.fenrir.dragonfly.core.event.customField.deleted";
	
	@EventMethod(eventName=EVENT_CUSTOM_FIELD_CREATED_ID)
    public void customFieldCreated(ICustomFieldDTO customField);
	
	@EventMethod(eventName=EVENT_CUSTOM_FIELD_UPDATED_ID)
    public void customFieldUpdated(ICustomFieldDTO oldCustomField, ICustomFieldDTO newCustomField);
	
	@EventMethod(eventName=EVENT_CUSTOM_FIELD_DELETED_ID)
    public void customFieldDeleted(ICustomFieldDTO customField);
}
