package org.fenrir.dragonfly.core.event;

import org.fenrir.yggdrasil.core.event.annotation.EventMethod;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.1.20131202
 */
public interface IDatabaseEventListener 
{
    public static final String EVENT_DATABASE_CONNECTION_ESTABLISHED_ID = "org.fenrir.dragonfly.core.event.database.connectionEstablished";
    public static final String EVENT_DATABASE_CONNECTION_RELEASED_ID = "org.fenrir.dragonfly.core.event.database.connectionReleased";
    
    @EventMethod(eventName=EVENT_DATABASE_CONNECTION_ESTABLISHED_ID)
    public void databaseConnectionEstablished() throws ApplicationException;
    
    @EventMethod(eventName=EVENT_DATABASE_CONNECTION_RELEASED_ID)
    public void databaseConnectionReleased() throws ApplicationException;
}
