package org.fenrir.dragonfly.core.event;

import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20140816
 */
public interface IDictionaryListener 
{
	public static final String EVENT_DICTIONARY_CREATED_ID = "org.fenrir.dragonfly.core.event.dictionary.created";
	public static final String EVENT_DICTIONARY_UPDATED_ID = "org.fenrir.dragonfly.core.event.dictionary.updated";
	public static final String EVENT_DICTIONARY_DELETED_ID = "org.fenrir.dragonfly.core.event.dictionary.deleted";
	
	@EventMethod(eventName=EVENT_DICTIONARY_CREATED_ID)
    public void dictionaryCreated(IDictionaryDTO dictionary);
	
	@EventMethod(eventName=EVENT_DICTIONARY_UPDATED_ID)
    public void dictionaryUpdated(IDictionaryDTO oldDictionary, IDictionaryDTO newDictionary);
	
	@EventMethod(eventName=EVENT_DICTIONARY_DELETED_ID)
    public void dictionaryDeleted(IDictionaryDTO dictionary);
}
