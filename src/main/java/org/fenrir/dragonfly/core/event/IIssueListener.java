package org.fenrir.dragonfly.core.event;

import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20140825
 */
public interface IIssueListener 
{
	public static final String EVENT_ISSUE_CREATED_ID = "org.fenrir.dragonfly.core.event.issue.created";
	public static final String EVENT_ISSUE_UPDATED_ID = "org.fenrir.dragonfly.core.event.issue.updated";
	public static final String EVENT_ISSUE_DELETED_ID = "org.fenrir.dragonfly.core.event.issue.deleted";
	public static final String EVENT_ISSUES_UPDATED_ID = "org.fenrir.dragonfly.core.event.issue.issuesUpdated";
	
	@EventMethod(eventName=EVENT_ISSUE_CREATED_ID)
    public void issueCreated(IIssueDTO issue);
	
	@EventMethod(eventName=EVENT_ISSUE_UPDATED_ID)
    public void issueUpdated(IIssueDTO oldIssue, IIssueDTO newIssue);
	
	@EventMethod(eventName=EVENT_ISSUE_DELETED_ID)
    public void issueDeleted(IIssueDTO issue);
	
	@EventMethod(eventName=EVENT_ISSUES_UPDATED_ID)
    public void issuesUpdated();
}
