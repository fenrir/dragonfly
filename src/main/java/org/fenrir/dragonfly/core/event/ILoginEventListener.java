package org.fenrir.dragonfly.core.event;

import org.fenrir.yggdrasil.core.event.annotation.EventMethod;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.1.20140405
 */
public interface ILoginEventListener 
{
    public static final String EVENT_LOGIN_ID = "org.fenrir.dragonfly.core.event.application.login";
    public static final String EVENT_LOGIN_ABORTED_ID = "org.fenrir.dragonfly.core.event.application.loginAborted";
    public static final String EVENT_LOGOUT_ID = "org.fenrir.dragonfly.core.event.application.logout";
    
    @EventMethod(eventName=EVENT_LOGIN_ID)
    public void loginPerformed() throws ApplicationException;
    
    @EventMethod(eventName=EVENT_LOGIN_ABORTED_ID)
    public void loginAborted() throws ApplicationException;
    
    @EventMethod(eventName=EVENT_LOGOUT_ID)
    public void logoutPerformed() throws ApplicationException;
}
