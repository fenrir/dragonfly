package org.fenrir.dragonfly.core.event;

import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20140804
 */
public interface IProjectListener 
{
	public static final String EVENT_PROJECT_CREATED_ID = "org.fenrir.dragonfly.core.event.project.created";
	public static final String EVENT_PROJECT_UPDATED_ID = "org.fenrir.dragonfly.core.event.project.updated";
	public static final String EVENT_PROJECT_DELETED_ID = "org.fenrir.dragonfly.core.event.project.deleted";
	
	@EventMethod(eventName=EVENT_PROJECT_CREATED_ID)
    public void projectCreated(IProjectDTO project);
	
	@EventMethod(eventName=EVENT_PROJECT_UPDATED_ID)
    public void projectUpdated(IProjectDTO oldProject, IProjectDTO newProject);
	
	@EventMethod(eventName=EVENT_PROJECT_DELETED_ID)
    public void projectDeleted(IProjectDTO project);
}
