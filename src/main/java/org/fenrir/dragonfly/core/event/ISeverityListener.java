package org.fenrir.dragonfly.core.event;

import org.fenrir.yggdrasil.core.event.annotation.EventMethod;
import org.fenrir.vespine.spi.dto.ISeverityDTO;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20140815
 */
public interface ISeverityListener 
{
	public static final String EVENT_SEVERITY_CREATED_ID = "org.fenrir.dragonfly.core.event.severity.created";
	public static final String EVENT_SEVERITY_UPDATED_ID = "org.fenrir.dragonfly.core.event.severity.updated";
	public static final String EVENT_SEVERITY_DELETED_ID = "org.fenrir.dragonfly.core.event.severity.deleted";
	
	@EventMethod(eventName=EVENT_SEVERITY_CREATED_ID)
    public void severityCreated(ISeverityDTO severity);
	
	@EventMethod(eventName=EVENT_SEVERITY_UPDATED_ID)
    public void severityUpdated(ISeverityDTO oldSeverity, ISeverityDTO newSeverity);
	
	@EventMethod(eventName=EVENT_SEVERITY_DELETED_ID)
    public void severityDeleted(ISeverityDTO severity);
}
