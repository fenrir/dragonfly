package org.fenrir.dragonfly.core.event;

import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20140818
 */
public interface ISprintListener 
{
	public static final String EVENT_SPRINT_CREATED_ID = "org.fenrir.dragonfly.core.event.sprint.created";
	public static final String EVENT_SPRINT_UPDATED_ID = "org.fenrir.dragonfly.core.event.sprint.updated";
	public static final String EVENT_SPRINT_DELETED_ID = "org.fenrir.dragonfly.core.event.sprint.deleted";
	
	@EventMethod(eventName=EVENT_SPRINT_CREATED_ID)
    public void sprintCreated(ISprintDTO sprint);
	
	@EventMethod(eventName=EVENT_SPRINT_UPDATED_ID)
    public void sprintUpdated(ISprintDTO oldSprint, ISprintDTO newSprint);
	
	@EventMethod(eventName=EVENT_SPRINT_DELETED_ID)
    public void sprintDeleted(ISprintDTO sprint);
}
