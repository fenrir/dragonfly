package org.fenrir.dragonfly.core.event;

import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20140816
 */
public interface IStatusListener 
{
	public static final String EVENT_STATUS_CREATED_ID = "org.fenrir.dragonfly.core.event.status.created";
	public static final String EVENT_STATUS_UPDATED_ID = "org.fenrir.dragonfly.core.event.status.updated";
	public static final String EVENT_STATUS_DELETED_ID = "org.fenrir.dragonfly.core.event.status.deleted";
	
	@EventMethod(eventName=EVENT_STATUS_CREATED_ID)
    public void statusCreated(IStatusDTO status);
	
	@EventMethod(eventName=EVENT_STATUS_UPDATED_ID)
    public void statusUpdated(IStatusDTO oldStatus, IStatusDTO newStatus);
	
	@EventMethod(eventName=EVENT_STATUS_DELETED_ID)
    public void statusDeleted(IStatusDTO status);
}
