package org.fenrir.dragonfly.core.event;

import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20140804
 */
public interface ITagListener 
{
	public static final String EVENT_TAG_LIST_UPDATED = "org.fenrir.dragonfly.core.event.tag.tagListUpdated";

    @EventMethod(eventName=EVENT_TAG_LIST_UPDATED)
    public void tagListUpdated();
}
