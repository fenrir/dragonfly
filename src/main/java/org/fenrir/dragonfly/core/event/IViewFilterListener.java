package org.fenrir.dragonfly.core.event;

import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

public interface IViewFilterListener 
{
	public static final String EVENT_VIEW_FILTER_CREATED_ID = "org.fenrir.dragonfly.core.event.view.filterCreated";
    public static final String EVENT_VIEW_FILTER_UPDATED_ID = "org.fenrir.dragonfly.core.event.view.filterModified";
    public static final String EVENT_VIEW_FILTER_DELETED_ID = "org.fenrir.dragonfly.core.event.view.filterDeleted";
    public static final String EVENT_VIEW_FILTER_ORDERED_ID = "org.fenrir.dragonfly.core.event.view.filterOrdered";
    
    @EventMethod(eventName=EVENT_VIEW_FILTER_CREATED_ID)
    public void viewFilterCreated(IViewFilterDTO filter);

    @EventMethod(eventName=EVENT_VIEW_FILTER_UPDATED_ID)
    public void viewFilterModified(IViewFilterDTO oldFilter, IViewFilterDTO newFilter);

    @EventMethod(eventName=EVENT_VIEW_FILTER_DELETED_ID)
    public void viewFilterDeleted(IViewFilterDTO filter);
    
    @EventMethod(eventName=EVENT_VIEW_FILTER_ORDERED_ID)
    public void viewFilterOrdered();
}
