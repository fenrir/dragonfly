package org.fenrir.dragonfly.core.event;

import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.3.20140816
 */
public interface IWorkflowListener 
{
	public static final String EVENT_WORKFLOW_CREATED_ID = "org.fenrir.dragonfly.core.event.workflow.created";
	public static final String EVENT_WORKFLOW_UPDATED_ID = "org.fenrir.dragonfly.core.event.workflow.updated";
	public static final String EVENT_WORKFLOW_DELETED_ID = "org.fenrir.dragonfly.core.event.workflow.deleted";
	
	@EventMethod(eventName=EVENT_WORKFLOW_CREATED_ID)
    public void workflowCreated(IWorkflowDTO workflow);
	
	@EventMethod(eventName=EVENT_WORKFLOW_UPDATED_ID)
    public void workflowUpdated(IWorkflowDTO oldWorkflow, IWorkflowDTO newWorkflow);
	
	@EventMethod(eventName=EVENT_WORKFLOW_DELETED_ID)
    public void workflowDeleted(IWorkflowDTO workflow);
}
