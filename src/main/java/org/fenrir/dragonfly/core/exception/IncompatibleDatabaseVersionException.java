package org.fenrir.dragonfly.core.exception;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140508
 */
@SuppressWarnings("serial")
public class IncompatibleDatabaseVersionException extends Exception
{
    public IncompatibleDatabaseVersionException()
    {
        super();
    }

    public IncompatibleDatabaseVersionException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public IncompatibleDatabaseVersionException(String message)
    {
        super(message);
    }

    public IncompatibleDatabaseVersionException(Throwable cause)
    {
        super(cause);
    }
}