package org.fenrir.dragonfly.core.exception;

/**
 * TODO v1.0 Javadoc
 * TODO Moure a Yggdrasil en crear el framework de validacions
 * @author Antonio Archilla Nava
 * @version v0.3.20140116
 */
@SuppressWarnings("serial")
public class ValidationException extends Exception 
{
    public ValidationException() 
    {
        super();	
    }

    public ValidationException(String message, Throwable cause) 
    {
        super(message, cause);
    }

    public ValidationException(String message) 
    {
        super(message);	
    }

    public ValidationException(Throwable cause) 
    {
        super(cause);	
    }
}