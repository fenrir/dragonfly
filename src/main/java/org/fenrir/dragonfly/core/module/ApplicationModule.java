package org.fenrir.dragonfly.core.module;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;

import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.ApplicationRuntimeException;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.vespine.core.util.DictionaryItemDTOConverter;
import org.fenrir.dragonfly.ApplicationConstants;
import org.fenrir.dragonfly.core.CorePreferenceConstants;
import org.fenrir.dragonfly.core.dao.IDatabaseUpdateDAO;
import org.fenrir.dragonfly.core.dao.IIssueTimerRegistryDAO;
import org.fenrir.dragonfly.core.dao.impl.DatabaseUpdateDAOImpl;
import org.fenrir.dragonfly.core.dao.impl.IssueTimerRegistryDAOImpl;
import org.fenrir.dragonfly.core.provider.CustomFieldDataProvider;
import org.fenrir.dragonfly.core.provider.SprintSearchProvider;
import org.fenrir.dragonfly.core.provider.UserSearchProvider;
import org.fenrir.dragonfly.core.service.IDatabaseUpdaterService;
import org.fenrir.dragonfly.core.service.IUserDataVaultService;
import org.fenrir.dragonfly.core.service.impl.DatabaseUpdaterServiceImpl;
import org.fenrir.dragonfly.core.service.impl.UserDataVaultServiceImpl;
import org.fenrir.dragonfly.util.ConnectionWrapper;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140722
 */
public class ApplicationModule extends AbstractModule 
{
	@Override
	protected void configure() 
	{
		IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
		
		/* DAOs */
        bind(IDatabaseUpdateDAO.class).to(DatabaseUpdateDAOImpl.class).in(Singleton.class);
        bind(IIssueTimerRegistryDAO.class).to(IssueTimerRegistryDAOImpl.class).in(Singleton.class);
        
        /* Services */
        bind(IDatabaseUpdaterService.class).to(DatabaseUpdaterServiceImpl.class).in(Singleton.class);
        bind(IUserDataVaultService.class).to(UserDataVaultServiceImpl.class).in(Singleton.class);
        
        /* Altres */
        bind(SprintSearchProvider.class);
        bind(UserSearchProvider.class);
        bind(CustomFieldDataProvider.class);
        bind(ConnectionWrapper.class);     
        // Conversors
        bind(DictionaryItemDTOConverter.class);
        
        // Configuració dels mòduls addicionals segons el mode d'execució de l'aplicació
        String applicationMode = preferenceService.getProperty(CorePreferenceConstants.APPLICATION_MODE);
        if(ApplicationConstants.APPLICATION_MODE_STANDALONE.equals(applicationMode)){
        	install(new StandaloneModeModule());
        }
        else if(ApplicationConstants.APPLICATION_MODE_CLIENT.equals(applicationMode)){
        	install(new ClientModeModule());
        }
        else{
        	throw new ApplicationRuntimeException("Mode no permès [" + applicationMode + "]");
        }
	}
}
