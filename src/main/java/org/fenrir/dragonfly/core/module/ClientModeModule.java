package org.fenrir.dragonfly.core.module;

import java.util.Properties;
import javax.inject.Named;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.persist.jpa.JpaPersistModule;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.vespine.client.VespineClientConstants;
import org.fenrir.vespine.client.broadcast.service.IClientBroadcastService;
import org.fenrir.vespine.client.broadcast.service.IMessageProcessor;
import org.fenrir.vespine.client.broadcast.service.impl.ClientBroadcastServiceImpl;
import org.fenrir.vespine.client.module.VespineClientModule;
import org.fenrir.vespine.client.util.IUserCredentialsProvider;
import org.fenrir.dragonfly.ApplicationConstants;
import org.fenrir.dragonfly.core.CorePreferenceConstants;
import org.fenrir.dragonfly.core.service.IAccessControlService;
import org.fenrir.dragonfly.core.service.IApplicationModeSetupService;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;
import org.fenrir.dragonfly.core.service.IUserDataVaultService;
import org.fenrir.dragonfly.core.service.impl.UserDataVaultServiceImpl;
import org.fenrir.dragonfly.core.service.impl.client.ClientAccessControlServiceImpl;
import org.fenrir.dragonfly.core.service.impl.client.ClientModeSetupServiceImpl;
import org.fenrir.dragonfly.core.service.impl.client.ExtendedIssueWorkFacadeRESTImpl;
import org.fenrir.dragonfly.core.service.impl.client.ExtendedUserManagementFacadeRESTImpl;
import org.fenrir.dragonfly.core.service.impl.client.MessageProcessorImpl;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140815
 */
public class ClientModeModule extends AbstractModule 
{
	@Override
	protected void configure()
	{
		install(new VespineClientModule());
		
    	IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
		
		/* Mòdul de persistència */
        // Es recullen les preferències del workspace
        String provider = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_PROVIDER, CorePreferenceConstants.CONNECTION_PROVIDER_DEFAULT_VALUE);
        String dialect = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_DIALECT, CorePreferenceConstants.CONNECTION_PROVIDER_DEFAULT_DIALECT_VALUE);
        String url = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_URL);
        String user = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_USER);
        String password = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_PASSWORD);
        Properties properties = new Properties();
        properties.put("hibernate.connection.driver_class", provider);
        properties.put("hibernate.connection.url", url);
        properties.put("hibernate.connection.username", user);
        properties.put("hibernate.connection.password", password);
        properties.put("hibernate.dialect", dialect);
        /* value='create' per crear una nova base de dades en cada execució;
         * value='update' per modificar una ja existent;
         * value='create-drop' igual que 'create' però eliminant les taules quan acaba l'execució;
         * value='validate' no modifica la base de dades
         */
        properties.put("hibernate.hbm2ddl.auto", "validate");
        properties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        JpaPersistModule persistenceModule = new JpaPersistModule(ApplicationConstants.PERSISTENCE_UNIT_CLIENT_MODE);
        persistenceModule.properties(properties);
        install(persistenceModule);
        
        /* Mòdul client */
        install(new VespineClientModule());
        
        /* Services */
        bind(IExtendedIssueWorkFacade.class).to(ExtendedIssueWorkFacadeRESTImpl.class).in(Singleton.class);
        bind(IExtendedUserFacade.class).to(ExtendedUserManagementFacadeRESTImpl.class).in(Singleton.class);
        bind(IMessageProcessor.class).to(MessageProcessorImpl.class).in(Singleton.class);
        bind(IAccessControlService.class).to(ClientAccessControlServiceImpl.class).in(Singleton.class);
        bind(IClientBroadcastService.class).to(ClientBroadcastServiceImpl.class).in(Singleton.class);
        
        /* Configuració de l'aplicació en mode CLIENT */
        bind(IApplicationModeSetupService.class).to(ClientModeSetupServiceImpl.class).in(Singleton.class);
	}
	
	@Provides
	private IUserCredentialsProvider getKeyVaultService(IUserDataVaultService keyVaultService)
	{
		if(keyVaultService instanceof IUserCredentialsProvider){
			return (UserDataVaultServiceImpl)keyVaultService;
		}
		
		// Aquest cas no es donarà mai
		return null;
	}
	
	@Provides
	@Named(VespineClientConstants.VESPINE_SERVER_ENDPOINT)
	public String getEndpoint(IPreferenceService preferenceService)
	{
		return preferenceService.getProperty(CorePreferenceConstants.APPLICATION_SERVER_URL);
	}
}
