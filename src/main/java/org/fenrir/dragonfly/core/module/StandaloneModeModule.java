package org.fenrir.dragonfly.core.module;

import java.util.HashMap;
import java.util.Properties;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.persist.jpa.JpaPersistModule;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;
import org.fenrir.vespine.core.module.VespineCoreModule;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.core.service.ISearchIndexFacade;
import org.fenrir.dragonfly.ApplicationConstants;
import org.fenrir.dragonfly.core.CorePreferenceConstants;
import org.fenrir.dragonfly.core.service.IAccessControlService;
import org.fenrir.dragonfly.core.service.IApplicationModeSetupService;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;
import org.fenrir.dragonfly.core.service.impl.IssueFacadeImpl;
import org.fenrir.dragonfly.core.service.impl.standalone.AdministrationFacadeImpl;
import org.fenrir.dragonfly.core.service.impl.standalone.ExtendedIssueWorkFacadeImpl;
import org.fenrir.dragonfly.core.service.impl.standalone.ExtendedUserManagementServiceImpl;
import org.fenrir.dragonfly.core.service.impl.standalone.SearchIndexFacadeImpl;
import org.fenrir.dragonfly.core.service.impl.standalone.StandaloneAccessControlServiceImpl;
import org.fenrir.dragonfly.core.service.impl.standalone.StandaloneModeSetupServiceImpl;
import org.postgresql.ds.PGSimpleDataSource;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140803
 */
public class StandaloneModeModule extends AbstractModule
{
    @Override
    protected void configure()
    {
    	IWorkspaceAdministrationService workspaceService = (IWorkspaceAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IWorkspaceAdministrationService.class);
    	IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
    	
    	/* Mòdul Core */
    	HashMap<String, Object> coreModuleProperties = new HashMap<String, Object>();
    	String indexProvider = preferenceService.getProperty(CorePreferenceConstants.INDEX_PROVIDER);
    	if(CorePreferenceConstants.INDEX_PROVIDER_DATABASE_VALUE.equals(indexProvider)){
    		coreModuleProperties.put(VespineCoreModule.PROPERTY_INDEX_STORE, VespineCoreModule.PROPERTY_INDEX_STORE_DATABASE_VALUE);
    		coreModuleProperties.put(VespineCoreModule.PROPERTY_INDEX_LUCENE_JDBC_DIALECT, "org.apache.lucene.store.jdbc.dialect.PostgreSQLDialect");
    		PGSimpleDataSource dataSource = new PGSimpleDataSource();
    		String host = preferenceService.getProperty(CorePreferenceConstants.INDEX_DB_HOST);
    		dataSource.setServerName(host);
    		String port = preferenceService.getProperty(CorePreferenceConstants.INDEX_DB_PORT);
    		dataSource.setPortNumber(Integer.valueOf(port));
    		String database = preferenceService.getProperty(CorePreferenceConstants.INDEX_DB_DATABASE);
    		dataSource.setDatabaseName(database);
    		String user = preferenceService.getProperty(CorePreferenceConstants.INDEX_DB_USER);
    		dataSource.setUser(user);
    		String password = preferenceService.getProperty(CorePreferenceConstants.INDEX_DB_PASSWORD);
    		dataSource.setPassword(password);
    		coreModuleProperties.put(VespineCoreModule.PROPERTY_INDEX_JDBC_DATASOURCE, dataSource);
    	}
    	// Índex per defecte ubicat a Filesystem
    	else{
    		coreModuleProperties.put(VespineCoreModule.PROPERTY_INDEX_STORE, VespineCoreModule.PROPERTY_INDEX_STORE_FILESYSTEM_VALUE);
    		String currentWorkspace = workspaceService.getCurrentWorkspaceFolder();
    		coreModuleProperties.put(VespineCoreModule.PROPERTY_WORKSPACE_FOLDER, currentWorkspace);
    	}
    	coreModuleProperties.put(VespineCoreModule.PROPERTY_DATA_PERSISTENCE_UNIT, ApplicationConstants.PERSISTENCE_UNIT_STANDALONE_MODE);
    	install(new VespineCoreModule(coreModuleProperties));
    	
    	/* Mòdul de persistència */
        // Es recullen les preferències del workspace
        String provider = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_PROVIDER, CorePreferenceConstants.CONNECTION_PROVIDER_DEFAULT_VALUE);
        String dialect = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_DIALECT, CorePreferenceConstants.CONNECTION_PROVIDER_DEFAULT_DIALECT_VALUE);
        String url = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_URL);
        String user = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_USER);
        String password = preferenceService.getProperty(CorePreferenceConstants.CONNECTION_PASSWORD);
        Properties properties = new Properties();
        properties.put("hibernate.connection.driver_class", provider);
        properties.put("hibernate.connection.url", url);
        properties.put("hibernate.connection.username", user);
        properties.put("hibernate.connection.password", password);
        properties.put("hibernate.dialect", dialect);
        /* value='create' per crear una nova base de dades en cada execució;
         * value='update' per modificar una ja existent;
         * value='create-drop' igual que 'create' però eliminant les taules quan acaba l'execució;
         * value='validate' no modifica la base de dades
         */
        properties.put("hibernate.hbm2ddl.auto", "validate");
        properties.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        /* Listeners lifecycle Hibernate */
        properties.put("hibernate.ejb.event.post-insert", "org.fenrir.vespine.core.entity.listener.TrackedFieldsListener, " +
        		"org.fenrir.vespine.core.entity.listener.IssueProjectListener");
        properties.put("hibernate.ejb.event.post-update", "org.fenrir.vespine.core.entity.listener.TrackedFieldsListener");
        properties.put("hibernate.ejb.event.post-delete", "org.fenrir.vespine.core.entity.listener.IssueProjectListener");
        JpaPersistModule persistenceModule = new JpaPersistModule(ApplicationConstants.PERSISTENCE_UNIT_STANDALONE_MODE);
        persistenceModule.properties(properties);
        install(persistenceModule);
    	
        /* Services */
        bind(IExtendedIssueWorkFacade.class).to(ExtendedIssueWorkFacadeImpl.class).in(Singleton.class);
        bind(IExtendedUserFacade.class).to(ExtendedUserManagementServiceImpl.class).in(Singleton.class);
        bind(IAdministrationFacade.class).to(AdministrationFacadeImpl.class).in(Singleton.class);
        bind(IIssueFacade.class).to(IssueFacadeImpl.class).in(Singleton.class);
        bind(ISearchIndexFacade.class).to(SearchIndexFacadeImpl.class).in(Singleton.class);
        bind(IAccessControlService.class).to(StandaloneAccessControlServiceImpl.class).in(Singleton.class);
        
        /* Configuració de l'aplicació en mode STANDALONE */
        bind(IApplicationModeSetupService.class).to(StandaloneModeSetupServiceImpl.class).in(Singleton.class);
    }        
}