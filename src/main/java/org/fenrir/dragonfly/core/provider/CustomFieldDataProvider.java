package org.fenrir.dragonfly.core.provider;

import java.util.Collections;
import java.util.List;
import javax.inject.Inject;

import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140222
 */
public class CustomFieldDataProvider 
{
	@Inject
	private IAdministrationFacade administrationService;
	
	public void setAdministrationService(IAdministrationFacade administrationService)
	{
		this.administrationService = administrationService;
	}
	
	public <T> List<T> getAllProviderData(IFieldType fieldType) throws BusinessException
	{
		if(IFieldType.TYPE_DICTIONARY.equals(fieldType.getType())){
			return (List<T>)administrationService.findAllDictionaries();
		}
		
		return Collections.emptyList();
	}
	
	public <T> T getProviderDataById(IFieldType fieldType, String id) throws BusinessException
	{
		if(IFieldType.TYPE_DICTIONARY.equals(fieldType.getType())){
			return (T)administrationService.findDictionaryById(id);
		}
		
		return null;
	}
	
	public String getProviderDataId(IFieldType fieldType, Object providerData)
	{
		if(IFieldType.TYPE_DICTIONARY.equals(fieldType.getType())){
			return ((IDictionaryDTO)providerData).getDictionaryId();
		}
		
		return null;
	}
}
