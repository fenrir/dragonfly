package org.fenrir.dragonfly.core.provider;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.collections.list.SetUniqueList;
import org.fenrir.yggdrasil.core.exception.ProviderException;
import org.fenrir.yggdrasil.core.provider.IElementSearchProvider;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140720
 */
public class SprintSearchProvider implements IElementSearchProvider<ISprintDTO> 
{
	@Inject
	private IExtendedIssueWorkFacade issueWorkService;
	
	public void setIssueWorkService(IExtendedIssueWorkFacade issueWorkService)
	{
		this.issueWorkService = issueWorkService;
	}
	
	@Override
	public List<ISprintDTO> findAllElements() throws ProviderException 
	{
		try{
			return issueWorkService.findAllSprints();
		}
		catch(BusinessException e){
			throw new ProviderException(e);
		}
	}

	@Override
	public List<ISprintDTO> findElementsNameLike(String name) throws ProviderException 
	{
		try{
			List<ISprintDTO> sprints = SetUniqueList.decorate(issueWorkService.findSprintsByProjectNameLike(name));
			sprints.addAll(issueWorkService.findSprintsByStartDateLike(name));
			sprints.addAll(issueWorkService.findSprintsByEndDateLike(name));
			
			return sprints;
		}
		catch(BusinessException e){
			throw new ProviderException(e);
		}
	}
}
