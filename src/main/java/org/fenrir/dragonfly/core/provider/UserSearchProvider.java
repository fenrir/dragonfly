package org.fenrir.dragonfly.core.provider;

import java.util.List;
import javax.inject.Inject;
import org.apache.commons.collections.list.SetUniqueList;
import org.fenrir.yggdrasil.core.exception.ProviderException;
import org.fenrir.yggdrasil.core.provider.IElementSearchProvider;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140504
 */
public class UserSearchProvider implements IElementSearchProvider<IUserDTO> 
{
	@Inject
	private IExtendedUserFacade userService;
	
	public void setUserService(IExtendedUserFacade userService)
	{
		this.userService = userService;
	}

	@Override
	public List<IUserDTO> findAllElements() throws ProviderException 
	{
		try{
			return userService.findAllActiveUsers();
		}
		catch(BusinessException e){
			throw new ProviderException(e);
		}
	}

	@Override
	public List<IUserDTO> findElementsNameLike(String name) throws ProviderException 
	{
		try{
			List<IUserDTO> users = SetUniqueList.decorate(userService.findUsersUsernameLike(name));
			users.addAll(userService.findUsersCompleteNameLike(name));
			
			return users;
		}
		catch(BusinessException e){
			throw new ProviderException(e);
		}
	}
}
