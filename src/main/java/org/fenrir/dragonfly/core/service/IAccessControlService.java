package org.fenrir.dragonfly.core.service;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140803
 */
public interface IAccessControlService 
{
	public boolean checkRole(String role);
	public boolean checkPermission(String permission); 
}
