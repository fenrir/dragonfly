package org.fenrir.dragonfly.core.service;

import org.fenrir.dragonfly.core.exception.AuthenticationException;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

public interface IApplicationModeSetupService 
{
	public void setupApplication() throws ApplicationException;
	public void performDataUpdates() throws ApplicationException;
	public void checkModelCompatibility() throws ApplicationException;
	public void scheduleTasks() throws ApplicationException;
	public void loginUser(String username, String password) throws AuthenticationException, ApplicationException;
}
