package org.fenrir.dragonfly.core.service;

import org.fenrir.dragonfly.core.exception.IncompatibleDatabaseVersionException;
import org.fenrir.yggdrasil.core.exception.ApplicationException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140721
 */
public interface IDatabaseUpdaterService
{
	public boolean isDatabaseSchemaCompatible(String... contexts) throws ApplicationException;
    public void performSchemaUpdate(String... contexts) throws ApplicationException, IncompatibleDatabaseVersionException;
    public void performDataUpdate();
    public boolean isDataUpdateNeeded();
}
