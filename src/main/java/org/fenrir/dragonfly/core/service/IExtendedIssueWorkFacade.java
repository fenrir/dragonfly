package org.fenrir.dragonfly.core.service;

import java.util.List;

import org.fenrir.vespine.core.service.IIssueWorkFacade;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.entity.IssueTimerRegistry;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140511
 */
public interface IExtendedIssueWorkFacade extends IIssueWorkFacade 
{
	/*---------------------------------*
     *       Registres de timer        *
     *---------------------------------*/
	public List<IssueTimerRegistry> findNonPublishedTimerRegistries() throws BusinessException;
	public long countNonPublishedTimerRegistries() throws BusinessException;
	public IssueTimerRegistry findIssueTimerRegistry(String issueId) throws BusinessException;
	public IssueTimerRegistry createTimerRegistry(String issueId) throws BusinessException;
	public IssueTimerRegistry updateTimerRegistry(Long registryId, Integer elapsedTime);
	public void deleteNonPublishedTimerRegistries() throws BusinessException;
	public void deleteTimerRegistry(Long registryId);
}
