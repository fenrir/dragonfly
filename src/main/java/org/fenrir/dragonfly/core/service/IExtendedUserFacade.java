package org.fenrir.dragonfly.core.service;

import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.core.service.IUserFacade;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140803
 */
public interface IExtendedUserFacade extends IUserFacade 
{
	public String getLoggedUsername();
	public User getLoggedUser();
	public IUserDTO getLoggedUserAsDTO() throws BusinessException;
	
	public boolean checkRole(String role);
	public boolean checkPermission(String permission);
}
