package org.fenrir.dragonfly.core.service;

import java.util.List;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140726
 */
public interface IUserDataVaultService 
{
	public static final String KEY_CREDENTIALS_USERNAME = "credentials.username";
	public static final String KEY_CREDENTIALS_PASSWORD = "credentials.password";
	
	public String getCredential(String key);
	public void setUserCredentials(String username, String password);
	
	public List<String> getRoles();
	public void setRoles(List<String> roles);
	public List<String> getPermissions();
	public void setPermissions(List<String> permissions);
}
