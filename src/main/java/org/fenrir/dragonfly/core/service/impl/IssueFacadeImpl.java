package org.fenrir.dragonfly.core.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.dto.IIssueAlertDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.dto.SearchQuery;
import org.fenrir.vespine.core.dto.adapter.CustomValueDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.IssueAlertDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.IssueDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.IssueNoteDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.TagDTOAdapter;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.AlertType;
import org.fenrir.vespine.core.entity.IssueAlert;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueCustomValue;
import org.fenrir.vespine.core.entity.IssueNote;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.core.entity.Tag;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueAdministrationService;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.core.service.IIssueWorkService;
import org.fenrir.vespine.core.service.IUserManagementService;
import org.fenrir.vespine.spi.dto.ICustomValueDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140720
 */
public class IssueFacadeImpl implements IIssueFacade 
{
	private static final String FILTER_VARIABLE_CURRENT_USER = "\\$\\{CURRENT_USER\\}";
	
	@Inject
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IIssueSearchService issueSearchService;
	
	@Inject
	private IIssueAdministrationService issueAdministrationService;
	
	@Inject
	private IIssueWorkService issueWorkService;
	
	@Inject
	private IExtendedUserFacade userExtendedService;
	
	@Inject
	private IUserManagementService userService;
	
	@Inject
	private IAuditService auditService;
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setIssueSearchService(IIssueSearchService issueSearchService)
	{
		this.issueSearchService = issueSearchService;
	}
	
	public void setIssueAdministrationService(IIssueAdministrationService issueAdministrationService)
	{
		this.issueAdministrationService = issueAdministrationService;
	}
	
	public void setIssueWorkService(IIssueWorkService issueWorkService)
	{
		this.issueWorkService = issueWorkService;
	}
	
	public void setUserExtendedService(IExtendedUserFacade userExtendedService)
	{
		this.userExtendedService = userExtendedService;
	}
	
	public void setUserService(IUserManagementService userService)
	{
		this.userService = userService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	/*---------------------------------*
     *           Incidències           *
     *---------------------------------*/
	@Override
	@Transactional
	public List<IIssueDTO> findAllIssues(int page, int pageSize)
	{
		List<IIssueDTO> issueDTOs = new ArrayList<IIssueDTO>();
		
		List<AbstractIssue> issues = issueSearchService.findAllIssues(page, pageSize);
		for(AbstractIssue issue:issues){
			issueDTOs.add(new IssueDTOAdapter(issue));
		}
		return issueDTOs;
	}
	
	@Override
	@Transactional
	public List<IIssueDTO> findTaggedIssues(String tagId, int page, int pageSize)
	{
		List<IIssueDTO> issueDTOs = new ArrayList<IIssueDTO>();
		List<AbstractIssue> issues = issueSearchService.findTaggedIssues(Long.valueOf(tagId), page, pageSize);
		for(AbstractIssue issue:issues){
			issueDTOs.add(new IssueDTOAdapter(issue));
		}
		return issueDTOs;
	}
	
	@Override
	@Transactional
	public List<IIssueDTO> findFilteredIssues(IViewFilterDTO filter, int page, int pageSize) throws BusinessException
	{
		List<IIssueDTO> issueDTOs = new ArrayList<IIssueDTO>();
		String filterQuery = replaceFilterQueryVariables(filter.getFilterQuery());
		List<AbstractIssue> issues = issueSearchService.findFilteredIssues(filterQuery, filter.getOrderClause(), page, pageSize);
		for(AbstractIssue issue:issues){
			issueDTOs.add(new IssueDTOAdapter(issue));
		}
		return issueDTOs;
	}
	
	@Override
	@Transactional
	public long countFilteredIssues(IViewFilterDTO filter) throws BusinessException
	{
		String filterQuery = replaceFilterQueryVariables(filter.getFilterQuery());
		return issueSearchService.countFilteredIssues(filterQuery);
	}
	
	@Override
	@Transactional
	public long countTaggedIssues(String tagId)
	{
		return issueSearchService.countTaggedIssues(Long.valueOf(tagId));
	}

	@Override
	public SearchQuery buildIssueTermSearchQuery(String term)
	{
		return issueSearchService.buildIssueTermSearchQuery(term);
	}
	
	@Override
	public Collection<IIssueDTO> findIssueSearchHits(SearchQuery query, int page, int pageSize)
	{
		List<IIssueDTO> issueDTOs = new ArrayList<IIssueDTO>();
		
		Collection<AbstractIssue> issues = issueSearchService.findIssueSearchHits(query, page, pageSize);
		for(AbstractIssue issue:issues){
			issueDTOs.add(new IssueDTOAdapter(issue));
		}
		return issueDTOs;
	}
	
	@Override
    public long countIssueSearchHits(SearchQuery query)
    {
    	return issueSearchService.countIssueSearchHits(query);
    }
	
	@Override
	@Transactional
	public long countIssuesByProject(String projectId)
	{
		IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(projectId));
		return issueSearchService.countIssuesByProject(project);
	}
	
	@Override
	@Transactional
	public long countIssuesByCategory(String categoryId)
	{
		IssueCategory category = coreAdministrationService.findCategoryById(Long.valueOf(categoryId));
		return issueSearchService.countIssuesByCategory(category);
	}
	
	@Override
	@Transactional
	public long countIssuesBySeverity(String severityId)
	{
		IssueSeverity severity = coreAdministrationService.findSeverityById(Long.valueOf(severityId));
		return issueSearchService.countIssuesBySeverity(severity);
	}
	
	@Override
	@Transactional
	public long countIssuesByStatus(String statusId)
	{
		IssueStatus status = coreAdministrationService.findStatusById(Long.valueOf(statusId));
		return issueSearchService.countIssuesByStatus(status);
	}
	
	@Override
	@Transactional
	public IIssueDTO findIssueById(String id)
	{
		AbstractIssue issue = issueSearchService.findIssueById(Long.valueOf(id));
		return new IssueDTOAdapter(issue);
	}
	
	@Override
	@Transactional
	public IIssueDTO findIssueByVisibleId(String visibleId)
	{
		AbstractIssue issue = issueSearchService.findIssueByVisibleId(visibleId);
		return new IssueDTOAdapter(issue);
	}
	
	@Override
	@Transactional
	public boolean issueFilterMatches(IViewFilterDTO filter, String issueId) throws BusinessException
	{
		String filterQuery = replaceFilterQueryVariables(filter.getFilterQuery());
		return issueSearchService.issueFilterMatches(filterQuery, Long.valueOf(issueId));
	}
	
	@Override
	@Transactional
	public IIssueDTO createIssue(String projectId,
			String categoryId,
			String severityId, 
    		String statusId,
    		String summary, 
    		String description,
    		String assignedUser,
    		String sprintId,
    		Integer estimatedTime,
    		Date slaDate,
    		Map<String, String> customFields) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		Long auditRegistryReference = auditService.auditAction(AuditConstants.ACTION_ISSUE_CREATE, loggedUser);
		try{
			IssueProject objProject = coreAdministrationService.findProjectById(Long.valueOf(projectId));
			IssueCategory objCategory = coreAdministrationService.findCategoryById(Long.valueOf(categoryId));
			IssueSeverity objSeverity = coreAdministrationService.findSeverityById(Long.valueOf(severityId));
			IssueStatus objStatus = coreAdministrationService.findStatusById(Long.valueOf(statusId));
			User reporterUser = userExtendedService.getLoggedUser();
			User objAssignedUser = null;
			if(assignedUser!=null){
				objAssignedUser = userService.findUserByUsername(assignedUser);
			}
			Sprint objSprint = null;
			if(sprintId!=null){
				objSprint = issueWorkService.findSprintById(Long.valueOf(sprintId));
			}
			Map<IssueCustomField, String> mapObjCustomFields = new HashMap<IssueCustomField, String>();
			for(String customFieldId:customFields.keySet()){
				 String value = customFields.get(customFieldId);
				 if(value!=null){
					 IssueCustomField field = coreAdministrationService.findCustomFieldById(Long.valueOf(customFieldId));
					 mapObjCustomFields.put(field, value);
				 }
			}
			AbstractIssue issue = issueAdministrationService.createIssue(objProject, objCategory, objSeverity, objStatus, summary, description, reporterUser, objAssignedUser, objSprint, estimatedTime, slaDate, mapObjCustomFields, auditRegistryReference);

			// Auditoria
	        auditService.resolveAuditAction(auditRegistryReference, issue.getId().toString(), AuditConstants.RESULT_CODE_OK, null);
	        
	        return new IssueDTOAdapter(issue);
		}
		catch(Exception e){
			// Auditoria
			auditService.resolveAuditAction(auditRegistryReference, null, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error creant incidència: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public IIssueDTO updateIssue(String issueId, 
			String projectId,
			String categoryId,
			String severityId, 
			String statusId,
    		String summary, 
    		String description,
    		String assignedUser,
    		String sprintId,
    		Integer estimatedTime,
    		Date slaDate,
    		Date resolutionDate,
    		Map<String, String> customFields) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		Long auditRegistryReference = auditService.auditAction(AuditConstants.ACTION_ISSUE_UPDATE, loggedUser);
		try{
			IssueProject objProject = coreAdministrationService.findProjectById(Long.valueOf(projectId));
			IssueCategory objCategory = coreAdministrationService.findCategoryById(Long.valueOf(categoryId));
			IssueSeverity objSeverity = coreAdministrationService.findSeverityById(Long.valueOf(severityId));
			IssueStatus objStatus = coreAdministrationService.findStatusById(Long.valueOf(statusId));
			User objAssignedUser = null;
			if(assignedUser!=null){
				objAssignedUser = userService.findUserByUsername(assignedUser);
			}
			Sprint objSprint = null;
			if(sprintId!=null){
				objSprint = issueWorkService.findSprintById(Long.valueOf(sprintId));
			}
			Map<IssueCustomField, String> mapObjCustomFields = new HashMap<IssueCustomField, String>();
			for(String customFieldId:customFields.keySet()){
				 String value = customFields.get(customFieldId);
				 if(value!=null){
					 IssueCustomField field = coreAdministrationService.findCustomFieldById(Long.valueOf(customFieldId));
					 mapObjCustomFields.put(field, value);
				 }
			}
			AbstractIssue issue = issueAdministrationService.updateIssue(Long.valueOf(issueId), objProject, objCategory, objSeverity, objStatus, summary, description, objAssignedUser, objSprint, estimatedTime, slaDate, resolutionDate, mapObjCustomFields, auditRegistryReference);

			// Auditoria
	        auditService.resolveAuditAction(auditRegistryReference, issue.getId().toString(), AuditConstants.RESULT_CODE_OK, null);
	        
	        return new IssueDTOAdapter(issue);
		}
		catch(Exception e){
			// Auditoria
			auditService.resolveAuditAction(auditRegistryReference, null, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error modificant les dades de l'incidència: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public void deleteIssue(String id, boolean bin) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		String action = null;
		AbstractIssue issue = issueSearchService.findIssueById(Long.valueOf(id));
		if(issue.isDeleted()){
			action = AuditConstants.ACTION_ISSUE_DELETE;
		}
		else{
			action = AuditConstants.ACTION_ISSUE_SEND2TRASH_BIN;
		}
		try{
			issueAdministrationService.deleteIssueRecycleBinAware(Long.valueOf(id));

			// Auditoria
	        auditService.auditAction(action, id, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
	        auditService.auditAction(action, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error eliminant incidència: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public Date computeSLADate(Date sendDate, String severityId)
	{
		IssueSeverity severity = coreAdministrationService.findSeverityById(Long.valueOf(severityId));
		return issueAdministrationService.computeIssueSlaDate(sendDate, severity);
	}
	
	@Override
	@Transactional
	public void updateIssueSLADate(String issueId, Date slaDate) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		try{
	        issueAdministrationService.setIssueSLADate(Long.valueOf(issueId), slaDate);
	        
	        // Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_UPDATE_SLA_DATE, issueId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_UPDATE_SLA_DATE, issueId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error actualitzant data SLA: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public void updateIssueSLADate(String issueId, boolean isSla) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		try{
			issueAdministrationService.setIssueSLADate(Long.valueOf(issueId), isSla);
	        
	        // Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_UPDATE_SLA_DATE, issueId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_UPDATE_SLA_DATE, issueId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error actualitzant data SLA: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public void restoreIssue(String id) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		try{
			issueAdministrationService.restoreIssueFromRecycleBin(Long.valueOf(id));

			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_RESTORE, id, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_RESTORE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error restaurant incidència: " + e.getMessage(), e);
		}
	}
	
	/*---------------------------------*
     *              Notes              *
     *---------------------------------*/
	@Override
	@Transactional
	public List<IIssueNoteDTO> findIssueNotes(String issueId)
	{
		List<IIssueNoteDTO> notes = new ArrayList<IIssueNoteDTO>();
		
		AbstractIssue issue = issueSearchService.findIssueById(Long.valueOf(issueId));
		for(IssueNote note:issue.getNotes()){
			notes.add(new IssueNoteDTOAdapter(note));
		}
		
		return notes;
	}
	
	@Override
	@Transactional
	public IIssueNoteDTO findIssueNote(String noteId, String issueId)
	{
		AbstractIssue issue = issueSearchService.findIssueById(Long.valueOf(issueId));
		Iterator<IssueNote> iterator = issue.getNotes().iterator();
		while(iterator.hasNext()){
			IssueNote note = iterator.next();
			if(note.getId().equals(noteId)){
				return new IssueNoteDTOAdapter(note);
			}
		}
		
		return null;
	}
	
	@Override
	@Transactional
	public IIssueNoteDTO createIssueNote(String issueId, String noteContents) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		User user = userExtendedService.getLoggedUser();
		try{
			AbstractIssue issue = issueSearchService.findIssueById(Long.valueOf(issueId));
			IssueNote note = issueAdministrationService.createIssueNote(issue, user, noteContents);

			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_CREATE, note.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
	        
	        return new IssueNoteDTOAdapter(note);
		}
		catch(Exception e){
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error creant nota: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public IIssueNoteDTO updateIssueNote(String noteId, String issueId, String noteContents) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		try{
			IssueNote note = issueAdministrationService.updateIssueNote(Long.valueOf(noteId), noteContents);

			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_UPDATE, note.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
	        
	        return new IssueNoteDTOAdapter(note);
		}
		catch(Exception e){
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_UPDATE, noteId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error modificant nota: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public void deleteIssueNote(String issueId, String noteId) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		try{
			issueAdministrationService.deleteIssueNote(Long.valueOf(noteId));

			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_DELETE, noteId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_NOTE_UPDATE, noteId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error eliminant nota: " + e.getMessage(), e);
		}
	}
	
	/*---------------------------------*
     *              Tags               *
     *---------------------------------*/
	@Override
	@Transactional
	public List<ITagDTO> findIssueTags(String issueId)
	{
		List<ITagDTO> tags = new ArrayList<ITagDTO>();
		
		AbstractIssue issue = issueSearchService.findIssueById(Long.valueOf(issueId));
		for(Tag tag:issue.getTags()){
			tags.add(new TagDTOAdapter(tag));
		}
		
		return tags;
	}
	
	@Override
	@Transactional
	public boolean isIssueTagged(String issueId, String tagId)
	{
		return issueSearchService.isIssueTagged(Long.valueOf(issueId), Long.valueOf(tagId));
	}
	
	@Override
	@Transactional
	public void tagIssue(String issueId, String tagId) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		try{
			issueAdministrationService.tagIssue(Long.valueOf(issueId), Long.valueOf(tagId));
	        
	        // Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_TAG_ATTACH, issueId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_TAG_ATTACH, issueId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error etiquetant incidència: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public void tagIssueByDescription(String issueId, String tagDescription) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		try{
			List<Tag> tags = coreAdministrationService.findTagsByName(tagDescription);
	        Tag tag;
	        // Crear un nou tag
	        if(tags.isEmpty()){
	            tag = coreAdministrationService.createTag(tagDescription, false);
	        }
	        // En cas que existeixi un tag amb la descripció proporcionada, s'agafa la primer coincidència
	        else{
	            tag = tags.get(0);
	        }
	        // S'associa el tag a la incidència
	        issueAdministrationService.tagIssue(Long.valueOf(issueId), tag.getId());
	        
	        // Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_TAG_ATTACH, issueId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_TAG_ATTACH, issueId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error etiquetant incidència: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public void dettachIssueTag(String issueId, String tagId) throws BusinessException
	{
		String loggedUser = userExtendedService.getLoggedUsername();
		try{
			issueAdministrationService.dettachIssueTag(Long.valueOf(issueId), Long.valueOf(tagId));
			
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_TAG_DETTACH, issueId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
	        auditService.auditAction(AuditConstants.ACTION_ISSUE_TAG_DETTACH, issueId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
	        throw new BusinessException("Error treient etiquet incidència: " + e.getMessage(), e);
		}
	}
	
	/*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
	@Override
	@Transactional
    public List<ICustomValueDTO> findCustomValuesByField(String fieldId)
    {
		List<ICustomValueDTO> valueDTOs = new ArrayList<ICustomValueDTO>();
		
		List<IssueCustomValue> values = issueSearchService.findCustomValuesByField(Long.valueOf(fieldId));
		for(IssueCustomValue value:values){
			valueDTOs.add(new CustomValueDTOAdapter(value));
		}
		
		return valueDTOs;
    }
	
	@Override
	@Transactional
    public List<ICustomValueDTO> findIssueCustomValues(String issueId)
    {
		List<ICustomValueDTO> valueDTOs = new ArrayList<ICustomValueDTO>();
		
		List<IssueCustomValue> values = issueSearchService.findIssueCustomValues(Long.valueOf(issueId));
		for(IssueCustomValue value:values){
			valueDTOs.add(new CustomValueDTOAdapter(value));
		}
		
		return valueDTOs;
    }
	
	/*---------------------------------*
     *             Alertes             *
     *---------------------------------*/
	@Override
	@Transactional
	public List<IIssueAlertDTO> findAllAlertsByType(String alertTypeId)
	{
		List<IIssueAlertDTO> alertDTOs = new ArrayList<IIssueAlertDTO>();
		List<IssueAlert> alerts = issueSearchService.findAllAlertsByType(Long.valueOf(alertTypeId));
		for(IssueAlert alert:alerts){
			alertDTOs.add(new IssueAlertDTOAdapter(alert));
		}
		
		return alertDTOs;
	}
	
	@Override
	@Transactional
	public List<IIssueAlertDTO> findIssueAlertsByIssueId(String issueId)
	{
		List<IIssueAlertDTO> alertDTOs = new ArrayList<IIssueAlertDTO>();
		List<IssueAlert> alerts = issueSearchService.findIssueAlertsByIssueId(Long.valueOf(issueId));
		for(IssueAlert alert:alerts){
			alertDTOs.add(new IssueAlertDTOAdapter(alert));
		}
		
		return alertDTOs;
	}
	
	@Override
	@Transactional
	public void createIssueAlerts(String alertTypeId)
	{
		AlertType alertType = coreAdministrationService.findAlertTypeById(Long.valueOf(alertTypeId));
		issueAdministrationService.createIssueAlerts(alertType);
	}
	
	@Override
	@Transactional
	public void validateIssueAlerts(String alertTypeId)
	{
		AlertType alertType = coreAdministrationService.findAlertTypeById(Long.valueOf(alertTypeId));
		issueAdministrationService.validateIssueAlerts(alertType);
	}
	
	@Override
	@Transactional
	public void deleteIssueAlert(String alertId, String issueId)
	{
		issueAdministrationService.deleteIssueAlert(Long.valueOf(alertId));
	}
	
	private String replaceFilterQueryVariables(String filterQuery) throws BusinessException
    {
    	IUserDTO currentUser = userExtendedService.getLoggedUserAsDTO();
    	filterQuery = filterQuery.replaceAll(FILTER_VARIABLE_CURRENT_USER, currentUser.getUsername());
    	
    	return filterQuery;
    }
}
