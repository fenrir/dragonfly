package org.fenrir.dragonfly.core.service.impl;

import java.util.List;
import org.fenrir.vespine.client.util.IUserCredentialsProvider;
import org.fenrir.dragonfly.core.service.IUserDataVaultService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140726
 */
public class UserDataVaultServiceImpl implements IUserDataVaultService, IUserCredentialsProvider 
{
	private String username;
	private String password;
	
	private List<String> roles;
	private List<String> permissions;

	@Override
	public String getCredential(String key)
	{
		if(KEY_CREDENTIALS_PASSWORD.equals(key)){
			return password;
		}
		else if(KEY_CREDENTIALS_USERNAME.equals(key)){
			return username;
		}
		
		return null;
	}
	
	@Override
	public void setUserCredentials(String username, String password)
	{
		this.username = username;
		this.password = password;
	}
	
	@Override
	public String getUsername() 
	{
		return username;
	}
	
	@Override
	public String getPassword() 
	{
		return password;
	}
	
	@Override
	public List<String> getRoles()
	{
		return roles;
	}
	
	@Override
	public void setRoles(List<String> roles)
	{
		this.roles = roles;
	}
	
	@Override
	public List<String> getPermissions()
	{
		return permissions;
	}
	
	@Override
	public void setPermissions(List<String> permissions)
	{
		this.permissions = permissions;
	}
}
