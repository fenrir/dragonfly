package org.fenrir.dragonfly.core.service.impl.client;

import javax.inject.Inject;
import org.fenrir.dragonfly.core.service.IAccessControlService;
import org.fenrir.dragonfly.core.service.IUserDataVaultService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140803
 */
public class ClientAccessControlServiceImpl implements IAccessControlService 
{
	@Inject
	private IUserDataVaultService userDataService;
	
	public void setUserDataService(IUserDataVaultService userDataService)
	{
		this.userDataService = userDataService;
	}
	
	@Override
	public boolean checkRole(String role) 
	{
		return userDataService.getRoles().contains(role);
	}

	@Override
	public boolean checkPermission(String permission) 
	{
		throw new UnsupportedOperationException("Operació no disponible");
	}
}
