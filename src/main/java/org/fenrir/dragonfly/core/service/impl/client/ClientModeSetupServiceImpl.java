package org.fenrir.dragonfly.core.service.impl.client;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.google.inject.persist.PersistService;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.vespine.client.application.service.IApplicationService;
import org.fenrir.vespine.client.authentication.dto.AuthenticatedUserJsonDTO;
import org.fenrir.vespine.client.authentication.dto.AuthenticationPermissionJsonDTO;
import org.fenrir.vespine.client.authentication.dto.AuthenticationRoleJsonDTO;
import org.fenrir.vespine.client.authentication.exception.VespineAuthenticationException;
import org.fenrir.vespine.client.authentication.service.IAuthenticationService;
import org.fenrir.vespine.client.broadcast.service.IClientBroadcastService;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ApplicationConstants;
import org.fenrir.dragonfly.core.CorePreferenceConstants;
import org.fenrir.dragonfly.core.event.IDatabaseEventListener;
import org.fenrir.dragonfly.core.exception.AuthenticationException;
import org.fenrir.dragonfly.core.exception.IncompatibleDatabaseVersionException;
import org.fenrir.dragonfly.core.service.IApplicationModeSetupService;
import org.fenrir.dragonfly.core.service.IDatabaseUpdaterService;
import org.fenrir.dragonfly.core.service.IUserDataVaultService;
import org.fenrir.dragonfly.core.service.impl.standalone.StandaloneModeSetupServiceImpl;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140730
 */
public class ClientModeSetupServiceImpl implements IApplicationModeSetupService 
{
	private final Logger log = LoggerFactory.getLogger(StandaloneModeSetupServiceImpl.class);
	
	@Inject
	private IDatabaseUpdaterService databaseUpdaterService;
	
	@Inject
	private PersistService persistenceService;
	
	@Inject
	private IEventNotificationService eventNotificationService;
	
	@Inject
	private IUserDataVaultService userVaultService;
	
	@Inject
	private IAuthenticationService authenticationService;
	
	@Inject
	private IApplicationService applicationService;
	
	@Inject
	private IPreferenceService preferenceService;
	
	@Inject
	private IClientBroadcastService clientBroadcastService;
	
	public void setDatabaseUpdaterService(IDatabaseUpdaterService databaseUpdaterService)
	{
		this.databaseUpdaterService = databaseUpdaterService;
	}
	
	public void setPersistenceService(PersistService persistenceService)
	{
		this.persistenceService = persistenceService;
	}
	
	public void setEventNotificationService(IEventNotificationService eventNotificationService)
	{
		this.eventNotificationService = eventNotificationService;
	}
	
	public void setKeyVaultService(IUserDataVaultService keyVaultService)
	{
		this.userVaultService = keyVaultService;
	}
	
	public void setAuthenticationService(IAuthenticationService authenticationService)
	{
		this.authenticationService = authenticationService;
	}
	
	public void setApplicationService(IApplicationService applicationService)
	{
		this.applicationService = applicationService;
	}
	
	public void setPreferenceService(IPreferenceService preferenceService) 
	{
		this.preferenceService = preferenceService;
	}
	
	public void setClientBroadcastService(IClientBroadcastService clientBroadcastService)
	{
		this.clientBroadcastService = clientBroadcastService;
	}
	
	@Override
	public void setupApplication() throws ApplicationException 
	{
		// Es mira si s'ha d'actualitzar el model de la base de dades
        try{
        	// FIXME Setup mode DEVEL configurable a application.xml
        	databaseUpdaterService.performSchemaUpdate(ApplicationConstants.DATABASE_SCHEMA_CONTEXT_PROD);
        }
        catch(IncompatibleDatabaseVersionException e){
        	log.warn("La versió de la base de dades és incompatible: {}", e.getMessage(), e);
        	throw new ApplicationException("La versió de la base de dades és incompatible: " + e.getMessage(), e);
        }
        
        // S'inicia el mòdul de persistència
        persistenceService.start();
        
    	// Es comunica l'event de conexió amb la base de dades
        try{
            eventNotificationService.notifyNamedEvent(IDatabaseEventListener.class, IDatabaseEventListener.EVENT_DATABASE_CONNECTION_ESTABLISHED_ID);
        }
        catch(Exception e){
            log.error("Error llançant event {}: {}", new Object[]{IDatabaseEventListener.EVENT_DATABASE_CONNECTION_ESTABLISHED_ID, e.getMessage(), e});
        }
	}

	@Override
	public void performDataUpdates() throws ApplicationException 
	{
		// Res a fer en mode client (per ara)...
	}
	
	@Override
	public void checkModelCompatibility() throws ApplicationException
	{
		if(!databaseUpdaterService.isDatabaseSchemaCompatible(ApplicationConstants.DATABASE_SCHEMA_CONTEXT_PROD)){
			throw new ApplicationException("La base de dades no es compatible amb la versió actual de l'aplicació. Actualitzi-la");
		}
		try{
			// TODO Parametritzar versió de l'API
			applicationService.checkRestAPICompatibility("0.1");
		}
		catch(BusinessException e){
			throw new ApplicationException(e.getMessage(), e);
		}
	}

	@Override
	public void scheduleTasks() throws ApplicationException 
	{
		
	}
	
	@Override
	public void loginUser(String username, String password) throws AuthenticationException, ApplicationException
	{
		AuthenticatedUserJsonDTO userData = null;
		try{
			userData = authenticationService.login(username, password);
		}
		catch(VespineAuthenticationException e){
			throw new AuthenticationException(e.getMessage(), e);
		}
		catch(BusinessException e){
			throw new ApplicationException(e.getMessage(), e);
		}
		
		userVaultService.setUserCredentials(username, password);
		// Recuperació de rols i permisos
		List<String> roles = new ArrayList<String>();
		for(AuthenticationRoleJsonDTO role:userData.getRoles()){
			roles.add(role.getName());
		}
		userVaultService.setRoles(roles);
		
		List<String> permissions = new ArrayList<String>();
		for(AuthenticationPermissionJsonDTO permission:userData.getPermissions()){
			permissions.add(permission.getValue());
		}
		userVaultService.setPermissions(permissions);

		try{
			// Es guarda l'username a les preferències perquè la proxima vegada que s'accedeixi a l'aplicació aparegui al login
			String currentUsername = preferenceService.getProperty(CorePreferenceConstants.APPLICATION_DEFAULT_USER);
			if(StringUtils.isBlank(currentUsername) || !currentUsername.equals(username)){
				preferenceService.setProperty(CorePreferenceConstants.APPLICATION_DEFAULT_USER, username);
				preferenceService.save(IPreferenceService.PREFERENCES_CONFIGURATION_ID);
			}
		}
		catch(Exception e){
			throw new ApplicationException("S'ha produit un error en el procés d'autenticació", e);
		}
		
		// S'estableix un canal de comunicació amb el servidor
		try{
			clientBroadcastService.connect();
		}
		catch(Exception e){
			log.error("Error establint connexió broadcast amb el servidor: {}", e.getMessage(), e);
		}
	}
}
