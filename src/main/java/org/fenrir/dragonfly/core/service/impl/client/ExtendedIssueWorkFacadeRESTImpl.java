package org.fenrir.dragonfly.core.service.impl.client;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.apache.commons.lang.time.DateUtils;
import org.fenrir.vespine.core.dto.IIssueWipRegistryDTO;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.core.service.IIssueWorkFacade;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.dao.IIssueTimerRegistryDAO;
import org.fenrir.dragonfly.core.entity.IssueTimerRegistry;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140722
 */
public class ExtendedIssueWorkFacadeRESTImpl implements IExtendedIssueWorkFacade 
{
	@Inject
	private IIssueWorkFacade issueWorkService;
	
	@Inject
	private IExtendedUserFacade userService;
	
	@Inject
	private IIssueTimerRegistryDAO timerRegistryDAO;
	
	public void setIssueWorkService(IIssueWorkFacade issueWorkFacade)
	{
		this.issueWorkService = issueWorkFacade;
	}
	
	public void setTimerRegistryDAO(IIssueTimerRegistryDAO timerRegistryDAO)
	{
		this.timerRegistryDAO = timerRegistryDAO;
	}
	
	public void setUserService(IExtendedUserFacade userService)
	{
		this.userService = userService;
	}
	
	@Override
	public List<ISprintDTO> findAllSprints() throws BusinessException 
	{
		return issueWorkService.findAllSprints();
	}

	@Override
	public List<ISprintDTO> findSprintsByProjectNameLike(String name) throws BusinessException 
	{
		return issueWorkService.findSprintsByProjectNameLike(name);
	}

	@Override
	public List<ISprintDTO> findSprintsByStartDateLike(String startDatePart) throws BusinessException 
	{
		return issueWorkService.findSprintsByStartDateLike(startDatePart);
	}

	@Override
	public List<ISprintDTO> findSprintsByEndDateLike(String endDatePart)throws BusinessException 
	{
		return issueWorkService.findSprintsByEndDateLike(endDatePart);
	}

	@Override
	public ISprintDTO findSprintById(String sprintId) throws BusinessException 
	{
		return issueWorkService.findSprintById(sprintId);
	}

	@Override
	public ISprintDTO createSprint(String projectId, Date startDate, Date endDate) throws BusinessException 
	{
		return issueWorkService.createSprint(projectId, startDate, endDate);
	}

	@Override
	public ISprintDTO updateSprint(String sprintId, String projectId, Date startDate, Date endDate) throws BusinessException 
	{
		return issueWorkService.updateSprint(sprintId, projectId, startDate, endDate);
	}

	@Override
	public void deleteSprint(String sprintId) throws BusinessException 
	{
		issueWorkService.deleteSprint(sprintId);
	}

	@Override
	public List<IWorkRegistryDTO> findAllIssueWorkRegistries(String issueId) throws BusinessException 
	{
		return issueWorkService.findAllIssueWorkRegistries(issueId);
	}

	@Override
	public IWorkRegistryDTO findIssueWorkRegistryById(String registryId, String issueId) throws BusinessException 
	{
		return issueWorkService.findIssueWorkRegistryById(registryId, issueId);
	}

	@Override
	public IWorkRegistryDTO createIssueWorkRegistry(String issueId, Date workingDay, boolean includeInSprint, String description, String username, Integer time) throws BusinessException 
	{
		return issueWorkService.createIssueWorkRegistry(issueId, workingDay, includeInSprint, description, username, time);
	}

	@Override
	public IWorkRegistryDTO updateIssueWorkRegistry(String registryId, String issueId, Date workingDay, boolean includeInSprint, String description, String username, Integer time) throws BusinessException 
	{
		return issueWorkService.updateIssueWorkRegistry(registryId, issueId, workingDay, includeInSprint, description, username, time);
	}

	@Override
	public void deleteIssueWorkRegistry(String registryId, String issueId) throws BusinessException 
	{
		issueWorkService.deleteIssueWorkRegistry(registryId, issueId);
	}

	@Override
	public List<IIssueWipRegistryDTO> findIssueWorkInProgressRegistriesByUser(String username) throws BusinessException 
	{
		return issueWorkService.findIssueWorkInProgressRegistriesByUser(username);
	}

	@Override
	public IIssueWipRegistryDTO findIssueWorkInProgressRegistry(String issueId, String username) throws BusinessException 
	{
		return issueWorkService.findIssueWorkInProgressRegistry(issueId, username);
	}

	@Override
	public IIssueWipRegistryDTO findIssueWorkInProgressRegistryById(String registryId, String issueId) throws BusinessException 
	{
		return issueWorkService.findIssueWorkInProgressRegistryById(registryId, issueId);
	}

	@Override
	public IIssueWipRegistryDTO createWorkInProgressRegistry(String issueId, String username) throws BusinessException 
	{
		return issueWorkService.createWorkInProgressRegistry(issueId, username);
	}

	@Override
	public void deleteWorkInProgressRegistry(String registryId, String issueId) throws BusinessException 
	{
		deleteTimerRegistry(Long.valueOf(registryId));
		issueWorkService.deleteWorkInProgressRegistry(registryId, issueId);
	}

	@Override
	public void deleteIssueWorkInProgressRegistry(String issueId, String username) throws BusinessException 
	{
		// TODO Auto-generated method stub
		
	}

	/*---------------------------------*
     *       Registres de timer        *
     *---------------------------------*/
	@Override
	public List<IssueTimerRegistry> findNonPublishedTimerRegistries() throws BusinessException
	{
		List<Long> wipRegistryIds = collectIssueWorkInProgressIds();
		// Si no hi han IDs es retorna una llista buida
		if(wipRegistryIds.isEmpty()){
			return Collections.emptyList();
		}
		
		// Data d'avui sense contar les hores
		Date date = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		return timerRegistryDAO.findOldWorkedRegistries(date, wipRegistryIds);
	}

	@Override
	public long countNonPublishedTimerRegistries() throws BusinessException
	{
		List<Long> wipRegistryIds = collectIssueWorkInProgressIds();
		// Si no hi han IDs es retorna una llista buida
		if(wipRegistryIds.isEmpty()){
			return 0;
		}
		
		// Data d'avui sense contar les hores
		Date date = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		return timerRegistryDAO.countOldWorkedRegistries(date, wipRegistryIds);
	}

	@Override
	public IssueTimerRegistry findIssueTimerRegistry(String issueId) throws BusinessException
	{
		IIssueWipRegistryDTO wipRegistry = findIssueWorkInProgressRegistry(issueId, userService.getLoggedUsername());
		
		if(wipRegistry!=null){
			return timerRegistryDAO.findRegistryById(Long.valueOf(wipRegistry.getRegistryId()));
		}
		else{
			return null;
		}
	}

	@Override
	public IssueTimerRegistry createTimerRegistry(String issueId) throws BusinessException
	{
		IIssueWipRegistryDTO wipRegistry = createWorkInProgressRegistry(issueId, userService.getLoggedUsername());
		
		IssueTimerRegistry timerRegistry = new IssueTimerRegistry();
		timerRegistry.setId(Long.valueOf(wipRegistry.getRegistryId()));
		timerRegistry.setIssueId(Long.valueOf(issueId));
		timerRegistry.setElapsedTime(0);
		timerRegistry.setWorkingDate(new Date());
		return timerRegistryDAO.createRegistry(timerRegistry);
	}

	@Override
	public IssueTimerRegistry updateTimerRegistry(Long registryId, Integer elapsedTime) 
	{
		IssueTimerRegistry registry = timerRegistryDAO.findRegistryById(Long.valueOf(registryId));
		registry.setElapsedTime(elapsedTime);
		return timerRegistryDAO.updateRegistry(registry);
	}

	@Override
	public void deleteNonPublishedTimerRegistries() throws BusinessException 
	{
		List<IssueTimerRegistry> nonPublishedWorkRegistries = findNonPublishedTimerRegistries();
    	for(IssueTimerRegistry registry:nonPublishedWorkRegistries){
    		timerRegistryDAO.deleteRegistry(registry);
    	}
	}

	@Override
	public void deleteTimerRegistry(Long registryId) 
	{
		IssueTimerRegistry registry = timerRegistryDAO.findRegistryById(registryId);
		// Pot ser null si s'ha indicat per treballar però no s'ha iniciat el timer mai
		if(registry!=null){
			timerRegistryDAO.deleteRegistry(registry);
		}
	}

	private List<Long> collectIssueWorkInProgressIds() throws BusinessException
	{
		List<IIssueWipRegistryDTO> wipRegistries = issueWorkService.findIssueWorkInProgressRegistriesByUser(userService.getLoggedUsername());
		List<Long> wipIds = new ArrayList<Long>();
		for(IIssueWipRegistryDTO elem:wipRegistries){
			wipIds.add(Long.valueOf(elem.getRegistryId()));
		}
		
		return wipIds;
	}
}
