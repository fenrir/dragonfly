package org.fenrir.dragonfly.core.service.impl.client;

import java.util.List;

import javax.inject.Inject;

import org.fenrir.dragonfly.core.service.IAccessControlService;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;
import org.fenrir.dragonfly.core.service.IUserDataVaultService;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.core.service.IUserFacade;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140722
 */
public class ExtendedUserManagementFacadeRESTImpl implements IExtendedUserFacade 
{
	@Inject 
	private IUserFacade userService;
	
	@Inject
	private IUserDataVaultService userVaultService;
	
	@Inject
	private IAccessControlService accessControlService;
	
	public void setUserService(IUserFacade userService)
	{
		this.userService = userService;
	}
	
	public void setKeyVaultService(IUserDataVaultService keyVaultService)
	{
		this.userVaultService = keyVaultService;
	}
	
	public void setAccessControlService(IAccessControlService accessControlService)
	{
		this.accessControlService = accessControlService;
	}
	
	@Override
	public List<IUserDTO> findAllActiveUsers() throws BusinessException 
	{
		return userService.findAllActiveUsers();
	}

	@Override
	public List<IUserDTO> findUsersCompleteNameLike(String name) throws BusinessException 
	{
		return userService.findUsersCompleteNameLike(name);
	}

	@Override
	public List<IUserDTO> findUsersUsernameLike(String name) throws BusinessException 
	{
		return userService.findUsersUsernameLike(name);
	}

	@Override
	public IUserDTO findUserByUsername(String username) throws BusinessException 
	{
		return userService.findUserByUsername(username);
	}

	@Override
	public String getLoggedUsername() 
	{
		return userVaultService.getCredential(IUserDataVaultService.KEY_CREDENTIALS_USERNAME);
	}

	@Override
	public User getLoggedUser() 
	{
		return null;
	}

	@Override
	public IUserDTO getLoggedUserAsDTO() throws BusinessException
	{
		return userService.findUserByUsername(getLoggedUsername());
	}
	
	@Override
	public boolean checkRole(String role)
	{
		return accessControlService.checkRole(role);
	}
	
	@Override
	public boolean checkPermission(String permission)
	{
		return accessControlService.checkPermission(permission);
	}
}
