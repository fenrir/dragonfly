package org.fenrir.dragonfly.core.service.impl.client;

import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.IApplicationNotification;
import org.fenrir.yggdrasil.core.event.IApplicationNotificationListener;
import org.fenrir.yggdrasil.core.event.NotificationEventConstants;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.vespine.client.broadcast.service.IMessageProcessor;
import org.fenrir.vespine.client.builder.AlertTypeJsonBuilder;
import org.fenrir.vespine.client.builder.CategoryJsonBuilder;
import org.fenrir.vespine.client.builder.CustomFieldJsonBuilder;
import org.fenrir.vespine.client.builder.DictionaryJsonBuilder;
import org.fenrir.vespine.client.builder.IssueJsonBuilder;
import org.fenrir.vespine.client.builder.ProjectJsonBuilder;
import org.fenrir.vespine.client.builder.SeverityJsonBuilder;
import org.fenrir.vespine.client.builder.SprintJsonBuilder;
import org.fenrir.vespine.client.builder.StatusJsonBuilder;
import org.fenrir.vespine.client.builder.ViewFilterJsonBuilder;
import org.fenrir.vespine.client.builder.WorkflowJsonBuilder;
import org.fenrir.vespine.core.dto.BroadcastMessage;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.dragonfly.core.event.IAlertListener;
import org.fenrir.dragonfly.core.event.ICategoryListener;
import org.fenrir.dragonfly.core.event.ICustomFieldListener;
import org.fenrir.dragonfly.core.event.IDictionaryListener;
import org.fenrir.dragonfly.core.event.IIssueListener;
import org.fenrir.dragonfly.core.event.IProjectListener;
import org.fenrir.dragonfly.core.event.ISeverityListener;
import org.fenrir.dragonfly.core.event.ISprintListener;
import org.fenrir.dragonfly.core.event.IStatusListener;
import org.fenrir.dragonfly.core.event.ITagListener;
import org.fenrir.dragonfly.core.event.IViewFilterListener;
import org.fenrir.dragonfly.core.event.IWorkflowListener;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140825
 */
public class MessageProcessorImpl implements IMessageProcessor 
{
	private final Logger log = LoggerFactory.getLogger(MessageProcessorImpl.class);
	
	@Inject
	private IEventNotificationService eventNotificationService;
	
	public void setEventNotificationService(IEventNotificationService eventNotificationService)
	{
		this.eventNotificationService = eventNotificationService;
	}
	
	@Override
	public void processMessage(BroadcastMessage message) 
	{
		if(log.isDebugEnabled()){
			log.debug("Rebut missatge: {}", message.toString());
		}

		try{
			if(BroadcastMessage.MESSAGE_TYPE_PROJECT_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_PROJECT_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_PROJECT_DELETED.equals(message.getType())){
				processProjectMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_CATEGORY_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_CATEGORY_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_CATEGORY_DELETED.equals(message.getType())){
				processCategoryMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_SEVERITY_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_SEVERITY_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_SEVERITY_DELETED.equals(message.getType())){
				processSeverityMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_STATUS_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_STATUS_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_STATUS_DELETED.equals(message.getType())){
				processStatusMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_WORKFLOW_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_WORKFLOW_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_WORKFLOW_DELETED.equals(message.getType())){
				processWorkflowMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_DICTIONARY_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_DICTIONARY_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_DICTIONARY_DELETED.equals(message.getType())){
				processDictionaryMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_CUSTOM_FIELD_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_CUSTOM_FIELD_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_CUSTOM_FIELD_DELETED.equals(message.getType())){
				processCustomFieldMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_ALERT_TYPE_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_ALERT_TYPE_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_ALERT_TYPE_DELETED.equals(message.getType())){
				processAlertTypeMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_DELETED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_REORDER.equals(message.getType())){
				processViewFilterMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_SPRINT_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_SPRINT_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_SPRINT_DELETED.equals(message.getType())){
				processSprintMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_TAG_LIST_UPDATED.equals(message.getType())){
				processTagMessage(message);
			}
			else if(BroadcastMessage.MESSAGE_TYPE_ISSUE_CREATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_ISSUE_UPDATED.equals(message.getType())
					|| BroadcastMessage.MESSAGE_TYPE_ISSUE_DELETED.equals(message.getType())){
				processIssueMessage(message);
			}
		}
		catch(Exception e){
			log.error("Error interpretant missatge: {}", e.getMessage(), e);
			
			IApplicationNotification notification = (IApplicationNotification)ApplicationContext.getInstance().getRegisteredComponent(IApplicationNotification.class, NotificationEventConstants.NOTIFICATION_ERROR);
			notification.setMessage("Error durant l'interpretació del missatge " + message.toString());
			notification.setParameter(NotificationEventConstants.NOTIFICATION_PARAMETER_ERROR_DESCRIPTION, "Error durant l'interpretació del missatge " + message.toString());
			notification.setParameter(NotificationEventConstants.NOTIFICATION_PARAMETER_ERROR_THROWABLE, e);
			try{
				eventNotificationService.notifyNamedEvent(IApplicationNotificationListener.class, 
						IApplicationNotificationListener.EVENT_SINGLE_NOTIFICATION_ID, notification);
			}
			catch(Exception e2){
				log.error("Error notificant missatge d'error: {}", e.getMessage(), e);
			}
		}
	}
	
	private void processProjectMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_PROJECT_CREATED.equals(message.getType())){
			String strProject = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IProjectDTO newValueDTO = new ProjectJsonBuilder().setJsonObj(new JSONObject(strProject)).build();
			eventNotificationService.notifyNamedEvent(IProjectListener.class, IProjectListener.EVENT_PROJECT_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_PROJECT_UPDATED.equals(message.getType())){
			String strProject = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IProjectDTO newValueDTO = new ProjectJsonBuilder().setJsonObj(new JSONObject(strProject)).build();
			strProject = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IProjectDTO oldValueDTO = new ProjectJsonBuilder().setJsonObj(new JSONObject(strProject)).build();
			eventNotificationService.notifyNamedEvent(IProjectListener.class, IProjectListener.EVENT_PROJECT_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_PROJECT_DELETED.equals(message.getType())){
			String strProject = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IProjectDTO oldValueDTO = new ProjectJsonBuilder().setJsonObj(new JSONObject(strProject)).build();
			eventNotificationService.notifyNamedEvent(IProjectListener.class, IProjectListener.EVENT_PROJECT_DELETED_ID, oldValueDTO);
		}
	}
	
	private void processCategoryMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_CATEGORY_CREATED.equals(message.getType())){
			String strCategory = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			ICategoryDTO newValueDTO = new CategoryJsonBuilder().setJsonObj(new JSONObject(strCategory)).build();
			eventNotificationService.notifyNamedEvent(ICategoryListener.class, ICategoryListener.EVENT_CATEGORY_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_CATEGORY_UPDATED.equals(message.getType())){
			String strCategory = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			ICategoryDTO newValueDTO = new CategoryJsonBuilder().setJsonObj(new JSONObject(strCategory)).build();
			strCategory = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			ICategoryDTO oldValueDTO = new CategoryJsonBuilder().setJsonObj(new JSONObject(strCategory)).build();
			eventNotificationService.notifyNamedEvent(ICategoryListener.class, ICategoryListener.EVENT_CATEGORY_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_CATEGORY_DELETED.equals(message.getType())){
			String strCategory = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			ICategoryDTO oldValueDTO = new CategoryJsonBuilder().setJsonObj(new JSONObject(strCategory)).build();
			eventNotificationService.notifyNamedEvent(ICategoryListener.class, ICategoryListener.EVENT_CATEGORY_DELETED_ID, oldValueDTO);
		}
	}
	
	private void processSeverityMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_SEVERITY_CREATED.equals(message.getType())){
			String strSeverity = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			ISeverityDTO newValueDTO = new SeverityJsonBuilder().setJsonObj(new JSONObject(strSeverity)).build();
			eventNotificationService.notifyNamedEvent(ISeverityListener.class, ISeverityListener.EVENT_SEVERITY_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_SEVERITY_UPDATED.equals(message.getType())){
			String strSeverity = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			ISeverityDTO newValueDTO = new SeverityJsonBuilder().setJsonObj(new JSONObject(strSeverity)).build();
			strSeverity = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			ISeverityDTO oldValueDTO = new SeverityJsonBuilder().setJsonObj(new JSONObject(strSeverity)).build();
			eventNotificationService.notifyNamedEvent(ISeverityListener.class, ISeverityListener.EVENT_SEVERITY_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_SEVERITY_DELETED.equals(message.getType())){
			String strSeverity = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			ISeverityDTO oldValueDTO = new SeverityJsonBuilder().setJsonObj(new JSONObject(strSeverity)).build();
			eventNotificationService.notifyNamedEvent(ISeverityListener.class, ISeverityListener.EVENT_SEVERITY_DELETED_ID, oldValueDTO);
		}
	}
	
	private void processStatusMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_STATUS_CREATED.equals(message.getType())){
			String strStatus = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IStatusDTO newValueDTO = new StatusJsonBuilder().setJsonObj(new JSONObject(strStatus)).build();
			eventNotificationService.notifyNamedEvent(IStatusListener.class, IStatusListener.EVENT_STATUS_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_STATUS_UPDATED.equals(message.getType())){
			String strStatus = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IStatusDTO newValueDTO = new StatusJsonBuilder().setJsonObj(new JSONObject(strStatus)).build();
			strStatus = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IStatusDTO oldValueDTO = new StatusJsonBuilder().setJsonObj(new JSONObject(strStatus)).build();
			eventNotificationService.notifyNamedEvent(IStatusListener.class, IStatusListener.EVENT_STATUS_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_STATUS_DELETED.equals(message.getType())){
			String strStatus = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IStatusDTO oldValueDTO = new StatusJsonBuilder().setJsonObj(new JSONObject(strStatus)).build();
			eventNotificationService.notifyNamedEvent(IStatusListener.class, IStatusListener.EVENT_STATUS_DELETED_ID, oldValueDTO);
		}
	}
	
	private void processWorkflowMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_WORKFLOW_CREATED.equals(message.getType())){
			String strWorkflow = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IWorkflowDTO newValueDTO = new WorkflowJsonBuilder().setJsonObj(new JSONObject(strWorkflow)).build();
			eventNotificationService.notifyNamedEvent(IWorkflowListener.class, IWorkflowListener.EVENT_WORKFLOW_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_WORKFLOW_UPDATED.equals(message.getType())){
			String strWorkflow = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IWorkflowDTO newValueDTO = new WorkflowJsonBuilder().setJsonObj(new JSONObject(strWorkflow)).build();
			strWorkflow = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IWorkflowDTO oldValueDTO = new WorkflowJsonBuilder().setJsonObj(new JSONObject(strWorkflow)).build();
			eventNotificationService.notifyNamedEvent(IWorkflowListener.class, IWorkflowListener.EVENT_WORKFLOW_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_WORKFLOW_DELETED.equals(message.getType())){
			String strWorkflow = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IWorkflowDTO oldValueDTO = new WorkflowJsonBuilder().setJsonObj(new JSONObject(strWorkflow)).build();
			eventNotificationService.notifyNamedEvent(IWorkflowListener.class, IWorkflowListener.EVENT_WORKFLOW_DELETED_ID, oldValueDTO);
		}
	}
	
	private void processDictionaryMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_DICTIONARY_CREATED.equals(message.getType())){
			String strDictionary = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IDictionaryDTO newValueDTO = new DictionaryJsonBuilder().setJsonObj(new JSONObject(strDictionary)).build();
			eventNotificationService.notifyNamedEvent(IDictionaryListener.class, IDictionaryListener.EVENT_DICTIONARY_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_DICTIONARY_UPDATED.equals(message.getType())){
			String strDictionary = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IDictionaryDTO newValueDTO = new DictionaryJsonBuilder().setJsonObj(new JSONObject(strDictionary)).build();
			strDictionary = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IDictionaryDTO oldValueDTO = new DictionaryJsonBuilder().setJsonObj(new JSONObject(strDictionary)).build();
			eventNotificationService.notifyNamedEvent(IDictionaryListener.class, IDictionaryListener.EVENT_DICTIONARY_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_DICTIONARY_DELETED.equals(message.getType())){
			String strDictionary = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IDictionaryDTO oldValueDTO = new DictionaryJsonBuilder().setJsonObj(new JSONObject(strDictionary)).build();
			eventNotificationService.notifyNamedEvent(IDictionaryListener.class, IDictionaryListener.EVENT_DICTIONARY_DELETED_ID, oldValueDTO);
		}
	}
	
	private void processCustomFieldMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_CUSTOM_FIELD_CREATED.equals(message.getType())){
			String strCustomField = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			ICustomFieldDTO newValueDTO = new CustomFieldJsonBuilder().setJsonObj(new JSONObject(strCustomField)).build();
			eventNotificationService.notifyNamedEvent(ICustomFieldListener.class, ICustomFieldListener.EVENT_CUSTOM_FIELD_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_CUSTOM_FIELD_UPDATED.equals(message.getType())){
			String strCustomField = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			ICustomFieldDTO newValueDTO = new CustomFieldJsonBuilder().setJsonObj(new JSONObject(strCustomField)).build();
			strCustomField = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			ICustomFieldDTO oldValueDTO = new CustomFieldJsonBuilder().setJsonObj(new JSONObject(strCustomField)).build();
			eventNotificationService.notifyNamedEvent(ICustomFieldListener.class, ICustomFieldListener.EVENT_CUSTOM_FIELD_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_CUSTOM_FIELD_DELETED.equals(message.getType())){
			String strCustomField = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			ICustomFieldDTO oldValueDTO = new CustomFieldJsonBuilder().setJsonObj(new JSONObject(strCustomField)).build();
			eventNotificationService.notifyNamedEvent(ICustomFieldListener.class, ICustomFieldListener.EVENT_CUSTOM_FIELD_DELETED_ID, oldValueDTO);
		}
	}
	
	private void processAlertTypeMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_ALERT_TYPE_CREATED.equals(message.getType())){
			String strAlertType = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IAlertTypeDTO newValueDTO = new AlertTypeJsonBuilder().setJsonObj(new JSONObject(strAlertType)).build();
			eventNotificationService.notifyNamedEvent(IAlertListener.class, IAlertListener.EVENT_ALERT_TYPE_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_ALERT_TYPE_UPDATED.equals(message.getType())){
			String strAlertType = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IAlertTypeDTO newValueDTO = new AlertTypeJsonBuilder().setJsonObj(new JSONObject(strAlertType)).build();
			strAlertType = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IAlertTypeDTO oldValueDTO = new AlertTypeJsonBuilder().setJsonObj(new JSONObject(strAlertType)).build();
			eventNotificationService.notifyNamedEvent(IAlertListener.class, IAlertListener.EVENT_ALERT_TYPE_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_ALERT_TYPE_DELETED.equals(message.getType())){
			String strAlertType = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IAlertTypeDTO oldValueDTO = new AlertTypeJsonBuilder().setJsonObj(new JSONObject(strAlertType)).build();
			eventNotificationService.notifyNamedEvent(IAlertListener.class, IAlertListener.EVENT_ALERT_TYPE_DELETED_ID, oldValueDTO);
		}
	}
	
	private void processViewFilterMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_CREATED.equals(message.getType())){
			String strViewFilter = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IViewFilterDTO newValueDTO = new ViewFilterJsonBuilder().setJsonObj(new JSONObject(strViewFilter)).build();
			eventNotificationService.notifyNamedEvent(IViewFilterListener.class, IViewFilterListener.EVENT_VIEW_FILTER_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_UPDATED.equals(message.getType())){
			String strViewFilter = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IViewFilterDTO newValueDTO = new ViewFilterJsonBuilder().setJsonObj(new JSONObject(strViewFilter)).build();
			strViewFilter = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IViewFilterDTO oldValueDTO = new ViewFilterJsonBuilder().setJsonObj(new JSONObject(strViewFilter)).build();
			eventNotificationService.notifyNamedEvent(IViewFilterListener.class, IViewFilterListener.EVENT_VIEW_FILTER_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_DELETED.equals(message.getType())){
			String strViewFilter = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IViewFilterDTO oldValueDTO = new ViewFilterJsonBuilder().setJsonObj(new JSONObject(strViewFilter)).build();
			eventNotificationService.notifyNamedEvent(IViewFilterListener.class, IViewFilterListener.EVENT_VIEW_FILTER_DELETED_ID, oldValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_VIEW_FILTER_REORDER.equals(message.getType())){
			eventNotificationService.notifyNamedEvent(IViewFilterListener.class, IViewFilterListener.EVENT_VIEW_FILTER_ORDERED_ID);
		}
	}
	
	private void processSprintMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_SPRINT_CREATED.equals(message.getType())){
			String strSprint = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			ISprintDTO newValueDTO = new SprintJsonBuilder().setJsonObj(new JSONObject(strSprint)).build();
			eventNotificationService.notifyNamedEvent(ISprintListener.class, ISprintListener.EVENT_SPRINT_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_SPRINT_UPDATED.equals(message.getType())){
			String strSprint = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			ISprintDTO newValueDTO = new SprintJsonBuilder().setJsonObj(new JSONObject(strSprint)).build();
			strSprint = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			ISprintDTO oldValueDTO = new SprintJsonBuilder().setJsonObj(new JSONObject(strSprint)).build();
			eventNotificationService.notifyNamedEvent(ISprintListener.class, ISprintListener.EVENT_SPRINT_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_SPRINT_DELETED.equals(message.getType())){
			String strSprint = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			ISprintDTO oldValueDTO = new SprintJsonBuilder().setJsonObj(new JSONObject(strSprint)).build();
			eventNotificationService.notifyNamedEvent(ISprintListener.class, ISprintListener.EVENT_SPRINT_DELETED_ID, oldValueDTO);
		}
	}
	
	private void processTagMessage(BroadcastMessage message) throws Exception
	{
		eventNotificationService.notifyNamedEvent(ITagListener.class, ITagListener.EVENT_TAG_LIST_UPDATED);
	}
	
	private void processIssueMessage(BroadcastMessage message) throws Exception
	{
		ObjectMapper mapper = new ObjectMapper();
		if(BroadcastMessage.MESSAGE_TYPE_ISSUE_CREATED.equals(message.getType())){
			String strIssue = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IIssueDTO newValueDTO = new IssueJsonBuilder().setJsonObj(new JSONObject(strIssue)).build();
			eventNotificationService.notifyNamedEvent(IIssueListener.class, IIssueListener.EVENT_ISSUE_CREATED_ID, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_ISSUE_UPDATED.equals(message.getType())){
			String strIssue = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_NEW_VALUE));
			IIssueDTO newValueDTO = new IssueJsonBuilder().setJsonObj(new JSONObject(strIssue)).build();
			strIssue = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IIssueDTO oldValueDTO = new IssueJsonBuilder().setJsonObj(new JSONObject(strIssue)).build();
			eventNotificationService.notifyNamedEvent(IIssueListener.class, IIssueListener.EVENT_ISSUE_UPDATED_ID, oldValueDTO, newValueDTO);
		}
		else if(BroadcastMessage.MESSAGE_TYPE_ISSUE_DELETED.equals(message.getType())){
			String strIssue = mapper.writeValueAsString(message.getExtraData().get(BroadcastMessage.MESSAGE_EXTRA_DATA_OLD_VALUE));
			IIssueDTO oldValueDTO = new IssueJsonBuilder().setJsonObj(new JSONObject(strIssue)).build();
			eventNotificationService.notifyNamedEvent(IIssueListener.class, IIssueListener.EVENT_ISSUE_DELETED_ID, oldValueDTO);
		}
	}
}
