package org.fenrir.dragonfly.core.service.impl.standalone;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import com.google.inject.persist.Transactional;

import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.core.dto.adapter.AlertTypeDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.CategoryDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.CustomFieldDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.DictionaryDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.DictionaryItemDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.ProjectDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.ProviderCategoryDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.ProviderProjectDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.ProviderSeverityDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.ProviderStatusDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.SeverityDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.StatusDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.TagDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.ViewFilterDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.WorkflowDTOAdapter;
import org.fenrir.vespine.core.entity.AlertType;
import org.fenrir.vespine.core.entity.Dictionary;
import org.fenrir.vespine.core.entity.DictionaryItem;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.core.entity.IssueCategory;
import org.fenrir.vespine.core.entity.IssueCustomField;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueSeverity;
import org.fenrir.vespine.core.entity.IssueStatus;
import org.fenrir.vespine.core.entity.IssueViewFilter;
import org.fenrir.vespine.core.entity.ProviderCategory;
import org.fenrir.vespine.core.entity.ProviderProject;
import org.fenrir.vespine.core.entity.ProviderSeverity;
import org.fenrir.vespine.core.entity.ProviderStatus;
import org.fenrir.vespine.core.entity.Tag;
import org.fenrir.vespine.core.entity.Workflow;
import org.fenrir.vespine.core.entity.WorkflowStep;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IProviderAdministrationService;
import org.fenrir.vespine.core.service.IWorkflowService;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140720
 */
public class AdministrationFacadeImpl implements IAdministrationFacade 
{
	@Inject
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IProviderAdministrationService providerAdministrationService;
	
	@Inject
	private IWorkflowService workflowService;

	@Inject
	private IExtendedUserFacade userManagementService;
	
	@Inject
	private IAuditService auditService;
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setProviderAdministrationService(IProviderAdministrationService providerAdministrationService)
	{
		this.providerAdministrationService = providerAdministrationService;
	}
	
	public void setWorkflowService(IWorkflowService workflowService)
	{
		this.workflowService = workflowService;
	}
	
	public void setUserManagementService(IExtendedUserFacade userManagementService)
	{
		this.userManagementService = userManagementService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	/*---------------------------------*
     *           Projectes             *
     *---------------------------------*/
	@Override
	@Transactional
	public List<IProjectDTO> findAllProjects()
	{
		List<IProjectDTO> projectDTOs = new ArrayList<IProjectDTO>();
		
		List<IssueProject> projects = coreAdministrationService.findAllProjects();
		for(IssueProject project:projects){
			projectDTOs.add(new ProjectDTOAdapter(project));
		}
		
		return projectDTOs;
	}
	
	@Override
	@Transactional
	public List<IProjectDTO> findProjectsByWorkflow(String workflowId)
	{
		List<IProjectDTO> projectDTOs = new ArrayList<IProjectDTO>();
		
		Workflow workflow = workflowService.findWorkflowById(Long.valueOf(workflowId));
		List<IssueProject> projects = workflowService.findProjectsByWorkflow(workflow);
		for(IssueProject project:projects){
			projectDTOs.add(new ProjectDTOAdapter(project));
		}
		
		return projectDTOs;		
	}
	
	@Override
	@Transactional
	public IProjectDTO findProjectById(String id)
	{
		IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(id));
		return new ProjectDTOAdapter(project);
	}
	
	@Override
	@Transactional
	public IProjectDTO createProject(String name, String abbreviation, String workflow, List<String> providerProjects) throws BusinessException
	{
		Workflow objWorkflow = null;
		if(workflow!=null){
			objWorkflow = workflowService.findWorkflowById(Long.valueOf(workflow));
		}
		Set<ProviderProject> providerDataList = new HashSet<ProviderProject>();
		for(String providerId:providerProjects){
			ProviderProject providerRegistry = providerAdministrationService.findProviderProjectById(Long.valueOf(providerId));
			providerDataList.add(providerRegistry);
		}
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueProject project = coreAdministrationService.createIssueProject(name, abbreviation, objWorkflow, providerDataList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_CREATE, project.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new ProjectDTOAdapter(project);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la creació del projecte: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public IProjectDTO updateProject(String id, String name, String workflow, List<String> providerProjects)  throws BusinessException
	{
		Workflow objWorkflow = null;
		if(workflow!=null){
			objWorkflow = workflowService.findWorkflowById(Long.valueOf(workflow));
		}
		Set<ProviderProject> providerDataList = new HashSet<ProviderProject>();
		for(String providerId:providerProjects){
			ProviderProject providerRegistry = providerAdministrationService.findProviderProjectById(Long.valueOf(providerId));
			providerDataList.add(providerRegistry);
		}
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueProject project = coreAdministrationService.updateIssueProject(Long.valueOf(id), name, objWorkflow, providerDataList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_UPDATE, project.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new ProjectDTOAdapter(project);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_UPDATE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error modificant les dades projecte: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public void deleteProject(String id) throws BusinessException
	{
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			coreAdministrationService.deleteIssueProject(Long.valueOf(id));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_PROJECT_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error esborrant projecte: " + e.getMessage(), e);
		}
	}
	
	/*---------------------------------*
     *           Categories            *
     *---------------------------------*/
	@Override
	@Transactional
    public List<ICategoryDTO> findAllCategories()
    {
		List<ICategoryDTO> categoryDTOs = new ArrayList<ICategoryDTO>();
		
		List<IssueCategory> categories = coreAdministrationService.findAllCategories();
		for(IssueCategory category:categories){
			categoryDTOs.add(new CategoryDTOAdapter(category));
		}
		
		return categoryDTOs;
    }
	
	@Override
	@Transactional
    public ICategoryDTO findCategoryById(String id)
    {
		IssueCategory category = coreAdministrationService.findCategoryById(Long.valueOf(id));
		return new CategoryDTOAdapter(category);
    }
	
	@Override
	@Transactional
    public ICategoryDTO createCategory(String name, List<String> providerCategories)  throws BusinessException
    {
		Set<ProviderCategory> providerDataList = new HashSet<ProviderCategory>();
		for(String providerId:providerCategories){
			ProviderCategory providerRegistry = providerAdministrationService.findProviderCategoryById(Long.valueOf(providerId));
			providerDataList.add(providerRegistry);
		}
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueCategory category = coreAdministrationService.createIssueCategory(name, providerDataList);
		
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CATEGORY_CREATE, category.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
		
			return new CategoryDTOAdapter(category);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CATEGORY_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la creació de la categoria: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public ICategoryDTO updateCategory(String id, String name, List<String> providerCategories) throws BusinessException
    {
		Set<ProviderCategory> providerDataList = new HashSet<ProviderCategory>();
		for(String providerId:providerCategories){
			ProviderCategory providerRegistry = providerAdministrationService.findProviderCategoryById(Long.valueOf(providerId));
			providerDataList.add(providerRegistry);
		}
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueCategory category = coreAdministrationService.updateIssueCategory(Long.valueOf(id), name, providerDataList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CATEGORY_UPDATE, category.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new CategoryDTOAdapter(category);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CATEGORY_UPDATE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error modificant les dades de la categoria: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public void deleteCategory(String id) throws BusinessException
    {
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			coreAdministrationService.deleteIssueCategory(Long.valueOf(id));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CATEGORY_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CATEGORY_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error esborant la categoria: " + e.getMessage(), e);
		}
    }
	
    /*---------------------------------*
     *           Severitats            *
     *---------------------------------*/
	@Override
	@Transactional
    public List<ISeverityDTO> findAllSeverities()
    {
		List<ISeverityDTO> severityDTOs = new ArrayList<ISeverityDTO>();
		
		List<IssueSeverity> severities = coreAdministrationService.findAllSeverities();
		for(IssueSeverity severity:severities){
			severityDTOs.add(new SeverityDTOAdapter(severity));
		}
		
		return severityDTOs;
    }
	
	@Override
	@Transactional
    public ISeverityDTO findSeverityById(String id)
    {
		IssueSeverity severity = coreAdministrationService.findSeverityById(Long.valueOf(id));
		return new SeverityDTOAdapter(severity);
    }
	
	@Override
	@Transactional
    public ISeverityDTO createSeverity(String name, String slaPattern, List<String> providerSeverities) throws BusinessException
    {
		Set<ProviderSeverity> providerDataList = new HashSet<ProviderSeverity>();
		for(String providerId:providerSeverities){
			ProviderSeverity providerRegistry = providerAdministrationService.findProviderSeverityById(Long.valueOf(providerId));
			providerDataList.add(providerRegistry);
		}
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueSeverity severity = coreAdministrationService.createIssueSeverity(name, slaPattern, providerDataList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_CREATE, severity.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new SeverityDTOAdapter(severity);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la creació de la severitat: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public ISeverityDTO updateSeverity(String id, String name, String slaPattern, List<String> providerSeverities) throws BusinessException
    {
		Set<ProviderSeverity> providerDataList = new HashSet<ProviderSeverity>();
		for(String providerId:providerSeverities){
			ProviderSeverity providerRegistry = providerAdministrationService.findProviderSeverityById(Long.valueOf(providerId));
			providerDataList.add(providerRegistry);
		}
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueSeverity severity = coreAdministrationService.updateIssueSeverity(Long.valueOf(id), name, slaPattern, providerDataList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_UPDATE, severity.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new SeverityDTOAdapter(severity);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_UPDATE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error modificant les dades de la severitat: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public void deleteSeverity(String id) throws BusinessException
    {
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			coreAdministrationService.deleteIssueSeverity(Long.valueOf(id));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEVERITY_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error esborrant la severitat: " + e.getMessage(), e);
		}
    }
    
    /*---------------------------------*
     *             Estats              *
     *---------------------------------*/
	@Override
	@Transactional
    public List<IStatusDTO> findAllStatus()
    {
		List<IStatusDTO> statusDTOs = new ArrayList<IStatusDTO>();
		
		List<IssueStatus> statusList = coreAdministrationService.findAllStatus();
		for(IssueStatus status:statusList){
			statusDTOs.add(new StatusDTOAdapter(status));
		}
		
		return statusDTOs;
    }
	
	@Override
	@Transactional
    public IStatusDTO findStatusById(String id)
    {
		IssueStatus status = coreAdministrationService.findStatusById(Long.valueOf(id));
		return new StatusDTOAdapter(status);
    }
	
	@Override
	@Transactional
    public List<IStatusDTO> findWorkflowNextStatus(String projectId, String currentStatus)
    {
		List<IStatusDTO> statusDTOs = new ArrayList<IStatusDTO>();
		
		IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(projectId));
		IssueStatus objCurrentStatus = currentStatus!=null ? coreAdministrationService.findStatusById(Long.valueOf(currentStatus)) : null;
		List<IssueStatus> statusList = workflowService.findNextWorkflowStatus(project, objCurrentStatus);
		for(IssueStatus status:statusList){
			statusDTOs.add(new StatusDTOAdapter(status));
		}
		
		return statusDTOs;
    }
	
	@Override
	@Transactional
    public String getStatusColor(String statusId)
    {
		IssueStatus status = coreAdministrationService.findStatusById(Long.valueOf(statusId));
		return status.getColor();
    }
	
	@Override
	@Transactional
    public IStatusDTO createStatus(String name, String color, List<String> providerStatus) throws BusinessException
    {
		Set<ProviderStatus> providerDataList = new HashSet<ProviderStatus>();
		for(String providerId:providerStatus){
			ProviderStatus providerRegistry = providerAdministrationService.findProviderStatusById(Long.valueOf(providerId));
			providerDataList.add(providerRegistry);
		}
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueStatus status = coreAdministrationService.createIssueStatus(name, color, providerDataList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_CREATE, status.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new StatusDTOAdapter(status);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la creació de l'estat: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public IStatusDTO updateStatus(String id, String name, String color, List<String> providerStatus) throws BusinessException
    {
		Set<ProviderStatus> providerDataList = new HashSet<ProviderStatus>();
		for(String providerId:providerStatus){
			ProviderStatus providerRegistry = providerAdministrationService.findProviderStatusById(Long.valueOf(providerId));
			providerDataList.add(providerRegistry);
		}
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueStatus status = coreAdministrationService.updateIssueStatus(Long.valueOf(id), name, color, providerDataList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_UPDATE, status.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new StatusDTOAdapter(status);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_UPDATE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error modificant les dades de l'estat: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public void deleteIssueStatus(String id) throws BusinessException
    {
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			coreAdministrationService.deleteIssueStatus(Long.valueOf(id));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_STATUS_DELETE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error esborrant l'estat: " + e.getMessage(), e);
		}
    }
    
    /*---------------------------------*
     *           Workflow              *
     *---------------------------------*/
	@Override
	@Transactional
    public List<IWorkflowDTO> findAllWorkflows()
    {
		List<IWorkflowDTO> workflowDTOs = new ArrayList<IWorkflowDTO>();
		
		List<Workflow> workflows = workflowService.findAllWorkflows();
		for(Workflow workflow:workflows){
			workflowDTOs.add(new WorkflowDTOAdapter(workflow));
		}
		
		return workflowDTOs;
    }
	
	@Override
	@Transactional
	public boolean hasWorkflowRelatedProjects(String workflowId)
	{
		Workflow workflow = workflowService.findWorkflowById(Long.valueOf(workflowId));
		return workflowService.hasWorkflowRelatedProjects(workflow);
	}
	
	@Override
	@Transactional
    public IWorkflowDTO findWorkflowById(String id)
    {
		Workflow workflow = workflowService.findWorkflowById(Long.valueOf(id));
		return new WorkflowDTOAdapter(workflow);
    }
	
	@Override
	@Transactional
    public IWorkflowDTO createWorkflow(String name, String initialStatus, List<IWorkflowStepDTO> steps) throws BusinessException
    {
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueStatus objInitialStatus = coreAdministrationService.findStatusById(Long.valueOf(initialStatus));
			Map<String, IssueStatus> statusCache = new HashMap<String, IssueStatus>();
			statusCache.put(objInitialStatus.getId().toString(), objInitialStatus);
			List<WorkflowStep> stepList = new ArrayList<WorkflowStep>();
			for(IWorkflowStepDTO dto:steps){
				WorkflowStep step = new WorkflowStep();
				IssueStatus sourceStatus = null;
				if(statusCache.containsKey(dto.getSourceStatus().getStatusId())){
					sourceStatus = statusCache.get(dto.getSourceStatus().getStatusId());
				}
				else{
					sourceStatus = coreAdministrationService.findStatusById(Long.valueOf(dto.getSourceStatus().getStatusId()));
					statusCache.put(sourceStatus.getId().toString(), sourceStatus);
				}
				step.setSourceStatus(sourceStatus);
				IssueStatus destinationStatus = null;
				if(statusCache.containsKey(dto.getDestinationStatus().getStatusId())){
					destinationStatus = statusCache.get(dto.getDestinationStatus().getStatusId());
				}
				else{
					destinationStatus = coreAdministrationService.findStatusById(Long.valueOf(dto.getDestinationStatus().getStatusId()));
					statusCache.put(destinationStatus.getId().toString(), destinationStatus);
				}
				step.setDestinationStatus(destinationStatus);
				step.setLastUpdated(dto.getLastUpdated());
				stepList.add(step);
			}
			Workflow workflow = workflowService.createWorkflow(name, objInitialStatus, stepList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_CREATE, workflow.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new WorkflowDTOAdapter(workflow);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la creació del fluxe de treball: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
	public IWorkflowDTO updateWorkflow(String id, String name, String initialStatus, List<IWorkflowStepDTO> steps) throws BusinessException
	{
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			Workflow currentWorkflow = workflowService.findWorkflowById(Long.valueOf(id));
			IssueStatus objInitialStatus = coreAdministrationService.findStatusById(Long.valueOf(initialStatus));
			Map<String, IssueStatus> statusCache = new HashMap<String, IssueStatus>();
			statusCache.put(objInitialStatus.getId().toString(), objInitialStatus);
			List<WorkflowStep> stepList = new ArrayList<WorkflowStep>();
			for(IWorkflowStepDTO dto:steps){
				WorkflowStep step = new WorkflowStep();
				IssueStatus sourceStatus = null;
				if(statusCache.containsKey(dto.getSourceStatus().getStatusId())){
					sourceStatus = statusCache.get(dto.getSourceStatus().getStatusId());
				}
				else{
					sourceStatus = coreAdministrationService.findStatusById(Long.valueOf(dto.getSourceStatus().getStatusId()));
					statusCache.put(sourceStatus.getId().toString(), sourceStatus);
				}
				step.setSourceStatus(sourceStatus);
				IssueStatus destinationStatus = null;
				if(statusCache.containsKey(dto.getDestinationStatus().getStatusId())){
					destinationStatus = statusCache.get(dto.getDestinationStatus().getStatusId());
				}
				else{
					destinationStatus = coreAdministrationService.findStatusById(Long.valueOf(dto.getDestinationStatus().getStatusId()));
					statusCache.put(destinationStatus.getId().toString(), destinationStatus);
				}
				step.setDestinationStatus(destinationStatus);
				step.setWorkflow(currentWorkflow);
				step.setLastUpdated(dto.getLastUpdated());
				stepList.add(step);
			}
			Workflow workflow = workflowService.updateWorkflow(Long.valueOf(id), name, objInitialStatus, stepList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_UPDATE, Long.toString(workflow.getId()), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new WorkflowDTOAdapter(workflow);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_UPDATE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error modificant les dades del fluxe de treball: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public void deleteWorkflow(String id) throws BusinessException
	{
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			workflowService.deleteWorkflow(Long.valueOf(id));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_WORKFLOW_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error esborrant el fluxe de treball: " + e.getMessage(), e);
		}
	}
	
	/*---------------------------------*
     *          Tipus d'alerta         *
     *---------------------------------*/
	@Override
	@Transactional
	public List<IAlertTypeDTO> findAllAlertTypes()
	{
		List<IAlertTypeDTO> typesDTOs = new ArrayList<IAlertTypeDTO>();
		
		List<AlertType> types = coreAdministrationService.findAllAlertTypes();
		for(AlertType type:types){
			typesDTOs.add(new AlertTypeDTOAdapter(type));
		}
		
		return typesDTOs;
	}
	
	@Override
	@Transactional
	public IAlertTypeDTO findAlertTypeById(String id)
	{
		AlertType alertType = coreAdministrationService.findAlertTypeById(Long.valueOf(id));
		return new AlertTypeDTOAdapter(alertType);
	}
	
	@Override
	@Transactional
	public IAlertTypeDTO createAlertType(String name, String description, String icon, String alertRule, boolean generateOld) throws BusinessException
	{
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			AlertType alertType = coreAdministrationService.createAlertType(name, description, icon, alertRule, generateOld);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_CREATE, alertType.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new AlertTypeDTOAdapter(alertType);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la creació del tipus d'alerta: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
    public IAlertTypeDTO updateAlertType(String id, String name, String description, String icon, String alertRule) throws BusinessException
    {
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			AlertType alertType = coreAdministrationService.updateAlertType(Long.valueOf(id), name, description, icon, alertRule);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_UPDATE, alertType.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new AlertTypeDTOAdapter(alertType);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_UPDATE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la modificació del tipus d'alerta: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public void deleteAlertType(String id) throws BusinessException
    {
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			coreAdministrationService.deleteAlertType(Long.valueOf(id));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_ALERT_TYPE_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la l'eliminació del tipus d'alerta: " + e.getMessage(), e);
		}
    }
	
    /*---------------------------------*
     *              Tags               *
     *---------------------------------*/
	@Override
	@Transactional
    public List<ITagDTO> findAllTags()
    {
		List<ITagDTO> tagDTOs = new ArrayList<ITagDTO>();
		
		List<Tag> tags = coreAdministrationService.findAllTags();
		for(Tag tag:tags){
			tagDTOs.add(new TagDTOAdapter(tag));
		}
		
		return tagDTOs;
    }
	
	@Override
	@Transactional
    public List<ITagDTO> findTagsFiltered(String prefix)
    {
		List<ITagDTO> tagDTOs = new ArrayList<ITagDTO>();
		
		List<Tag> tags = coreAdministrationService.findTagsFiltered(prefix);
		for(Tag tag:tags){
			tagDTOs.add(new TagDTOAdapter(tag));
		}
		
		return tagDTOs;
    }
	
	@Override
	@Transactional
    public List<ITagDTO> findTagsByName(String name)
    {
		List<ITagDTO> tagDTOs = new ArrayList<ITagDTO>();
		
		List<Tag> tags = coreAdministrationService.findTagsByName(name);
		for(Tag tag:tags){
			tagDTOs.add(new TagDTOAdapter(tag));
		}
		
		return tagDTOs;
    }
	
	@Override
	@Transactional
    public List<ITagDTO> findPreferredTags()
    {
		List<ITagDTO> tagDTOs = new ArrayList<ITagDTO>();
		
		List<Tag> tags = coreAdministrationService.findPreferredTags();
		for(Tag tag:tags){
			tagDTOs.add(new TagDTOAdapter(tag));
		}
		
		return tagDTOs;
    }
	
	@Override
	@Transactional
    public List<ITagDTO> findPreferredTagsFiltered(String prefix)
    {
		List<ITagDTO> tagDTOs = new ArrayList<ITagDTO>();
		
		List<Tag> tags = coreAdministrationService.findPreferredTagsFiltered(prefix);
		for(Tag tag:tags){
			tagDTOs.add(new TagDTOAdapter(tag));
		}
		
		return tagDTOs;
    }
	
	@Override
	@Transactional
	public ITagDTO findTagById(String tagId)
	{
		Tag tag = coreAdministrationService.findTagById(Long.valueOf(tagId));
		if(tag!=null){
			return new TagDTOAdapter(tag);
		}
		
		return null;
	}
	
	@Override
	@Transactional
    public ITagDTO createTag(String name, boolean preferred) throws BusinessException
    {
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			Tag tag = coreAdministrationService.createTag(name, preferred);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_TAG_CREATE, tag.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new TagDTOAdapter(tag);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_TAG_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la creació de l'etiqueta: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public ITagDTO updateTag(String tagId, String name, boolean preferred) throws BusinessException
    {
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			Tag tag = coreAdministrationService.updateTag(Long.valueOf(tagId), name, preferred);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_TAG_UPDATE, tag.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new TagDTOAdapter(tag);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_TAG_UPDATE, tagId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error modificant les dades de l'etiqueta: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public void deleteTag(String tagId) throws BusinessException
	{
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			coreAdministrationService.deleteTag(Long.valueOf(tagId));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_TAG_DELETE, tagId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_TAG_DELETE, tagId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error esborrant l'etiqueta: " + e.getMessage(), e);
		}
	}
	
    /*---------------------------------*
     *           Diccionaris           *
     *---------------------------------*/
	@Override
	@Transactional
    public List<IDictionaryDTO> findAllDictionaries()
    {
		List<IDictionaryDTO> dictionaryDTOs = new ArrayList<IDictionaryDTO>();
		
		List<Dictionary> dictionaries = coreAdministrationService.findAllDictionaries();
		for(Dictionary dictionary:dictionaries){
			dictionaryDTOs.add(new DictionaryDTOAdapter(dictionary));
		}
		
		return dictionaryDTOs;
    }
	
	@Override
	@Transactional
    public IDictionaryDTO findDictionaryById(String dictionaryId)
    {
		Dictionary dictionary = coreAdministrationService.findDictionaryById(Long.valueOf(dictionaryId));
		return new DictionaryDTOAdapter(dictionary);
    }
	
	@Override
	@Transactional
    public List<IDictionaryItemDTO> findDictionaryItems(String dictionaryId)
    {
		List<IDictionaryItemDTO> itemDTOs = new ArrayList<IDictionaryItemDTO>();
		
		List<DictionaryItem> items = coreAdministrationService.findDictionaryItems(Long.valueOf(dictionaryId));
		for(DictionaryItem item:items){
			itemDTOs.add(new DictionaryItemDTOAdapter(item));
		}
		
		return itemDTOs;
    }
	
	@Override
	@Transactional
	public IDictionaryItemDTO findDictionaryItem(String itemId) throws BusinessException
	{
		return new DictionaryItemDTOAdapter(coreAdministrationService.findDictionaryItem(Long.valueOf(itemId)));
	}
    
	@Override
	@Transactional
    public IDictionaryDTO createDictionary(String name, String description, List<IDictionaryItemDTO> items) throws BusinessException
    {
		List<DictionaryItem> itemList = new ArrayList<DictionaryItem>();
		for(IDictionaryItemDTO dto:items){
			DictionaryItem item = new DictionaryItem();
			Long itemId = dto.getItemId()!=null ? Long.valueOf(dto.getItemId()) : null;
			item.setId(itemId);
			item.setDescription(dto.getDescription());
			item.setValue(dto.getValue());
			item.setLastUpdated(dto.getLastUpdated());
			itemList.add(item);
		}
		
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			Dictionary dictionary = coreAdministrationService.createDictionary(name, description, itemList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_CREATE, dictionary.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new DictionaryDTOAdapter(dictionary);
	    }
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error creant diccionari: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public IDictionaryDTO updateDictionary(String id, String name, String description, List<IDictionaryItemDTO> items) throws BusinessException
    {
		Dictionary currentDictionary = coreAdministrationService.findDictionaryById(Long.valueOf(id));
		HashMap<Long, DictionaryItem> itemCache = new HashMap<Long, DictionaryItem>();
		for(DictionaryItem elem:currentDictionary.getItems()){
			itemCache.put(elem.getId(), elem);
		}
		
		List<DictionaryItem> itemList = new ArrayList<DictionaryItem>();
		for(IDictionaryItemDTO dto:items){
			DictionaryItem item;
			if(dto.getItemId()!=null){
				item = itemCache.get(Long.valueOf(dto.getItemId()));
			}
			else{
				item = new DictionaryItem();
			}
			item.setDictionary(currentDictionary);
			item.setDescription(dto.getDescription());
			item.setValue(dto.getValue());
			item.setLastUpdated(dto.getLastUpdated());
			itemList.add(item);
		}
		
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			Dictionary dictionary = coreAdministrationService.updateDictionary(Long.valueOf(id), name, description, itemList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_UPDATE, dictionary.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new DictionaryDTOAdapter(dictionary);
	    }
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_UPDATE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error modificant les dades del diccionari: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public void deleteDictionary(String id) throws BusinessException
    {
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			coreAdministrationService.deleteDictionary(Long.valueOf(id));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_OK, null);
	    }
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_DICTIONARY_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error esborrant el diccionari: " + e.getMessage(), e);
		}
    }
    
    /*---------------------------------*
     *           Camps custom          *
     *---------------------------------*/
	@Override
	public List<IFieldType> getAllFieldTypes()
	{
		List<IFieldType> fieldTypes = new ArrayList<IFieldType>();
		fieldTypes.addAll(Arrays.asList(FieldType.values()));
		return fieldTypes;
	}
	
	@Override
	@Transactional
    public List<ICustomFieldDTO> findAllCustomFields()
    {
		List<ICustomFieldDTO> customFieldDTOs = new ArrayList<ICustomFieldDTO>();
		
		List<IssueCustomField> fields = coreAdministrationService.findAllCustomFields();
		for(IssueCustomField field:fields){
			customFieldDTOs.add(new CustomFieldDTOAdapter(field));
		}
		
		return customFieldDTOs;
    }
	
	@Override
	@Transactional
    public List<ICustomFieldDTO> findCustomFieldsByProject(String projectId)
    {
		List<ICustomFieldDTO> customFieldDTOs = new ArrayList<ICustomFieldDTO>();
		
		IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(projectId));
		List<IssueCustomField> fields = coreAdministrationService.findCustomFieldsByProject(project);
		for(IssueCustomField field:fields){
			customFieldDTOs.add(new CustomFieldDTOAdapter(field));
		}
		
		return customFieldDTOs;
    }
	
	@Override
	@Transactional
    public ICustomFieldDTO findCustomFieldById(String fieldId)
    {
		IssueCustomField customField = coreAdministrationService.findCustomFieldById(Long.valueOf(fieldId));
		return new CustomFieldDTOAdapter(customField);
    }
    
	@Override
	@Transactional
    public ICustomFieldDTO createCustomField(String name, IFieldType fieldType, String dataProviderId, boolean mandatory, List<String> projects) throws BusinessException
	{
		List<IssueProject> projectList = new ArrayList<IssueProject>();
		for(String projectId:projects){
			IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(projectId));
			projectList.add(project);
		}
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			FieldType type = FieldType.valueOf(fieldType.getType());
			IssueCustomField customField = coreAdministrationService.createCustomField(name, type, dataProviderId, mandatory, projectList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_CREATE, customField.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new CustomFieldDTOAdapter(customField);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la creació del camp personalitzat: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
    public ICustomFieldDTO updateCustomField(String id, String name, IFieldType fieldType, String dataProviderId, boolean mandatory, List<String> projects) throws BusinessException
    {
		List<IssueProject> projectList = new ArrayList<IssueProject>();
		for(String projectId:projects){
			IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(projectId));
			projectList.add(project);
		}
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			FieldType type = FieldType.valueOf(fieldType.getType());
			IssueCustomField customField = coreAdministrationService.updateCustomField(Long.valueOf(id), name, type, dataProviderId, mandatory, projectList);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_UPDATE, customField.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new CustomFieldDTOAdapter(customField);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_UPDATE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error modificant les dades del camp personalitzat: " + e.getMessage(), e);
		}
    }
	
	@Override
	@Transactional
    public void deleteCustomField(String id) throws BusinessException
    {
		String loggedUser = userManagementService.getLoggedUsername();
		try{
			coreAdministrationService.deleteCustomField(Long.valueOf(id));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_CUSTOM_FIELD_DELETE, id, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error esborrant el camp personalitzat: " + e.getMessage(), e);
		}
    }
	
	/*---------------------------------*
     *       Providers Projectes       *
     *---------------------------------*/
	@Override
	@Transactional
    public List<IProviderElementDTO> findAllProviderProjects()
    {
		List<IProviderElementDTO> elementDTOs = new ArrayList<IProviderElementDTO>();
		
		List<ProviderProject> projects = providerAdministrationService.findAllProviderProjects();
		for(ProviderProject project:projects){
			elementDTOs.add(new ProviderProjectDTOAdapter(project));
		}
		
		return elementDTOs;
    }
	
	@Override
	@Transactional
    public List<IProviderElementDTO> findProviderProjectsByProvider(String provider)
    {
		List<IProviderElementDTO> elementDTOs = new ArrayList<IProviderElementDTO>();
		
		List<ProviderProject> projects = providerAdministrationService.findProviderProjectsByProvider(provider);
		for(ProviderProject project:projects){
			elementDTOs.add(new ProviderProjectDTOAdapter(project));
		}
		
		return elementDTOs;
    }
	
	@Override
	@Transactional
    public IProviderElementDTO findProviderProjectById(String id)
	{
		ProviderProject project = providerAdministrationService.findProviderProjectById(Long.valueOf(id));
		return new ProviderProjectDTOAdapter(project);
	}
	
	@Override
	@Transactional
    public IProviderElementDTO findProviderProjectByProviderData(String provider, String providerId)
	{
		ProviderProject project = providerAdministrationService.findProviderProjectByProviderData(provider, providerId);
		return new ProviderProjectDTOAdapter(project);
	}

    /*---------------------------------*
     *         Providers Estats        *
     *---------------------------------*/
    @Override
	@Transactional
    public List<IProviderElementDTO> findAllProviderStatus()
    {
    	List<IProviderElementDTO> elementDTOs = new ArrayList<IProviderElementDTO>();
		
		List<ProviderStatus> status = providerAdministrationService.findAllProviderStatus();
		for(ProviderStatus elem:status){
			elementDTOs.add(new ProviderStatusDTOAdapter(elem));
		}
		
		return elementDTOs;
    }
    
    @Override
	@Transactional
    public List<IProviderElementDTO> findProviderStatusByProvider(String provider)
    {
    	List<IProviderElementDTO> elementDTOs = new ArrayList<IProviderElementDTO>();
		
		List<ProviderStatus> status = providerAdministrationService.findProviderStatusByProvider(provider);
		for(ProviderStatus elem:status){
			elementDTOs.add(new ProviderStatusDTOAdapter(elem));
		}
		
		return elementDTOs;
    }
    
    @Override
	@Transactional
    public IProviderElementDTO findProviderStatusById(String id)
    {
    	ProviderStatus status = providerAdministrationService.findProviderStatusById(Long.valueOf(id));
    	return new ProviderStatusDTOAdapter(status);
    }
    
    @Override
	@Transactional
    public IProviderElementDTO findProviderStatusByProviderData(String provider, String providerId)
    {
    	ProviderStatus status = providerAdministrationService.findProviderStatusByProviderData(provider, providerId);
    	return new ProviderStatusDTOAdapter(status);
    }
    
    /*---------------------------------*
     *       Providers Severitats      *
     *---------------------------------*/
    @Override
	@Transactional
    public List<IProviderElementDTO> findAllProviderSeverities()
    {
    	List<IProviderElementDTO> elementDTOs = new ArrayList<IProviderElementDTO>();
		
		List<ProviderSeverity> severities = providerAdministrationService.findAllProviderSeverities();
		for(ProviderSeverity elem:severities){
			elementDTOs.add(new ProviderSeverityDTOAdapter(elem));
		}
		
		return elementDTOs;
    }
    
    @Override
	@Transactional
    public List<IProviderElementDTO> findProviderSeveritiesByProvider(String provider)
    {
    	List<IProviderElementDTO> elementDTOs = new ArrayList<IProviderElementDTO>();
		
		List<ProviderSeverity> severities = providerAdministrationService.findProviderSeveritiesByProvider(provider);
		for(ProviderSeverity elem:severities){
			elementDTOs.add(new ProviderSeverityDTOAdapter(elem));
		}
		
		return elementDTOs;
    }
    
    @Override
	@Transactional
    public IProviderElementDTO findProviderSeverityById(String id)
    {
    	ProviderSeverity severity = providerAdministrationService.findProviderSeverityById(Long.valueOf(id));
    	return new ProviderSeverityDTOAdapter(severity);
    }
    
    @Override
	@Transactional
    public IProviderElementDTO findProviderSeverityByProviderData(String provider, String providerId)
    {
    	ProviderSeverity severity = providerAdministrationService.findProviderSeverityByProviderData(provider, providerId);
    	return new ProviderSeverityDTOAdapter(severity);
    }
    
    /*---------------------------------*
     *       Providers Categories      *
     *---------------------------------*/
    @Override
	@Transactional    
    public List<IProviderElementDTO> findAllProviderCategories()
    {
    	List<IProviderElementDTO> elementDTOs = new ArrayList<IProviderElementDTO>();
		
		List<ProviderCategory> categories = providerAdministrationService.findAllProviderCategories();
		for(ProviderCategory elem:categories){
			elementDTOs.add(new ProviderCategoryDTOAdapter(elem));
		}
		
		return elementDTOs;
    }
    
    @Override
	@Transactional
    public List<IProviderElementDTO> findProviderCategoriesByProvider(String provider)
    {
    	List<IProviderElementDTO> elementDTOs = new ArrayList<IProviderElementDTO>();
		
		List<ProviderCategory> categories = providerAdministrationService.findProviderCategoriesByProvider(provider);
		for(ProviderCategory elem:categories){
			elementDTOs.add(new ProviderCategoryDTOAdapter(elem));
		}
		
		return elementDTOs;
    }
    
    @Override
	@Transactional
    public IProviderElementDTO findProviderCategoryById(String id)
    {
    	ProviderCategory category = providerAdministrationService.findProviderCategoryById(Long.valueOf(id));
    	return new ProviderCategoryDTOAdapter(category);
    }
    
    @Override
	@Transactional
    public IProviderElementDTO findProviderCategoryByProviderData(String provider, String providerId)
    {
    	ProviderCategory category = providerAdministrationService.findProviderCategoryByProviderData(provider, providerId);
    	return new ProviderCategoryDTOAdapter(category);
    }
    
    /*---------------------------------*
     *             Filtres             *
     *---------------------------------*/
    @Override
	@Transactional
    public List<IViewFilterDTO> findApplicationViewFilters()
    {
    	List<IViewFilterDTO> filterDTOs = new ArrayList<IViewFilterDTO>();
    	List<IssueViewFilter> filters = coreAdministrationService.findApplicationViewFilters();
    	for(IssueViewFilter filter:filters){
    		filterDTOs.add(new ViewFilterDTOAdapter(filter));
    	}
    	return filterDTOs;
    }
    
    @Override
	@Transactional
    public List<IViewFilterDTO> findUserViewFilters()
    {
    	List<IViewFilterDTO> filterDTOs = new ArrayList<IViewFilterDTO>();
    	List<IssueViewFilter> filters = coreAdministrationService.findUserViewFilters();
    	for(IssueViewFilter filter:filters){
    		filterDTOs.add(new ViewFilterDTOAdapter(filter));
    	}
    	return filterDTOs;
    }
    
    @Override
    @Transactional
    public IViewFilterDTO findViewFilterById(String filterId)
    {
    	IssueViewFilter filter = coreAdministrationService.findViewFilterById(Long.valueOf(filterId));
    	if(filter!=null){
    		return new ViewFilterDTOAdapter(filter);
    	}
    	
    	return null;
    }
    
    @Override
	@Transactional
    public void reorderViewFilter(String filterId, Integer order) throws BusinessException
    {
    	String loggedUser = userManagementService.getLoggedUsername();
		try{
			coreAdministrationService.reorderIssueViewFilter(Long.valueOf(filterId), order);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_CREATE, filterId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_CREATE, filterId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error reordenant el filtre: " + e.getMessage(), e);
		}
    }
    
    @Override
	@Transactional
    public IViewFilterDTO createViewFilter(String filterName, String filterDescription, String filterQuery, String filterOrderClause) throws BusinessException
    {
    	String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueViewFilter filter = coreAdministrationService.createIssueViewFilter(filterName, filterDescription, filterQuery, filterOrderClause);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_CREATE, Long.toString(filter.getId()), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new ViewFilterDTOAdapter(filter);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error modificant el filtre: " + e.getMessage(), e);
		}
    }
    
    @Override
	@Transactional
    public IViewFilterDTO updateViewFilter(String filterId, String filterName, String filterDescription, String filterQuery, String filterOrderClause) throws BusinessException
    {
    	String loggedUser = userManagementService.getLoggedUsername();
		try{
			IssueViewFilter filter = coreAdministrationService.updateIssueViewFilter(Long.valueOf(filterId), filterName, filterDescription, filterQuery, filterOrderClause);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_UPDATE, filterId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new ViewFilterDTOAdapter(filter);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_UPDATE, filterId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error modificant el filtre: " + e.getMessage(), e);
		}
    }
    
    @Override
	@Transactional
    public void deleteViewFilter(String filterId) throws BusinessException
    {
    	String loggedUser = userManagementService.getLoggedUsername();
		try{
			coreAdministrationService.deleteIssueViewFilter(Long.valueOf(filterId));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_DELETE, filterId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_VIEW_FILTER_DELETE, filterId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error esborrant el filtre: " + e.getMessage(), e);
		}
    }
}
