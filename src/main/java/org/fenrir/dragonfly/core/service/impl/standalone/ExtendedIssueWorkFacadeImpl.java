package org.fenrir.dragonfly.core.service.impl.standalone;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang.time.DateUtils;

import com.google.inject.persist.Transactional;

import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.dto.IIssueWipRegistryDTO;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.core.dto.adapter.IssueWipRegistryDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.SprintDTOAdapter;
import org.fenrir.vespine.core.dto.adapter.WorkRegistryDTOAdapter;
import org.fenrir.vespine.core.entity.AbstractIssue;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueWipRegistry;
import org.fenrir.vespine.core.entity.IssueWorkRegistry;
import org.fenrir.vespine.core.entity.Sprint;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ICoreAdministrationService;
import org.fenrir.vespine.core.service.IIssueSearchService;
import org.fenrir.vespine.core.service.IIssueWorkService;
import org.fenrir.vespine.core.service.IUserManagementService;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.dao.IIssueTimerRegistryDAO;
import org.fenrir.dragonfly.core.entity.IssueTimerRegistry;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140511
 */
public class ExtendedIssueWorkFacadeImpl implements IExtendedIssueWorkFacade 
{
	@Inject
	private IIssueWorkService workService;
	
	@Inject
	private ICoreAdministrationService coreAdministrationService;
	
	@Inject
	private IIssueSearchService issueSearchService;
	
	@Inject
	private IIssueTimerRegistryDAO timerRegistryDAO;
	
	@Inject
	private IUserManagementService userManagementService;
	
	@Inject 
	private IExtendedUserFacade extendedUserManagementService;
	
	@Inject
	private IAuditService auditService;
	
	public void setWorkService(IIssueWorkService workService)
	{
		this.workService = workService;
	}
	
	public void setCoreAdministrationService(ICoreAdministrationService coreAdministrationService)
	{
		this.coreAdministrationService = coreAdministrationService;
	}
	
	public void setIssueSearchService(IIssueSearchService issueSearchService)
	{
		this.issueSearchService = issueSearchService;
	}
	
	public void setTimerRegistryDAO(IIssueTimerRegistryDAO timerRegistryDAO)
	{
		this.timerRegistryDAO = timerRegistryDAO;
	}
	
	public void setUserManagementService(IUserManagementService userManagementService)
	{
		this.userManagementService = userManagementService;
	}
	
	public void setExtendedUserManagementService(IExtendedUserFacade extendedUserManagementService)
	{
		this.extendedUserManagementService = extendedUserManagementService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	/*---------------------------------*
     *             Sprints             *
     *---------------------------------*/
	@Override
	@Transactional
	public List<ISprintDTO> findAllSprints()
	{
		List<ISprintDTO> sprintDTOs = new ArrayList<ISprintDTO>();
		List<Sprint> sprints = workService.findAllSprints();
		for(Sprint sprint:sprints){
			sprintDTOs.add(new SprintDTOAdapter(sprint));
		}
		return sprintDTOs;
	}
	
	@Override
	@Transactional
	public List<ISprintDTO> findSprintsByProjectNameLike(String name)
	{
		List<ISprintDTO> sprintDTOs = new ArrayList<ISprintDTO>();
		List<Sprint> sprints = workService.findSprintsByProjectNameLike(name);
		for(Sprint sprint:sprints){
			sprintDTOs.add(new SprintDTOAdapter(sprint));
		}
		return sprintDTOs;
	}
	
	@Override
	@Transactional
	public List<ISprintDTO> findSprintsByStartDateLike(String startDatePart)
	{
		List<ISprintDTO> sprintDTOs = new ArrayList<ISprintDTO>();
		List<Sprint> sprints = workService.findSprintsByStartDateLike(startDatePart);
		for(Sprint sprint:sprints){
			sprintDTOs.add(new SprintDTOAdapter(sprint));
		}
		return sprintDTOs;
	}
	
	@Override
	@Transactional
	public List<ISprintDTO> findSprintsByEndDateLike(String endDatePart)
	{
		List<ISprintDTO> sprintDTOs = new ArrayList<ISprintDTO>();
		List<Sprint> sprints = workService.findSprintsByEndDateLike(endDatePart);
		for(Sprint sprint:sprints){
			sprintDTOs.add(new SprintDTOAdapter(sprint));
		}
		return sprintDTOs;
	}
	
	@Override
	@Transactional
	public ISprintDTO findSprintById(String sprintId)
	{
		Sprint sprint = workService.findSprintById(Long.valueOf(sprintId));
		if(sprint!=null){
			return new SprintDTOAdapter(sprint);
		}
		return null;
	}
	
	@Override
	@Transactional
	public ISprintDTO createSprint(String projectId, Date startDate, Date endDate) throws BusinessException
	{
		String loggedUser = extendedUserManagementService.getLoggedUsername();
		try{
			IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(projectId));
			Sprint sprint = workService.createSprint(project, startDate, endDate);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_CREATE, sprint.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new SprintDTOAdapter(sprint);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la creació de l'sprint: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public ISprintDTO updateSprint(String sprintId, String projectId, Date startDate, Date endDate) throws BusinessException
	{
		String loggedUser = extendedUserManagementService.getLoggedUsername();
		try{
			IssueProject project = coreAdministrationService.findProjectById(Long.valueOf(projectId));
			Sprint sprint = workService.updateSprint(Long.valueOf(sprintId), project, startDate, endDate);
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_UPDATE, sprint.getId().toString(), loggedUser, AuditConstants.RESULT_CODE_OK, null);
			
			return new SprintDTOAdapter(sprint);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_UPDATE, sprintId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a la modificació de l'sprint: " + e.getMessage(), e);
		}
	}
	
	@Override
	@Transactional
	public void deleteSprint(String sprintId) throws BusinessException
	{
		String loggedUser = extendedUserManagementService.getLoggedUsername();
		try{
			workService.deleteSprint(Long.valueOf(sprintId));
			
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_DELETE, sprintId, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(Exception e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SPRINT_DELETE, sprintId, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			
			throw new BusinessException("Error a l'eliminació de l'sprint: " + e.getMessage(), e);
		}
	}

	/*---------------------------------*
     *      Registres de treball       *
     *---------------------------------*/
	@Override
	@Transactional
	public List<IWorkRegistryDTO> findAllIssueWorkRegistries(String issueId)
	{
		AbstractIssue issue = issueSearchService.findIssueById(Long.valueOf(issueId));
		List<IssueWorkRegistry> registries = workService.findAllIssueWorkRegistriesByIssue(issue);
		List<IWorkRegistryDTO> registryDTOs = new ArrayList<IWorkRegistryDTO>();
		for(IssueWorkRegistry registry:registries){
			registryDTOs.add(new WorkRegistryDTOAdapter(registry));
		}
		return registryDTOs;
	}
	
	@Override
	@Transactional
	public IWorkRegistryDTO findIssueWorkRegistryById(String registryId, String issueId) throws BusinessException
	{
		IssueWorkRegistry registry = workService.findIssueWorkRegistryById(Long.valueOf(registryId));
		if(registry!=null){
			return new WorkRegistryDTOAdapter(registry);
		}
		
		return null;
	}
	
	@Override
	@Transactional
	public IWorkRegistryDTO createIssueWorkRegistry(String issueId, Date workingDay, boolean includeInSprint, String description, String username, Integer time)
	{
		AbstractIssue issue = issueSearchService.findIssueById(Long.valueOf(issueId));
		User user = userManagementService.findUserByUsername(username); 
		IssueWorkRegistry workRegistry = workService.createIssueWorkRegistry(issue, workingDay, includeInSprint, description, user, time);
		return new WorkRegistryDTOAdapter(workRegistry);
	}
	
	@Override
	@Transactional
	public IWorkRegistryDTO updateIssueWorkRegistry(String registryId, String issueId, Date workingDay, boolean includeInSprint, String description, String username, Integer time)
	{
		AbstractIssue issue = issueSearchService.findIssueById(Long.valueOf(issueId));
		User user = userManagementService.findUserByUsername(username); 
		IssueWorkRegistry workRegistry = workService.updateIssueWorkRegistry(Long.valueOf(registryId), issue, workingDay, includeInSprint, description, user, time);
		return new WorkRegistryDTOAdapter(workRegistry);
	}
	
	@Override
	@Transactional
	public void deleteIssueWorkRegistry(String registryId, String issueId)
	{
		workService.deleteIssueWorkRegistry(Long.valueOf(registryId));
	}
	
	/*---------------------------------*
     *      	Registres W.I.P.       *
     *---------------------------------*/
	@Override
	@Transactional
	public List<IIssueWipRegistryDTO> findIssueWorkInProgressRegistriesByUser(String username)
	{
		List<IIssueWipRegistryDTO> registryDTOs = new ArrayList<IIssueWipRegistryDTO>();
		
		User user = userManagementService.findUserByUsername(username);
		List<IssueWipRegistry> registries = workService.findIssueWorkInProgressRegistriesByUser(user);
		for(IssueWipRegistry registry:registries){
			registryDTOs.add(new IssueWipRegistryDTOAdapter(registry));
		}
		
		return registryDTOs;
	}
	
	
	@Override
	@Transactional
	public IIssueWipRegistryDTO findIssueWorkInProgressRegistry(String issueId, String username)
	{
		User user = userManagementService.findUserByUsername(username); 
		IssueWipRegistry registry = workService.findIssueWorkInProgressRegistry(Long.valueOf(issueId), user);
		if(registry!=null){
			return new IssueWipRegistryDTOAdapter(registry);
		}
		else{
			return null;
		}
	}
	
	@Override
	@Transactional
	public IIssueWipRegistryDTO findIssueWorkInProgressRegistryById(String registryId, String issueId) throws BusinessException
	{
		IssueWipRegistry registry = workService.findIssueWorkInProgressRegistryById(Long.valueOf(registryId));
		if(registry!=null){
			return new IssueWipRegistryDTOAdapter(registry);
		}
		else{
			return null;
		}
	}

	@Override
	@Transactional
	public IIssueWipRegistryDTO createWorkInProgressRegistry(String issueId, String username)
	{
		User user = userManagementService.findUserByUsername(username); 
		IssueWipRegistry registry = workService.createWorkInProgressRegistry(Long.valueOf(issueId), user);
		return new IssueWipRegistryDTOAdapter(registry);
	}
	
	@Override
	@Transactional
	public void deleteWorkInProgressRegistry(String registryId, String issueId)
	{
		Long id = Long.valueOf(registryId);
		deleteTimerRegistry(id);
		workService.deleteWorkInProgressRegistry(id);
	}
	
	@Override
	@Transactional
	public void deleteIssueWorkInProgressRegistry(String issueId, String username)
	{
		User user = userManagementService.findUserByUsername(username);
		IssueWipRegistry wipRegistry = workService.findIssueWorkInProgressRegistry(Long.valueOf(issueId), user);
		deleteTimerRegistry(wipRegistry.getId());
		workService.deleteWorkInProgressRegistry(wipRegistry.getId());
	}
	
	/*---------------------------------*
     *       Registres de timer        *
     *---------------------------------*/
	@Override
	@Transactional
	public List<IssueTimerRegistry> findNonPublishedTimerRegistries()
	{
		List<Long> wipRegistryIds = collectIssueWorkInProgressIds();
		// Si no hi han IDs es retorna una llista buida
		if(wipRegistryIds.isEmpty()){
			return Collections.emptyList();
		}
		
		// Data d'avui sense contar les hores
		Date date = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		return timerRegistryDAO.findOldWorkedRegistries(date, wipRegistryIds);
	}
	
	@Override
	@Transactional
	public long countNonPublishedTimerRegistries()
	{
		List<Long> wipRegistryIds = collectIssueWorkInProgressIds();
		// Si no hi han IDs es retorna una llista buida
		if(wipRegistryIds.isEmpty()){
			return 0;
		}
		
		// Data d'avui sense contar les hores
		Date date = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		return timerRegistryDAO.countOldWorkedRegistries(date, wipRegistryIds);
	}
	
	@Override
	@Transactional
	public IssueTimerRegistry findIssueTimerRegistry(String issueId)
	{
		IIssueWipRegistryDTO wipRegistry = findIssueWorkInProgressRegistry(issueId, extendedUserManagementService.getLoggedUsername());
		
		if(wipRegistry!=null){
			return timerRegistryDAO.findRegistryById(Long.valueOf(wipRegistry.getRegistryId()));
		}
		else{
			return null;
		}
	}
	
	@Override
	@Transactional
	public IssueTimerRegistry createTimerRegistry(String issueId)
	{
		IIssueWipRegistryDTO wipRegistry = createWorkInProgressRegistry(issueId, extendedUserManagementService.getLoggedUsername());
		
		IssueTimerRegistry timerRegistry = new IssueTimerRegistry();
		timerRegistry.setId(Long.valueOf(wipRegistry.getRegistryId()));
		timerRegistry.setIssueId(Long.valueOf(issueId));
		timerRegistry.setElapsedTime(0);
		timerRegistry.setWorkingDate(new Date());
		return timerRegistryDAO.createRegistry(timerRegistry);
	}
	
	@Override
	@Transactional
	public IssueTimerRegistry updateTimerRegistry(Long registryId, Integer elapsedTime)
	{
		IssueTimerRegistry registry = timerRegistryDAO.findRegistryById(Long.valueOf(registryId));
		registry.setElapsedTime(elapsedTime);
		return timerRegistryDAO.updateRegistry(registry);
	}
	
	@Override
	@Transactional
	public void deleteNonPublishedTimerRegistries()
	{
		List<IssueTimerRegistry> nonPublishedWorkRegistries = findNonPublishedTimerRegistries();
    	for(IssueTimerRegistry registry:nonPublishedWorkRegistries){
    		timerRegistryDAO.deleteRegistry(registry);
    	}
	}
	
	@Override
	@Transactional
	public void deleteTimerRegistry(Long registryId)
	{
		IssueTimerRegistry registry = timerRegistryDAO.findRegistryById(registryId);
		// Pot ser null si s'ha indicat per treballar però no s'ha iniciat el timer mai
		if(registry!=null){
			timerRegistryDAO.deleteRegistry(registry);
		}
	}
	
	private List<Long> collectIssueWorkInProgressIds()
	{
		List<IssueWipRegistry> wipRegistries = workService.findIssueWorkInProgressRegistriesByUser(extendedUserManagementService.getLoggedUser());
		List<Long> wipIds = new ArrayList<Long>();
		for(IssueWipRegistry elem:wipRegistries){
			wipIds.add(elem.getId());
		}
		
		return wipIds;
	}
}
