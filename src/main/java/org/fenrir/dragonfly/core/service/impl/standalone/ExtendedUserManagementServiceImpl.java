package org.fenrir.dragonfly.core.service.impl.standalone;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import com.google.inject.persist.Transactional;
import org.fenrir.vespine.core.dto.adapter.UserDTOAdapter;
import org.fenrir.vespine.core.entity.User;
import org.fenrir.vespine.core.service.IUserManagementService;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.dragonfly.core.service.IAccessControlService;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;
import org.fenrir.dragonfly.core.service.IUserDataVaultService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140803
 */
public class ExtendedUserManagementServiceImpl implements IExtendedUserFacade 
{
	@Inject
	private IUserManagementService userService;

	@Inject
	private IUserDataVaultService userDataService;
	
	@Inject
	private IAccessControlService accessControlService;
	
	public void setUserDataService(IUserDataVaultService userDataService)
	{
		this.userDataService = userDataService;
	}
	
	public void setUserService(IUserManagementService userService)
	{
		this.userService = userService;
	}
	
	public void setAccessControlService(IAccessControlService accessControlService)
	{
		this.accessControlService = accessControlService;
	}
	
	@Override
	@Transactional
	public List<IUserDTO> findAllActiveUsers()
	{
		List<IUserDTO> userDTOs = new ArrayList<IUserDTO>();
		List<User> users = userService.findAllActiveUsers();
		for(User user:users){
			userDTOs.add(new UserDTOAdapter(user));
		}
		return userDTOs;
	}
	
	@Override
	@Transactional
	public List<IUserDTO> findUsersCompleteNameLike(String name)
	{
		List<IUserDTO> userDTOs = new ArrayList<IUserDTO>();
		List<User> users = userService.findUsersCompleteNameLike(name);
		for(User user:users){
			userDTOs.add(new UserDTOAdapter(user));
		}
		return userDTOs;
	}
	
	@Override
	@Transactional
	public List<IUserDTO> findUsersUsernameLike(String name)
	{
		List<IUserDTO> userDTOs = new ArrayList<IUserDTO>();
		List<User> users = userService.findUsersUsernameLike(name);
		for(User user:users){
			userDTOs.add(new UserDTOAdapter(user));
		}
		return userDTOs;
	}
	
	@Override
	@Transactional
	public IUserDTO findUserByUsername(String username)
	{
		User user = userService.findUserByUsername(username);
		return new UserDTOAdapter(user); 
	}
	
	@Override
	@Transactional
	public String getLoggedUsername()
	{
		return userDataService.getCredential(IUserDataVaultService.KEY_CREDENTIALS_USERNAME);
	}
	
	@Transactional
	@Override
	public User getLoggedUser()
	{
		String username = getLoggedUsername();
		return userService.findUserByUsername(username);
	}
	
	@Transactional
	@Override
	public IUserDTO getLoggedUserAsDTO()
	{
		return new UserDTOAdapter(getLoggedUser());
	}

	@Override
	public boolean checkRole(String role)
	{
		return accessControlService.checkRole(role);
	}
	
	@Override
	public boolean checkPermission(String permission)
	{
		return accessControlService.checkPermission(permission);
	}
}
