package org.fenrir.dragonfly.core.service.impl.standalone;

import javax.inject.Inject;
import org.fenrir.vespine.core.AuditConstants;
import org.fenrir.vespine.core.service.IAuditService;
import org.fenrir.vespine.core.service.ISearchIndexFacade;
import org.fenrir.vespine.core.service.ISearchIndexService;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140609
 */
public class SearchIndexFacadeImpl implements ISearchIndexFacade
{
	@Inject
	private ISearchIndexService indexService;
	
	@Inject
	private IAuditService auditService;
	
	@Inject
	private IExtendedUserFacade userManagementService;
	
	public void setIndexService(ISearchIndexService indexService)
	{
		this.indexService = indexService;
	}
	
	public void setAuditService(IAuditService auditService)
	{
		this.auditService = auditService;
	}
	
	public void setUserManagementService(IExtendedUserFacade userManagementService)
	{
		this.userManagementService = userManagementService;
	}
	
	@Override
	public void createIndex() 
	{
		String loggedUser = userManagementService.getLoggedUsername();
		
		try{
			indexService.createIndex();
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEARCH_INDEX_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_OK, null);
		}
		catch(RuntimeException e){
			// Auditoria
			auditService.auditAction(AuditConstants.ACTION_SEARCH_INDEX_CREATE, null, loggedUser, AuditConstants.RESULT_CODE_ERROR, e.getMessage());
			throw e;
		}
	}

	@Override
	public void enableIndex() 
	{
		indexService.enableIndex();
	}

	@Override
	public void disableIndex() 
	{
		indexService.disableIndex();
	}
}
