package org.fenrir.dragonfly.core.service.impl.standalone;

import org.fenrir.dragonfly.core.service.IAccessControlService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140803
 */
public class StandaloneAccessControlServiceImpl implements IAccessControlService 
{
	@Override
	public boolean checkRole(String role) 
	{
		return true;
	}

	@Override
	public boolean checkPermission(String permission) 
	{
		return true;
	}
}
