package org.fenrir.dragonfly.core.service.impl.standalone;

import javax.inject.Inject;

import com.google.inject.persist.PersistService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.service.ISearchIndexFacade;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.dragonfly.ApplicationConstants;
import org.fenrir.dragonfly.core.CorePreferenceConstants;
import org.fenrir.dragonfly.core.event.IDatabaseEventListener;
import org.fenrir.dragonfly.core.exception.AuthenticationException;
import org.fenrir.dragonfly.core.exception.IncompatibleDatabaseVersionException;
import org.fenrir.dragonfly.core.service.IApplicationModeSetupService;
import org.fenrir.dragonfly.core.service.IDatabaseUpdaterService;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;
import org.fenrir.dragonfly.core.service.IUserDataVaultService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140803
 */
public class StandaloneModeSetupServiceImpl implements IApplicationModeSetupService 
{
	private final Logger log = LoggerFactory.getLogger(StandaloneModeSetupServiceImpl.class);
	
	@Inject
	private IDatabaseUpdaterService databaseUpdaterService;
	
	@Inject
	private PersistService persistenceService;
	
	@Inject
	private IEventNotificationService eventNotificationService;
	
	@Inject
	private IPreferenceService preferenceService;
	
	@Inject
	private IExtendedUserFacade userManagementService;

	public void setDatabaseUpdaterService(IDatabaseUpdaterService databaseUpdaterService)
	{
		this.databaseUpdaterService = databaseUpdaterService;
	}
	
	public void setPersistenceService(PersistService persistenceService)
	{
		this.persistenceService = persistenceService;
	}
	
	public void setEventNotificationService(IEventNotificationService eventNotificationService)
	{
		this.eventNotificationService = eventNotificationService;
	}
	
	public void setPreferenceService(IPreferenceService preferenceService) 
	{
		this.preferenceService = preferenceService;
	}

	public void setUserManagementService(IExtendedUserFacade userManagementService) 
	{
		this.userManagementService = userManagementService;
	}

	@Override
	public void setupApplication() throws ApplicationException
	{
		// Es mira si s'ha d'actualitzar el model de la base de dades
        try{
        	// FIXME Setup mode DEVEL configurable a application.xml
        	databaseUpdaterService.performSchemaUpdate(ApplicationConstants.DATABASE_SCHEMA_CONTEXT_BASE, ApplicationConstants.DATABASE_SCHEMA_CONTEXT_PROD);
        }
        catch(IncompatibleDatabaseVersionException e){
        	log.warn("La versió de la base de dades és incompatible: {}", e.getMessage(), e);
        	throw new ApplicationException("La versió de la base de dades és incompatible: " + e.getMessage(), e);
        }
        
        // S'inicia el mòdul de persistència
        persistenceService.start();
        
        // S'habilita d'índex de cerca
    	try{
    		// Important recuperar la referència del servei de gestió de l'índex DESPRÉS d'iniciar el servei de persistència perquè la inicialització de Compass així ho requereix
    		ISearchIndexFacade indexService = (ISearchIndexFacade)ApplicationContext.getInstance().getRegisteredComponent(ISearchIndexFacade.class);
    		indexService.enableIndex();
    	}
    	catch(Exception e){
    		log.error("Error habilitant l'índex de cerca: {}", e.getMessage(), e);
            throw new ApplicationException("Error habilitant l'índex de cerca: " + e.getMessage(), e);
    	}
    	
    	// Es comunica l'event de conexió amb la base de dades
        try{
            eventNotificationService.notifyNamedEvent(IDatabaseEventListener.class, IDatabaseEventListener.EVENT_DATABASE_CONNECTION_ESTABLISHED_ID);
        }
        catch(Exception e){
            log.error("Error llançant event {}: {}", new Object[]{IDatabaseEventListener.EVENT_DATABASE_CONNECTION_ESTABLISHED_ID, e.getMessage(), e});
        }  
	}
	
	@Override
	public void performDataUpdates() throws ApplicationException
    {
		ISearchIndexFacade indexService = (ISearchIndexFacade)ApplicationContext.getInstance().getRegisteredComponent(ISearchIndexFacade.class);
		
    	// Actualització de la base de dades en cas que calgui actualitzar les dades emmagatzemades                
        databaseUpdaterService.performDataUpdate();
        
        if(log.isDebugEnabled()){
            log.debug("Reindexant contingut");
        }
        
        try{
        	indexService.createIndex();
        }
        catch(BusinessException e){
        	throw new ApplicationException(e.getMessage(), e);
        }
        
        if(log.isDebugEnabled()){
            log.debug("Reindexat finalitzat");
        }
    }
	
	@Override
	public void checkModelCompatibility() throws ApplicationException
	{
		if(!databaseUpdaterService.isDatabaseSchemaCompatible(ApplicationConstants.DATABASE_SCHEMA_CONTEXT_BASE, ApplicationConstants.DATABASE_SCHEMA_CONTEXT_PROD)){
			throw new ApplicationException("La base de dades no es compatible amb la versió actual de l'aplicació. Actualitzi-la");
		}
	}
	
	@Override
	public void scheduleTasks()
	{
		// XXX Es configuren les tasques automàtiques depenents de les dades de la BDD una vegada s'han fet totes les actualitzacions de dades
        // Si s'han configurat els projectes a gestionar, s'inicia las tasca automàtica
//        ICoreAdministrationService administrationService = (ICoreAdministrationService)applicationContext.getRegisteredComponent(ICoreAdministrationService.class);
//        ITaskLauncherService taskLauncherService = (ITaskLauncherService)applicationContext.getRegisteredComponent(ITaskLauncherService.class);
//        if(!administrationService.isInitialUpdateNeeded()){            
//            taskLauncherService.scheduleTask(IssueCheckTask.ID);
//        }
	}

	@Override
	public void loginUser(String username, String password) throws AuthenticationException, ApplicationException
	{
		try{
			if(userManagementService.findUserByUsername(username)!=null){
				preferenceService.setProperty(CorePreferenceConstants.APPLICATION_DEFAULT_USER, username);
				preferenceService.save(IPreferenceService.PREFERENCES_CONFIGURATION_ID);
				
				IUserDataVaultService userDataService = (IUserDataVaultService)ApplicationContext.getInstance().getRegisteredComponent(IUserDataVaultService.class);
				userDataService.setUserCredentials(username, password);
			}
			else{
				throw new AuthenticationException("L'usuari introduit no existeix");
			}
		}
		catch(AuthenticationException e){
			throw e;
		}
		catch(Exception e){
			throw new ApplicationException("S'ha produit un error en el procés d'autenticació", e);
		}
	}
}
