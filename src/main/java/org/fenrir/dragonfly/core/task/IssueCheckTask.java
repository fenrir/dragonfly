package org.fenrir.dragonfly.core.task;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.IApplicationNotification;
import org.fenrir.yggdrasil.core.event.IApplicationNotificationListener;
import org.fenrir.yggdrasil.core.event.NotificationEventConstants;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.vespine.core.provider.IssueContentProvider;
import org.fenrir.dragonfly.core.event.CoreEventConstants;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.service.IIssueDataService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20131202
 */
public class IssueCheckTask implements Runnable
{
    public static final String ID = "org.fenrir.dragonfly.core.task.issueCheckTask";
    
    private final Logger log = LoggerFactory.getLogger(IssueCheckTask.class);
    
    @Inject
    private IPreferenceService preferenceService;

    @Inject
    private IssueContentProvider issueContentProvider;
    
//    @Inject
//    private IIssueSearchService issueSearchService;
//
//    @Inject
//    private IIssueAdministrationService issueAdministrationService;

    @Inject
    private IEventNotificationService eventNotificationService;
    
    public void setPreferenceService(IPreferenceService preferenceService)
    {
        this.preferenceService = preferenceService;
    }

    public void setIssueContentProvider(IssueContentProvider issueContentProvider)
    {
        this.issueContentProvider = issueContentProvider;
    }
    
//    public void setIssueSearchService(IIssueSearchService issueSearchService)
//    {
//        this.issueSearchService = issueSearchService;
//    }
//
//    public void setIssueAdministrationService(IIssueAdministrationService issueAdministrationService)
//    {
//        this.issueAdministrationService = issueAdministrationService;
//    }

    public void setEventNotificationService(IEventNotificationService eventNotificationService)
    {
        this.eventNotificationService = eventNotificationService;
    }

    @Override
    public void run()
    {
        if(log.isDebugEnabled()){
            log.debug("Iniciada tasca de comprobació del mantis");
        }

        Long initialTime = System.currentTimeMillis();

//        IIssueDataService issueProviderService = issueContentProvider.getIssueDataService(MantisConstants.ISSUE_PROVIDER_MANTIS);
//        
//        try{
//            int managedIssues = 0;
//            int ignoredIssues = 0;
//            int deletedIssues = 0;
//            // Es recuperen els projectes gestionats
//            List<IssueProject> managedProjects = issueSearchService.findAllProjects();
//            final List<Long> updatedIssues = new ArrayList<Long>();
//            for(IssueProject project:managedProjects){
//                // Es comproba l'hora de l'última comprobació sobre les incidències del projecte
//                AbstractIssue lastModifiedIssue = issueSearchService.findLastModifiedIssueByProject(project.getId());
//                // Es busquen les incidències que han canviat al projecte                
//                try{                    
//                    int page = 1;
//                    boolean finished = false;
//                    while(!finished){
//                        List<IIssueDTO> issues;
//                        // TODO Parametritzar límit pàgina
//                        if(lastModifiedIssue!=null){
//                            if(log.isDebugEnabled()){
//                                log.debug("Comprovant incidències posteriors a {}", lastModifiedIssue.getModifiedDate());
//                            }
//                            
//                            issues = issueProviderService.findProjectIssues(project.getId(), lastModifiedIssue.getModifiedDate(), page++, 10);
//                        }
//                        else{
//                            issues = issueProviderService.findProjectIssues(project.getId(), page++, 10);
//                        }
//
//                        if(issues.isEmpty()){
//                            finished = true;
//                        }
//                        else{
//                            for(IIssueDTO issue:issues){
//                                int result = issueAdministrationService.manageIssue(issue);
//                                if(result==IIssueAdministrationService.ISSUE_CREATED_OR_UPDATED){
//                                    // S'afegeix a la llista d'incidències a mostrar en el filtre de la notificació
//                                    updatedIssues.add(Long.parseLong(issue.getProviderId()));
//                                    managedIssues++;
//                                }
//                                else if(result==IIssueAdministrationService.ISSUE_DELETED){
//                                    deletedIssues++;
//                                }
//                                else if(result==IIssueAdministrationService.ISSUE_IGNORED){
//                                    ignoredIssues++;
//                                }
//                            }                            
//                        }
//                    }
//
//                    if(log.isDebugEnabled()){
//                       log.debug("Gestionades {} incidències del projecte {}", managedIssues, project.getName());
//                    }
//                }
//                // Si es produeix un error es continua amb el següent projecte
//                catch(BusinessException e){
//                    log.error("Error al obtenir les incidències del projecte {}: {}", new Object[]{project.getId(), e.getMessage(), e});
//                    
//                    IApplicationNotification notification = (IApplicationNotification)ApplicationContext.getInstance().getRegisteredComponent(IApplicationNotification.class, NotificationEventConstants.NOTIFICATION_ERROR);
//                    notification.setMessage("Error a l'execució de la tasca d'actualització de les incidències del projecte " + project.getName());
//                    notification.setParameter(NotificationEventConstants.NOTIFICATION_PARAMETER_ERROR_DESCRIPTION, "Error a l'execució de la tasca d'actualització de les incidènciesdel projecte " + project.getName());
//                    notification.setParameter(NotificationEventConstants.NOTIFICATION_PARAMETER_ERROR_THROWABLE, e);
//                    eventNotificationService.notifyNamedEvent(IApplicationNotificationListener.class, 
//                            IApplicationNotificationListener.EVENT_SINGLE_NOTIFICATION_ID, notification);
//                }
//            }
//
//            if(log.isDebugEnabled()){
//                log.debug("Finalitzada tasca de comprobació del mantis en {} seg.", (System.currentTimeMillis() - initialTime)/1000);
//            }
//            
//            if(managedIssues + deletedIssues > 0){
//                // Es llança l'event de refresc
//                if(log.isDebugEnabled()){
//                    log.debug("Llançant refresc");
//                }
//                eventNotificationService.notifyNamedEvent(IIssueListener.class, IIssueListener.EVENT_ISSUES_UPDATED_ID);
//                
//                // Es mostra l'avís si cal
//                String msg = MessageFormat.format("Resultat de l''actualització.\n"
//                        + "{0,number,integer} Actualitzades; {1,number,integer} Esborrades; {2,number,integer} Ignorades", 
//                        new Object[]{managedIssues, deletedIssues, ignoredIssues});                
//                try{
//                    IApplicationNotification notification = (IApplicationNotification)ApplicationContext.getInstance().getRegisteredComponent(IApplicationNotification.class, CoreEventConstants.NOTIFICATION_ISSUES_UPDATED);
//                    notification.setMessage(msg);
//                    notification.setParameter(CoreEventConstants.NOTIFICATION_PARAMETER_ISSUES, updatedIssues);
//                    eventNotificationService.notifyNamedEvent(IApplicationNotificationListener.class, 
//                            IApplicationNotificationListener.EVENT_SINGLE_NOTIFICATION_ID, notification);
//                }
//                catch(Exception e){
//                    log.error("Error reportant notificació: {}", e.getMessage(), e);                                                            
//                }
//            }
//        }
//        // Catch general per no parar la tasca automàtica si es produeix un error
//        catch(Exception e){
//            log.error("Error a la tasca de comprobació de mantis: {}", e.getMessage(), e);
//        }
    }
}
