package org.fenrir.dragonfly.ui;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.action.SwitchWorkspaceAction;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;
import org.fenrir.dragonfly.core.event.ILoginEventListener;
import org.fenrir.dragonfly.core.service.IAccessControlService;
import org.fenrir.dragonfly.ui.action.CustomFieldManagementAction;
import org.fenrir.dragonfly.ui.action.DictionaryManagementAction;
import org.fenrir.dragonfly.ui.action.IssueCategoryManagementAction;
import org.fenrir.dragonfly.ui.action.IssueManagementAction;
import org.fenrir.dragonfly.ui.action.IssueProjectManagementAction;
import org.fenrir.dragonfly.ui.action.IssueSeverityManagementAction;
import org.fenrir.dragonfly.ui.action.IssueStatusManagementAction;
import org.fenrir.dragonfly.ui.action.SprintManagementAction;
import org.fenrir.dragonfly.ui.action.IssueSearchAction;
import org.fenrir.dragonfly.ui.action.RegenerateIndexAction;
import org.fenrir.dragonfly.ui.action.TagManagementAction;
import org.fenrir.dragonfly.ui.action.IssueViewFilterManagementAction;
import org.fenrir.dragonfly.ui.action.AlertTypeManagementAction;
import org.fenrir.dragonfly.ui.action.ReportPrintAction;
import org.fenrir.dragonfly.ui.action.AboutAction;
import org.fenrir.dragonfly.ui.action.WorkflowManagementAction;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140803
 */
@EventListener(definitions=ILoginEventListener.class)
@SuppressWarnings("serial")
public class MenuBar extends JMenuBar implements ILoginEventListener
{		
    /*----------*
     * ATRIBUTS *
     *----------*/
    private Logger log = LoggerFactory.getLogger(MenuBar.class);
    
    private JMenu mSearch;
    private JMenuItem iRegenerateIndex;
    
    private JMenu mIssues;
    private JMenuItem iCreateIssue;
    private JMenuItem iManageProjects;
    private JMenuItem iManageStatus;
    private JMenuItem iManageCategories;
    private JMenuItem iManageSeverities;
    private JMenuItem iManageWorkflow;
    private JMenuItem iManageDictionaries;
    private JMenuItem iManageCustomFields;
    private JMenuItem iManageTags;
    private JMenuItem iManageAlerts;		
    
    private JMenu mViews;
    private JMenuItem iManageViewFilters;
    
    private JMenu mPlanning;
    private JMenuItem iManageSprints;
    
    private JMenu mReport;
        
    public MenuBar()
    {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        IWorkspaceAdministrationService workspaceAdministrationService = (IWorkspaceAdministrationService)applicationContext.getRegisteredComponent(IWorkspaceAdministrationService.class);
        
        /* Menú File */
        JMenu mFile = new JMenu("Fitxer");
        // Gestió del workspace
        JMenu mWorkspaceAdministration = new JMenu("Canviar workspace");
        try{
            List<String> workspaces = workspaceAdministrationService.getWorkspacesList();
            String currentWorkspace = workspaceAdministrationService.getCurrentWorkspaceFolder();
            // S'eliminen el workspace actual de la llista de candidats
            if(StringUtils.isNotBlank(currentWorkspace)){
                Iterator<String> iterator = workspaces.iterator();
                boolean found = false;
                while(iterator.hasNext() && !found){
                    String folder = iterator.next();
                    if(FilenameUtils.equalsNormalizedOnSystem(currentWorkspace, folder)){
                        iterator.remove();
                        found = true;
                    }
                }                
            }
            // Es carrega la llista de workspaces configurats
            for(String workspaceFolder:workspaces){
                JMenuItem item = new JMenuItem(new SwitchWorkspaceAction(StringUtils.abbreviateMiddle(workspaceFolder, "...", 40), workspaceFolder));
                mWorkspaceAdministration.add(item);
            }
            if(!workspaces.isEmpty()){
                mWorkspaceAdministration.addSeparator();
            }
        }
        catch(ApplicationException e){
            log.error("Error carregant la llista de workspaces diponibles: {}", e.getMessage(), e);
        }
        
        JMenuItem iSwitchWorkspace = new JMenuItem();
        iSwitchWorkspace.setAction(new SwitchWorkspaceAction());
        mWorkspaceAdministration.add(iSwitchWorkspace);
        mFile.add(mWorkspaceAdministration);        
        // Acció Sortir
        mFile.addSeparator();
        JMenuItem iExit = new JMenuItem(new AbstractAction("Sortir") 
        {        	
            @Override
            public void actionPerformed(ActionEvent arg0)
            {
                // TODO v0.2 Mostrar advertència al sortir
                ApplicationWindowManager.getInstance().closeWindow();
            }
        });
        mFile.add(iExit);
        this.add(mFile);                

        /* Menú buscar */
        mSearch = new JMenu("Buscar");
        // Buscar incidencia per IssueId
        JMenuItem iSearchIssueId = new JMenuItem(new IssueSearchAction(IssueSearchAction.MODE_ID_SEARCH));
        mSearch.add(iSearchIssueId);
        // Buscar incidencia per descripció
        JMenuItem iSearchIssue = new JMenuItem(new IssueSearchAction(IssueSearchAction.MODE_GLOBAL_SEARCH));
        mSearch.add(iSearchIssue);
        mSearch.addSeparator();
        // Regenerar índex
        iRegenerateIndex = new JMenuItem(new RegenerateIndexAction());
        mSearch.add(iRegenerateIndex);
        this.add(mSearch);
        
        /* Menú incidències */
        mIssues = new JMenu("Incidències");
        iCreateIssue = new JMenuItem(new IssueManagementAction());
        mIssues.add(iCreateIssue);
        mIssues.addSeparator();
        // Projectes
        iManageProjects = new JMenuItem(new IssueProjectManagementAction());
        mIssues.add(iManageProjects);
        // Estats
        iManageStatus = new JMenuItem(new IssueStatusManagementAction());
        mIssues.add(iManageStatus);
        // Categories
        iManageCategories = new JMenuItem(new IssueCategoryManagementAction());
        mIssues.add(iManageCategories);
        // Severitats
        iManageSeverities = new JMenuItem(new IssueSeverityManagementAction());
        mIssues.add(iManageSeverities);
        // Workflow
        iManageWorkflow = new JMenuItem(new WorkflowManagementAction());
        mIssues.add(iManageWorkflow);
        mIssues.addSeparator();
        // Gestió de diccionaris de dades
        iManageDictionaries = new JMenuItem(new DictionaryManagementAction());
        mIssues.add(iManageDictionaries);
        // Camps custom
        iManageCustomFields = new JMenuItem(new CustomFieldManagementAction());
        mIssues.add(iManageCustomFields);
        // Gestió de tags
        iManageTags = new JMenuItem(new TagManagementAction());
        mIssues.add(iManageTags);        
        // Gestió d'alertes
        iManageAlerts = new JMenuItem(new AlertTypeManagementAction());
        mIssues.add(iManageAlerts);
        this.add(mIssues);

        /* Menú planificació */
        mPlanning = new JMenu("Planificació");
        // Sprints
        iManageSprints = new JMenuItem(new SprintManagementAction());
        mPlanning.add(iManageSprints);
        this.add(mPlanning);
        
        /* Menú vistes */
        mViews = new JMenu("Vistes");
        // Gestió de filtres
        iManageViewFilters = new JMenuItem(new IssueViewFilterManagementAction());
        mViews.add(iManageViewFilters);
        this.add(mViews);
        
        /* Menú Informe */
        // TODO 
//        mReport = new JMenu("Informes");
//        // Sprint SLA
//        JMenuItem iSprintSLA = new JMenuItem(new ReportPrintAction("Sprint SLA", "/org/fenrir/dragonfly/report/sla.jrxml"));
//        mReport.add(iSprintSLA);
//        this.add(mReport);
        
        /* Menú Ajuda */
        JMenu mHelp = new JMenu("Ajuda");
        // "About"
        JMenuItem iAbout = new JMenuItem(new AboutAction());
        mHelp.add(iAbout);
        this.add(mHelp);
        
        // Es deshabiliten els menús fins rebre l'event de login
        logoutPerformed();
    }

    @Override
    public final void loginPerformed()
    {
    	IAccessControlService accessControlService = (IAccessControlService)ApplicationContext.getInstance().getRegisteredComponent(IAccessControlService.class);

        boolean systemRole =  accessControlService.checkRole(SecurityConstants.AUTHORIZATION_ROLE_SYSTEM);
        boolean administratorRole = accessControlService.checkRole(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR);
        boolean reporterRole = accessControlService.checkRole(SecurityConstants.AUTHORIZATION_ROLE_REPORTER);
        boolean observerRole = accessControlService.checkRole(SecurityConstants.AUTHORIZATION_ROLE_OBSERVER);
        
        mSearch.setEnabled(true);
    	iRegenerateIndex.setVisible(systemRole);

    	mIssues.setEnabled(true);
    	mIssues.setVisible(administratorRole || reporterRole);
        iCreateIssue.setVisible(reporterRole);
        iManageProjects.setVisible(administratorRole);
        iManageStatus.setVisible(administratorRole);
        iManageCategories.setVisible(administratorRole);
        iManageSeverities.setVisible(administratorRole);
        iManageWorkflow.setVisible(administratorRole);
        iManageDictionaries.setVisible(administratorRole);
        iManageCustomFields.setVisible(administratorRole);
        iManageTags.setVisible(administratorRole);
        iManageAlerts.setVisible(administratorRole);
        
        mPlanning.setEnabled(true);
        mPlanning.setVisible(administratorRole);
//        iManageSprints.setVisible(administratorRole);
        
        mViews.setEnabled(true);
        mViews.setVisible(administratorRole);
//        iManageViewFilters.setVisible(administratorRole);
        
//      mReport.setEnabled(true);
    }
	
    @Override
    public final void logoutPerformed()
    {
        mSearch.setEnabled(false);
        mIssues.setEnabled(false);
        mPlanning.setEnabled(false);
        mViews.setEnabled(false);
//        mReport.setEnabled(false);
    }

	@Override
	public void loginAborted() 
	{
		
	}
}