package org.fenrir.dragonfly.ui;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.0.20121226
 */
public class UIPreferenceConstants 
{
    public static final String UI_ISSUE_LIST_PAINT_STATUS_COLOR = "//preferences/ui/issueList/paintStatusColor";
    public static final String UI_ISSUE_LIST_PAGE_SIZE = "//preferences/ui/issueList/pageSize";
}