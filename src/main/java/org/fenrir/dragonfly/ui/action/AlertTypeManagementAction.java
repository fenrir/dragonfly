package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.dragonfly.core.event.IAlertListener;
import org.fenrir.dragonfly.ui.dialog.AlertTypeManagementDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140822
 */
public class AlertTypeManagementAction extends AbstractAction
{
    private static final long serialVersionUID = 1L;

    private final Logger log = LoggerFactory.getLogger(AlertTypeManagementAction.class);

    public AlertTypeManagementAction()
    {
        super("Administrar tipus d'alerta");
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	AlertTypeManagementDialog dialog = null;
        try{
            dialog = new AlertTypeManagementDialog();
            eventNotificationService.addListener(IAlertListener.class, dialog);
            dialog.open();            
        }
        catch(Exception e){
            log.error("Error al obrir finestra de gestió d'alertes: {}", e.getMessage(), e);
        }
        finally{
        	if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
        }
    }
}
