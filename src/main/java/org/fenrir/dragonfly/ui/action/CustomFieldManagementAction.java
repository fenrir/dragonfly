package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.dragonfly.core.event.ICustomFieldListener;
import org.fenrir.dragonfly.ui.dialog.CustomFieldManagementDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140818
 */
public class CustomFieldManagementAction extends AbstractAction 
{
	private static final long serialVersionUID = -7780904475869589620L;
	
	private final Logger log = LoggerFactory.getLogger(SprintManagementAction.class);

    public CustomFieldManagementAction()
    {
        super("Administrar camps personalitzats");
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	CustomFieldManagementDialog dialog = null; 
        try{
			dialog = new CustomFieldManagementDialog();
			eventNotificationService.addListener(ICustomFieldListener.class, dialog);
            dialog.open();            
        }
        catch(Exception e){
            log.error("Error al obrir finestra de gestió de camps personalitzats: {}", e.getMessage(), e);
        }
        finally{
        	if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
        }
    }
}
