package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.dragonfly.ui.dialog.DictionaryManagementDialog;
import org.fenrir.dragonfly.core.event.IDictionaryListener;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140817
 */
public class DictionaryManagementAction extends AbstractAction 
{
	private static final long serialVersionUID = -1034188058223367424L;
	
	private final Logger log = LoggerFactory.getLogger(SprintManagementAction.class);

    public DictionaryManagementAction()
    {
        super("Administrar diccionaris");
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	DictionaryManagementDialog dialog = null;
        try{
        	dialog = new DictionaryManagementDialog();
        	eventNotificationService.addListener(IDictionaryListener.class, dialog);
            dialog.open();            
        }
        catch(Exception e){
            log.error("Error al obrir finestra de gestió de diccionaris: {}", e.getMessage(), e);
        }
        finally{
        	if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
        }
    }
}
