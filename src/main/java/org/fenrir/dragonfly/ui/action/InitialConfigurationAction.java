package org.fenrir.dragonfly.ui.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.action.Action;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.PreferenceException;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.dragonfly.ApplicationConstants;
import org.fenrir.dragonfly.core.CorePreferenceConstants;
import org.fenrir.dragonfly.ui.dialog.InitialConfigurationDialog;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionlitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140723
 */
@SuppressWarnings("serial")
public class InitialConfigurationAction extends Action 
{
	private final Logger log = LoggerFactory.getLogger(InitialConfigurationAction.class);
	
	@Override
	public void actionPerformed()
	{
		InitialConfigurationDialog dialog = new InitialConfigurationDialog();
		if(dialog.open()==InitialConfigurationDialog.DIALOG_OK){
			IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
			
			String applicationMode = dialog.getApplicationMode();
			preferenceService.setProperty(CorePreferenceConstants.APPLICATION_MODE, applicationMode);
			if(ApplicationConstants.APPLICATION_MODE_CLIENT.equals(applicationMode)){
				String serverUrl = dialog.getServerUrl();
				// FIXME S'ha de millorar la API del servei de gestió de preferències per tal de fes més fàcil la creació de preferències
				preferenceService.addProperty("//preferences/application server", "");
				preferenceService.addProperty(CorePreferenceConstants.APPLICATION_SERVER + " url", serverUrl);
			}
			
			try{
				preferenceService.clearProperty(CorePreferenceConstants.INITIAL_CONFIGURATION_DONE);
				preferenceService.save(IPreferenceService.PREFERENCES_CONFIGURATION_ID);
			}
			catch(PreferenceException e){
				log.error("Error guardant preferències inicials");
				ApplicationWindowManager.getInstance().displayErrorMessage("S'ha produit un error guardant les preferències inicials de l'aplicació", e);
			}
		}
	}
}