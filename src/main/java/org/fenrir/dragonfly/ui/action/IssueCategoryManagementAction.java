package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.dragonfly.ui.dialog.IssueCategoryManagementDialog;
import org.fenrir.dragonfly.core.event.ICategoryListener;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140815
 */
public class IssueCategoryManagementAction extends AbstractAction
{
    private static final long serialVersionUID = 1L;

    private final Logger log = LoggerFactory.getLogger(IssueCategoryManagementAction.class);

    public IssueCategoryManagementAction()
    {
        super("Administrar categories");
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	IssueCategoryManagementDialog dialog = null;
        try{
        	dialog = new IssueCategoryManagementDialog();
        	eventNotificationService.addListener(ICategoryListener.class, dialog);
            dialog.open();            
        }
        catch(Exception e){
            log.error("Error al obrir finestra de gestió de categories: {}", e.getMessage(), e);
        }
        finally{
        	if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
        }
    }
}
