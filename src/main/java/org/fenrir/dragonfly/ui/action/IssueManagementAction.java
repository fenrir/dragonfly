package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.yggdrasil.core.ApplicationContext;


/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20131212
 */
public class IssueManagementAction extends AbstractAction 
{
	private static final long serialVersionUID = -8033302181118839921L;
	
    public IssueManagementAction()
    {
        super("Crear incidencia");
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
    	ApplicationController controller = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);
    	controller.openNewIssueView();
    }
}
