package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.dragonfly.core.event.IProjectListener;
import org.fenrir.dragonfly.ui.dialog.IssueProjectManagementDialog;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140814
 */
public class IssueProjectManagementAction extends AbstractAction
{
    private static final long serialVersionUID = 1L;

    private final Logger log = LoggerFactory.getLogger(IssueStatusManagementAction.class);

    public IssueProjectManagementAction()
    {
        super("Administrar projectes");
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	IssueProjectManagementDialog dialog = null;
        try{
        	dialog = new IssueProjectManagementDialog();
            eventNotificationService.addListener(IProjectListener.class, dialog);
            dialog.open();
        }
        catch(Exception e){
            log.error("Error al obrir finestra de gestió de projectes: {}", e.getMessage(), e);
        }
        finally{
        	if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
        }
    }
}