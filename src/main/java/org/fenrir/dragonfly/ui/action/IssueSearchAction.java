package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.KeyStroke;
import org.fenrir.dragonfly.ui.dialog.IssueIdSearchDialog;
import org.fenrir.dragonfly.ui.dialog.IssueSearchDialog;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.FramelessDialog;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.1.20130706
 */
public class IssueSearchAction extends AbstractAction
{
    public static final int MODE_ID_SEARCH = 1;
    public static final int MODE_GLOBAL_SEARCH = 2;
    public static final int MODE_VIEW_SEARCH = 3;
    
    private int mode;
    
    public IssueSearchAction(int mode)
    {
        super("Buscar incidencia per ID");
        
        this.mode = mode;        
        if(mode==MODE_ID_SEARCH){
            putValue(NAME, "Buscar incidencia per ID");
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
        }                
        else if(mode==MODE_GLOBAL_SEARCH){
            putValue(NAME, "Buscar incidencia");
            putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
        }
        else{
            throw new IllegalArgumentException("Mode no permès");
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
        FramelessDialog dialog = null;
        if(mode==MODE_ID_SEARCH){
            dialog = new IssueIdSearchDialog(ApplicationWindowManager.getInstance().getMainWindow());
            dialog.open();        
        }
        else if(mode==MODE_GLOBAL_SEARCH){
            dialog = new IssueSearchDialog(ApplicationWindowManager.getInstance().getMainWindow());            
        }
        dialog.open();
    }
}
