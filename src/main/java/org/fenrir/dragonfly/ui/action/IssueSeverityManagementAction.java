package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.dragonfly.core.event.ISeverityListener;
import org.fenrir.dragonfly.ui.dialog.IssueSeverityManagementDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140816
 */
public class IssueSeverityManagementAction extends AbstractAction
{
    private static final long serialVersionUID = 1L;

    private final Logger log = LoggerFactory.getLogger(IssueSeverityManagementAction.class);

    public IssueSeverityManagementAction()
    {
        super("Administrar severitats");
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	IssueSeverityManagementDialog dialog = null;
        try{
        	dialog = new IssueSeverityManagementDialog();
        	eventNotificationService.addListener(ISeverityListener.class, dialog);
            dialog.open();            
        }
        catch(Exception e){
            log.error("Error al obrir finestra de gestió de severitats: {}", e.getMessage(), e);
        }
        finally{
        	if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
        }
    }
}
