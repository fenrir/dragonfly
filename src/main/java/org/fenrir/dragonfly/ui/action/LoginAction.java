package org.fenrir.dragonfly.ui.action;

import javax.swing.JOptionPane;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.action.Action;
import org.fenrir.yggdrasil.ui.dialog.ErrorDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.dragonfly.ApplicationConstants;
import org.fenrir.dragonfly.ui.dialog.LoginDialog;
import org.fenrir.dragonfly.core.CorePreferenceConstants;
import org.fenrir.dragonfly.core.event.ILoginEventListener;
import org.fenrir.dragonfly.core.exception.AuthenticationException;
import org.fenrir.dragonfly.core.service.IApplicationModeSetupService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionlitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140722
 */
@SuppressWarnings("serial")
public class LoginAction extends Action 
{
	private Logger log = LoggerFactory.getLogger(LoginAction.class);
	
	@Override
	public void actionPerformed() 
	{
		IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);
		IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
		IApplicationModeSetupService applicationSetupService = (IApplicationModeSetupService)ApplicationContext.getInstance().getRegisteredComponent(IApplicationModeSetupService.class);
		
		String applicationMode = preferenceService.getProperty(CorePreferenceConstants.APPLICATION_MODE);
		String user = preferenceService.getProperty(CorePreferenceConstants.APPLICATION_DEFAULT_USER);
		if(!ApplicationConstants.APPLICATION_MODE_CLIENT.equals(applicationMode) && StringUtils.isNotBlank(user)){
			try{
				log.debug("Aplicació en mode {} amb usuari configurat a les preferències; S'obvia el procés de login", applicationMode);
				
				applicationSetupService.loginUser(user, null);
				eventNotificationService.notifyNamedEvent(ILoginEventListener.class, ILoginEventListener.EVENT_LOGIN_ID);
			}
			catch(Exception e){
				log.error("Procés de login - Error en el procés d'autenticació: {}", e.getMessage(), e);
			}
			
			return;
		}
		
		boolean repeat = true;
		while(repeat){
			// Password només requerit en el mode "CLIENT"
			LoginDialog dialog = new LoginDialog(user, ApplicationConstants.APPLICATION_MODE_CLIENT.equals(applicationMode));
			if(dialog.open()==LoginDialog.DIALOG_OK){				
				user = dialog.getUsername();
				char[] password = dialog.getPassword();
				try{
					applicationSetupService.loginUser(user, String.valueOf(password));
					repeat = false;
				}
				catch(AuthenticationException e){
					log.error("Procés de login - Usuari inexistent: {}", new Object[]{user, e.getMessage(), e});
					JOptionPane.showMessageDialog(null, "L'usuari introduit no existeix", "Error", JOptionPane.ERROR_MESSAGE);
				}
				catch(Exception e){
					log.error("Procés de login - Error en el procés d'autenticació: {}", e.getMessage(), e);
					new ErrorDialog(null, "S'ha produit un error en el procés d'autenticació", e).open();
				}
				
				if(!repeat){
					try{
						eventNotificationService.notifyNamedEvent(ILoginEventListener.class, ILoginEventListener.EVENT_LOGIN_ID);
					}
					catch(Exception e){
						log.error("Procés de login - Error notificant event de login OK: {}", e.getMessage(), e);
						new ErrorDialog(null, e.getMessage(), e).open();
					}
				}
			}
			else{
				JOptionPane.showMessageDialog(null, "Procés d'autenticació abortat", "Error", JOptionPane.ERROR_MESSAGE);
				try{
					eventNotificationService.notifyNamedEvent(ILoginEventListener.class, ILoginEventListener.EVENT_LOGIN_ABORTED_ID);
				}
				catch(Exception e){
					log.error("Procés de login - Error notificant event de login abortat: {}", e.getMessage(), e);
				}
				
				repeat = false;
			}
		}
	}
}
