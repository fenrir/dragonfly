package org.fenrir.dragonfly.ui.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.action.Action;
import org.fenrir.yggdrasil.ui.dialog.TaskProgressDialog;
import org.fenrir.yggdrasil.ui.worker.AbstractWorker;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.service.ISearchIndexFacade;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140510
 */
public class RegenerateIndexAction extends Action
{
    private final Logger log = LoggerFactory.getLogger(RegenerateIndexAction.class);
    
    public RegenerateIndexAction()
    {
        super("Regenerar índex");
    }
    
    @Override
    public void actionPerformed() 
    {
        AbstractWorker<Void, String> worker = new AbstractWorker<Void, String>() 
        {
            @Override
            protected Void doInBackground() throws Exception 
            {  
                ISearchIndexFacade indexService = (ISearchIndexFacade)ApplicationContext.getInstance().getRegisteredComponent(ISearchIndexFacade.class);
                indexService.createIndex();
                
                return null;
            }
        };
        
        TaskProgressDialog dialog = null;
        try{
            dialog = new TaskProgressDialog("Regenerant índex", worker);
            dialog.open();
        }
        catch(Exception e){
            log.error("Error actualitzant les dades gestionades per l'aplicació: {}", e.getMessage(), e);
            // Primes es tanca la finestra de progrés en cas que s'hagi quedat oberta
            if(dialog!=null && dialog.isVisible()){
                dialog.setVisible(false);
                dialog.dispose();
            }
            ApplicationWindowManager.getInstance().displayErrorMessage("Error actualitzant les dades gestionades per l'aplicació", e);
        }
    }    
}
