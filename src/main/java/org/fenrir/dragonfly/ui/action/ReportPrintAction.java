package org.fenrir.dragonfly.ui.action;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.dragonfly.ui.wizard.ReportPrintWizard;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.WizardDialog;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.0.20121104
 */
public class ReportPrintAction extends AbstractAction
{
    private static final long serialVersionUID = 1L;

    private final Logger log = LoggerFactory.getLogger(ReportPrintAction.class);

    private String reportPath;

    public ReportPrintAction(String name, String reportPath)
    {
        super(name);

        this.reportPath = reportPath;
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
        try{
            ReportPrintWizard wizard = new ReportPrintWizard(reportPath);
            new WizardDialog<ReportPrintWizard>(ApplicationWindowManager.getInstance().getMainWindow(),
                    "Impresió d'informes",
                    new Dimension(500, 450),
                    wizard).open();
        }
        catch(Exception e){
            log.error("Error al crear wizard: {}", e.getMessage(), e);
            JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error al crear wizard: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
