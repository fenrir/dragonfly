package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.dragonfly.ui.dialog.SprintManagementDialog;
import org.fenrir.dragonfly.core.event.ISprintListener;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140818
 */
public class SprintManagementAction extends AbstractAction
{
	private static final long serialVersionUID = 3082160073994564198L;
	
	private final Logger log = LoggerFactory.getLogger(SprintManagementAction.class);

    public SprintManagementAction()
    {
        super("Administrar sprints");
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	SprintManagementDialog dialog = null;
        try{
        	dialog = new SprintManagementDialog();
        	eventNotificationService.addListener(ISprintListener.class, dialog);
            dialog.open();            
        }
        catch(Exception e){
            log.error("Error al obrir finestra de gestió d'sprints: {}", e.getMessage(), e);
        }
        finally{
        	if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
        }
    }
}