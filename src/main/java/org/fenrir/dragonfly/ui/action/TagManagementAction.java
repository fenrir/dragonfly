package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.dragonfly.ui.dialog.TagManagementDialog;
import org.fenrir.dragonfly.core.event.ITagListener;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140825
 */
public class TagManagementAction extends AbstractAction 
{
    private static final long serialVersionUID = 1L;
	
    private final Logger log = LoggerFactory.getLogger(TagManagementAction.class);
	
    public TagManagementAction()
    {
        super("Administrar Tags");
    }
	
    @Override
    public void actionPerformed(ActionEvent event) 
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	TagManagementDialog dialog = null;
        try{
            dialog = new TagManagementDialog();
            // Listener per modificacions externes
            eventNotificationService.addListener(ITagListener.class, dialog);
            dialog.open();
			
        }
        catch(Exception e){
            log.error("Error al obrir finestra de gestió de tags: {}", e.getMessage(), e);
        }
        finally{
        	if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
        }
        
        try{
        	/* Una vegada tancada la finestra de gestió de tags, es llança l'event per tal que es recarreguin a on faci falta
        	 * Aquesta notificació és interna i és addicional a la que es realitza des de l'exterior a través d'un missatge broadcast
        	 */
        	eventNotificationService.notifyNamedEvent(ITagListener.class, ITagListener.EVENT_TAG_LIST_UPDATED);
        }
        catch(Exception e){
        	log.error("Error notificant canvi a la llista de tags: {}", e.getMessage(), e);
        }
    }
}