package org.fenrir.dragonfly.ui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.dragonfly.core.event.IProjectListener;
import org.fenrir.dragonfly.ui.dialog.WorkflowManagementDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20131210
 */
public class WorkflowManagementAction extends AbstractAction
{
	private static final long serialVersionUID = -249890934392865967L;
	
	private final Logger log = LoggerFactory.getLogger(IssueStatusManagementAction.class);

    public WorkflowManagementAction()
    {
        super("Administrar fluxes de treball");
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	WorkflowManagementDialog dialog = null;
        try{
    		dialog = new WorkflowManagementDialog();
    		eventNotificationService.addListener(IProjectListener.class, dialog);
            dialog.open();            
        }
        catch(Exception e){
            log.error("Error al obrir finestra de gestió del fluxe de treball: {}", e.getMessage(), e);
        }
        finally{
        	if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
        }
    }
}
