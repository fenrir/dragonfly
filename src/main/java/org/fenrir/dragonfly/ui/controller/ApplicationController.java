package org.fenrir.dragonfly.ui.controller;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;
import java.beans.PropertyChangeEvent;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IPreferenceService;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.FramelessDialog;
import org.fenrir.yggdrasil.ui.dialog.WizardDialog;
import org.fenrir.yggdrasil.ui.mvc.AbstractController;
import org.fenrir.yggdrasil.ui.mvc.AbstractPerspective;
import org.fenrir.vespine.spi.builder.IIssueURLBuilder;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.ICustomValueDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.vespine.spi.service.IIssueDataService;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IVespineIssueDTO;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.dto.SearchQuery;
import org.fenrir.vespine.core.provider.IssueContentProvider;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.dragonfly.ui.UIPreferenceConstants;
import org.fenrir.dragonfly.ui.model.ApplicationModel;
import org.fenrir.dragonfly.ui.perspective.IssueBrowserPerspective;
import org.fenrir.dragonfly.ui.view.IssueListView;
import org.fenrir.dragonfly.ui.view.IssueDetailView;
import org.fenrir.dragonfly.ui.view.IssueFilterExplorerView;
import org.fenrir.dragonfly.ui.view.IssueTimerView;
import org.fenrir.dragonfly.ui.view.TagExplorerView;
import org.fenrir.dragonfly.ui.dialog.IssueStatusEditDialog;
import org.fenrir.dragonfly.ui.dialog.IssueTagsDialog;
import org.fenrir.dragonfly.ui.wizard.IssueViewFilterAdministrationWizard;
import org.fenrir.dragonfly.core.entity.IssueTimerRegistry;
import org.fenrir.dragonfly.core.event.IAlertListener;
import org.fenrir.dragonfly.core.event.IIssueListener;
import org.fenrir.dragonfly.core.event.ITagListener;
import org.fenrir.dragonfly.core.event.IViewFilterListener;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140825
 */
@EventListener(definitions={ITagListener.class, IIssueListener.class, IViewFilterListener.class, IAlertListener.class})
public class ApplicationController extends AbstractController<ApplicationModel> implements ITagListener, IIssueListener, IViewFilterListener, IAlertListener
{
    private Logger log = LoggerFactory.getLogger(ApplicationController.class);

    @Inject 
    private IssueTimerView timerView;
    
    @Inject
    private IssueFilterExplorerView filterExplorerView;

    @Inject
    private TagExplorerView tagExplorerView;

    @Inject
    private IssueListView issueListView;
    
    @Inject
    private IssueContentProvider issueContentProvider;
    
    @Inject
    private IIssueFacade issueService;
    
    @Inject
    private IAdministrationFacade administrationService;
    
    @Inject
    private IExtendedIssueWorkFacade workService;

    @Inject
    private IPreferenceService preferenceService;
    
    @Inject
    private IEventNotificationService eventNotificationService;
    
    @Inject
    private IExtendedUserFacade userManagementService;

    private boolean fireIssueListViewRefresh = true;
    
    @Inject
    public void setApplicationModel(ApplicationModel model)
    {
        setModel(model);
    }

    public void setTimerView(IssueTimerView timerView)
    {
    	this.timerView = timerView;
    }
    
    public void setFilterExplorerView(IssueFilterExplorerView filterExplorerView)
    {
        this.filterExplorerView = filterExplorerView;
        attachView(filterExplorerView);
    }

    public void setTagExplorerView(TagExplorerView tagExplorerView)
    {
        this.tagExplorerView = tagExplorerView;
        attachView(tagExplorerView);
    }

    public void setIssueListView(IssueListView issueListView)
    {
        this.issueListView = issueListView;
        attachView(issueListView);
    }
    
    public void setIssueContentProvider(IssueContentProvider issueContentProvider)
    {
        this.issueContentProvider = issueContentProvider;
    }
    
    public void setAdministrationService(IAdministrationFacade administrationService)
    {
    	this.administrationService = administrationService;
    }
    
    public void setIssueService(IIssueFacade issueService)
    {
    	this.issueService = issueService;
    }

    public void setWorkService(IExtendedIssueWorkFacade workService)
    {
    	this.workService = workService;
    }
    
    public void setPreferenceService(IPreferenceService preferenceService) 
    {
        this.preferenceService = preferenceService;
    }
    
    public void setEventNotificationService(IEventNotificationService eventNotificationService)
    {
        this.eventNotificationService = eventNotificationService;
    }
    
    public void setUserManagementService(IExtendedUserFacade userManagementService)
    {
    	this.userManagementService = userManagementService;
    }

    /* Model de dades */
    public void setSelectedFilter(IViewFilterDTO viewFilter)
    {
        model.setSelectedFilter(viewFilter);
    }
    
    public void setSelectedTag(ITagDTO tag)
    {
        model.setSelectedTag(tag);
    }
    
    public void setIssueList(List<String> issues)
    {
        model.setIssueList(issues);
    }

    /* Listeners */
    @Override
    public void propertyChange(PropertyChangeEvent event)
    {
    	ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
    	
        if(ApplicationModel.PROPERTY_SELECTED_FILTER.equals(event.getPropertyName())){
        	// No hi haurà cap incidencia sel.leccionada
        	model.setSelectedIssue(null);
            loadIssuesView();
            tagExplorerView.clearSelection();
            windowManager.switchToPerspective(IssueBrowserPerspective.ID);
            windowManager.getPerspective(IssueBrowserPerspective.ID).switchToView(IssueListView.ID);
        }        
        else if(ApplicationModel.PROPERTY_SELECTED_TAG.equals(event.getPropertyName())){
        	// No hi haurà cap incidencia sel.leccionada
        	model.setSelectedIssue(null);
            loadIssuesView();
            filterExplorerView.clearSelection();
            windowManager.switchToPerspective(IssueBrowserPerspective.ID);
            windowManager.getPerspective(IssueBrowserPerspective.ID).switchToView(IssueListView.ID);
        }
        else if(ApplicationModel.PROPERTY_ISSUE_LIST.equals(event.getPropertyName())){
        	// No hi haurà cap incidencia sel.leccionada
        	model.setSelectedIssue(null);
            loadIssuesView();
            filterExplorerView.clearSelection();
            tagExplorerView.clearSelection();
            windowManager.switchToPerspective(IssueBrowserPerspective.ID);
            windowManager.getPerspective(IssueBrowserPerspective.ID).switchToView(IssueListView.ID);
        }
        else if(ApplicationModel.PROPERTY_SEARCH_QUERY.equals(event.getPropertyName())){
        	// No hi haurà cap incidencia sel.leccionada
        	model.setSelectedIssue(null);
            loadIssuesView();
            filterExplorerView.clearSelection();
            tagExplorerView.clearSelection();
            windowManager.switchToPerspective(IssueBrowserPerspective.ID);
            windowManager.getPerspective(IssueBrowserPerspective.ID).switchToView(IssueListView.ID);
        }        
        else if(ApplicationModel.PROPERTY_WORKING_ISSUE.equals(event.getPropertyName())){
        	IIssueDTO workingIssue = (IIssueDTO)event.getNewValue();
        	IssueTimerRegistry timerRegistry = null;
        	if(workingIssue!=null){
        		try{
		        	timerRegistry = workService.findIssueTimerRegistry(workingIssue.getIssueId());
		        	if(timerRegistry==null){
		        		timerRegistry = workService.createTimerRegistry(workingIssue.getIssueId());
		        	}
        		}
        		catch(BusinessException e){
        			log.error("Error canviant la incidència activa: {}", e.getMessage(), e);
                    ApplicationWindowManager.getInstance().displayErrorMessage("Error canviant la incidència activa: " + e.getMessage(), e);
        		}
        	}
    		timerView.setWorkingIssue(timerRegistry);
        	
        	// Es refresca la llista d'incidències
        	reloadIssuesView();
        }
    }

    
    
    @Override
	public void issueCreated(IIssueDTO issue) 
    {
		reloadIssuesView();
	}

	@Override
	public void issueUpdated(IIssueDTO oldIssue, IIssueDTO newIssue) 
	{
		if(model.getSelectedIssue()!=null && model.getSelectedIssue().equals(oldIssue)){
			model.setSelectedIssue(newIssue);
		}
		
		issueListView.updateIssueWidget(newIssue);
		// Actualització de la vista detall, si cal
		ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
    	IssueDetailView detailView = (IssueDetailView)windowManager.getViewFromRegistry(IssueDetailView.createViewId(oldIssue.getIssueId()));
    	if(detailView!=null){
    		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                    "Les dades de la incidència han sigut actualitzades",
                    "Advertència",
                    JOptionPane.INFORMATION_MESSAGE);
    		try{
    			detailView.setIssueDetails(IssueDetailView.MODE_VISUALIZATION, newIssue);
    		}
    		catch(BusinessException e){
    			log.error("Error actualitzant el contingut de la vista: {}", e.getMessage(), e);
    			windowManager.displayErrorMessage("Error actualitzant la vista", e);
    		}
    	}
	}

	@Override
	public void issueDeleted(IIssueDTO issue) 
	{
		reloadIssuesView();
		// Tancament de la vista detall, si cal
		ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
    	IssueDetailView detailView = (IssueDetailView)windowManager.getViewFromRegistry(IssueDetailView.createViewId(issue.getIssueId()));
    	if(detailView!=null){
    		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                    "La incidència ha sigut eliminada",
                    "Advertència",
                    JOptionPane.INFORMATION_MESSAGE);
    		// S'abandona la vista de detall i s'actualitza el model a null
    		leaveIssueDetailView(detailView.getComposedId());
    	}
	}

	@Override
    public void issuesUpdated()
    {
        reloadIssuesView();
    }

    @Override
    public void viewFilterCreated(IViewFilterDTO filter)
    {
    	try{
    		loadFilterExplorerView();
    	}
    	catch(BusinessException e){
    		log.error("Error carregant la vista de filtres: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant la vista de filtres: " + e.getMessage(), e);
    	}
    }

    @Override
    public void viewFilterModified(IViewFilterDTO oldFilter, IViewFilterDTO newFilter)
    {
    	try{
    		loadFilterExplorerView();
    	}
    	catch(BusinessException e){
    		log.error("Error carregant la vista de filtres: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant la vista de filtres: " + e.getMessage(), e);
    	}
    	
        // Es comproba si el filtre modificat és el que s'està visualitzant i es recarrega la vista
        IViewFilterDTO selectedFilter = model.getSelectedFilter();
        if(selectedFilter!=null && selectedFilter.equals(oldFilter)){
            model.setSelectedFilter(newFilter);
        }
    }

    @Override
    public void viewFilterDeleted(IViewFilterDTO filter)
    {
    	try{
    		loadFilterExplorerView();
    	}
    	catch(BusinessException e){
    		log.error("Error carregant la vista de filtres: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant la vista de filtres: " + e.getMessage(), e);
    	}
    	
        // Es comproba si el filtre modificat és el que s'està visualitzant
        IViewFilterDTO selectedFilter = model.getSelectedFilter();
        if(selectedFilter!=null && selectedFilter.equals(filter)){
            model.setSelectedFilter(null);
        }
    }
    
    @Override
    public void viewFilterOrdered()
    {
    	try{
    		loadFilterExplorerView();
    	}
    	catch(BusinessException e){
    		log.error("Error carregant la vista de filtres: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant la vista de filtres: " + e.getMessage(), e);
    	}
    }

    @Override
    public void tagListUpdated()
    {
        loadTagExplorerView();
        reloadIssuesView();
    }
    
    @Override
    public void alertTypeCreated(IAlertTypeDTO type) 
    {
        reloadIssuesView();
    }

    @Override
    public void alertTypeUpdated(IAlertTypeDTO oldType, IAlertTypeDTO newType) 
    {
        reloadIssuesView();
    }

    @Override
    public void alertTypeDeleted(IAlertTypeDTO type) 
    {
        reloadIssuesView();
    }

    /* Accions */
    public void loadViews() throws BusinessException
    {
        loadExplorerViews();
        loadIssuesView();
    }

    public void loadExplorerViews() throws BusinessException
    {
        loadFilterExplorerView();
        loadTagExplorerView();
    }

    public void loadFilterExplorerView() throws BusinessException
    {
        List<IViewFilterDTO> applicationFilters = administrationService.findApplicationViewFilters();
        filterExplorerView.setApplicationFilterList(applicationFilters);
        
        List<IViewFilterDTO> userFilters = administrationService.findUserViewFilters();
        filterExplorerView.setUserFilterList(userFilters);
    }

    public void loadTagExplorerView()
    {
        // Actualització de la vista d'exploració de tags
    	try{
	    	List<ITagDTO> tags = administrationService.findAllTags();
	        tagExplorerView.setTagList(tags);
    	}
    	catch(Exception e){
            log.error("Error carregant la vista d'etiquetes: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant la vista d'etiquetes: " + e.getMessage(), e);            
        }
    }

    public void loadIssuesView()
    {
        try{            
            int pageSize = Integer.parseInt(preferenceService.getProperty(UIPreferenceConstants.UI_ISSUE_LIST_PAGE_SIZE, "50"));
            loadIssuesViewPrivate(0, pageSize);
            model.setLoadedPage(0);
        }
        catch(Exception e){
            log.error("Error carregant la vista d'incidències: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant la vista d'incidències: " + e.getMessage(), e);            
        }
    }

    public void reloadIssuesView()
    {
        try{
            int currentPage = model.getLoadedPage();            
            int pageSize = Integer.parseInt(preferenceService.getProperty(UIPreferenceConstants.UI_ISSUE_LIST_PAGE_SIZE, "50"));
            int issuesToLoad = currentPage * pageSize + pageSize;
            loadIssuesViewPrivate(0, issuesToLoad);
        }
        catch(Exception e){
            log.error("Error recarregant la vista d'incidències: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant la vista d'incidències: " + e.getMessage(), e);
        }             
    }

    public void loadMoreIssues()
    {
        try{
            int page = model.getLoadedPage() + 1;
            int pageSize = Integer.parseInt(preferenceService.getProperty(UIPreferenceConstants.UI_ISSUE_LIST_PAGE_SIZE, "50"));
            loadIssuesViewPrivate(page, pageSize);
            model.setLoadedPage(page++);
        }
        catch(Exception e){
            log.error("Error en carregar la vista d'incidències: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant la vista d'incidències: " + e.getMessage(), e);
        }
    }

    private void loadIssuesViewPrivate(final int page, final int pageSize) throws Exception
    {
        if(fireIssueListViewRefresh){
        	// Capa "En procés"
        	ApplicationWindowManager.getInstance().showInProcess(true);

        	final int lastResultIndex = page * pageSize + pageSize;
        	new SwingWorker<Map<String, Object>, String>()
        	{
				@Override
				protected Map<String, Object> doInBackground() throws Exception 
				{
					Map<String, Object> params = new HashMap<String, Object>();
					
					if(model.getSelectedFilter()!=null){
						IViewFilterDTO filter = model.getSelectedFilter();
						long issueCount = issueService.countFilteredIssues(filter);
						List<IIssueDTO> issues = issueService.findFilteredIssues(filter, page, pageSize);
						params.put("UPDATE_VIEW", Boolean.TRUE);
						params.put("ISSUES", issues);
						params.put("TITLE", filter.getName() + " (" + issueCount + ")");
						params.put("HAS_MORE_ELEMENTS", Boolean.valueOf(issueCount > lastResultIndex));
					}
					// Es mostra el tag sel.leccionat
					else if(model.getSelectedTag()!=null){
						ITagDTO tag = model.getSelectedTag();
						long issueCount = issueService.countTaggedIssues(tag.getTagId());
						List<IIssueDTO> issues = issueService.findTaggedIssues(tag.getTagId(), page, pageSize);
						params.put("UPDATE_VIEW", Boolean.TRUE);
						params.put("ISSUES", issues);
						params.put("TITLE", tag.getName() + " (" + issueCount + ")");
						params.put("HAS_MORE_ELEMENTS", Boolean.valueOf(issueCount > lastResultIndex));
					}
					// Es mostren les incidències sel.leccionades
					else if(model.getSelectedIssues()!=null){
						List<String> issueIDs = model.getSelectedIssues();
						List<IIssueDTO> issues = new ArrayList<IIssueDTO>();
						int startIndex = page * pageSize;                    
						for(int i=startIndex; i<issueIDs.size() && i<lastResultIndex; i++){
							String id = issueIDs.get(i);
							IIssueDTO issue = issueService.findIssueById(id);
							issues.add(issue);                        
						}
						params.put("UPDATE_VIEW", Boolean.TRUE);
						params.put("ISSUES", issues);
						params.put("TITLE", "Incidències sel.leccionades (" + issueIDs.size() + ")");
						params.put("HAS_MORE_ELEMENTS", Boolean.valueOf(issueIDs.size() > lastResultIndex));
					}
					// Es mostren les incidències de la cerca
					else if(model.getSearchQuery()!=null){
						SearchQuery searchQuery = model.getSearchQuery();
						long issueCount = issueService.countIssueSearchHits(searchQuery);
						Collection<IIssueDTO> issues = issueService.findIssueSearchHits(searchQuery, page, pageSize);
						params.put("UPDATE_VIEW", Boolean.TRUE);
						params.put("ISSUES", new ArrayList<IIssueDTO>(issues));
						params.put("TITLE", "Resultats de la cerca (" + issueCount + ")");
						params.put("HAS_MORE_ELEMENTS", Boolean.valueOf(issueCount > lastResultIndex));
					}
					else{
						params.put("UPDATE_VIEW", Boolean.FALSE);
					}
					
					return params;
				}

				@Override
				protected void done() 
				{
					try{
						Map<String, Object> params = get();
						if(!(Boolean)params.get("UPDATE_VIEW")){
							return;
						}
						
						String title = (String)params.get("TITLE");
						Boolean hasMoreElements = (Boolean)params.get("HAS_MORE_ELEMENTS");
						List<IIssueDTO> issues = (List<IIssueDTO>)params.get("ISSUES");
						
						// Si es carrega desde la plana 0 es neteja la llista d'incidències
						if(page==0){
							issueListView.clearIssues();
						}
						
						issueListView.setTitle(title);
						issueListView.addIssues(issues, hasMoreElements);
					}
					catch(Exception e){
						throw new RuntimeException(e.getMessage(), e);
					}
					finally{
						ApplicationWindowManager.getInstance().showInProcess(false);
					}
				}
        		
        	}.execute();
        }
    }

    public void seekIssueInView(String issueVisibleId)
    {
        boolean found = issueListView.seekIssue(issueVisibleId);
        if(!found){
            int option = JOptionPane.showConfirmDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                    "La incidència especificada no es troba amb les incidències carregades actualment.\nVol comprobar la resta d'incidències del filtre?",
                    "Advertència",
                    JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_OPTION){
                IViewFilterDTO filter = model.getSelectedFilter();
                ITagDTO tag = model.getSelectedTag();
                List<String> issueIDs = model.getSelectedIssues();
                // Si hi ha filtre sel.leccionat
                try{
                	// Es recupera la ID associada a la ID visible
                	IIssueDTO issueDTO = issueService.findIssueByVisibleId(issueVisibleId.toUpperCase());
                	// Si la ID no existeix, s'avisa i no es continua
                	if(issueDTO==null){
                		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                                "La incidència especificada no existeix",
                                "Advertència",
                                JOptionPane.INFORMATION_MESSAGE);
                	}
                	
                    if(filter!=null){
                        if(issueService.issueFilterMatches(filter, issueDTO.getIssueId())){
                            long issueCount = issueService.countFilteredIssues(filter);
                            loadIssuesViewPrivate(0, new Long(issueCount).intValue());                            
                        }
                    }
                    // Si hi ha tag sel.leccionat
                    else if(tag!=null){
                        if(issueService.isIssueTagged(issueDTO.getIssueId(), tag.getTagId())){
                            long issueCount = issueService.countTaggedIssues(tag.getTagId());
                            loadIssuesViewPrivate(0, new Long(issueCount).intValue());
                        }
                    }
                    // Si hi ha incidències sel.leccionades
                    else if(issueIDs!=null){
                        if(issueIDs.contains(issueDTO.getIssueId())){
                            loadIssuesViewPrivate(0, issueIDs.size());
                        }
                    }

                    if(!issueListView.seekIssue(issueVisibleId)){
                        JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                            "La incidència especificada no es troba a la llista",
                            "Advertència",
                            JOptionPane.INFORMATION_MESSAGE);
                    }
                }
                catch(Exception e){
                    log.error("Error buscant la incidència indicada: {}", e.getMessage(), e);
                    ApplicationWindowManager.getInstance()
                            .displayErrorMessage("Error buscant la incidència indicada: " + e.getMessage(), e);
                }
            }
        }
    }
    
    public void searchIssueByVisibleId(String issueVisibleId)
    {   
    	try{
	        // TODO Canviar per un exists
	        IIssueDTO issue = issueService.findIssueByVisibleId(issueVisibleId.toUpperCase());
	        List<String> issues = new ArrayList<String>();
	        if(issue!=null){
	        	 issues.add(issue.getIssueId());
	        }
	        
	        setIssueList(issues);
    	}
    	catch(Exception e){
            log.error("Error buscant la incidència indicada: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error buscant la incidència indicada: " + e.getMessage(), e);
        }
    }
    
    public void searchIssue(String term)
    {
    	try{
	        SearchQuery query = issueService.buildIssueTermSearchQuery(term);
	        model.setSearchQuery(query);
    	}
    	catch(BusinessException e){
    		log.error("Error cercant incidències: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error cercant incidències: " + e.getMessage(), e);
    	}
    }

    public IIssueDTO updateIssueSLADate(String issueId, boolean isSLA) throws BusinessException
    {
    	try{
    		issueService.updateIssueSLADate(issueId, isSLA);
    	}
    	catch(Exception e){
            log.error("Error actualitzant la data SLA per la incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error actualitzant data SLA: " + e.getMessage(), e);            
        }
    	return issueService.findIssueById(issueId);
    }

    public IIssueDTO updateIssueSLADate(String issueId, String strSlaDate) throws BusinessException
    {
        try{
            Date slaDate = new SimpleDateFormat("dd/MM/yyyy").parse(strSlaDate);
            issueService.updateIssueSLADate(issueId, slaDate);
        }
        catch(Exception e){
            log.error("Error actualitzant la data SLA {} per la incidència {}: {}", new Object[]{strSlaDate, issueId, e.getMessage(), e});
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error actualitzant data SLA: " + e.getMessage(), e);            
        }

        return issueService.findIssueById(issueId);
    }
    
    public void openNewFilterDialog()
    {
        try{
            IssueViewFilterAdministrationWizard wizard = new IssueViewFilterAdministrationWizard();
            int result = new WizardDialog<IssueViewFilterAdministrationWizard>(
                    ApplicationWindowManager.getInstance().getMainWindow(),
                    "Crear nou filtre",
                    new Dimension(500, 450),
                    wizard
            ).open();

            // Es refresca la llista amb el filtre creat
            if(WizardDialog.DIALOG_OK==result){
                setSelectedFilter(wizard.getFilter());
            }
        }
        catch(Exception e){
            log.error("Error al crear wizard: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error creant el wizard: " + e.getMessage(), e);                        
        }
    }
    
    public void openModifyFilterDialog()
    {
        IViewFilterDTO selectedFilter = model.getSelectedFilter();
        if(selectedFilter==null){
            JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), 
                    "No hi ha cap filtre sel.leccionat", 
                    "Advertència", 
                    JOptionPane.INFORMATION_MESSAGE);
            return;
        }        
            
        try{            
            IssueViewFilterAdministrationWizard wizard = new IssueViewFilterAdministrationWizard(selectedFilter);
            int result = new WizardDialog<IssueViewFilterAdministrationWizard>(
                    ApplicationWindowManager.getInstance().getMainWindow(),
                    "Modificar filtre existent",
                    new Dimension(500, 600),
                    wizard
            ).open();

            // Es refresca la llista amb el filtre modificat
            if(WizardDialog.DIALOG_OK==result){
                setSelectedFilter(wizard.getFilter());
            }            
        }
        catch(Exception e){
            log.error("Error al crear wizard: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error creant el wizard: " + e.getMessage(), e);            
        }
    }

    public void openBrowser(final String issueId)
    {
//        try{
//        	Map<String, Object> params = new HashMap<String, Object>();
//        	
//        	IIssueURLBuilder issueUrlBuilder = issueContentProvider.getIssueURLBuilder(MantisConstants.ISSUE_PROVIDER_MANTIS);
//        	String issueUrl = issueUrlBuilder.buildIssueURL(issueId.toString());
//            if(StringUtils.isBlank(issueUrl)){
//                log.warn("No s'ha especificat una url vàlida per obrir el navegador");
//                JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
//                        "No s'ha especificat una url vàlida per obrir la incidència",
//                        "Error",
//                        JOptionPane.ERROR_MESSAGE);
//            }
//            else{                
//                params.put("url", issueUrl);
//                String browser = preferenceService.getProperty(CorePreferenceConstants.EXT_APPLICATION_BROWSER);
//                if(StringUtils.isBlank(browser)){
//                    log.warn("No s'ha especificat una url vàlida per obrir el navegador");
//                    JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
//                            "No s'ha especificat un navegador vàlid per obrir la incidència",
//                            "Error",
//                            JOptionPane.ERROR_MESSAGE);
//                }
//                else{
//                    // Per paths amb espais en blanc cal afegit cometes                    
//                    browser = "\"" + browser + "\"";
//                    CommandLine command = CommandLine.parse(browser);
//                    command.addArgument("${url}", false);
//                    command.setSubstitutionMap(params);
//                    ExecuteResultHandler resultHandler = new ExecuteResultHandler()
//                    {
//                        @Override
//                        public void onProcessFailed(ExecuteException e)
//                        {
//                            log.error("Error en l'execució del navegador exitCode={}: {}", new Object[]{e.getExitValue(), e.getMessage(), e});
//                            JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
//                                    "Error en obrir el navegador",
//                                    "Error",
//                                    JOptionPane.ERROR_MESSAGE);
//                        }
//
//                        @Override
//                        public void onProcessComplete(int exitValue)
//                        {
//                            if(log.isDebugEnabled()){
//                                log.debug("Oberta incidència {} al navegador", issueId);
//                            }
//                        }
//                    };
//
//                    DefaultExecutor executor = new DefaultExecutor();
//                    // Al proporcionar un resultHandler s'habilita l'execució asincrona del procés
//                    executor.execute(command, resultHandler);
//                }
//            }            
//        }
//        catch(Exception e){
//            log.error("Error d'execució del navegador: {}", e.getMessage(), e);
//            ApplicationWindowManager.getInstance()
//                    .displayErrorMessage("Error obrint el navegador: " + e.getMessage(), e);            
//        }
    }

    public Color getStatusColor(String statusId)
    {
        boolean showStatusColor = Boolean.parseBoolean(preferenceService.getProperty(UIPreferenceConstants.UI_ISSUE_LIST_PAINT_STATUS_COLOR));
        if(showStatusColor){
        	try{
	        	IStatusDTO status = administrationService.findStatusById(statusId);
	            if(status!=null){
	                String strColor = status.getColorRGB();
	                String[] rgb = strColor.split(",");
	                return new Color(Integer.parseInt(rgb[0].trim()),
	                       Integer.parseInt(rgb[1].trim()),
	                        Integer.parseInt(rgb[2].trim()));
	            }
        	}
        	catch(BusinessException e){
            	log.error("Error recuperant el color de l'estat {}: ", new Object[]{statusId, e.getMessage(), e});
            }
        }
        
        return UIManager.getColor("Panel.background");        
    }

    public ITagDTO tagIssueByDescription(String issueId, String tagDescription)
    {
    	ITagDTO tag = null;
    	try{
	        List<ITagDTO> tags = administrationService.findTagsByName(tagDescription);
	        // Crear un nou tag
	        if(tags.isEmpty()){
	            tag = administrationService.createTag(tagDescription, false);
	            try{
	                // Es desactiva el refresc de la vista d'incidències perquè es farà a continuació sel.lectivament
	                fireIssueListViewRefresh = false;
	                eventNotificationService.notifyNamedEvent(ITagListener.class, ITagListener.EVENT_TAG_LIST_UPDATED);
	            }
	            catch(Exception e){
	                log.error("Error en notificar event de llista de tags actualitzada: {}", e.getMessage(), e);
	                ApplicationWindowManager.getInstance()
	                    .displayErrorMessage("Error associant el tag a la incidència: " + e.getMessage(), e);                
	            }
	            finally{
	                // Es torna a activar el refresc de la vista d'incidències
	                fireIssueListViewRefresh = true;
	            }
	        }
	        // En cas que existeixi un tag amb la descripció proporcionada, s'agafa la primer coincidència
	        else{
	            tag = tags.get(0);
	        }
	        // S'associa el tag a la incidència
	        tagIssue(issueId, tag.getTagId());
    	}
    	catch(Exception e){
            log.error("Error associant el tag a la incidència: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                .displayErrorMessage("Error associant el tag a la incidència: " + e.getMessage(), e);                
        }
    	
    	return tag;
    }
    
    public void tagIssue(String issueId, String tagId)
    {
    	try{
    		issueService.tagIssue(issueId, tagId);
    		
    		IIssueDTO issue = issueService.findIssueById(issueId);
            issueListView.updateIssueWidget(issue);
            // Si hi ha una vista de la incidencia relacionada, també es refresquen els tags
            ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
            IssueDetailView detailView = (IssueDetailView)windowManager.getViewFromRegistry(IssueDetailView.createViewId(issueId.toString()));
            if(detailView!=null){
            	detailView.setIssueDetails(detailView.getMode(), issue);
            }
    	}
    	catch(Exception e){
            log.error("Error etiquetant l'incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error etiquetant l'incidència: " + e.getMessage(), e);            
        }
    }
    
    public void dettachTag(String issueId, String tagId)
    {
    	IIssueDTO issue = null;
    	try{
    		issueService.dettachIssueTag(issueId, tagId);
    	
	    	issue = issueService.findIssueById(issueId);
	        issueListView.updateIssueWidget(issue);
    	}
    	catch(Exception e){
            log.error("Error eliminant etiqueta de l'incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error eliminant etiqueta de l'incidència: " + e.getMessage(), e);            
        }
    	
        // Si hi ha una vista de la incidencia relacionada, també es refresquen els tags
        ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
        IssueDetailView detailView = (IssueDetailView)windowManager.getViewFromRegistry(IssueDetailView.createViewId(issueId.toString()));
        if(detailView!=null && issue!=null){
        	try{
        		detailView.setIssueDetails(detailView.getMode(), issue);
        	}
        	catch(Exception e){
                log.error("Error refrescant la vista de detall de l'incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
                ApplicationWindowManager.getInstance()
                        .displayErrorMessage("Error refrescant la vista de detall de l'incidència " + issueId + ": " + e.getMessage(), e);            
            }
        }
    }
    
    public void createIssue(final IProjectDTO project,
    		final ICategoryDTO category,
    		final ISeverityDTO severity, 
    		final IStatusDTO status,
    		final String summary, 
    		final String description,
    		final IUserDTO assignedUser,
    		final ISprintDTO sprint,
    		final String strEstimatedTime,
    		final String strSlaDate,
    		final Map<ICustomFieldDTO, String> customFields)
    {
    	ApplicationWindowManager.getInstance().showInProcess(true);
    	
    	new SwingWorker<IIssueDTO, String>()
    	{
			@Override
			protected IIssueDTO doInBackground() throws Exception 
			{
				Date slaDate = null;
		    	try{
			    	if(StringUtils.isNotBlank(strSlaDate)){
			    		slaDate = new SimpleDateFormat("dd/MM/yyyy").parse(strSlaDate);
			    	}
		    	}
		    	catch(Exception e){
		    		// Aqui no hauria d'arribar pels controls implementats a la vista
		    		log.error("Error interpretant la data SLA: {}", e.getMessage(), e);
		    	}
		    	
		    	// Es calcula el nombre de minuts corresponents al temps estimat
		    	Integer estimatedTime = null;
		    	if(StringUtils.isNotBlank(strEstimatedTime)){
		    		Matcher matcher = Pattern.compile("(?:(\\d+)h){0,1}\\W*(?:(\\d+)m){0,1}").matcher(strEstimatedTime);
		    		if(matcher.find()){
		    			String strHours = StringUtils.defaultIfBlank(matcher.group(1), "0");
		    			String strMinutes = StringUtils.defaultIfBlank(matcher.group(2), "0");
		    			estimatedTime = Integer.valueOf(strHours) * 60 + Integer.valueOf(strMinutes);
		    		}
		    	}
		    	// Es crea la llista de valors custom
		    	Map<String, String> customFieldValues = new HashMap<String, String>();
		    	for(ICustomFieldDTO field:customFields.keySet()){
		    		customFieldValues.put(field.getFieldId(), customFields.get(field));
		    	}
		    	
		    	return issueService.createIssue(project.getProjectId(), 
		    			category.getCategoryId(),
		    			severity.getSeverityId(), 
		    			status.getStatusId(), 
		    			summary, 
		    			description, 
		    			assignedUser!=null ? assignedUser.getUsername() : null, 
		    			sprint!=null ? sprint.getSprintId() : null, 
		    			estimatedTime, 
		    			slaDate, 
		    			customFieldValues);
			}

			@Override
			protected void done() 
			{
				IIssueDTO issue = null;
				try{
			    	issue = get();
		    	}
		    	catch(Exception e){
		            log.error("Error creant l'incidència: {}", e.getMessage(), e);
		            ApplicationWindowManager.getInstance()
		                    .displayErrorMessage("Error creant l'incidència: " + e.getMessage(), e);            
		        }
				finally{
					ApplicationWindowManager.getInstance().showInProcess(false);
				}
				
				if(issue!=null){
					openIssueDetailsView(issue.getIssueId());
			    	ApplicationWindowManager.getInstance().removeViewFromRegistry(IssueDetailView.ID);
				}
			}
    		
    	}.execute();
    }
    
    public void updateIssue(final String issueId,
    		final IProjectDTO project,
    		final ICategoryDTO category,
    		final ISeverityDTO severity, 
    		final IStatusDTO status,
    		final String summary, 
    		final String description,
    		final IUserDTO assignedUser,
    		final ISprintDTO sprint,
    		final String strEstimatedTime,
    		final String strSlaDate,
    		final String strResolutionDate,
    		final Map<ICustomFieldDTO, String> customFields)
    {
    	ApplicationWindowManager.getInstance().showInProcess(true);
    	
    	new SwingWorker<IIssueDTO, String>()
    	{
			@Override
			protected IIssueDTO doInBackground() throws Exception 
			{
				Date slaDate = null;
		    	Date resolutionDate = null;
		    	try{
			    	if(StringUtils.isNotBlank(strSlaDate)){
			    		slaDate = new SimpleDateFormat("dd/MM/yyyy").parse(strSlaDate);
			    	}
			    	if(StringUtils.isNotBlank(strResolutionDate)){
			    		resolutionDate = new SimpleDateFormat("dd/MM/yyyy").parse(strResolutionDate);
			    	}
		    	}
		    	catch(Exception e){
		    		// Aqui no hauria d'arribar pels controls implementats a la vista
		    		log.error("Error interpretant les dates SLA i de resolució de la incidencia: {}", e.getMessage(), e);
		    	}
		    	// Es calcula el nombre de minuts corresponents al temps estimat
		    	Integer estimatedTime = null;
		    	if(StringUtils.isNotBlank(strEstimatedTime)){
		    		Matcher matcher = Pattern.compile("(?:(\\d+)h){0,1}\\W*(?:(\\d+)m){0,1}").matcher(strEstimatedTime);
		    		if(matcher.find()){
		    			String strHours = StringUtils.defaultIfBlank(matcher.group(1), "0");
		    			String strMinutes = StringUtils.defaultIfBlank(matcher.group(2), "0");
		    			estimatedTime = Integer.valueOf(strHours) * 60 + Integer.valueOf(strMinutes);
		    		}
		    	}
		    	// Es crea la llista de valors custom
		    	Map<String, String> customFieldValues = new HashMap<String, String>();
		    	for(ICustomFieldDTO field:customFields.keySet()){
		    		customFieldValues.put(field.getFieldId(), customFields.get(field));
		    	}
		    	
		    	return issueService.updateIssue(issueId, 
		    			project.getProjectId(), 
		    			category.getCategoryId(), 
		    			severity.getSeverityId(), 
		    			status.getStatusId(), 
		    			summary, 
		    			description, 
		    			assignedUser!=null ? assignedUser.getUsername() : null, 
		    			sprint!=null ? sprint.getSprintId() : null, 
		    			estimatedTime, 
		    			slaDate, 
		    			resolutionDate, 
		    			customFieldValues);
			}
			
			@Override
			protected void done() 
			{
				IIssueDTO issue = null;
				try{
			    	issue = get();
			    	ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
			    	IssueDetailView detailView = (IssueDetailView)windowManager.getViewFromRegistry(IssueDetailView.createViewId(issue.getIssueId()));
			    	detailView.setIssueDetails(IssueDetailView.MODE_VISUALIZATION, issue);
		    	}
		    	catch(Exception e){
		            log.error("Error modificant les dades de l'incidència: {}", e.getMessage(), e);
		            ApplicationWindowManager.getInstance()
		                    .displayErrorMessage("Error modificant les dades de l'incidència: " + e.getMessage(), e);            
		        }
				finally{
					ApplicationWindowManager.getInstance().showInProcess(false);
				}
			}
    	}.execute();    	
    }
    
    public void syncIssue(Long issueId)
    {
//        try{
//        	IIssueDataService issueDataService = issueContentProvider.getIssueDataService(MantisConstants.ISSUE_PROVIDER_MANTIS); 
//            IIssueDTO issueDTO = issueDataService.findIssue(issueId);
//            int result = issueAdministrationService.manageIssue(issueDTO);
//            if(result==IIssueAdministrationService.ISSUE_CREATED_OR_UPDATED){
//                eventNotificationService.notifyNamedEvent(IContentUpdatedListener.class, IContentUpdatedListener.EVENT_ISSUES_UPDATED_ID);
//            }
//        }
//        catch(BusinessException e){
//            log.error("Error obtenint les dades de la incidencia: {}", e.getMessage(), e);
//            ApplicationWindowManager.getInstance()
//                    .displayErrorMessage("Error obtenint les dades de la incidència: " + e.getMessage(), e);            
//        }
//        catch(Exception e){
//            log.error("Error carregant la vista d'incidencies: {}", e.getMessage(), e);
//            ApplicationWindowManager.getInstance()
//                    .displayErrorMessage("Error carregant la vista de les incidències: " + e.getMessage(), e);            
//        }
    }
    
    public void deleteIssue(String issueId)
    {
    	try{
    		issueService.deleteIssue(issueId, true);
    		
    		// Si s'està dins la vista de detall, es passa a la llista
            ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
            String viewId = IssueDetailView.createViewId(issueId.toString());
            if(windowManager.getViewFromRegistry(viewId)!=null){
            	leaveIssueDetailView(viewId);
            }
            if(model.getWorkingIssue()!=null && model.getWorkingIssue().getIssueId().equals(issueId)){
            	model.setWorkingIssue(null);
            }
    	}
    	catch(Exception e){
            log.error("Error esborrant l'incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error esborrant l'incidència: " + e.getMessage(), e);            
        }
    	
        // Es recarrega la llista d'incidencies
        reloadIssuesView();
    }
    
    public void restoreIssue(String issueId)
    {
    	try{
    		issueService.restoreIssue(issueId);
    	}
    	catch(Exception e){
            log.error("Error restaurant l'incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error restaurant l'incidència: " + e.getMessage(), e);            
        }
    	
        reloadIssuesView();
    }
    
    public List<IIssueNoteDTO> getSelectedIssueNotes()
    {
    	IIssueDTO selectedIssue = model.getSelectedIssue();
    	if(selectedIssue!=null){
    		try{
	    		// XXX Mirar si fa falta: Es torna a demanar l'incidencia per refrescar les referencies
	    		IIssueDTO issue = issueService.findIssueById(selectedIssue.getIssueId());
	    		return new ArrayList<IIssueNoteDTO>(issue.getNotes());
    		}
    		catch(Exception e){
                log.error("Error recuperant les notes de l'incidencia: {}", e.getMessage(), e);
                ApplicationWindowManager.getInstance()
                        .displayErrorMessage("Error recuperant les notes de l'incidencia: " + e.getMessage(), e);            
            }
    	}
    	return Collections.emptyList();
    }
    
    /**
     * Mètode que crea una nova nota associant-la a l'incidencia actualment sel.leccionada. 
     * Per recuperar la referència a l'incidencia es consulta el model.
     * @param noteContents {@link String} - Text de la nova nota
     */
    public void createIssueNote(String noteContents)
    {
    	IIssueDTO selectedIssue = model.getSelectedIssue();
    	if(selectedIssue==null){
    		throw new IllegalStateException("No hi ha cap incidencia sel.leccionada");
    	}
    	try{
    		issueService.createIssueNote(selectedIssue.getIssueId(), noteContents);
    	}
    	catch(Exception e){
            log.error("Error creant nota a l'incidència {}: {}", new Object[]{selectedIssue.getIssueId(), e.getMessage(), e});
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error creant nota: " + e.getMessage(), e);            
        }
    }
    
    public void updateIssueNote(String issueId, String noteId, String noteContents)
    {
    	try{
    		issueService.updateIssueNote(noteId, issueId, noteContents);
    	}
    	catch(Exception e){
            log.error("Error modificant nota a l'incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error modificant nota: " + e.getMessage(), e);            
        }
    }
    
    public void deleteIssueNote(String issueId, String noteId)
    {
    	try{
    		issueService.deleteIssueNote(issueId, noteId);
    	}
    	catch(Exception e){
            log.error("Error esborrant nota a l'incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error modificant nota: " + e.getMessage(), e);            
        }
    }
    
    public void manageIssueTags(String issueId)
    {
        FramelessDialog dialog = new IssueTagsDialog(ApplicationWindowManager.getInstance().getMainWindow(), issueId);
        dialog.open();        
    }
	
    public void openNewIssueView()
    {
    	ApplicationWindowManager.getInstance().showInProcess(true);
    	
    	new SwingWorker<Void, String>()
    	{
			@Override
			protected Void doInBackground() throws Exception 
			{
				// Realment no s'ha de fer res
				return null;
			}

			@Override
			protected void done() 
			{
				ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
		        AbstractPerspective perspective = windowManager.getPerspective(IssueBrowserPerspective.ID);
		        perspective.addView(IssueDetailView.ID, IssueDetailView.class);
		        IssueDetailView issueDetailView = (IssueDetailView)perspective.getView(IssueDetailView.ID);
		        issueDetailView.setMode(IssueDetailView.MODE_CREATION);
		        windowManager.switchToPerspective(IssueBrowserPerspective.ID);
		        perspective.switchToView(IssueDetailView.ID);
		        
		        ApplicationWindowManager.getInstance().showInProcess(false);
			}
    		
    	}.execute();
    }
    
	public void openIssueDetailsView(final String issueId)
    {
		ApplicationWindowManager.getInstance().showInProcess(true);
		
		new SwingWorker<IIssueDTO, String>()
    	{
			@Override
			protected IIssueDTO doInBackground() throws Exception 
			{
		        return issueService.findIssueById(issueId);
			}

			@Override
			protected void done() 
			{
				try{
					IIssueDTO issueDTO = get();
					model.setSelectedIssue(issueDTO);
			        
			        ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
			        String viewId = IssueDetailView.createViewId(issueId.toString());
			        // Si ja està creada no s'afegeix al manager
			        AbstractPerspective perspective = windowManager.getPerspective(IssueBrowserPerspective.ID);
			        IssueDetailView detailView = (IssueDetailView)perspective.getView(viewId);
			        if(detailView==null){
			        	perspective.addView(viewId, IssueDetailView.class);
			        	detailView = (IssueDetailView)perspective.getView(viewId);
			        }
			        detailView.setIssueDetails(IssueDetailView.MODE_VISUALIZATION, issueDTO);
			        windowManager.switchToPerspective(IssueBrowserPerspective.ID);
			        windowManager.getPerspective(IssueBrowserPerspective.ID).switchToView(viewId);
				}
				catch(Exception e){
					log.error("Error visualitzant el detall de l'incidència {}: {}", new Object[]{issueId, e.getMessage(), e});
		            ApplicationWindowManager.getInstance()
		                    .displayErrorMessage("Error visualitzant el detall de l'incidència: " + e.getMessage(), e);
				}
				finally{
					ApplicationWindowManager.getInstance().showInProcess(false);
				}
			}
    		
    	}.execute();
    }
    
    public void leaveIssueDetailView(String viewId)
    {
        model.setSelectedIssue(null);
        ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
        windowManager.switchToPerspective(IssueBrowserPerspective.ID);
        AbstractPerspective perspective = windowManager.getPerspective(IssueBrowserPerspective.ID);
        perspective.switchToView(IssueListView.ID);
        windowManager.removeViewFromRegistry(viewId);
        // Es refresca el contingut del filtre sel.leccionat per reflexar els possibles canvis fets
        reloadIssuesView();        
    }
    
    public void reloadIssueDetailView()
    {
    	IIssueDTO issueDTO = model.getSelectedIssue();
    	if(issueDTO==null){
    		return;
    	}
    	
    	String viewId = IssueDetailView.createViewId(issueDTO.getIssueId());
        AbstractPerspective perspective = ApplicationWindowManager.getInstance().getPerspective(IssueBrowserPerspective.ID);
        IssueDetailView detailView = (IssueDetailView)perspective.getView(viewId);
        if(detailView==null || detailView.getMode()==IssueDetailView.MODE_CREATION){
        	return;
        }
        // En cas de mode==UPDATE es pregunta si es volen actualitzar els canvis abans de fer-ho
        if(detailView.getMode()!=IssueDetailView.MODE_VISUALIZATION){
        	int option = JOptionPane.showConfirmDialog(ApplicationWindowManager.getInstance().getMainWindow(), 
                    "Si s'actualitza la vista es pendran els canvis. Desitja continuar?", 
                    "Advertència", 
                    JOptionPane.INFORMATION_MESSAGE); 
        	if(option!=JOptionPane.OK_OPTION){
        		return;
        	}
        }

        ApplicationWindowManager.getInstance().showInProcess(true);
        
    	new SwingWorker<IIssueDTO, String>()
    	{
			@Override
			protected IIssueDTO doInBackground() throws Exception 
			{
				IIssueDTO issueDTO = model.getSelectedIssue();
		        return issueService.findIssueById(issueDTO.getIssueId());
			}

			@Override
			protected void done() 
			{
				try{
					IIssueDTO issueDTO = get();
					if(issueDTO!=null){
						String viewId = IssueDetailView.createViewId(issueDTO.getIssueId());
				        AbstractPerspective perspective = ApplicationWindowManager.getInstance().getPerspective(IssueBrowserPerspective.ID);
				        IssueDetailView detailView = (IssueDetailView)perspective.getView(viewId);
				        if(detailView!=null){
				        	detailView.setIssueDetails(IssueDetailView.MODE_VISUALIZATION, issueDTO);
				        }
						model.setSelectedIssue(issueDTO);
					}
				}
				catch(Exception e){
					log.error("Error visualitzant el detall de l'incidència: {}", e.getMessage(), e);
				    ApplicationWindowManager.getInstance()
				            .displayErrorMessage("Error visualitzant el detall de l'incidència: " + e.getMessage(), e);
				}
				finally{
					ApplicationWindowManager.getInstance().showInProcess(false);
				}
			}
    		
    	}.execute();
    }
    
    public List<IStatusDTO> getIssueNextStatus(String project, String currentStatus)
    {
    	if(project==null){
    		throw new IllegalArgumentException("S'ha d'especificar un projecte");
    	}
    	
    	List<IStatusDTO> issueStatus = new ArrayList<IStatusDTO>();
    	IStatusDTO objStatus = null;
    	
    	try{
	    	// Es permet mantenir l'estat
	    	if(currentStatus!=null){
	    		objStatus = administrationService.findStatusById(currentStatus);
	    		issueStatus.add(objStatus);
	    	}
	    	issueStatus.addAll(administrationService.findWorkflowNextStatus(project, currentStatus));
    	}
    	catch(BusinessException e){
			log.error("Error recuperant la llista d'estats: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error recuperant la llista d'estats: " + e.getMessage(), e);
		}
    	
    	return issueStatus;
    }
    
    public void addIssueToWorkQueue(IIssueDTO issue)
    {
    	try{
    		workService.createWorkInProgressRegistry(issue.getIssueId(), userManagementService.getLoggedUsername());
    	}
    	catch(BusinessException e){
			log.error("Error afegint la incidència a la cua de treball: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error afegint la incidència a la cua de treball: " + e.getMessage(), e);
		}
    	
    	issueListView.updateIssueWidget(issue);
    	// Es refresca la vista de detall en cas de ser necessari
    	ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
    	String viewId = IssueDetailView.createViewId(issue.getIssueId());
    	IssueDetailView detailView = (IssueDetailView)windowManager.getViewFromRegistry(viewId);
    	if(detailView!=null){
    		try{
    			detailView.setIssueDetails(detailView.getMode(), issue);
    		}
    		catch(Exception e){
                log.error("Error refrescant la vista de detall de l'incidència {}: {}", new Object[]{issue.getIssueId(), e.getMessage(), e});
                ApplicationWindowManager.getInstance()
                        .displayErrorMessage("Error refrescant la vista de detall de l'incidència " + issue.getIssueId() + ": " + e.getMessage(), e);            
            }
        }
    }
    
    public void removeIssueFromWorkQueue(IIssueDTO issue)
    {
    	IIssueDTO currentIssue = model.getWorkingIssue();
    	if(issue.equals(currentIssue)){
    		model.setWorkingIssue(null);
    	}

    	ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
    	try{
    		workService.deleteIssueWorkInProgressRegistry(issue.getIssueId(), userManagementService.getLoggedUsername());
    	}
    	catch(BusinessException e){
			log.error("Error treient la incidència a la cua de treball: {}", e.getMessage(), e);
            windowManager.displayErrorMessage("Error treient la incidència a la cua de treball: " + e.getMessage(), e);
		}
    	
    	reloadIssuesView();
    	// Es refresca la vista de detall en cas de ser necessari
    	String viewId = IssueDetailView.createViewId(issue.getIssueId());
    	IssueDetailView detailView = (IssueDetailView)windowManager.getViewFromRegistry(viewId);
    	if(detailView!=null){
    		try{
    			detailView.setIssueDetails(detailView.getMode(), issue);
    		}
    		catch(Exception e){
                log.error("Error refrescant la vista de detall de l'incidència {}: {}", new Object[]{issue.getIssueId(), e.getMessage(), e});
                windowManager.displayErrorMessage("Error refrescant la vista de detall de l'incidència " + issue.getIssueId() + ": " + e.getMessage(), e);            
            }
        }
    }
    
    public void setWorkingIssue(IIssueDTO workingIssue)
    {
    	IIssueDTO currentIssue = model.getWorkingIssue();
    	if(workingIssue==null || !workingIssue.equals(currentIssue)){
    		model.setWorkingIssue(workingIssue);
    	}
    	else if(workingIssue!=null){
    		JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), 
                    "Ja s'està treballant sobre l'incidència especificada", 
                    "Advertència", 
                    JOptionPane.INFORMATION_MESSAGE);
    	}
    }
    
    public void publishIssueWork(IIssueDTO issue, Date workingDay, int minutes)
    {
    	try{
	    	String description = "Registre automàtic";
	    	IUserDTO user = userManagementService.getLoggedUserAsDTO();
	    	workService.createIssueWorkRegistry(issue.getIssueId(), workingDay, true, description, user.getUsername(), minutes);
    	}
    	catch(Exception e){
            log.error("Error publicant registre de treball: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("publicant registre de treball: " + e.getMessage(), e);            
        }
    }
    
    public void changeIssueStatus(IIssueDTO issue)
    {
    	IssueStatusEditDialog dialog = new IssueStatusEditDialog(issue);
    	if(dialog.open()==IssueStatusEditDialog.DIALOG_OK){
	    	ApplicationWindowManager.getInstance().showInProcess(true);

	    	final IVespineIssueDTO extendedIssue = (IVespineIssueDTO)issue;
    		final IStatusDTO selectedStatus = dialog.getSelectedStatus();
    		final IUserDTO assignedUser = dialog.getAssignedUser();
	    	new SwingWorker<IIssueDTO, String>()
	    	{
				@Override
				protected IIssueDTO doInBackground() throws Exception 
				{
					// TODO Fer més eficient passant només els valors requerits en el canvi
					List<ICustomValueDTO> values = issueService.findIssueCustomValues(extendedIssue.getIssueId());
			    	// S'organitzen els valors rebuts per poder recuperar-los més ràpidament
			    	HashMap<String, String> valuesMap = new HashMap<String, String>();
			    	for(ICustomValueDTO elem:values){
			    		valuesMap.put(elem.getField().getFieldId(), elem.getValue());
			    	}
					
		    		return issueService.updateIssue(extendedIssue.getIssueId(), 
		    				extendedIssue.getProject().getProjectId(), 
			    			extendedIssue.getCategory().getCategoryId(), 
			    			extendedIssue.getSeverity().getSeverityId(), 
			    			selectedStatus.getStatusId(), 
			    			extendedIssue.getSummary(), 
			    			extendedIssue.getDescription(), 
			    			assignedUser!=null ? assignedUser.getUsername() : null, 
			    			extendedIssue.getSprint()!=null ? extendedIssue.getSprint().getSprintId() : null, 
							extendedIssue.getEstimatedTime(), 
			    			extendedIssue.getSlaDate(), 
			    			extendedIssue.getResolutionDate(), 
			    			valuesMap);
				}
				
				@Override
				protected void done() 
				{
					try{
				    	IIssueDTO issue = get();
				    	ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
				    	// Actualització de la vista detall, si cal
				    	IssueDetailView detailView = (IssueDetailView)windowManager.getViewFromRegistry(IssueDetailView.createViewId(issue.getIssueId()));
				    	if(detailView!=null){
				    		detailView.setIssueDetails(IssueDetailView.MODE_VISUALIZATION, issue);
				    	}
			    		issueListView.updateIssueWidget(issue);
			    	}
			    	catch(Exception e){
			            log.error("Error modificant les dades de l'incidència: {}", e.getMessage(), e);
			            ApplicationWindowManager.getInstance()
			                    .displayErrorMessage("Error modificant les dades de l'incidència: " + e.getMessage(), e);            
			        }
					finally{
						ApplicationWindowManager.getInstance().showInProcess(false);
					}
				}
	    	}.execute();
    	}
    }
}
