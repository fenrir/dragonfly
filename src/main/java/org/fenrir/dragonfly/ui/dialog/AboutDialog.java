package org.fenrir.dragonfly.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.font.TextAttribute;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle;
import javax.swing.ImageIcon;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionlitzar
 * @author Antonio Archilla Nava
 * @version v0.0.20130324
 */
@SuppressWarnings("serial")
public class AboutDialog extends JDialog
{
    protected static final int DIALOG_WIDTH = 500;
    protected static final int COLLAPSED_HEIGHT = 350;
    protected static final int EXPANDED_HEIGHT = 600;
    
    private final Logger log = LoggerFactory.getLogger(AboutDialog.class);
    
    protected JLabel lBanner;
    protected JLabel lDescription;
    protected JButton bChangelog;
    protected JButton bClose;
    protected JTextPane changelogDetail;
    protected JScrollPane scrollDetail;
    
    protected boolean changelogVisible = false;
    
    public AboutDialog(Frame parent)
    {
        super(parent, "Informació sobre l'aplicació", true);
        
        setResizable(false);
        JPanel pRoot = new JPanel();
        pRoot.setLayout(new BorderLayout());            	
    	JComponent pTitle = createTitlePanel();
    	JComponent pComponents = createContents();
    	JComponent pBotonera = createButtonPanel();
    	pRoot.add(pTitle, BorderLayout.NORTH);
    	pRoot.add(pComponents, BorderLayout.CENTER);
    	pRoot.add(pBotonera, BorderLayout.SOUTH);
        
        getContentPane().add(pRoot);
        
        setSize(new Dimension(DIALOG_WIDTH, COLLAPSED_HEIGHT));

    	// Es centra al mig de la pantalla i es fa visible
    	Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    	setLocation((d.width - getWidth()) / 2, (d.height - getHeight()) / 2);

    	addWindowListener(new WindowAdapter()
    	{
            @Override
            public void windowClosing(WindowEvent event)
            {
                if(log.isDebugEnabled()){
                    log.debug("Tancat finestra de dialeg a partir de WindowEvent");
                }
                setVisible(false);
                dispose();
            }
    	});
        
        try{
            ImageIcon icon = new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/dialog/banner.png"));
            lBanner.setIcon(icon);
            
            InputStream is = getClass().getResourceAsStream("/org/fenrir/dragonfly/banner.html");
            StringWriter writer = new StringWriter();
            IOUtils.copy(is, writer, "UTF-8");
            String strDescription = writer.toString();            
            lDescription.setText(strDescription);
            writer.close();
            is.close();

            writer.flush();
            is = getClass().getResourceAsStream("/org/fenrir/dragonfly/changelog.html");
            writer = new StringWriter();
            IOUtils.copy(is, writer, "UTF-8");
            String strChangelog = writer.toString();
            changelogDetail.setContentType("text/html");
            changelogDetail.setText(strChangelog);
            writer.close();
            is.close();            
        }        
        catch(IOException e){
            // No hauria de passar mai
            log.error("Error obtenint URL: {}", e.getMessage(), e);
        }
    }
    
    public void open()
    {
    	setVisible(true);
    }
    
    protected JComponent createContents() 
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        changelogDetail = new JTextPane();
        changelogDetail.setEditable(false);
        scrollDetail = new JScrollPane(changelogDetail);
        scrollDetail.setVisible(false);
        
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollDetail)                
                .addContainerGap()
            )
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()                
                .addComponent(scrollDetail, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addContainerGap()
            )
        );
        
        return pContents;
    }
    
    protected JComponent createTitlePanel()
    {
        JPanel pTitle = new JPanel();
        pTitle.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(200, 200, 200)));
        lBanner = new JLabel();
        lBanner.setHorizontalAlignment(JLabel.CENTER);
        lBanner.setVerticalAlignment(JLabel.CENTER);
        lDescription = new JLabel();
        bChangelog = new JButton(new ToggleChangelogAction());  
        Map<TextAttribute, Integer> fontAttributes = new HashMap<TextAttribute, Integer>();
        fontAttributes.put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
        bChangelog.setFont(new Font("Sans-Serif",Font.BOLD, 12).deriveFont(fontAttributes));
        bChangelog.setForeground(Color.BLUE);
        bChangelog.setBorderPainted(false);
        bChangelog.setContentAreaFilled(false);
        bChangelog.setRolloverEnabled(true);

        GroupLayout layout = new GroupLayout(pTitle);
        pTitle.setLayout(layout);
        /* Grup Horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(lBanner, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(lDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                    .addComponent(bChangelog, GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lDescription, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(bChangelog, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
            )
            .addComponent(lBanner, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
        );

        return pTitle;
    }
    
    protected JComponent createButtonPanel()
    {
        JPanel pBotonera = new JPanel();
       
        bClose = new JButton(new CloseAction()); 
        
        GroupLayout layout = new GroupLayout(pBotonera);
        pBotonera.setLayout(layout);
        // Si s'ha de mostrar només el botó de tancar (bAccept)                
        /* Grup Horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(462, Short.MAX_VALUE)
                .addComponent(bClose)
                .addContainerGap()
            )
        );
        /* Grup Vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bClose)                
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

        return pBotonera;
    }	    
    
    private class ToggleChangelogAction extends AbstractAction
    {
    	public ToggleChangelogAction()
    	{
            super("changelog");
    	}
    	
        @Override
        public void actionPerformed(ActionEvent event)
        {
            changelogVisible = !changelogVisible;
            
            if(changelogVisible){
                Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
                setLocation((d.width - getWidth()) / 2, (d.height - EXPANDED_HEIGHT) / 2);
            }
            else{
                Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
                setLocation((d.width - getWidth()) / 2, (d.height - COLLAPSED_HEIGHT) / 2);
            }
            
            int height = changelogVisible ? EXPANDED_HEIGHT : COLLAPSED_HEIGHT;
            setSize(DIALOG_WIDTH, height);
            scrollDetail.setVisible(changelogVisible);
            if(changelogVisible){
                scrollDetail.getViewport().setViewPosition(new Point(0,0));
            }
        }
    }
    
    private final class CloseAction extends AbstractAction
    {
    	public CloseAction()
    	{
            super("Tancar");
    	}
    	
        @Override
        public void actionPerformed(ActionEvent event)
        {                            	
            if(log.isDebugEnabled()){
                log.debug("Tancant finestra de dialeg a partir d'un ActionEvent");
            }

            setVisible(false);
            dispose();
        }
    }
}
