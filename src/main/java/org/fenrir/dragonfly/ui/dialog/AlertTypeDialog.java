package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JCheckBox;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import org.apache.commons.lang.StringUtils;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.ui.widget.AdvancedTextField;
import org.fenrir.dragonfly.core.dto.AlertTypeFormDTO;
import org.fenrir.dragonfly.core.event.IAlertListener;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20141018
 */
@EventListener(definitions=IAlertListener.class)
public class AlertTypeDialog extends AbstractConfirmDialog implements IAlertListener 
{
    private static final long serialVersionUID = 1L; 

    // Valor de retorn
    private AlertTypeFormDTO alertType;
    // Valor original en cas de modificació
    private IAlertTypeDTO originalType;
    
    private JTextField inputName;
    private JTextField inputDescription;
    private AdvancedTextField inputIcon;
    private JCheckBox checkOldIssues;
    private JEditorPane editorRule;
    
    public AlertTypeDialog()
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear nou tipus d'alerta", new Dimension(530, 620), true);
    }
    
    public AlertTypeDialog(IAlertTypeDTO alertType)
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar tipus d'alerta", new Dimension(530, 620), true);
                
        this.originalType = alertType;
        loadAlertTypeData();
    }        

    @Override
    protected JComponent createContents() 
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        /* Definició dels components */
        JLabel lName = new JLabel("Nom", SwingConstants.RIGHT);
        JLabel lDesc = new JLabel("Descripció", SwingConstants.RIGHT);
        JLabel lAlertIcon = new JLabel("Icona", SwingConstants.RIGHT);
        JLabel lRule = new JLabel("Regla", SwingConstants.LEFT);
        
        inputName = new JTextField();
        inputDescription = new JTextField();
        inputIcon = new AdvancedTextField("/org/fenrir/dragonfly/ui/icons/file_open_16.png");
        inputIcon.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                JFileChooser fileChooser = new JFileChooser();
                if(StringUtils.isBlank(inputIcon.getText())){
                    fileChooser.setCurrentDirectory(new File("./data/icons"));
                }
                else{
                    fileChooser.setCurrentDirectory(new File(inputIcon.getText()));
                }
                
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                fileChooser.setFileFilter(new FileNameExtensionFilter("Fitxers", "png"));
                int retValue = fileChooser.showOpenDialog(null);
                if(retValue==JFileChooser.APPROVE_OPTION){
                    inputIcon.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }
        });
        checkOldIssues = new JCheckBox("Generar alertes per les incidències existents");
        editorRule = new JEditorPane();
        JScrollPane scrollEditor = new JScrollPane(editorRule);
        
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lAlertIcon, GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                            .addComponent(lRule, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(GroupLayout.Alignment.LEADING, layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                .addComponent(lDesc, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
                                .addComponent(lName, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            )
                        )
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(inputName)
                            .addComponent(inputDescription, GroupLayout.Alignment.TRAILING)
                            .addComponent(inputIcon, GroupLayout.Alignment.TRAILING)
                            .addComponent(checkOldIssues, GroupLayout.Alignment.TRAILING)
                        )
                    )
                    .addComponent(scrollEditor)
                )
                .addContainerGap()
            )
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lName)
                    .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lDesc)
                    .addComponent(inputDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lAlertIcon)
                    .addComponent(inputIcon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkOldIssues)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lRule)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollEditor, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addContainerGap()
            )
        );
        
        return pContents;
    }
    
    private void loadAlertTypeData()
    {
    	inputName.setText(originalType.getName());
        inputDescription.setText(originalType.getDescription());
        inputIcon.setText(originalType.getIcon());
        editorRule.setText(originalType.getAlertRule());
    }
    
    @Override
    protected boolean onAccept() 
    {
        String name = inputName.getText().trim();
        String description = inputDescription.getText().trim();
        String icon = inputIcon.getText().trim();
        String rule = editorRule.getText();
        
        if(StringUtils.isBlank(name)){
            setMessage("El camp NOM no pot ser buit", MSG_TYPE_ERROR);
            return false;
        }
        if(StringUtils.isBlank(description)){
            setMessage("El camp DESCRIPCIÓ no pot ser buit", MSG_TYPE_ERROR);
            return false;
        }
        if(StringUtils.isBlank(icon)){
            setMessage("El camp ICONA no pot ser buit", MSG_TYPE_ERROR);
            return false;
        }
        // TODO Comprobació icona existent i vàlida
        // TODO Comprobació icona MAX 26x26
        
        alertType = new AlertTypeFormDTO();
        // En cas de moficiació també es retorna la ID
        if(originalType!=null){
            alertType.setAlertTypeId(originalType.getAlertTypeId());
        }
        alertType.setName(name);
        alertType.setDescription(description);
        alertType.setIcon(icon);
        alertType.setAlertRule(rule);
        
        return true;
    }

    @Override
    protected boolean onCancel() 
    {
        alertType = null;
        return true;
    }
    
    public IAlertTypeDTO getFormData()
    {
        return alertType;
    }

    public boolean isAlertGenerationNeeded()
    {
        return checkOldIssues.isSelected();
    }

	@Override
	public void alertTypeCreated(IAlertTypeDTO type) 
	{
		// Res a fer...
	}

	@Override
	public void alertTypeUpdated(final IAlertTypeDTO oldType, final IAlertTypeDTO newType) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalType!=null && originalType.getAlertTypeId().equals(newType.getAlertTypeId())){
				this.originalType = (IAlertTypeDTO)newType;
				JOptionPane.showMessageDialog(this, 
		                "Les dades del tipus d'alerta han sigut modificades externament. Es procedirà a carregar-les de nou", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				loadAlertTypeData();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	alertTypeUpdated(oldType, newType);
                }
            });
		}
	}

	@Override
	public void alertTypeDeleted(final IAlertTypeDTO type) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalType!=null && originalType.getAlertTypeId().equals(type.getAlertTypeId())){
				JOptionPane.showMessageDialog(this, 
		                "El tipus d'alerta ha sigut eliminat.", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				close();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	alertTypeDeleted(type);
                }
            });
		}
	}
}
