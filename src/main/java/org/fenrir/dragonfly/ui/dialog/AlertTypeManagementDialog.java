package org.fenrir.dragonfly.ui.dialog;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.dto.IIssueAlertDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.event.IAlertListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140822
 */
@SuppressWarnings("serial")
@EventListener(definitions=IAlertListener.class)
public class AlertTypeManagementDialog extends AbstractEntityManagementDialog<IAlertTypeDTO> implements IAlertListener
{
	private static final String PARAMETER_NAME = "ALERT_TYPE";
	
	private final Logger log = LoggerFactory.getLogger(AlertTypeManagementDialog.class);
	
    public AlertTypeManagementDialog() throws Exception
    {
        super("Administrar Tipus d'Alerta", 450, 600);
    }

    @Override
    protected void loadData() throws BusinessException
    {
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

        List<IAlertTypeDTO> elements = administrationService.findAllAlertTypes();
        addElements(elements);
    }

    @Override
    protected boolean openAddDialog(Map<String, Object> parameters) 
    {
        AlertTypeDialog dialog = new AlertTypeDialog();
        boolean returnValue = dialog.open()==TagDialog.DIALOG_OK;
        IAlertTypeDTO alertType = dialog.getFormData();
        parameters.put(PARAMETER_NAME, alertType);
        boolean alertGenerationNeeded = dialog.isAlertGenerationNeeded();
        parameters.put("ALERT_GENERATION_NEEDED", alertGenerationNeeded);
        
        return returnValue;
    }
    
    @Override
    protected void doAdd(Map<String, Object> parameters) throws BusinessException
    {        
    	IAlertTypeDTO alertType = (IAlertTypeDTO)parameters.get("ALERT_TYPE");
        Boolean alertGenerationNeeded = (Boolean)parameters.get("ALERT_GENERATION_NEEDED");
        
        try{
        	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
        	administrationService.createAlertType(alertType.getName(), alertType.getDescription(), alertType.getIcon(), alertType.getAlertRule(), alertGenerationNeeded);

            // Una vegada esborrat el registre es llança una notificació
            IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
            eventNotificationService.notifyNamedEvent(IAlertListener.class, IAlertListener.EVENT_ALERT_TYPE_CREATED_ID, alertType);
        }
        catch(Exception e){
            throw new BusinessException(e);
        }
    }

    @Override
    protected boolean openModifyDialog(Map<String, Object> parameters) 
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	IAlertTypeDTO selectedType = (IAlertTypeDTO)listEntities.getSelectedValue();
        AlertTypeDialog dialog = null;
        try{
        	dialog = new AlertTypeDialog(selectedType);
        	eventNotificationService.addListener(IAlertListener.class, dialog);
        	
        	boolean returnValue = dialog.open()==TagDialog.DIALOG_OK;
        	IAlertTypeDTO alertType = dialog.getFormData();
        	parameters.put(PARAMETER_NAME, alertType);
        	boolean alertGenerationNeeded = dialog.isAlertGenerationNeeded();
        	parameters.put("ALERT_GENERATION_NEEDED", alertGenerationNeeded);
        	
        	return returnValue;
        }
        finally{
			if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
		}
        
    }

    @Override
    protected void doModify(Map<String, Object> parameters) throws BusinessException
    {        
    	IAlertTypeDTO selectedType = (IAlertTypeDTO)listEntities.getSelectedValue();        
    	IAlertTypeDTO alertType = (IAlertTypeDTO)parameters.get(PARAMETER_NAME);
        Boolean alertGenerationNeeded = (Boolean)parameters.get("ALERT_GENERATION_NEEDED");
        
        try{
        	IIssueFacade issueService = (IIssueFacade)ApplicationContext.getInstance().getRegisteredComponent(IIssueFacade.class);
        	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
        	
            List<IIssueAlertDTO> alerts = issueService.findAllAlertsByType(alertType.getAlertTypeId());
            // Es necessari que sigui mutable per poder alterar el valor al thread UI
            final MutableBoolean deletePreviousAlerts = new MutableBoolean(false);
            if(!alerts.isEmpty()){
                // La creació del alert s'ha de fer desde el thread UI
                SwingUtilities.invokeAndWait(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        int result = JOptionPane.showConfirmDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                                "Existeixen alertes prèvies pel tipus modificat. \nVoleu esborrar-les?",
                                "Esborrar alertes",
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE);
                        deletePreviousAlerts.setValue(result==JOptionPane.YES_OPTION);
                    }
                });
            }
            alertType = administrationService.updateAlertType(alertType.getAlertTypeId(), alertType.getName(), alertType.getDescription(), alertType.getIcon(), alertType.getAlertRule());
            if(deletePreviousAlerts.booleanValue()){
            	issueService.validateIssueAlerts(alertType.getAlertTypeId());
            }
            if(alertGenerationNeeded){
                issueService.createIssueAlerts(alertType.getAlertTypeId());
            }

            // Una vegada esborrat el registre es llança una notificació
            IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
            eventNotificationService.notifyNamedEvent(IAlertListener.class, IAlertListener.EVENT_ALERT_TYPE_UPDATED_ID, selectedType, alertType);
        }
        catch(Exception e){
            throw new BusinessException(e);
        }
    }

    @Override
    protected void doDelete(Map<String, Object> parameters) throws BusinessException
    {
        try{
        	IAlertTypeDTO selectedElement = (IAlertTypeDTO)listEntities.getSelectedValue();
        	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
            administrationService.deleteAlertType(selectedElement.getAlertTypeId());

            // Una vegada esborrat el registre es llança una notificació
            IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
            eventNotificationService.notifyNamedEvent(IAlertListener.class, IAlertListener.EVENT_ALERT_TYPE_DELETED_ID, selectedElement);
        }
        catch(Exception e){
            throw new BusinessException(e);
        }
    }
 
    @Override
	public void alertTypeCreated(IAlertTypeDTO type) 
    {
    	reloadData();
	}

	@Override
	public void alertTypeUpdated(IAlertTypeDTO oldType, IAlertTypeDTO newType) 
	{
		reloadData();
	}

	@Override
	public void alertTypeDeleted(IAlertTypeDTO type) 
	{
		reloadData();
	}

	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
				log.error("Error recarregant els tipus d'alerta: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant els tipus d'alerta: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}