package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.vespine.core.dto.IVespineCustomFieldDTO;
import org.fenrir.vespine.core.dto.IVespineProjectDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.dto.CustomFieldFormDTO;
import org.fenrir.dragonfly.core.event.ICustomFieldListener;
import org.fenrir.dragonfly.core.provider.CustomFieldDataProvider;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20141018
 */
@EventListener(definitions=ICustomFieldListener.class)
public class CustomFieldDialog extends AbstractConfirmDialog implements ICustomFieldListener 
{
	private static final long serialVersionUID = -6651407984790572063L;
	
	private final Logger log = LoggerFactory.getLogger(CustomFieldDialog.class);
	
	// Valor de retorn
	private CustomFieldFormDTO field;
	// Valor original en cas de modificació
	private IVespineCustomFieldDTO originalField;
	
	private JTextField inputName;
	private JComboBox comboFieldTypes;
	private JComboBox comboDataProvider;
	private JCheckBox checkMandatory;
	private JList listProjects;
	
	public CustomFieldDialog()
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear nou camp personalitzat", new Dimension(530, 620), true);
        
        loadData();
    }
	
	public CustomFieldDialog(IVespineCustomFieldDTO field)
	{
		super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar camp personalitzat", new Dimension(530, 620), true);
		this.originalField = field;
		
		loadData();
		loadCustomFieldData();
	}
	
	@Override
	protected JComponent createContents() 
	{
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        /* Definició dels components */
        JLabel lName = new JLabel("Nom", SwingConstants.RIGHT);
        JLabel lFieldType = new JLabel("Tipus de dada", SwingConstants.RIGHT);
        JLabel lDataProviderId = new JLabel("Id proveidor dades", SwingConstants.RIGHT);
        JLabel lProjects = new JLabel("Projectes", SwingConstants.RIGHT);
        inputName = new JTextField();
        comboFieldTypes = new JComboBox(new DefaultComboBoxModel());
        comboFieldTypes.addItemListener(new ItemListener() 
        {
			@Override
			public void itemStateChanged(ItemEvent event) 
			{
				if(ItemEvent.SELECTED==event.getStateChange())
				{
					IFieldType selectedType = (IFieldType)event.getItem();
					comboDataProvider.setEnabled(selectedType.needDataProviderId());		
					if(selectedType.needDataProviderId()){
						CustomFieldDataProvider dataProvider = (CustomFieldDataProvider)ApplicationContext.getInstance().getRegisteredComponent(CustomFieldDataProvider.class);						
						DefaultComboBoxModel model = (DefaultComboBoxModel)comboDataProvider.getModel();
						model.removeAllElements();
						try{
							List<Object> data = dataProvider.getAllProviderData(selectedType);
							for(Object elem:data){
								model.addElement(elem);
							}
						}
						catch(BusinessException e){
							log.error("Error recuperant la llista de valors associats al camp: {}", e.getMessage(), e);
							ApplicationWindowManager.getInstance()
					        		.displayErrorMessage("Error recuperant la llista de valors associats al camp: " + e.getMessage(), e);
						}
					}
				}
			}
		});
        comboDataProvider = new JComboBox(new DefaultComboBoxModel());
        checkMandatory = new JCheckBox("Obligatori");
        listProjects = new JList(new DefaultListModel());
        JScrollPane scrollProjects = new JScrollPane(listProjects);
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
				.addComponent(lName, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(lFieldType, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(lDataProviderId, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(lProjects, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                .addComponent(comboFieldTypes, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                .addComponent(comboDataProvider, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                .addComponent(checkMandatory)
                .addComponent(scrollProjects, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
            )
            .addContainerGap()
        );
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lName)
                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lFieldType)
                .addComponent(comboFieldTypes)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lDataProviderId)
                .addComponent(comboDataProvider)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(checkMandatory)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lProjects)
                .addComponent(scrollProjects, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
            )
            .addContainerGap()
        );
        
        return pContents;
	}
	
	@SuppressWarnings("unchecked")
	private void loadData()
	{
		try{
			IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
			for(IFieldType type:administrationService.getAllFieldTypes()){
				((DefaultComboBoxModel)comboFieldTypes.getModel()).addElement(type);
			}
			List<IProjectDTO> projects = administrationService.findAllProjects();
			for(IProjectDTO project:projects){
				((DefaultListModel)listProjects.getModel()).addElement(project);
			}
		}
		catch(BusinessException e){
			log.error("Error recuperant la llista d'estats: {}", e.getMessage(), e);
			ApplicationWindowManager.getInstance()
	        		.displayErrorMessage("Error recuperant la llista d'estats: " + e.getMessage(), e);
		}
	}
	
	public ICustomFieldDTO getFormData()
	{
		return field;
	}
	
	private void loadCustomFieldData()
	{
		inputName.setText(originalField.getName());
		comboFieldTypes.getModel().setSelectedItem(originalField.getFieldType());
		if(originalField.getDataProviderId()!=null){
			CustomFieldDataProvider dataProvider = (CustomFieldDataProvider)ApplicationContext.getInstance().getRegisteredComponent(CustomFieldDataProvider.class);
			try{
				Object data = dataProvider.getProviderDataById(originalField.getFieldType(), originalField.getDataProviderId());
				comboDataProvider.setSelectedItem(data);
			}
			catch(BusinessException e){
				log.error("Error recuperant el valor sel.leccionat: {}", e.getMessage(), e);
				ApplicationWindowManager.getInstance()
		        		.displayErrorMessage("Error recuperant el valor sel.leccionat: " + e.getMessage(), e);
			}
		}		
		checkMandatory.setSelected(originalField.isMandatory());
		Collection<IVespineProjectDTO> projects = originalField.getProjects();
		for(IVespineProjectDTO project:projects){
			int index = ((DefaultListModel)listProjects.getModel()).indexOf(project);
			if(index>=0){
				listProjects.addSelectionInterval(index, index);
			}
		}
	}
	
	@Override
	protected boolean onAccept()
	{
		CustomFieldDataProvider dataProvider = (CustomFieldDataProvider)ApplicationContext.getInstance().getRegisteredComponent(CustomFieldDataProvider.class);
		
		String name = inputName.getText().trim();
		IFieldType type = (IFieldType)comboFieldTypes.getSelectedItem();
		Object selectedData = comboDataProvider.getSelectedItem();
    	/* Validacions */
    	if(StringUtils.isBlank(name)){
            setMessage("El camp 'nom' no pot ser buit", MSG_TYPE_ERROR);
            return false;
        }
    	if(type==null){
    		setMessage("Ha de sel.leccionar un tipus de dada pel camp", MSG_TYPE_ERROR);
            return false;
    	}
    	if(type.needDataProviderId() && selectedData==null){
    		setMessage("El camp 'Id proveidor' no pot ser buit", MSG_TYPE_ERROR);
            return false;
    	}
    	// En el cas d'haver modificat el tipus de dada, es mostra un avís
    	if(originalField!=null 
    			&& (type.equals(originalField.getFieldType())
    					|| selectedData!=null && selectedData.equals(originalField.getDataProviderId())
    			)
    	){
    		if(JOptionPane.OK_OPTION != JOptionPane.showConfirmDialog(CustomFieldDialog.this, 
                    "Modificant el tipus del camp esborrareu els valors associats.\n Voleu continuar?", 
                    "Confirmar modificació", 
                    JOptionPane.OK_CANCEL_OPTION, 
                    JOptionPane.QUESTION_MESSAGE)){
    			return false;
    		}    		
    	}
    	
    	field = new CustomFieldFormDTO();
    	// En cas de modificació també es retorna la ID
        if(originalField!=null){
        	field.setFieldId(originalField.getFieldId());
        }
    	field.setName(name);
    	field.setFieldType(type);    	
    	if(type.needDataProviderId()){
    		String dataId = dataProvider.getProviderDataId(type, selectedData);
    		field.setDataProviderId(dataId);
    	}
    	field.setMandatory(checkMandatory.isSelected());  
    	List<IVespineProjectDTO> selectedProjects = new ArrayList<IVespineProjectDTO>();
    	CollectionUtils.addAll(selectedProjects, listProjects.getSelectedValues());    	
    	field.setProjects(selectedProjects);
		
		return true;
	}
	
	@Override
	protected boolean onCancel() 
	{
		field = null;
		return true;
	}

	@Override
	public void customFieldCreated(ICustomFieldDTO customField) 
	{
		// Res a fer...
	}

	@Override
	public void customFieldUpdated(final ICustomFieldDTO oldCustomField, final ICustomFieldDTO newCustomField) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalField!=null && originalField.getFieldId().equals(newCustomField.getFieldId())){
				this.originalField = (IVespineCustomFieldDTO)newCustomField;
				JOptionPane.showMessageDialog(this, 
		                "Les dades del camp han sigut modificades externament. Es procedirà a carregar-les de nou", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				loadCustomFieldData();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	customFieldUpdated(oldCustomField, newCustomField);
                }
            });
		}
	}

	@Override
	public void customFieldDeleted(final ICustomFieldDTO customField) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalField!=null && originalField.getFieldId().equals(customField.getFieldId())){
				JOptionPane.showMessageDialog(this, 
		                "El camp personalitzat ha sigut eliminat.", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				close();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	customFieldDeleted(customField);
                }
            });
		}
	}
}
