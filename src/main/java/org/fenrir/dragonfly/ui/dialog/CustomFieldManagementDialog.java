package org.fenrir.dragonfly.ui.dialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.vespine.core.dto.IVespineCustomFieldDTO;
import org.fenrir.vespine.core.dto.IVespineProjectDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.event.ICustomFieldListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140817
 */
@EventListener(definitions=ICustomFieldListener.class)
public class CustomFieldManagementDialog extends AbstractEntityManagementDialog<ICustomFieldDTO> implements ICustomFieldListener 
{
	private static final long serialVersionUID = -864037464079575603L;
	
	private final Logger log = LoggerFactory.getLogger(CustomFieldManagementDialog.class);
	
	private static final String PARAMETER_NAME = "CUSTOM_FIELD";
	
    public CustomFieldManagementDialog() throws Exception
    {
        super("Administrar Camps Personalitzats", 450, 600);
    }

    @Override
    protected void loadData() throws BusinessException
    {
    	IAdministrationFacade administratrionService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
        List<ICustomFieldDTO> fields = administratrionService.findAllCustomFields();
        addElements(fields);
    }
    
    @Override
    protected boolean openAddDialog(Map<String, Object> parameters) 
    {
    	CustomFieldDialog dialog = new CustomFieldDialog();
        boolean returnValue = dialog.open()==CustomFieldDialog.DIALOG_OK;
        ICustomFieldDTO field = dialog.getFormData();
        parameters.put(PARAMETER_NAME, field);
        
        return returnValue;
    }
    
    @Override
    protected void doAdd(Map<String, Object> parameters) throws Exception
    {
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
    	IVespineCustomFieldDTO field = (IVespineCustomFieldDTO)parameters.get(PARAMETER_NAME);
    	List<String> projectIds = new ArrayList<String>();
    	for(IVespineProjectDTO project:field.getProjects()){
    		projectIds.add(project.getProjectId());
    	}
    	administrationService.createCustomField(field.getName(), 
    			field.getFieldType(), 
    			field.getDataProviderId(), 
    			field.isMandatory(), 
    			projectIds);
    }

    @Override
    protected boolean openModifyDialog(Map<String, Object> parameters) 
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	IVespineCustomFieldDTO selectedField = (IVespineCustomFieldDTO)listEntities.getSelectedValue();
    	CustomFieldDialog dialog = null;
    	try{
    		dialog = new CustomFieldDialog(selectedField);
    		eventNotificationService.addListener(ICustomFieldListener.class, dialog);
    		
    		boolean returnValue = dialog.open()==CustomFieldDialog.DIALOG_OK;
    		ICustomFieldDTO field = dialog.getFormData();
    		parameters.put(PARAMETER_NAME, field);
    		
    		return returnValue;
    	}
    	finally{
			if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
		}
    }
    
    @Override
    protected void doModify(Map<String, Object> parameters) throws Exception
    {        
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
    	IVespineCustomFieldDTO field = (IVespineCustomFieldDTO)parameters.get(PARAMETER_NAME);
    	List<String> projectIds = new ArrayList<String>();
    	for(IVespineProjectDTO project:field.getProjects()){
    		projectIds.add(project.getProjectId());
    	}
    	administrationService.updateCustomField(field.getFieldId(), 
    			field.getName(), 
    			field.getFieldType(), 
    			field.getDataProviderId(), 
    			field.isMandatory(), 
    			projectIds);
    }

    @Override
    protected boolean openDeleteDialog(Map<String, Object> parameters)
    {
        return JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(CustomFieldManagementDialog.this, 
                "Eliminant el camp personalitzat també esborrareu els valors associats.\n Voleu eliminar el registre sel.leccionat?", 
                "Confirmar eliminació", 
                JOptionPane.OK_CANCEL_OPTION, 
                JOptionPane.QUESTION_MESSAGE);       
    }
    
    @Override
    protected void doDelete(Map<String, Object> parameters) throws Exception
    {
    	ICustomFieldDTO selectedField = (ICustomFieldDTO)listEntities.getSelectedValue();
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
    	administrationService.deleteCustomField(selectedField.getFieldId());
    }

	@Override
	public void customFieldCreated(ICustomFieldDTO customField) 
	{
		reloadData();
	}

	@Override
	public void customFieldUpdated(ICustomFieldDTO oldCustomField, ICustomFieldDTO newCustomField) 
	{
		reloadData();
	}

	@Override
	public void customFieldDeleted(ICustomFieldDTO customField) 
	{
		reloadData();
	}
    
	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
				log.error("Error recarregant els camps personalitzats: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant els camps personalitzats: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}
