package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import org.apache.commons.lang.StringUtils;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;
import org.fenrir.dragonfly.ui.externalize.widget.TableTooltipCellRenderer;
import org.fenrir.dragonfly.ui.widget.DictionaryItemTableModel;
import org.fenrir.dragonfly.core.dto.DictionaryFormDTO;
import org.fenrir.dragonfly.core.dto.DictionaryItemFormDTO;
import org.fenrir.dragonfly.core.event.IDictionaryListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20141018
 */
@EventListener(definitions=IDictionaryListener.class)
public class DictionaryDialog extends AbstractConfirmDialog implements ActionListener, IDictionaryListener 
{
	private static final long serialVersionUID = 7614223946503364177L;
	
	private static final String ACTION_COMMAND_ADD_ITEM = "ADD_ITEM";
	private static final String ACTION_COMMAND_MODIFY_ITEM = "MODIFY_ITEM";
    private static final String ACTION_COMMAND_REMOVE_ITEM = "REMOVE_ITEM";
    private static final String ACTION_COMMAND_CANCEL_MODIFICATION = "CANCEL_MODIFICATION";
	
	// Valor de retorn
	private DictionaryFormDTO dictionary;
	// Valor original en cas de modificació
	private IDictionaryDTO originalDictionary;
	// Item sel.leccionat
	private IDictionaryItemDTO selectedItem;
	
	private JTextField inputName;
	private JTextField inputDescription;
	private JTextField inputItemValue;
	private JTextField inputItemDescription;
	private JButton bAddItem;
	private JButton bModifyItem;
    private JButton bRemoveItem;
    private JButton bCancelModification;
    private JTable tableItems;
    private DictionaryItemTableModel modelTableItems;
	
	public DictionaryDialog()
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear nou diccionari", new Dimension(530, 620), true);
    }
	
	public DictionaryDialog(IDictionaryDTO dictionary)
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar diccionari existent", new Dimension(530, 620), true);
    	this.originalDictionary = dictionary;
    	
    	loadDictionaryData();
    }
	
	@Override
	protected JComponent createContents() 
	{
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        /* Definició dels components */
        JLabel lName = new JLabel("Nom", SwingConstants.RIGHT);
        JLabel lDescription = new JLabel("Descripció", SwingConstants.RIGHT);
        inputName = new JTextField();
        inputDescription = new JTextField();
        JLabel lItems = new JLabel("Items");
        JLabel lItemValue = new JLabel("Valor");
        JLabel lItemDescription = new JLabel("Descripció");
        inputItemValue = new JTextField();
        inputItemDescription = new JTextField();
        modelTableItems = new DictionaryItemTableModel();
        tableItems = new JTable(modelTableItems);
        tableItems.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // S'afegeixen tooltips per mostrar els valors de les cel.les de la taula
        for(int i=0; i<tableItems.getColumnCount(); i++){
        	TableColumn column = tableItems.getColumnModel().getColumn(i);
        	column.setCellRenderer(new TableTooltipCellRenderer());
        }
        tableItems.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
        {
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				int index = tableItems.getSelectedRow();
				if(index>=0){
					IDictionaryItemDTO item = modelTableItems.getModelData().get(index);
					setSelectedItem(item);
				}
				else{
					setSelectedItem(null);
				}
			}
		});
        JScrollPane scrollItems = new JScrollPane(tableItems);
        // Botons
        bAddItem = new JButton("Afegir");
        bAddItem.setActionCommand(ACTION_COMMAND_ADD_ITEM);
        bAddItem.addActionListener(this);
        bModifyItem = new JButton("Modificar");
        bModifyItem.setActionCommand(ACTION_COMMAND_MODIFY_ITEM);
        bModifyItem.addActionListener(this);
        bModifyItem.setVisible(false);
        bRemoveItem = new JButton("Eliminar");
        bRemoveItem.setActionCommand(ACTION_COMMAND_REMOVE_ITEM);
        bRemoveItem.addActionListener(this);
        bRemoveItem.setVisible(false);
        bCancelModification = new JButton("Cancelar");
        bCancelModification.setActionCommand(ACTION_COMMAND_CANCEL_MODIFICATION);
        bCancelModification.addActionListener(this);
        bCancelModification.setVisible(false);

        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
            		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
        				.addComponent(lName, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        				.addComponent(lDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    )
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                        .addComponent(inputDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                    )
                )
                .addComponent(lItems)
                .addGroup(layout.createSequentialGroup()
            		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
        				.addComponent(lItemValue, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        				.addComponent(lItemDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    )
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(inputItemValue, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                        .addComponent(inputItemDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                    )
                )
                .addComponent(scrollItems, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createSequentialGroup()
                	.addGap(20, 20, Short.MAX_VALUE)
                	.addComponent(bAddItem, 100, 100, 100)
                	.addComponent(bModifyItem, 100, 100, 100)
                	.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                	.addComponent(bRemoveItem, 100, 100, 100)
                	.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                	.addComponent(bCancelModification, 100, 100, 100)
        		)
            )
            .addContainerGap()
        );
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lName)
                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lDescription)
                .addComponent(inputDescription)
            )
            .addGap(20)
            .addComponent(lItems)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lItemValue)
                .addComponent(inputItemValue)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lItemDescription)
                .addComponent(inputItemDescription)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(bAddItem)
                .addComponent(bModifyItem)
                .addComponent(bRemoveItem)
                .addComponent(bCancelModification)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(scrollItems, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addContainerGap()
        );
        
        return pContents;
	}

	public IDictionaryDTO getFormData()
	{
		return dictionary;
	}
	
	private void loadDictionaryData()
	{
		inputName.setText(originalDictionary.getName());
    	inputDescription.setText(originalDictionary.getDescription());
    	/* S'afegeixen els elements a la taula. No es fa a través del mètode addElement del model perquè notificaria cada inserció.
		 * Al final del procés es farà una notificació indicant els canvis a la taula. D'aquesta manera es millora el rendiment.
		 */
		for(IDictionaryItemDTO elem:originalDictionary.getDictionaryItems()){
			modelTableItems.getModelData().add(elem);
		}
		// Es notifica els canvis a la taula ja que el mètode d'inserció utilitzat no ho fa
		modelTableItems.fireTableDataChanged();
	}
	
	@Override
	protected boolean onAccept() 
	{
		String name = inputName.getText().trim();
		String description = inputDescription.getText().trim();
    	/* Validacions */
    	if(StringUtils.isBlank(name)){
            setMessage("El camp 'nom' no pot ser buit", MSG_TYPE_ERROR);
            return false;
        }
    	if(StringUtils.isBlank(description)){
    		setMessage("El camp 'descripció' no pot ser buit", MSG_TYPE_ERROR);
            return false;
    	}
    	
    	dictionary = new DictionaryFormDTO();
    	// En cas de moficiació també es retorna la ID
        if(originalDictionary!=null){
        	dictionary.setDictionaryId(originalDictionary.getDictionaryId());
        }
    	dictionary.setName(name);
    	dictionary.setDescription(description);
    	dictionary.setDictionaryItems(modelTableItems.getModelData());
    	
    	return true;
	}

	@Override
	protected boolean onCancel() 
	{
		dictionary = null;
		return true;
	}
	
	private void setSelectedItem(IDictionaryItemDTO selectedItem)
	{
		this.selectedItem = selectedItem;
		
		String strValue = null;
		String strDescription = null;
		if(selectedItem!=null){
			strValue = selectedItem.getValue();
			strDescription = selectedItem.getDescription();
		}
		inputItemValue.setText(strValue);
		inputItemDescription.setText(strDescription);
		
		bAddItem.setVisible(selectedItem==null);
		bRemoveItem.setVisible(selectedItem!=null);
		bModifyItem.setVisible(selectedItem!=null);
		bCancelModification.setVisible(selectedItem!=null);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if(ACTION_COMMAND_ADD_ITEM.equals(event.getActionCommand())
				|| ACTION_COMMAND_MODIFY_ITEM.equals(event.getActionCommand())){
			// Comprobació dels valors introduits
			String strValue = inputItemValue.getText();
			String strDescription = inputItemDescription.getText();
			if(StringUtils.isBlank(strValue)){
				setMessage("Ha d'introduir un valor en camp 'valor'", MSG_TYPE_ERROR);
				return;
			}

			if(ACTION_COMMAND_ADD_ITEM.equals(event.getActionCommand())){
				DictionaryItemFormDTO item = new DictionaryItemFormDTO();
				item.setValue(strValue);
				item.setDescription(strDescription);
				modelTableItems.addData(item);
			}
			else{
				if(!(selectedItem instanceof DictionaryItemFormDTO)){
					DictionaryItemFormDTO item = new DictionaryItemFormDTO();
					item.setItemId(selectedItem.getItemId());
					item.setValue(strValue);
					item.setDescription(strDescription);
					int index = modelTableItems.getModelData().indexOf(selectedItem);
					modelTableItems.removeData(selectedItem);
					modelTableItems.getModelData().add(index, item);
				}
				else{
					((DictionaryItemFormDTO)selectedItem).setValue(strValue);
					((DictionaryItemFormDTO)selectedItem).setDescription(strDescription);
				}
				tableItems.getSelectionModel().clearSelection();
				modelTableItems.fireTableDataChanged();
			}
		}
		else if(ACTION_COMMAND_REMOVE_ITEM.equals(event.getActionCommand())){
			int selectedIndex = tableItems.getSelectedRow();
			IDictionaryItemDTO item = modelTableItems.getModelData().get(selectedIndex);
			modelTableItems.removeData(item);
		}
		else if(ACTION_COMMAND_CANCEL_MODIFICATION.equals(event.getActionCommand())){
			tableItems.getSelectionModel().clearSelection();
		}
	}

	@Override
	public void dictionaryCreated(IDictionaryDTO dictionary) 
	{
		// Res a fer...
	}

	@Override
	public void dictionaryUpdated(final IDictionaryDTO oldDictionary, final IDictionaryDTO newDictionary) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalDictionary!=null && originalDictionary.getDictionaryId().equals(newDictionary.getDictionaryId())){
				this.originalDictionary = (IDictionaryDTO)newDictionary;
				JOptionPane.showMessageDialog(this, 
		                "Les dades del diccionari han sigut modificades externament. Es procedirà a carregar-les de nou", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				loadDictionaryData();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	dictionaryUpdated(oldDictionary, newDictionary);
                }
            });
		}
	}

	@Override
	public void dictionaryDeleted(final IDictionaryDTO dictionary) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalDictionary!=null && originalDictionary.getDictionaryId().equals(dictionary.getDictionaryId())){
				JOptionPane.showMessageDialog(this, 
		                "El diccionari ha sigut eliminat.", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				close();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	dictionaryDeleted(dictionary);
                }
            });
		}
	}
}
