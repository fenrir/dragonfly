package org.fenrir.dragonfly.ui.dialog;

import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.dragonfly.core.event.IDictionaryListener;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.dto.IDictionaryDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140817
 */
@EventListener(definitions=IDictionaryListener.class)
public class DictionaryManagementDialog extends AbstractEntityManagementDialog<IDictionaryDTO> implements IDictionaryListener 
{
	private static final long serialVersionUID = -5444457152212107259L;
	
	private final Logger log = LoggerFactory.getLogger(DictionaryManagementDialog.class);
	
	private static final String PARAMETER_NAME = "DICTIONARY";
	
    public DictionaryManagementDialog() throws Exception
    {
        super("Administrar Diccionaris", 450, 600);
    }

    @Override
    protected void loadData() throws BusinessException
    {
        IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
        List<IDictionaryDTO> dictionaries = administrationService.findAllDictionaries();
        addElements(dictionaries);
    }
    
    @Override
    protected boolean openAddDialog(Map<String, Object> parameters) 
    {
    	DictionaryDialog dialog = new DictionaryDialog();
        boolean returnValue = dialog.open()==DictionaryDialog.DIALOG_OK;
        IDictionaryDTO dictionary = dialog.getFormData();
        parameters.put(PARAMETER_NAME, dictionary);
        
        return returnValue;
    }
    
    @Override
    protected void doAdd(Map<String, Object> parameters) throws Exception
    {
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
    	IDictionaryDTO dictionary = (IDictionaryDTO)parameters.get(PARAMETER_NAME);
    	administrationService.createDictionary(dictionary.getName(), 
    			dictionary.getDescription(), 
    			dictionary.getDictionaryItems());
    }

    @Override
    protected boolean openModifyDialog(Map<String, Object> parameters) 
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	IDictionaryDTO selectedDictionary = (IDictionaryDTO)listEntities.getSelectedValue();
    	DictionaryDialog dialog = null;
    	try{
    		dialog = new DictionaryDialog(selectedDictionary);
    		eventNotificationService.addListener(IDictionaryListener.class, dialog);
    		
    		boolean returnValue = dialog.open()==DictionaryDialog.DIALOG_OK;
    		IDictionaryDTO dictionary = dialog.getFormData();
    		parameters.put(PARAMETER_NAME, dictionary);
    		
    		return returnValue;
    	}
    	finally{
			if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
		}
    }
    
    @Override
    protected void doModify(Map<String, Object> parameters) throws Exception
    {        
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
    	IDictionaryDTO dictionary = (IDictionaryDTO)parameters.get(PARAMETER_NAME);
    	administrationService.updateDictionary(dictionary.getDictionaryId(), 
    			dictionary.getName(), 
    			dictionary.getDescription(), 
    			dictionary.getDictionaryItems());
    }

    @Override
    protected void doDelete(Map<String, Object> parameters) throws Exception
    {
    	IDictionaryDTO selectedDictionary = (IDictionaryDTO)listEntities.getSelectedValue();
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
    	administrationService.deleteDictionary(selectedDictionary.getDictionaryId());
    }

	@Override
	public void dictionaryCreated(IDictionaryDTO dictionary) 
	{
		reloadData();
	}

	@Override
	public void dictionaryUpdated(IDictionaryDTO oldDictionary, IDictionaryDTO newDictionary) 
	{
		reloadData();
	}

	@Override
	public void dictionaryDeleted(IDictionaryDTO dictionary) 
	{
		reloadData();
	}
	
	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
				log.error("Error recarregant els diccionaris: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant els diccionaris: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}
