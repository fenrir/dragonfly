package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.dragonfly.ApplicationConstants;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.3.20140718
 */
@SuppressWarnings("serial")
public class InitialConfigurationDialog extends AbstractConfirmDialog 
{
	private JComboBox comboModes;
	// Opcions mode CLIENT
	private JLabel lUrlServidor;
	private JTextField inputUrlServidor;
	
	private String applicationMode;
	private String serverUrl;
	
	public InitialConfigurationDialog()
	{
		super(ApplicationWindowManager.getInstance().getMainWindow(), "Configuració inicial", new Dimension(530, 220), true);

//      setIconAsResource("/org/fenrir/yggdrasil/ui/icons/gear_48.png");
	}

	@Override
	protected JComponent createContents() 
	{
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
    
        /* Definició dels components */
        JLabel lMode = new JLabel("Mode");
        comboModes = new JComboBox(new Object[]{ApplicationConstants.APPLICATION_MODE_STANDALONE, ApplicationConstants.APPLICATION_MODE_CLIENT});
        comboModes.addItemListener(new ItemListener() 
        {
			@Override
			public void itemStateChanged(ItemEvent event) 
			{
				if(ItemEvent.SELECTED==event.getStateChange()){
					if(ApplicationConstants.APPLICATION_MODE_CLIENT.equals(event.getItem())){
						lUrlServidor.setVisible(true);
				        inputUrlServidor.setVisible(true);
					}
					else{
						lUrlServidor.setVisible(false);
				        inputUrlServidor.setVisible(false);
					}
				}
			}
		});
        lUrlServidor = new JLabel("Url Servidor");
        inputUrlServidor = new JTextField();
        
        lUrlServidor.setVisible(false);
        inputUrlServidor.setVisible(false);

        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
            		.addComponent(lMode, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            		.addComponent(lUrlServidor, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            	)
            	.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            	.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
        			.addComponent(comboModes, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
            		.addComponent(inputUrlServidor, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
            	)
                .addContainerGap()
            )
        );

        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lMode, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboModes, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lUrlServidor, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputUrlServidor, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                )
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );
        
        return pContents;
	}
	
	@Override
	protected boolean onAccept() 
	{
		applicationMode = (String)comboModes.getSelectedItem();
		if(ApplicationConstants.APPLICATION_MODE_CLIENT.equals(applicationMode)){
			serverUrl = inputUrlServidor.getText();
			// TODO Prova de connexió amb el servidor
		}
		
		return true;
	}

	@Override
	protected boolean onCancel() 
	{
		return true;
	}
	
	public String getApplicationMode()
	{
		return applicationMode;
	}
	
	public String getServerUrl()
	{
		return serverUrl;
	}
}