package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.vespine.core.dto.IVespineCategoryDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.widget.ProviderDataTableModel;
import org.fenrir.dragonfly.core.dto.CategoryFormDTO;
import org.fenrir.dragonfly.core.event.ICategoryListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20141018
 */
@EventListener(definitions=ICategoryListener.class)
public class IssueCategoryDialog extends AbstractConfirmDialog implements ActionListener, ICategoryListener
{
	private static final long serialVersionUID = 5346417075864801363L;
	
	private static final String ACTION_COMMAND_ADD_MAPPED_DATA = "ADD_MAPPED_DATA";
    private static final String ACTION_COMMAND_REMOVE_MAPPED_DATA = "REMOVE_MAPPED_DATA";

    private final Logger log = LoggerFactory.getLogger(IssueCategoryDialog.class);
    
    // Valor de retorn
    private CategoryFormDTO category;
    // Valor original en cas de modificació
    private IVespineCategoryDTO originalCategory;
    
    private JTextField inputName;
    private JButton bAddMappedData;
    private JButton bRemoveMappedData;
    private JTable tableMappedData;
    private ProviderDataTableModel<IProviderElementDTO> modelMappedData;
    
    public IssueCategoryDialog()
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear nova categoria", new Dimension(530, 620), true);
    }
    
    public IssueCategoryDialog(IVespineCategoryDTO category)
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar categoria existent", new Dimension(530, 620), true);
    	this.originalCategory = category;
    	loadCategoryData();
    }
    
    @Override
    protected JComponent createContents() 
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        /* Definició dels components */
        JLabel lName = new JLabel("Nom", SwingConstants.RIGHT);
        JLabel lMappedData = new JLabel("Registres externs associats");

        inputName = new JTextField();
        
        bAddMappedData = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/mapping_select_16.png")));
        bAddMappedData.addActionListener(this);
        bAddMappedData.setActionCommand(ACTION_COMMAND_ADD_MAPPED_DATA);
        bRemoveMappedData = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/mapping_remove_16.png")));
        // Inciialment desactivat perquè no hi ha cap registre sel.leccionat
        bRemoveMappedData.setEnabled(false);
        bRemoveMappedData.setActionCommand(ACTION_COMMAND_REMOVE_MAPPED_DATA);
        bRemoveMappedData.addActionListener(this);
        modelMappedData = new ProviderDataTableModel<IProviderElementDTO>();
        tableMappedData = new JTable(modelMappedData);
        // S'ajusta el tamany de la primera columna
        TableColumn col = tableMappedData.getColumnModel().getColumn(0);        
        col.setMaxWidth(20);
        tableMappedData.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
        {
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				boolean selectedRegistry = tableMappedData.getSelectedRow()>=0;
				bRemoveMappedData.setEnabled(selectedRegistry);
			}
		});
        JScrollPane scrollProviderData = new JScrollPane(tableMappedData);
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                		.addComponent(lName, GroupLayout.PREFERRED_SIZE, 50, 50)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                    )
                    .addGroup(layout.createSequentialGroup()
                		.addComponent(lMappedData, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(bAddMappedData, 22, 22, 22)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(bRemoveMappedData, 22, 22, 22)
            		)
                    .addComponent(scrollProviderData)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
	        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	        .addGroup(layout.createSequentialGroup()
	            .addContainerGap()
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lName)
	                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addGap(20)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lMappedData)
	                .addComponent(bAddMappedData)
	                .addComponent(bRemoveMappedData)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)	            
	            .addComponent(scrollProviderData, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
	            .addContainerGap()
	        )
	    );
        
        return pContents;
    }
    
    private void loadCategoryData()
    {
    	inputName.setText(originalCategory.getName());
    	// Carregar les dades mapejades
    	for(IProviderElementDTO elem:originalCategory.getProviderCategories()){
    		modelMappedData.addData(elem);
    	}
    }
    
    @Override
    protected boolean onAccept() 
    {
    	String name = inputName.getText().trim();
    	
    	category = new CategoryFormDTO();
    	// En cas de moficiació també es retorna la ID
        if(originalCategory!=null){
        	category.setCategoryId(originalCategory.getCategoryId());
        }
    	category.setName(name);
    	category.setProviderCategories(modelMappedData.getModelData());
    	
    	return true;
    }
    
    @Override
    protected boolean onCancel() 
    {
        category = null;
        return true;
    }
    
    public IVespineCategoryDTO getFormData()
    {
        return category;
    }
    
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if(ACTION_COMMAND_ADD_MAPPED_DATA.equals(event.getActionCommand())){
			IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
			
			try{
				List<IProviderElementDTO> providerCategories = administrationService.findAllProviderCategories();
				List<IProviderElementDTO> selectedCategories = modelMappedData.getModelData();
				ProviderDataDialog dialog = new ProviderDataDialog(providerCategories, selectedCategories);
		        boolean addValue = dialog.open()==ProviderDataDialog.DIALOG_OK;
		        if(addValue){
		        	String selectedProvider = dialog.getSelectedProvider();
		        	List<IProviderElementDTO> selectedData = dialog.getSelectedData();
		        	// S'eliminen els mappings que s'ha tret
		        	Iterator<IProviderElementDTO> iterator = modelMappedData.getModelData().iterator();
		        	while(iterator.hasNext()){
		        		IProviderElementDTO elem = iterator.next();
		        		if(selectedProvider.equals(elem.getProvider())){
		        			if(!selectedData.contains(elem)){
		        				iterator.remove();
			        		}
		        			else{
		        				selectedData.remove(elem);
		        			}
		        		}
		        	}
		        	// S'afegeixen els nous mappings
		        	for(IProviderElementDTO elem:selectedData){
		        		modelMappedData.addData(elem);
		        	}
		        	modelMappedData.fireTableDataChanged();
		        }
			}
			catch(BusinessException e){
	    		log.error("Error afegint associació externa: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error afegint associació externa: " + e.getMessage(), e);
	    	}
		}
		else if(ACTION_COMMAND_REMOVE_MAPPED_DATA.equals(event.getActionCommand())){
			int selectedIndex = tableMappedData.getSelectedRow();
			modelMappedData.getModelData().remove(selectedIndex);
			// Es torna a desactiva la opció d'esborrar
			bRemoveMappedData.setEnabled(false);
		}
	}

	@Override
	public void categoryCreated(ICategoryDTO category) 
	{
		// Res a fer...
	}

	@Override
	public void categoryUpdated(final ICategoryDTO oldCategory, final ICategoryDTO newCategory) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalCategory!=null && originalCategory.getCategoryId().equals(newCategory.getCategoryId())){
				this.originalCategory = (IVespineCategoryDTO)newCategory;
				JOptionPane.showMessageDialog(this, 
		                "Les dades de la categoria han sigut modificades externament. Es procedirà a carregar-les de nou", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				loadCategoryData();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	categoryUpdated(oldCategory, newCategory);
                }
            });
		}
	}

	@Override
	public void categoryDeleted(final ICategoryDTO category) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalCategory!=null && originalCategory.getCategoryId().equals(category.getCategoryId())){
				JOptionPane.showMessageDialog(this, 
		                "La categoria ha sigut eliminada.", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				close();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	categoryDeleted(category);
                }
            });
		}
	}
}
