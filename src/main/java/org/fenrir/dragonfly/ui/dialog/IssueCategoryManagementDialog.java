package org.fenrir.dragonfly.ui.dialog;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.vespine.core.dto.IVespineCategoryDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.event.ICategoryListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140815
 */
@SuppressWarnings("serial")
@EventListener(definitions=ICategoryListener.class)
public class IssueCategoryManagementDialog extends AbstractEntityManagementDialog<ICategoryDTO> implements ICategoryListener
{
	private static final String PARAMETER_NAME = "CATEGORY";
	
	private final Logger log = LoggerFactory.getLogger(IssueCategoryManagementDialog.class);
	
	public IssueCategoryManagementDialog() throws Exception
    {
        super("Administrar Categories", 450, 600);
    }
	
	@Override
	protected void loadData() throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

        List<ICategoryDTO> elements = administrationService.findAllCategories();
        addElements(elements);
	}

	@Override
	protected boolean openAddDialog(Map<String, Object> parameters) 
	{
		IssueCategoryDialog dialog = new IssueCategoryDialog();
        boolean returnValue = dialog.open()==IssueCategoryDialog.DIALOG_OK;
        IVespineCategoryDTO category = dialog.getFormData();
        parameters.put(PARAMETER_NAME, category);
        
        return returnValue;
	}

	@Override
	protected void doAdd(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IVespineCategoryDTO category = (IVespineCategoryDTO)parameters.get(PARAMETER_NAME);
		List<String> providerCategoriesIds = new ArrayList<String>();
		for(IProviderElementDTO elem:category.getProviderCategories()){
			providerCategoriesIds.add(elem.getProviderElementId());
		}
        administrationService.createCategory(category.getName(), providerCategoriesIds);
	}

	@Override
	protected boolean openModifyDialog(Map<String, Object> parameters) 
	{
		IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
		
		IVespineCategoryDTO selectedCategory = (IVespineCategoryDTO)listEntities.getSelectedValue();
		IssueCategoryDialog dialog = null;
		try{
			dialog = new IssueCategoryDialog(selectedCategory);
			eventNotificationService.addListener(ICategoryListener.class, dialog);
			
			boolean returnValue = dialog.open()==IssueCategoryDialog.DIALOG_OK;
	        IVespineCategoryDTO category = dialog.getFormData();
	        parameters.put(PARAMETER_NAME, category);
	        
	        return returnValue;
		}
		finally{
			if(dialog!=null){
	    		eventNotificationService.removeListener(dialog);
	    	}
		}
	}

	@Override
	protected void doModify(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IVespineCategoryDTO category = (IVespineCategoryDTO)parameters.get(PARAMETER_NAME);
		List<String> providerCategoriesIds = new ArrayList<String>();
		for(IProviderElementDTO elem:category.getProviderCategories()){
			providerCategoriesIds.add(elem.getProviderElementId());
		}
        administrationService.updateCategory(category.getCategoryId(), 
        		category.getName(), 
        		providerCategoriesIds);		
	}

	protected boolean openDeleteDialog(Map<String, Object> parameters)
    {
		try{
			IIssueFacade issueService = (IIssueFacade)ApplicationContext.getInstance().getRegisteredComponent(IIssueFacade.class);
			ICategoryDTO selectedCategory = (ICategoryDTO)listEntities.getSelectedValue();
			if(issueService.countIssuesByCategory(selectedCategory.getCategoryId())>0){
				JOptionPane.showMessageDialog(this, 
		                "No es pot eliminar el registre perquè té incidencies relacionades", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				return false;
			}
			return super.openDeleteDialog(parameters);
		}
		catch(BusinessException e){
    		log.error("Error realitzant les verificacions prèvies: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error realitzant les verificacions prèvies: " + e.getMessage(), e);
    	}
		
		return false;
    }
	
	@Override
	protected void doDelete(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		ICategoryDTO selectedCategory = (ICategoryDTO)listEntities.getSelectedValue();
        administrationService.deleteCategory(selectedCategory.getCategoryId());
	}

	@Override
	public void categoryCreated(ICategoryDTO category) 
	{
		reloadData();
	}

	@Override
	public void categoryUpdated(ICategoryDTO oldCategory, ICategoryDTO newCategory) 
	{
		reloadData();
	}

	@Override
	public void categoryDeleted(ICategoryDTO category) 
	{
		reloadData();
	}
	
	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
	            log.error("Error recarregant les categories: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant les categories: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}
