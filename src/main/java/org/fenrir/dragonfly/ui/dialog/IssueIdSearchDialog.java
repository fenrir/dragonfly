package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.FramelessDialog;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20130613
 */
public class IssueIdSearchDialog extends FramelessDialog
{
    private final Logger log = LoggerFactory.getLogger(IssueIdSearchDialog.class);
    
    private JTextField inputIssueId;
    
    public IssueIdSearchDialog(Frame parent)
    {
        super(parent, new Dimension(400, 60));        
    }
    
    @Override
    protected JComponent createContents() 
    {
        JPanel pContents = new JPanel();
        
        JLabel lTitle = new JLabel("Cerca per identificador");
        inputIssueId = new JTextField();
        inputIssueId.addKeyListener(new KeyAdapter() 
        {
            @Override
            public void keyPressed(KeyEvent event) 
            {
                if(event.getKeyCode()==KeyEvent.VK_ENTER){
                    search();
                    close();
                }
            }            
        }); 
                
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createSequentialGroup()    
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(lTitle)
                    .addComponent(inputIssueId, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                )
                .addContainerGap()
        );                            
        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lTitle)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(inputIssueId, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );          
        
        return pContents;
    }   
    
    public void search()
    {
        try{
            String value = inputIssueId.getText();
            ApplicationController controller = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);
            controller.searchIssueByVisibleId(value);            
        }
        catch(NumberFormatException e){            
            log.warn("Error buscant incidència amb id {}: Format incorrecte", inputIssueId.getText(), e);
            // Abans de mostrar l'error es tanca el dialeg per que no el tapi
            close();
            JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                    "El número d'incidència especificat no es correcte",
                    "Advertència",
                    JOptionPane.WARNING_MESSAGE);
        }        
    }
}
