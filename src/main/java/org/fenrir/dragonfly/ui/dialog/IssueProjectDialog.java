package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.dto.IVespineProjectDTO;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.widget.ProviderDataTableModel;
import org.fenrir.dragonfly.core.dto.ProjectFormDTO;
import org.fenrir.dragonfly.core.event.IProjectListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20141018
 */
@SuppressWarnings("serial")
@EventListener(definitions=IProjectListener.class)
public class IssueProjectDialog extends AbstractConfirmDialog implements ActionListener, IProjectListener
{
    private static final String ACTION_COMMAND_ADD_MAPPED_DATA = "ADD_MAPPED_DATA";
    private static final String ACTION_COMMAND_REMOVE_MAPPED_DATA = "REMOVE_MAPPED_DATA";

    private final Logger log = LoggerFactory.getLogger(IssueProjectDialog.class);
    
    // Valor de retorn
    private ProjectFormDTO project;
    // Valor original en cas de modificació
    private IVespineProjectDTO originalProject;
    
    private JTextField inputName;
    private JTextField inputAbbreviation;
    private JComboBox comboWorkflow;
    private DefaultComboBoxModel modelComboWorkflow;
    private JButton bAddMappedData;
    private JButton bRemoveMappedData;
    private JTable tableMappedData;
    private ProviderDataTableModel<IProviderElementDTO> modelMappedData;
    
    public IssueProjectDialog()
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear nou projecte", new Dimension(530, 620), true);
        // Es carreguen les dades dels mestres necessaries 
        loadData();
    }
    
    public IssueProjectDialog(IVespineProjectDTO project)
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar projecte existent", new Dimension(530, 620), true);
    	this.originalProject = project;
    	// Es carreguen les dades dels mestres necessaries
    	loadData();
    	// Els carreguen les dades del projecte a modificar
    	loadProjectData();
    }
    
    @Override
    protected JComponent createContents() 
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        /* Definició dels components */
        JLabel lName = new JLabel("Nom", SwingConstants.RIGHT);
        JLabel lAbbreviation = new JLabel("Sigles");
        JLabel lWorkflow = new JLabel("Fluxe de treball");
        JLabel lMappedData = new JLabel("Registres externs associats");

        inputName = new JTextField();
        inputName.getDocument().addDocumentListener(new DocumentListener()
        {
            @Override
            public void removeUpdate(DocumentEvent event)
            {
                String name = inputName.getText();
                if(name.length()<3){
                	inputAbbreviation.setText(name.toUpperCase());
                }
                else{
                	inputAbbreviation.setText(name.substring(0, 3).toUpperCase());
                }
            }

            @Override
            public void insertUpdate(DocumentEvent event)
            {
            	String name = inputName.getText();
                if(name.length()<3){
                	inputAbbreviation.setText(name.toUpperCase());
                }
                else{
                	inputAbbreviation.setText(name.substring(0, 3).toUpperCase());
                }
            }

            /**
             * No s'utilitza
             */
            @Override
            public void changedUpdate(DocumentEvent event)
            {

            }
        });
        inputAbbreviation = new JTextField();
        modelComboWorkflow = new DefaultComboBoxModel();
        comboWorkflow = new JComboBox(modelComboWorkflow);
        
        bAddMappedData = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/mapping_select_16.png")));
        bAddMappedData.addActionListener(this);
        bAddMappedData.setActionCommand(ACTION_COMMAND_ADD_MAPPED_DATA);
        bRemoveMappedData = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/mapping_remove_16.png")));
        // Inciialment desactivat perquè no hi ha cap registre sel.leccionat
        bRemoveMappedData.setEnabled(false);
        bRemoveMappedData.setActionCommand(ACTION_COMMAND_REMOVE_MAPPED_DATA);
        bRemoveMappedData.addActionListener(this);
        modelMappedData = new ProviderDataTableModel<IProviderElementDTO>();
        tableMappedData = new JTable(modelMappedData);
        // S'ajusta el tamany de la primera columna
        TableColumn col = tableMappedData.getColumnModel().getColumn(0);        
        col.setMaxWidth(20);
        tableMappedData.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
        {
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				boolean selectedRegistry = tableMappedData.getSelectedRow()>=0;
				bRemoveMappedData.setEnabled(selectedRegistry);
			}
		});
        JScrollPane scrollProviderData = new JScrollPane(tableMappedData);
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
            				.addComponent(lName, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            				.addComponent(lAbbreviation, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            				.addComponent(lWorkflow, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                        )
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(inputName)
                            .addComponent(inputAbbreviation)
                            .addComponent(comboWorkflow, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                        )
                    )
                    .addGroup(layout.createSequentialGroup()
                		.addComponent(lMappedData, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(bAddMappedData, 22, 22, 22)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(bRemoveMappedData, 22, 22, 22)
            		)
                    .addComponent(scrollProviderData)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
	        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	        .addGroup(layout.createSequentialGroup()
	            .addContainerGap()
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lName)
	                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lAbbreviation)
	                .addComponent(inputAbbreviation, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lWorkflow)
	                .addComponent(comboWorkflow)
	            )
	            .addGap(20)	            
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lMappedData)
	                .addComponent(bAddMappedData)
	                .addComponent(bRemoveMappedData)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)	            
	            .addComponent(scrollProviderData, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
	            .addContainerGap()
	        )
	    );
        
        return pContents;
    }
    
    protected void loadData()
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

		try{
	        List<IWorkflowDTO> elements = administrationService.findAllWorkflows(); 
	        for(IWorkflowDTO elem:elements){
	        	modelComboWorkflow.addElement(elem);
	        }
		}
		catch(BusinessException e){
			log.error("Error recuperant la llista de fluxes de treball: {}", e.getMessage(), e);
			ApplicationWindowManager.getInstance()
            		.displayErrorMessage("Error recuperant la llista de fluxes de treball: " + e.getMessage(), e);
		}
	}
    
    private void loadProjectData()
    {
    	inputName.setText(originalProject.getName());
    	inputAbbreviation.setText(originalProject.getAbbreviation());
    	// Les sigles no es podran canviar
    	inputAbbreviation.setEnabled(false);
    	comboWorkflow.setSelectedItem(originalProject.getWorkflow());
    	// Carregar les dades mapejades
    	for(IProviderElementDTO elem:originalProject.getProviderProjects()){
    		modelMappedData.addData(elem);
    	}
    }
    
    @Override
    protected boolean onAccept() 
    {
    	String name = inputName.getText().trim();
    	String abbreviation = inputAbbreviation.getText().trim().toUpperCase();
    	IWorkflowDTO workflow = (IWorkflowDTO)comboWorkflow.getSelectedItem();
    	/* Validacions */
    	if(StringUtils.isBlank(name)){
            setMessage("El camp 'Nom' no pot ser buit", MSG_TYPE_ERROR);
            return false;
        }
    	if(StringUtils.isBlank(abbreviation)){
    		setMessage("El camp 'Sigles' no pot ser buit", MSG_TYPE_ERROR);
            return false;
    	}
    	if(abbreviation.length()>5){
    		setMessage("El camp 'Sigles' no pot superar els 5 caràcters", MSG_TYPE_ERROR);
            return false;
    	}
    	if(workflow==null){
    		setMessage("Ha de definir un fluxe de treball pel projecte", MSG_TYPE_ERROR);
            return false;
    	}
    	
    	project = new ProjectFormDTO();
    	// En cas de moficiació també es retorna la ID
        if(originalProject!=null){
        	project.setProjectId(originalProject.getProjectId());
        }
    	project.setName(name);
    	project.setAbbreviation(abbreviation);
    	project.setWorkflow(workflow);
    	project.setProviderProjects(modelMappedData.getModelData());
    	
    	return true;
    }
    
    @Override
    protected boolean onCancel() 
    {
        project = null;
        return true;
    }
    
    public IVespineProjectDTO getFormData()
    {
        return project;
    }
    
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if(ACTION_COMMAND_ADD_MAPPED_DATA.equals(event.getActionCommand())){
			IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
			try{
				List<IProviderElementDTO> providerProject = administrationService.findAllProviderProjects();
				List<IProviderElementDTO> selectedProjects = modelMappedData.getModelData();
				ProviderDataDialog dialog = new ProviderDataDialog(providerProject, selectedProjects);
		        boolean addValue = dialog.open()==ProviderDataDialog.DIALOG_OK;
		        if(addValue){
		        	String selectedProvider = dialog.getSelectedProvider();
		        	List<IProviderElementDTO> selectedData = dialog.getSelectedData();
		        	// S'eliminen els mappings que s'ha tret
		        	Iterator<IProviderElementDTO> iterator = modelMappedData.getModelData().iterator();
		        	while(iterator.hasNext()){
		        		IProviderElementDTO elem = iterator.next();
		        		if(selectedProvider.equals(elem.getProvider())){
		        			if(!selectedData.contains(elem)){
		        				iterator.remove();
			        		}
		        			else{
		        				selectedData.remove(elem);
		        			}
		        		}
		        	}
		        	// S'afegeixen els nous mappings
		        	for(IProviderElementDTO elem:selectedData){
		        		modelMappedData.addData(elem);
		        	}
		        	modelMappedData.fireTableDataChanged();
		        }
			}
			catch(BusinessException e){
	    		log.error("Error afegint associació externa: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error afegint associació externa: " + e.getMessage(), e);
	    	}
		}
		else if(ACTION_COMMAND_REMOVE_MAPPED_DATA.equals(event.getActionCommand())){
			int selectedIndex = tableMappedData.getSelectedRow();
			modelMappedData.getModelData().remove(selectedIndex);
			// Es torna a desactiva la opció d'esborrar
			bRemoveMappedData.setEnabled(false);
		}
	}

	@Override
	public void projectCreated(IProjectDTO project) 
	{
		// Res a fer...
	}

	@Override
	public void projectUpdated(final IProjectDTO oldProject, final IProjectDTO newProject) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalProject!=null && originalProject.getProjectId().equals(newProject.getProjectId())){
				this.originalProject = (IVespineProjectDTO)newProject;
				JOptionPane.showMessageDialog(this, 
		                "Les dades del projecte han sigut modificades externament. Es procedirà a carregar-les de nou", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				loadProjectData();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	projectUpdated(oldProject, newProject);
                }
            });
		}
	}

	@Override
	public void projectDeleted(final IProjectDTO project) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalProject!=null && originalProject.getProjectId().equals(project.getProjectId())){
				JOptionPane.showMessageDialog(this, 
		                "El projecte ha sigut eliminat.", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				close();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	projectDeleted(project);
                }
            });
		}
	}
}
