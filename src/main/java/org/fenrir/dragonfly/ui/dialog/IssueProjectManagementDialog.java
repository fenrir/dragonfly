package org.fenrir.dragonfly.ui.dialog;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.dragonfly.core.event.IProjectListener;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.vespine.core.dto.IVespineProjectDTO;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140816
 */
@EventListener(definitions=IProjectListener.class)
public class IssueProjectManagementDialog extends AbstractEntityManagementDialog<IProjectDTO> implements IProjectListener
{
	private static final long serialVersionUID = 4442814223731129674L;
	
	private static final String PARAMETER_NAME = "PROJECT";

	private final Logger log = LoggerFactory.getLogger(IssueProjectManagementDialog.class);
	
	public IssueProjectManagementDialog() throws Exception
    {
        super("Administrar Projectes", 450, 600);
    }
	
	@Override
	protected void loadData() throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

        List<IProjectDTO> elements = administrationService.findAllProjects();
        addElements(elements);
	}

	@Override
	protected boolean openAddDialog(Map<String, Object> parameters) 
	{
		IssueProjectDialog dialog = new IssueProjectDialog();
        boolean returnValue = dialog.open()==IssueProjectDialog.DIALOG_OK;
        IVespineProjectDTO project = dialog.getFormData();
        parameters.put(PARAMETER_NAME, project);
        
        return returnValue;
	}

	@Override
	protected void doAdd(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IVespineProjectDTO project = (IVespineProjectDTO)parameters.get(PARAMETER_NAME);
		List<String> providerProjectIds = new ArrayList<String>();
		for(IProviderElementDTO elem:project.getProviderProjects()){
			providerProjectIds.add(elem.getProviderElementId());
		}
        administrationService.createProject(project.getName(),
        		project.getAbbreviation(),
        		project.getWorkflow().getWorkflowId(), 
        		providerProjectIds);
	}

	@Override
	protected boolean openModifyDialog(Map<String, Object> parameters) 
	{
		IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
		
		IVespineProjectDTO selectedProject = (IVespineProjectDTO)listEntities.getSelectedValue();
		IssueProjectDialog dialog = null;
		try{
			dialog = new IssueProjectDialog(selectedProject);
			eventNotificationService.addListener(IProjectListener.class, dialog);
	        
	        boolean returnValue = dialog.open()==IssueProjectDialog.DIALOG_OK;
	        IVespineProjectDTO project = dialog.getFormData();
	        parameters.put(PARAMETER_NAME, project);
	        
	        return returnValue;
		}
		finally{
			if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
		}
	}

	@Override
	protected void doModify(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IVespineProjectDTO project = (IVespineProjectDTO)parameters.get(PARAMETER_NAME);
		List<String> providerProjectIds = new ArrayList<String>();
		for(IProviderElementDTO elem:project.getProviderProjects()){
			providerProjectIds.add(elem.getProviderElementId());
		}
        administrationService.updateProject(project.getProjectId(), 
        		project.getName(), 
        		project.getWorkflow().getWorkflowId(), 
        		providerProjectIds);
	}

	@Override
	protected boolean openDeleteDialog(Map<String, Object> parameters)
    {
		try{
			IIssueFacade issueService = (IIssueFacade)ApplicationContext.getInstance().getRegisteredComponent(IIssueFacade.class);
			IProjectDTO selectedProject = (IProjectDTO)listEntities.getSelectedValue();
			if(issueService.countIssuesByProject(selectedProject.getProjectId())>0){
				JOptionPane.showMessageDialog(this, 
		                "No es pot eliminar el registre perquè té incidencies relacionades", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				return false;
			}
			return super.openDeleteDialog(parameters);
		}
		catch(BusinessException e){
    		log.error("Error realitzant les verificacions prèvies: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error realitzant les verificacions prèvies: " + e.getMessage(), e);
    	}
		
		return false;
    }
	
	@Override
	protected void doDelete(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IProjectDTO selectedProject = (IProjectDTO)listEntities.getSelectedValue();
        administrationService.deleteProject(selectedProject.getProjectId());
	}
	
	@Override
	public void projectCreated(IProjectDTO project)
	{
		reloadData();
	}
	
	@Override
	public void projectUpdated(IProjectDTO oldProject, IProjectDTO newProject)
	{
		reloadData();
	}
	
	@Override
	public void projectDeleted(IProjectDTO project)
	{
		reloadData();
	}
	
	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
				log.error("Error recarregant els projectes: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant els projectes: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}
