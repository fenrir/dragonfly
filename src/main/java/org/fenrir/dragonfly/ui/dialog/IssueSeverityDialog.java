package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.dto.IVespineSeverityDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.widget.ProviderDataTableModel;
import org.fenrir.dragonfly.core.dto.SeverityFormDTO;
import org.fenrir.dragonfly.core.event.ISeverityListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20141018
 */
public class IssueSeverityDialog extends AbstractConfirmDialog implements ActionListener, ISeverityListener
{
    private static final long serialVersionUID = 1L;
    
    private static final Pattern SLA_TERM_PATTERN = Pattern.compile("\\d+[mwdh]");
    
    private static final String ACTION_COMMAND_ADD_MAPPED_DATA = "ADD_MAPPED_DATA";
    private static final String ACTION_COMMAND_REMOVE_MAPPED_DATA = "REMOVE_MAPPED_DATA";
    
    private final Logger log = LoggerFactory.getLogger(IssueSeverityDialog.class);
    
    // Valor de retorn
    private SeverityFormDTO severity;
    // Valor original en cas de modificació
    private IVespineSeverityDTO originalSeverity;
    
    private JTextField inputName;
    private JTextField inputSlaPattern;
    private JButton bAddMappedData;
    private JButton bRemoveMappedData;
    private JTable tableMappedData;
    private ProviderDataTableModel<IProviderElementDTO> modelMappedData;
    
    public IssueSeverityDialog()
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear nova severitat", new Dimension(530, 620), true);
    }
    
    public IssueSeverityDialog(IVespineSeverityDTO severity)
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar severitat existent", new Dimension(530, 620), true);
    	this.originalSeverity = severity;
    	
    	loadSeverityData();
    }
    
    @Override
    protected JComponent createContents() 
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        /* Definició dels components */
        JLabel lName = new JLabel("Nom", SwingConstants.RIGHT);
        JLabel lSla = new JLabel("Duració SLA", SwingConstants.RIGHT);
        JLabel lMappedData = new JLabel("Registres externs associats");

        inputName = new JTextField();
        inputSlaPattern = new JTextField();        
        inputSlaPattern.setToolTipText("<html>Indicar format del periode SLA:<br/>"
                + "[num. mesos]<b>m</b> per mesos;<br/>"
                + "[num. setmanes]<b>w</b> per setmanes;<br/>"
                + "[num. dies]<b>d</b> per dies;<br/>"
                + "[num. hores]<b>h</b> per hores</html>");
        
        bAddMappedData = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/mapping_select_16.png")));
        bAddMappedData.addActionListener(this);
        bAddMappedData.setActionCommand(ACTION_COMMAND_ADD_MAPPED_DATA);
        bRemoveMappedData = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/mapping_remove_16.png")));
        // Inciialment desactivat perquè no hi ha cap registre sel.leccionat
        bRemoveMappedData.setEnabled(false);
        bRemoveMappedData.setActionCommand(ACTION_COMMAND_REMOVE_MAPPED_DATA);
        bRemoveMappedData.addActionListener(this);
        modelMappedData = new ProviderDataTableModel<IProviderElementDTO>();
        tableMappedData = new JTable(modelMappedData);
        // S'ajusta el tamany de la primera columna
        TableColumn col = tableMappedData.getColumnModel().getColumn(0);        
        col.setMaxWidth(20);
        tableMappedData.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
        {
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				boolean selectedRegistry = tableMappedData.getSelectedRow()>=0;
				bRemoveMappedData.setEnabled(selectedRegistry);
			}
		});
        JScrollPane scrollProviderData = new JScrollPane(tableMappedData);
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
            				.addComponent(lName, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
            				.addComponent(lSla, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        )
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(inputName)
                            .addComponent(inputSlaPattern)
                        )
                    )
                    .addGroup(layout.createSequentialGroup()
                		.addComponent(lMappedData, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(bAddMappedData, 22, 22, 22)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(bRemoveMappedData, 22, 22, 22)
            		)
                    .addComponent(scrollProviderData)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
	        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	        .addGroup(layout.createSequentialGroup()
	            .addContainerGap()
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lName)
	                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lSla)
	                .addComponent(inputSlaPattern, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addGap(20)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lMappedData)
	                .addComponent(bAddMappedData)
	                .addComponent(bRemoveMappedData)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)	            
	            .addComponent(scrollProviderData, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
	            .addContainerGap()
	        )
	    );
        
        return pContents;
    }
    
    private void loadSeverityData()
    {
    	inputName.setText(originalSeverity.getName());
    	inputSlaPattern.setText(originalSeverity.getSlaTerm());
    	// Carregar les dades mapejades
    	for(IProviderElementDTO elem:originalSeverity.getProviderSeverities()){
    		modelMappedData.addData(elem);
    	}
    }
    
    @Override
    protected boolean onAccept() 
    {
    	String name = inputName.getText().trim();
    	String slaTerm = inputSlaPattern.getText().trim();
    	if(StringUtils.isBlank(name)){
            setMessage("El camp NOM no pot ser buit", MSG_TYPE_ERROR);
            return false;
        }
    	if(StringUtils.isBlank(slaTerm)){           
            setMessage("S'ha d'especificar la durada de tots els periodes SLA", MSG_TYPE_ERROR);
            return false;
        }
        else if(!SLA_TERM_PATTERN.matcher(slaTerm).matches()){
            setMessage("El format de la durada d'un periode SLA és incorrecte", MSG_TYPE_ERROR);
            return false;
        }
    	
    	severity = new SeverityFormDTO();
    	// En cas de moficiació també es retorna la ID
        if(originalSeverity!=null){
        	severity.setSeverityId(originalSeverity.getSeverityId());
        }
    	severity.setName(name);
    	severity.setSlaTerm(slaTerm);
    	severity.setProviderSeverities(modelMappedData.getModelData());
    	
    	return true;
    }
    
    @Override
    protected boolean onCancel() 
    {
        severity = null;
        return true;
    }
    
    public IVespineSeverityDTO getFormData()
    {
        return severity;
    }
    
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if(ACTION_COMMAND_ADD_MAPPED_DATA.equals(event.getActionCommand())){
			try{
				IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
				
				List<IProviderElementDTO> providerSeverities = administrationService.findAllProviderSeverities();
				List<IProviderElementDTO> selectedSeverities = modelMappedData.getModelData();
				ProviderDataDialog dialog = new ProviderDataDialog(providerSeverities, selectedSeverities);
		        boolean addValue = dialog.open()==ProviderDataDialog.DIALOG_OK;
		        if(addValue){
		        	String selectedProvider = dialog.getSelectedProvider();
		        	List<IProviderElementDTO> selectedData = dialog.getSelectedData();
		        	// S'eliminen els mappings que s'ha tret
		        	Iterator<IProviderElementDTO> iterator = modelMappedData.getModelData().iterator();
		        	while(iterator.hasNext()){
		        		IProviderElementDTO elem = iterator.next();
		        		if(selectedProvider.equals(elem.getProvider())){
		        			if(!selectedData.contains(elem)){
		        				iterator.remove();
			        		}
		        			else{
		        				selectedData.remove(elem);
		        			}
		        		}
		        	}
		        	// S'afegeixen els nous mappings
		        	for(IProviderElementDTO elem:selectedData){
		        		modelMappedData.addData(elem);
		        	}
		        	modelMappedData.fireTableDataChanged();
		        }
			}
			catch(BusinessException e){
	    		log.error("Error afegint associació externa: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error afegint associació externa: " + e.getMessage(), e);
	    	}
		}
		else if(ACTION_COMMAND_REMOVE_MAPPED_DATA.equals(event.getActionCommand())){
			int selectedIndex = tableMappedData.getSelectedRow();
			modelMappedData.getModelData().remove(selectedIndex);
			// Es torna a desactiva la opció d'esborrar
			bRemoveMappedData.setEnabled(false);
		}
	}

	@Override
	public void severityCreated(ISeverityDTO severity) 
	{
		// Res a fer...
	}

	@Override
	public void severityUpdated(final ISeverityDTO oldSeverity, final ISeverityDTO newSeverity) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalSeverity!=null && originalSeverity.getSeverityId().equals(newSeverity.getSeverityId())){
				this.originalSeverity = (IVespineSeverityDTO)newSeverity;
				JOptionPane.showMessageDialog(this, 
		                "Les dades de la severitat han sigut modificades externament. Es procedirà a carregar-les de nou", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				loadSeverityData();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	severityUpdated(oldSeverity, newSeverity);
                }
            });
		}
	}

	@Override
	public void severityDeleted(final ISeverityDTO severity) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalSeverity!=null && originalSeverity.getSeverityId().equals(severity.getSeverityId())){
				JOptionPane.showMessageDialog(this, 
		                "La severitat ha sigut eliminada.", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				close();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	severityDeleted(severity);
                }
            });
		}
	}
}
