package org.fenrir.dragonfly.ui.dialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.dragonfly.core.event.ISeverityListener;
import org.fenrir.vespine.core.dto.IVespineSeverityDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140816
 */
@EventListener(definitions=ISeverityListener.class)
@SuppressWarnings("serial")
public class IssueSeverityManagementDialog extends AbstractEntityManagementDialog<ISeverityDTO> implements ISeverityListener
{
	private static final String PARAMETER_NAME = "SEVERITY";
	
	private final Logger log = LoggerFactory.getLogger(IssueSeverityManagementDialog.class);
	
	public IssueSeverityManagementDialog() throws Exception
    {
        super("Administrar Severitats", 450, 600);
    }
	
	@Override
	protected void loadData() throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

        List<ISeverityDTO> elements = administrationService.findAllSeverities();
        addElements(elements);
	}

	@Override
	protected boolean openAddDialog(Map<String, Object> parameters) 
	{
		IssueSeverityDialog dialog = new IssueSeverityDialog();
        boolean returnValue = dialog.open()==IssueSeverityDialog.DIALOG_OK;
        ISeverityDTO severity = dialog.getFormData();
        parameters.put(PARAMETER_NAME, severity);
        
        return returnValue;
	}

	@Override
	protected void doAdd(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IVespineSeverityDTO severity = (IVespineSeverityDTO)parameters.get(PARAMETER_NAME);
		List<String> providerSeverityIds = new ArrayList<String>();
		for(IProviderElementDTO elem:severity.getProviderSeverities()){
			providerSeverityIds.add(elem.getProviderElementId());
		}
		administrationService.createSeverity(severity.getName(), severity.getSlaTerm(), providerSeverityIds);
	}

	@Override
	protected boolean openModifyDialog(Map<String, Object> parameters) 
	{
		IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
		
		IVespineSeverityDTO selectedSeverity = (IVespineSeverityDTO)listEntities.getSelectedValue();
		IssueSeverityDialog dialog = null;
		try{
			dialog = new IssueSeverityDialog(selectedSeverity);
			eventNotificationService.addListener(ISeverityListener.class, dialog);
			
			boolean returnValue = dialog.open()==IssueSeverityDialog.DIALOG_OK;
	        IVespineSeverityDTO severity = dialog.getFormData();
	        parameters.put(PARAMETER_NAME, severity);
	        
	        return returnValue;
		}
		finally{
			if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
		}
	}

	@Override
	protected void doModify(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IVespineSeverityDTO severity = (IVespineSeverityDTO)parameters.get(PARAMETER_NAME);
		List<String> providerSeverityIds = new ArrayList<String>();
		for(IProviderElementDTO elem:severity.getProviderSeverities()){
			providerSeverityIds.add(elem.getProviderElementId());
		}
		administrationService.updateSeverity(severity.getSeverityId(),				
				severity.getName(), 
				severity.getSlaTerm(), 
				providerSeverityIds);
	}

	protected boolean openDeleteDialog(Map<String, Object> parameters)
    {
		IIssueFacade issueService = (IIssueFacade)ApplicationContext.getInstance().getRegisteredComponent(IIssueFacade.class);
		ISeverityDTO selectedSeverity = (ISeverityDTO)listEntities.getSelectedValue();
		try{
			if(issueService.countIssuesBySeverity(selectedSeverity.getSeverityId())>0){
				JOptionPane.showMessageDialog(this, 
		                "No es pot eliminar el registre perquè té incidencies relacionades", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				return false;
			}
			return super.openDeleteDialog(parameters);
		}
		catch(BusinessException e){
    		log.error("Error realitzant les verificacions prèvies: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error realitzant les verificacions prèvies: " + e.getMessage(), e);
    	}
		
		return false;
    }
	
	@Override
	protected void doDelete(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		ISeverityDTO selectedSeverity = (ISeverityDTO)listEntities.getSelectedValue();
        administrationService.deleteSeverity(selectedSeverity.getSeverityId());
	}

	@Override
	public void severityCreated(ISeverityDTO severity) 
	{
		reloadData();
	}

	@Override
	public void severityUpdated(ISeverityDTO oldSeverity, ISeverityDTO newSeverity) 
	{
		reloadData();
	}

	@Override
	public void severityDeleted(ISeverityDTO severity) 
	{
		reloadData();
	}
	
	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
				log.error("Error recarregant les severitats: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant les severitats: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}
