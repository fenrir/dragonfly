package org.fenrir.dragonfly.ui.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.vespine.core.dto.IVespineStatusDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.widget.ProviderDataTableModel;
import org.fenrir.dragonfly.core.dto.StatusFormDTO;
import org.fenrir.dragonfly.core.event.IStatusListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20141018
 */
@EventListener(definitions=IStatusListener.class)
public class IssueStatusDialog extends AbstractConfirmDialog implements ActionListener, IStatusListener
{
    private static final long serialVersionUID = 1L;
    
    private static final String ACTION_COMMAND_ADD_MAPPED_DATA = "ADD_MAPPED_DATA";
    private static final String ACTION_COMMAND_REMOVE_MAPPED_DATA = "REMOVE_MAPPED_DATA";

    private final Logger log = LoggerFactory.getLogger(IssueStatusDialog.class);
    
    // Valor de retorn
    private StatusFormDTO status;
    // Valor original en cas de modificació
    private IVespineStatusDTO originalStatus;
    
    private JTextField inputName;
    private JButton inputColor;
    private JButton bAddMappedData;
    private JButton bRemoveMappedData;
    private JTable tableMappedData;
    private ProviderDataTableModel<IProviderElementDTO> modelMappedData;
    
    public IssueStatusDialog()
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear nou estat", new Dimension(530, 620), true);
    }
    
    public IssueStatusDialog(IVespineStatusDTO status)
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar estat existent", new Dimension(530, 620), true);
    	this.originalStatus = status;
    	
    	// Els carreguen les dades de l'estat a modificar
    	loadStatusData();
    }
    
    @Override
    protected JComponent createContents() 
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        /* Definició dels components */
        JLabel lName = new JLabel("Nom", SwingConstants.RIGHT);
        JLabel lColor = new JLabel("Color", SwingConstants.RIGHT);
        JLabel lMappedData = new JLabel("Registres externs associats");

        inputName = new JTextField();
        inputColor = new JButton();
        inputColor.addActionListener(new ActionListener() 
        {
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				Color newColor = JColorChooser.showDialog(
						IssueStatusDialog.this,
						"Tria un color per l'estat",
						inputColor.getBackground());
				inputColor.setBackground(newColor);
			}
		});
        inputColor.setBackground(Color.WHITE);
        
        bAddMappedData = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/mapping_select_16.png")));
        bAddMappedData.addActionListener(this);
        bAddMappedData.setActionCommand(ACTION_COMMAND_ADD_MAPPED_DATA);
        bRemoveMappedData = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/mapping_remove_16.png")));
        // Inciialment desactivat perquè no hi ha cap registre sel.leccionat
        bRemoveMappedData.setEnabled(false);
        bRemoveMappedData.setActionCommand(ACTION_COMMAND_REMOVE_MAPPED_DATA);
        bRemoveMappedData.addActionListener(this);
        modelMappedData = new ProviderDataTableModel<IProviderElementDTO>();
        tableMappedData = new JTable(modelMappedData);
        // S'ajusta el tamany de la primera columna
        TableColumn col = tableMappedData.getColumnModel().getColumn(0);        
        col.setMaxWidth(20);
        tableMappedData.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
        {
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				boolean selectedRegistry = tableMappedData.getSelectedRow()>=0;
				bRemoveMappedData.setEnabled(selectedRegistry);
			}
		});
        JScrollPane scrollProviderData = new JScrollPane(tableMappedData);
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
            				.addComponent(lName, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 77, Short.MAX_VALUE)
            				.addComponent(lColor, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        )
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(inputName)
                            .addComponent(inputColor, 50, 50, 50)
                        )
                    )
                    .addGroup(layout.createSequentialGroup()
                		.addComponent(lMappedData, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(bAddMappedData, 22, 22, 22)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(bRemoveMappedData, 22, 22, 22)
            		)
                    .addComponent(scrollProviderData)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
	        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	        .addGroup(layout.createSequentialGroup()
	            .addContainerGap()
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lName)
	                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lColor)
	                .addComponent(inputColor, 20, 20, 20)
	            )
	            .addGap(20)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lMappedData)
	                .addComponent(bAddMappedData)
	                .addComponent(bRemoveMappedData)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)	            
	            .addComponent(scrollProviderData, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
	            .addContainerGap()
	        )
	    );
        
        return pContents;
    }
    
    private void loadStatusData()
    {
    	inputName.setText(originalStatus.getName());
    	inputColor.setBackground(getColor(originalStatus.getColorRGB()));
    	// Carregar les dades mapejades
    	for(IProviderElementDTO elem:originalStatus.getProviderStatus()){
    		modelMappedData.addData(elem);
    	}
    }
    
    @Override
    protected boolean onAccept() 
    {
    	String name = inputName.getText().trim();
    	Color color = inputColor.getBackground();
    	Object[] msgArguments = new Object[]{color.getRed(), color.getGreen(), color.getBlue()};
        String strColor = MessageFormat.format("{0, number, integer},{1, number, integer},{2, number, integer}", msgArguments);
    	if(StringUtils.isBlank(name)){
            setMessage("El camp NOM no pot ser buit", MSG_TYPE_ERROR);
            return false;
        }
    	
    	status = new StatusFormDTO();
    	// En cas de moficiació també es retorna la ID
        if(originalStatus!=null){
        	status.setStatusId(originalStatus.getStatusId());
        }
    	status.setName(name);
    	status.setColorRGB(strColor);
    	status.setProviderStatus(modelMappedData.getModelData());
    	
    	return true;
    }
    
    @Override
    protected boolean onCancel() 
    {
        status = null;
        return true;
    }
    
    public IVespineStatusDTO getFormData()
    {
        return status;
    }
    
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if(ACTION_COMMAND_ADD_MAPPED_DATA.equals(event.getActionCommand())){
			IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
			try{
				List<IProviderElementDTO> providerStatus = administrationService.findAllProviderStatus();
				List<IProviderElementDTO> selectedStatus = modelMappedData.getModelData();
				ProviderDataDialog dialog = new ProviderDataDialog(providerStatus, selectedStatus);
		        boolean addValue = dialog.open()==ProviderDataDialog.DIALOG_OK;
		        if(addValue){
		        	String selectedProvider = dialog.getSelectedProvider();
		        	List<IProviderElementDTO> selectedData = dialog.getSelectedData();
		        	// S'eliminen els mappings que s'ha tret
		        	Iterator<IProviderElementDTO> iterator = modelMappedData.getModelData().iterator();
		        	while(iterator.hasNext()){
		        		IProviderElementDTO elem = iterator.next();
		        		if(selectedProvider.equals(elem.getProvider())){
		        			if(!selectedData.contains(elem)){
		        				iterator.remove();
			        		}
		        			else{
		        				selectedData.remove(elem);
		        			}
		        		}
		        	}
		        	// S'afegeixen els nous mappings
		        	for(IProviderElementDTO elem:selectedData){
		        		modelMappedData.addData(elem);
		        	}
		        	modelMappedData.fireTableDataChanged();
		        }
			}
			catch(BusinessException e){
	    		log.error("Error afegint associació externa: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error afegint associació externa: " + e.getMessage(), e);
	    	}
		}
		else if(ACTION_COMMAND_REMOVE_MAPPED_DATA.equals(event.getActionCommand())){
			int selectedIndex = tableMappedData.getSelectedRow();
			modelMappedData.getModelData().remove(selectedIndex);
			// Es torna a desactiva la opció d'esborrar
			bRemoveMappedData.setEnabled(false);
		}
	}
	
	private Color getColor(String strColor)
	{
        if(StringUtils.isNotBlank(strColor)){
            String[] rgb = strColor.split(",");
            return new Color(Integer.parseInt(rgb[0].trim()),
                    Integer.parseInt(rgb[1].trim()),
                    Integer.parseInt(rgb[2].trim()));
        }
        else{
            return Color.WHITE;
        }
	}

	@Override
	public void statusCreated(IStatusDTO status) 
	{
		// Res a fer...
	}

	@Override
	public void statusUpdated(final IStatusDTO oldStatus, final IStatusDTO newStatus) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalStatus!=null && originalStatus.getStatusId().equals(newStatus.getStatusId())){
				this.originalStatus = (IVespineStatusDTO)newStatus;
				JOptionPane.showMessageDialog(this, 
		                "Les dades de l'estat han sigut modificades externament. Es procedirà a carregar-les de nou", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				loadStatusData();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	statusUpdated(oldStatus, newStatus);
                }
            });
		}
	}

	@Override
	public void statusDeleted(final IStatusDTO status) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalStatus!=null && originalStatus.getStatusId().equals(status.getStatusId())){
				JOptionPane.showMessageDialog(this, 
		                "L'estat ha sigut eliminat.", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				close();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	statusDeleted(status);
                }
            });
		}
	}
}
