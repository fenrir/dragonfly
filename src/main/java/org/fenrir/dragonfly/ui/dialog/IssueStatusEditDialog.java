package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.ui.widget.ElementSearchTextField;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.provider.UserSearchProvider;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140824
 */
@SuppressWarnings("serial")
public class IssueStatusEditDialog extends AbstractConfirmDialog 
{
	private final Logger log = LoggerFactory.getLogger(IssueStatusEditDialog.class);
	
	private IIssueDTO originalIssue;
	private IStatusDTO selectedStatus;
	private IUserDTO assignedUser;
	
	private JComboBox comboSelectedStatus;
	private ElementSearchTextField<IUserDTO> inputAssignedUser;
	
	public IssueStatusEditDialog(IIssueDTO issue)
	{
		super(ApplicationWindowManager.getInstance().getMainWindow(), "Editar estat", new Dimension(530, 220), true);
		
		this.originalIssue = issue;
		loadData();
	}
	
	@Override
    protected JComponent createContents() 
    {
        JPanel pContents = new JPanel();
        
        JLabel lAssignedUser = new JLabel("Usuari assignat");
        JLabel lSelectedStatus = new JLabel("Estat");
        comboSelectedStatus = new JComboBox(new DefaultComboBoxModel());
        inputAssignedUser = new ElementSearchTextField<IUserDTO>(UserSearchProvider.class);

        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createSequentialGroup()
    		.addContainerGap()
    		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
				.addComponent(lAssignedUser, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(lSelectedStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			)
			.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
			.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addComponent(comboSelectedStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
				.addComponent(inputAssignedUser, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
			)
            .addContainerGap()
        );                            
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addContainerGap()
    		// L'alineament d'aquest grup ha de ser CENTER perquè BASELINE no funciona bé amb els camps de tipus AdvancedTextField
    		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
        		.addComponent(lSelectedStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(comboSelectedStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    		// L'alineament d'aquest grup ha de ser CENTER perquè BASELINE no funciona bé amb els camps de tipus AdvancedTextField
    		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
        		.addComponent(lAssignedUser, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(inputAssignedUser, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
            .addContainerGap()
        );
        
        return pContents;
    }
	
	private void loadData()
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

		try{
			String projectId = originalIssue.getProject().getProjectId();
			IStatusDTO currentStatus = originalIssue.getStatus();
	        List<IStatusDTO> elements = administrationService.findWorkflowNextStatus(projectId, currentStatus.getStatusId());
	        // Es permet mantenir l'estat
	    	if(currentStatus!=null){
	    		((DefaultComboBoxModel)comboSelectedStatus.getModel()).addElement(currentStatus);
	    	}
	    	for(IStatusDTO elem:elements){
	    		((DefaultComboBoxModel)comboSelectedStatus.getModel()).addElement(elem);
	    	}
	        comboSelectedStatus.setSelectedItem(currentStatus);
	        inputAssignedUser.setSelectedElement(originalIssue.getAssignedUser());
		}
		catch(BusinessException e){
			log.error("Error recuperant la llista d'estats disponibles: {}", e.getMessage(), e);
			ApplicationWindowManager.getInstance()
            		.displayErrorMessage("Error recuperant la llista d'estats disponibles: " + e.getMessage(), e);
		}
	}
	
	@Override
    protected boolean onAccept() 
    {
		if((IStatusDTO)comboSelectedStatus.getSelectedItem()==null){
			setMessage("Cal especificar l'estat en que es troba la incidencia", MSG_TYPE_ERROR);
			return false;
		}
		
		selectedStatus = (IStatusDTO)comboSelectedStatus.getSelectedItem();
		assignedUser = inputAssignedUser.getSelectedElement();
		
		return true;
    }
	
	@Override
    protected boolean onCancel() 
    {
        return true;
    }
	
	public IStatusDTO getSelectedStatus()
	{
		return selectedStatus;
	}
	
	public IUserDTO getAssignedUser()
	{
		return assignedUser;
	}
}
