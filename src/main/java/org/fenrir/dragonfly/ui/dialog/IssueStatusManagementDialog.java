package org.fenrir.dragonfly.ui.dialog;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.vespine.core.dto.IVespineStatusDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.event.IStatusListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140816
 */
@EventListener(definitions=IStatusListener.class)
@SuppressWarnings("serial")
public class IssueStatusManagementDialog extends AbstractEntityManagementDialog<IStatusDTO> implements IStatusListener
{
	private static final String PARAMETER_NAME = "STATUS";
	
	private final Logger log = LoggerFactory.getLogger(IssueStatusManagementDialog.class);
	
	public IssueStatusManagementDialog() throws Exception
    {
        super("Administrar Estats", 450, 600);
    }
	
	@Override
	protected void loadData() throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

        List<IStatusDTO> elements = administrationService.findAllStatus();
        addElements(elements);
	}

	@Override
	protected boolean openAddDialog(Map<String, Object> parameters) 
	{
		IssueStatusDialog dialog = new IssueStatusDialog();
        boolean returnValue = dialog.open()==IssueStatusDialog.DIALOG_OK;
        IVespineStatusDTO status = dialog.getFormData();
        parameters.put(PARAMETER_NAME, status);
        
        return returnValue;
	}

	@Override
	protected void doAdd(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IVespineStatusDTO status = (IVespineStatusDTO)parameters.get(PARAMETER_NAME);
		List<String> providerStatusIds = new ArrayList<String>();
		for(IProviderElementDTO elem:status.getProviderStatus()){
			providerStatusIds.add(elem.getProviderElementId());
		}
		administrationService.createStatus(status.getName(), status.getColorRGB(), providerStatusIds);
	}

	@Override
	protected boolean openModifyDialog(Map<String, Object> parameters) 
	{
		IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
		
		IVespineStatusDTO selectedStatus = (IVespineStatusDTO)listEntities.getSelectedValue();
		IssueStatusDialog dialog = null;
		try{
			dialog = new IssueStatusDialog(selectedStatus);
			eventNotificationService.addListener(IStatusListener.class, dialog);
			
			boolean returnValue = dialog.open()==IssueStatusDialog.DIALOG_OK;
			IVespineStatusDTO status = dialog.getFormData();
			parameters.put(PARAMETER_NAME, status);
			
			return returnValue;
		}
		finally{
			if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
		}
        
	}

	@Override
	protected void doModify(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IVespineStatusDTO status = (IVespineStatusDTO)parameters.get(PARAMETER_NAME);
		List<String> providerStatusIds = new ArrayList<String>();
		for(IProviderElementDTO elem:status.getProviderStatus()){
			providerStatusIds.add(elem.getProviderElementId());
		}
		administrationService.updateStatus(status.getStatusId(),
				status.getName(), 
				status.getColorRGB(), 
				providerStatusIds);
	}

	protected boolean openDeleteDialog(Map<String, Object> parameters)
    {
		try{
			IIssueFacade issueService = (IIssueFacade)ApplicationContext.getInstance().getRegisteredComponent(IIssueFacade.class);
			IStatusDTO selectedStatus = (IStatusDTO)listEntities.getSelectedValue();
			if(issueService.countIssuesByStatus(selectedStatus.getStatusId())>0){
				JOptionPane.showMessageDialog(this, 
		                "No es pot eliminar el registre perquè té incidencies relacionades?", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				return false;
			}
			return super.openDeleteDialog(parameters);
		}
		catch(BusinessException e){
    		log.error("Error realitzant les verificacions prèvies: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error realitzant les verificacions prèvies: " + e.getMessage(), e);
    	}
		
		return false;
    }
	
	@Override
	protected void doDelete(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IStatusDTO selectedStatus = (IStatusDTO)listEntities.getSelectedValue();
        administrationService.deleteIssueStatus(selectedStatus.getStatusId());
	}

	@Override
	public void statusCreated(IStatusDTO status) 
	{
		reloadData();
	}

	@Override
	public void statusUpdated(IStatusDTO oldStatus, IStatusDTO newStatus) 
	{
		reloadData();
	}

	@Override
	public void statusDeleted(IStatusDTO status) 
	{
		reloadData();
	}
	
	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
				log.error("Error recarregant els estats: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant els estats: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}
