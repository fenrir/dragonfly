package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.AbstractAction;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;
import javax.swing.JButton;
import java.util.Collection;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.FramelessDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.ui.widget.IssueTagTableModel;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar
 * @author Antonio Archilla Nava
 * @version v0.3.20140503
 */
public class IssueTagsDialog extends FramelessDialog
{
	private final Logger log = LoggerFactory.getLogger(IssueTagsDialog.class);
	
    private String issueId;
    
    private JTextField inputTag;
    private JToggleButton togglePreferredTags;
    private JTable tableTags; 
    
    private ApplicationController controller;
    
    public IssueTagsDialog(Frame parent, String issueId)
    {
        super(parent, new Dimension(400, 300));
        setModal(true);
        
        this.issueId = issueId;
        
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        controller = (ApplicationController)applicationContext.getRegisteredComponent(ApplicationController.class);
        
        // Es carreguen els tags de la incidència especificada
        IIssueFacade issueService = (IIssueFacade)applicationContext.getRegisteredComponent(IIssueFacade.class);
        try{
	        IIssueDTO issue = issueService.findIssueById(issueId);
	        Collection<ITagDTO> issueTags = issue.getTags();
	        IssueTagTableModel tableModel = (IssueTagTableModel)tableTags.getModel();
	        tableModel.addTags(issueTags, true);
        }
        catch(BusinessException e){
        	log.error("Error carregant les etiquetes associades a l'incidència: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant les etiquetes associades a l'incidència: " + e.getMessage(), e);
        }
    }
    
    @Override
    protected JComponent createContents() 
    {
        JPanel pTags = new JPanel();
		
        GroupLayout layout = new GroupLayout(pTags);
        pTags.setLayout(layout);
		
        JLabel lTitle = new JLabel("Etiquetes");
        inputTag = new JTextField();
        inputTag.addKeyListener(new KeyAdapter() 
        {
            @Override
            public void keyReleased(KeyEvent event) 
            {
                int keyPressed = event.getKeyCode();
                String tagName = inputTag.getText();
                if(StringUtils.isNotBlank(tagName)){
                    /* Si la tecla premuda es ENTER:
                     * 1) Es crea el tag si el nombre si no hi ha cap coincidència exacte (Case insensitive)
                     * 2) S'associa si hi ha una coincidència exacte (Case insensitive)
                     */
                    if(keyPressed==KeyEvent.VK_ENTER){														
                        if(StringUtils.isNotBlank(tagName)){			
                            ITagDTO tag = controller.tagIssueByDescription(issueId, tagName);
                            ((IssueTagTableModel)tableTags.getModel()).addTag(tag, true);
							
                            // S'esborra el contingut de l'input
                            inputTag.setText(null);							
                        }
                    }
                    /* Si la tecla premuda es una altra, es realitza la cerca 
                     * depenent de l'estat de l'indicador de favorits
                     */
                    else if(togglePreferredTags.isSelected()){
                        searchTags(tagName, true);
                    }
                    else{
                        searchTags(tagName, false);
                    }
                }
                /* Si el filtre es buit però l'indicador de favorits està activat, 
                 * es retornen tots el favorits
                 */
                else if(togglePreferredTags.isSelected()){
                    searchTags(null, true);
                }
            }        	
        });
        togglePreferredTags = new JToggleButton(new AbstractAction(null, new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/favourite_16.png"))) 
        {			
            @Override
            public void actionPerformed(ActionEvent event) 
            {
                IssueTagTableModel tableModel = (IssueTagTableModel)tableTags.getModel();
                tableModel.removeAllUncheckedTags();

                if(togglePreferredTags.isSelected()){
                    tableModel.removeAllUncheckedTags();
                    searchTags(inputTag.getText(), true);
                }				
            }
        });
        
        tableTags = new JTable(new IssueTagTableModel())
        {
            /**
             * Implementació dels tooltips de les descripcions de la taula de tags associats a la imatge
             */
            @Override
            public String getToolTipText(MouseEvent event) 
            {
            	String tip;
                Point p = event.getPoint();
                int rowIndex = rowAtPoint(p);
                int colIndex = columnAtPoint(p);
                int realColumnIndex = convertColumnIndexToModel(colIndex);

                /* Columna descripció. S'ha de comprobar que a fila estigui dins del rang 0 - getColumns-1.
                 * En cas contrari rowIndex = -1 
                 */
                if(realColumnIndex==1 && rowIndex!=-1){
                    tip = (String)getValueAt(rowIndex, colIndex);

                } 
                /* La resta de columnes. Aquesy pas es podria ometre ja que no hi ha mes renderers
                 * que proporcionin tooltips per aquesta taula, però d'aquesta manera és més general
                 */
                else{                     
                    tip = super.getToolTipText(event);
                }
                return tip;
            }
        };
        tableTags.setShowGrid(false);
        // Es redimensiona la columna del check        
        tableTags.setFillsViewportHeight(true);
        TableColumn col = tableTags.getColumnModel().getColumn(0);        
        col.setMaxWidth(20);
        // Al marcar el check s'associarà el tag a la imatge
        tableTags.getModel().addTableModelListener(new TableModelListener() 
        {			
            @Override
            public void tableChanged(TableModelEvent event) 
            {
                if(event.getType()==TableModelEvent.UPDATE && event.getColumn()==0){
                    int row = event.getFirstRow();
                    ITagDTO tag = ((IssueTagTableModel)tableTags.getModel()).getTagAt(row);
                    boolean checked = ((IssueTagTableModel)tableTags.getModel()).isTagChecked(row); 						
                    // Si el check està marcat s'associa el tag a la imatge
                    if(checked){
                        controller.tagIssue(issueId, tag.getTagId());
                    }
                    // en cas contrari s'elimina l'associació
                    else{
                        controller.dettachTag(issueId, tag.getTagId());
                    }
                }				
            }
        });
        JScrollPane scrollTags = new JScrollPane(tableTags);        
        JButton bClose = new JButton(new AbstractAction("Tancar") 
        {
            @Override
            public void actionPerformed(ActionEvent event) 
            {
                close();
            }
        });
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()                
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(lTitle)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()                                                
                        .addComponent(inputTag, GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(togglePreferredTags, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                    )
                    .addComponent(scrollTags, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)
                    .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()                                                
                        .addComponent(bClose, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    )
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lTitle, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)                    
                    .addComponent(inputTag, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(togglePreferredTags)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollTags, GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bClose, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap()
            )
        );
        
        return pTags;
    }
    
    private void searchTags(String input, boolean preferred)
    {
        ApplicationContext context = ApplicationContext.getInstance();
        IAdministrationFacade administrationService = (IAdministrationFacade)context.getRegisteredComponent(IAdministrationFacade.class);

        try{
	        List<ITagDTO> tags;
	        if(StringUtils.isEmpty(input) && !preferred){
	            tags = administrationService.findAllTags();
	        }
	        else if(!preferred){
	            tags = administrationService.findTagsFiltered(input);
	        }
	        else if(StringUtils.isEmpty(input) && preferred){
	            tags = administrationService.findPreferredTags();
	        }
	        else{
	            tags = administrationService.findPreferredTagsFiltered(input);
	        }
	        IssueTagTableModel tableModel = (IssueTagTableModel)tableTags.getModel();
	        tableModel.removeAllUncheckedTags();
	        tableModel.addTags(tags, false);
        }
        catch(BusinessException e){
        	log.error("Error buscant etiquetes etiquetes: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error buscant etiquetes etiquetes: " + e.getMessage(), e);
        }
    }
}
