package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.widget.IssueTimerRegistryTableModel;
import org.fenrir.dragonfly.core.entity.IssueTimerRegistry;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140720
 */
@SuppressWarnings("serial")
public class IssueTimerRegistryPublishingDialog extends AbstractDialog implements ActionListener
{
	private static final String COMMAND_SELECT_ALL = "SELECT_ALL";
	private static final String COMMAND_SELECT_NONE = "SELECT_NONE";
	private static final String COMMAND_PUBLISH = "PUBLISH";
	
	private final Logger log = LoggerFactory.getLogger(IssueTimerRegistryPublishingDialog.class);
	
	private JTable table;
	
	public IssueTimerRegistryPublishingDialog()
	{
		super(ApplicationWindowManager.getInstance().getMainWindow(), "Publicar registres de temps", new Dimension(800, 600), true);
		setDescriptionMessage("Publicació de registres de temps anteriors");
		setMessage("Els registres no publicats seran eliminats", AbstractDialog.MSG_TYPE_INFO);
		
		try{
			loadRegistries();
		}
		catch(BusinessException e){
			log.error("Error carregant els registres de treball pendents de publicació: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant els registres de treball pendents de publicació: " + e.getMessage(), e);
            
            // Si no s'ha pogut carregar el contingut, es tanca la finestra després de mostrar l'error
            setVisible(false);
            dispose();
		}
	}
	
	@Override
	protected JComponent createContents() 
	{
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        table = new JTable(new IssueTimerRegistryTableModel());
        // Es redimensiona la columna del check        
        table.setFillsViewportHeight(true);
        TableColumn col = table.getColumnModel().getColumn(0);        
        col.setMaxWidth(20);        
        JScrollPane scrollTable = new JScrollPane(table);
        JButton bSelectAll = new JButton("Tots");
        bSelectAll.setActionCommand(COMMAND_SELECT_ALL);
        bSelectAll.addActionListener(this);
        JButton bSelectNone = new JButton("Cap");
        bSelectNone.setActionCommand(COMMAND_SELECT_NONE);
        bSelectNone.addActionListener(this);
        JButton bPublish = new JButton("Publicar");
        bPublish.setActionCommand(COMMAND_PUBLISH);
        bPublish.addActionListener(this);
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bSelectAll)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bSelectNone)
                        .addGap(10, 10, Short.MAX_VALUE)
                        .addComponent(bPublish)
                    )
                    .addComponent(scrollTable, GroupLayout.DEFAULT_SIZE, 655, Short.MAX_VALUE)
                )
                .addGap(12, 12, 12)
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(bSelectAll)
                    .addComponent(bSelectNone)
                    .addComponent(bPublish)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollTable, GroupLayout.PREFERRED_SIZE, 218, Short.MAX_VALUE)
                .addContainerGap()
            )
        );
        
        return pContents;
	}

	@Override
	protected boolean onClose() 
	{
		return true;
	}
	
	private void loadRegistries() throws BusinessException
	{
		IExtendedIssueWorkFacade issueWorkService = (IExtendedIssueWorkFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedIssueWorkFacade.class);
		List<IssueTimerRegistry> registries = issueWorkService.findNonPublishedTimerRegistries();
		IssueTimerRegistryTableModel model = (IssueTimerRegistryTableModel)table.getModel();
		for(IssueTimerRegistry registry:registries){
			IssueTimerRegistryTableModel.ModelData modelData = new IssueTimerRegistryTableModel.ModelData(registry); 
			model.addData(modelData);
		}
	}

	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if(COMMAND_SELECT_ALL.equals(event.getActionCommand())){
			((IssueTimerRegistryTableModel)table.getModel()).selectAll();
		}
		else if(COMMAND_SELECT_NONE.equals(event.getActionCommand())){
			((IssueTimerRegistryTableModel)table.getModel()).selectNone();
		}
		else if(COMMAND_PUBLISH.equals(event.getActionCommand())){
			showInProcess(true);
			
			IExtendedIssueWorkFacade workService = (IExtendedIssueWorkFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedIssueWorkFacade.class);
			IExtendedUserFacade userService = (IExtendedUserFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedUserFacade.class);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
			IssueTimerRegistryTableModel model = (IssueTimerRegistryTableModel)table.getModel();
			List<IssueTimerRegistryTableModel.ModelData> modelData = model.getData();
			Iterator<IssueTimerRegistryTableModel.ModelData> iterator = modelData.iterator();
			while(iterator.hasNext()){
				IssueTimerRegistryTableModel.ModelData elem = iterator.next();
				if(elem.isChecked()){
					try{
						String description = "Registre automàtic";
				    	IUserDTO user = userService.getLoggedUserAsDTO();
				    	Date workingDate = dateFormat.parse(elem.getWorkingDate()); 
				    	Integer amount = elem.getAmount();
				    	workService.createIssueWorkRegistry(elem.getIssueId(), workingDate, true, description, user.getUsername(), amount);
						workService.deleteTimerRegistry(elem.getRegistryId());
						iterator.remove();
					}
					catch(ParseException e){
						// No s'hauria de donar mai perquè els registre es validen a la finestra de dialeg
						log.error("Error publicant el registre de treball: {}", e.getMessage(), e);
					}
					catch(BusinessException e){
			    		log.error("Error publicant el registre de treball: {}", e.getMessage(), e);
			            ApplicationWindowManager.getInstance().displayErrorMessage("Error publicant el registre de treball: " + e.getMessage(), e);
			    	}
				}
			}
			((AbstractTableModel)table.getModel()).fireTableDataChanged();
			
			showInProcess(false);
		}
	}
}
