package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.DropMode;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractDialog;
import org.fenrir.yggdrasil.ui.dialog.WizardDialog;
import org.fenrir.yggdrasil.ui.widget.JListDNDOrderSupport;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.wizard.IssueViewFilterAdministrationWizard;
import org.fenrir.dragonfly.core.event.IProjectListener;
import org.fenrir.dragonfly.core.event.IViewFilterListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140818
 */
@EventListener(definitions=IViewFilterListener.class)
public class IssueViewFilterManagementDialog extends AbstractDialog implements IViewFilterListener
{
    private static final long serialVersionUID = 1L;

    private final Logger log = LoggerFactory.getLogger(IssueViewFilterManagementDialog.class);

    private JList listFilters;
    private JButton bModify;
    private JButton bDelete;

    public IssueViewFilterManagementDialog() throws Exception
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Administrar filtres", new Dimension(450, 600), true);

        setIconAsResource("/org/fenrir/dragonfly/ui/icons/search_48.png");
        loadData();
    }

    @Override
    @SuppressWarnings("serial")
    protected JComponent createContents()
    {
        final IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        listFilters = new JList(new DefaultListModel());
        listFilters.setDragEnabled(true);
        listFilters.setDropMode(DropMode.INSERT);
        JListDNDOrderSupport orderSupport = new JListDNDOrderSupport(listFilters) 
        {
            @Override
            public void onDrop(int sourceIndex, int destinationIndex) 
            {
                try{
                    DefaultListModel model = (DefaultListModel)listFilters.getModel();
                    IViewFilterDTO filter = (IViewFilterDTO)model.getElementAt(sourceIndex);
                    administrationService.reorderViewFilter(filter.getFilterId(), destinationIndex);

                    // Una vegada esborrat el filtre es llança una notificació
                    IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
                    eventNotificationService.notifyNamedEvent(IViewFilterListener.class, IViewFilterListener.EVENT_VIEW_FILTER_ORDERED_ID, filter, sourceIndex, destinationIndex);
                }
                catch(Exception e){
                    log.error("Error reordenant filtre: {}", e.getMessage(), e);
                    JOptionPane.showMessageDialog(IssueViewFilterManagementDialog.this, "Error reordenant filtre: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        };
        listFilters.setTransferHandler(orderSupport);
        listFilters.addListSelectionListener(new ListSelectionListener()
        {
            @Override
            public void valueChanged(ListSelectionEvent event)
            {
            	IViewFilterDTO tag = (IViewFilterDTO)listFilters.getSelectedValue();
                if(tag!=null){
                    bModify.setEnabled(true);
                    bDelete.setEnabled(true);
                }
                else{
                    bModify.setEnabled(false);
                    bDelete.setEnabled(false);
                }
            }
        });
        JScrollPane scrollPane = new JScrollPane(listFilters);
        JButton bAdd = new JButton();
        bAdd.setAction(new AbstractAction("Afegir")
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                try{
                    IssueViewFilterAdministrationWizard wizard = new IssueViewFilterAdministrationWizard();
                    new WizardDialog<IssueViewFilterAdministrationWizard>(
                            ApplicationWindowManager.getInstance().getMainWindow(),
                            "Crear nou filtre",
                            new Dimension(500, 600),
                            wizard
                    ).open();

                    // Es refresca la llista una vegada s'ha realitzat la modificació
                    loadData();
                }
                catch(Exception e){
                    log.error("Error al crear wizard: {}", e.getMessage(), e);
                    JOptionPane.showMessageDialog(IssueViewFilterManagementDialog.this, "Error al crear wizard: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        bModify = new JButton("Modificar");
        bModify.setAction(new AbstractAction("Modificar")
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
            	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
            	
            	WizardDialog<IssueViewFilterAdministrationWizard> dialog = null;
                try{
                	IViewFilterDTO selectedFilter = (IViewFilterDTO)listFilters.getSelectedValue();
                    IssueViewFilterAdministrationWizard wizard = new IssueViewFilterAdministrationWizard(selectedFilter);
                    dialog = new WizardDialog<IssueViewFilterAdministrationWizard>(
                            ApplicationWindowManager.getInstance().getMainWindow(),
                            "Modificar filtre existent",
                            new Dimension(500, 600),
                            wizard
                    );
                    eventNotificationService.addListener(IViewFilterListener.class, wizard);
                    
                    dialog.open();

                    // Es refresca la llista una vegada s'ha realitzat la modificació
                    loadData();
                }
                catch(Exception e){
                    log.error("Error al crear wizard: {}", e.getMessage(), e);
                    JOptionPane.showMessageDialog(IssueViewFilterManagementDialog.this, "Error al crear wizard: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
                finally{
        			if(dialog!=null){
                		eventNotificationService.removeListener(dialog);
                	}
        		}
            }
        });
        bDelete = new JButton("Eliminar");
        bDelete.setAction(new AbstractAction("Eliminar")
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                try{
                	boolean deleteRegistry = JOptionPane.OK_OPTION == JOptionPane.showConfirmDialog(IssueViewFilterManagementDialog.this, 
                            "Voleu eliminar el registre sel.leccionat?", 
                            "Confirmar eliminació", 
                            JOptionPane.OK_CANCEL_OPTION, 
                            JOptionPane.QUESTION_MESSAGE);
                	if(deleteRegistry){
	                	IViewFilterDTO selectedFilter = (IViewFilterDTO)listFilters.getSelectedValue();
	                    administrationService.deleteViewFilter(selectedFilter.getFilterId());
	
	                    // Es refresca la llista una vegada s'ha realitzat la modificació
	                    loadData();
	
	                    // Una vegada esborrat el filtre es llança una notificació
	                    IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
	                    eventNotificationService.notifyNamedEvent(IViewFilterListener.class, IViewFilterListener.EVENT_VIEW_FILTER_DELETED_ID, selectedFilter);
                	}
                }
                catch(Exception e){
                    log.error("Error al eliminar filtre: {}", e.getMessage(), e);
                    JOptionPane.showMessageDialog(IssueViewFilterManagementDialog.this, "Error al eliminar filtre: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        bModify.setEnabled(false);
        bDelete.setEnabled(false);

        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 248, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(bDelete, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bModify, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(bAdd, GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bAdd)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bModify)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bDelete)
                    )
                    .addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );

        return pContents;
    }

    @Override
    protected boolean onClose()
    {
        return true;
    }

    private void loadData() throws BusinessException
    {
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

        List<IViewFilterDTO> filters = administrationService.findUserViewFilters();
        DefaultListModel model = (DefaultListModel)listFilters.getModel();
        model.removeAllElements();
        for(IViewFilterDTO elem:filters){
            model.addElement(elem);
        }
    }

	@Override
	public void viewFilterCreated(IViewFilterDTO filter) 
	{
		reloadData();
	}

	@Override
	public void viewFilterModified(IViewFilterDTO oldFilter, IViewFilterDTO newFilter) 
	{
		reloadData();
	}

	@Override
	public void viewFilterDeleted(IViewFilterDTO filter) 
	{
		reloadData();
	}

	@Override
	public void viewFilterOrdered() 
	{
		reloadData();
	}
	
	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
				log.error("Error recarregant els filtres: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant els filtres: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}
