package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import org.apache.commons.lang.StringUtils;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.3.20140405
 */
@SuppressWarnings("serial")
public class LoginDialog extends AbstractConfirmDialog 
{
    /* Components del formulari */
    private JTextField inputUsername;
    private JLabel lPassword;
    private JPasswordField inputPassword;

    private boolean passwordRequired;
    
    /* Valor de retorn */
    private String username;
    private char[] password;
    
    public LoginDialog()
    {
    	this(null, true);
    }
    
    public LoginDialog(String username, boolean passwordRequired)
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Identificació d'usuari", new Dimension(530, 220), true);

//      setIconAsResource("/org/fenrir/yggdrasil/ui/icons/gear_48.png");
    
    	inputUsername.setText(username);
    	this.passwordRequired = passwordRequired;
    	if(!passwordRequired){
    		lPassword.setVisible(false);
    		inputPassword.setVisible(false);
    	}
    }

    @Override
    protected JComponent createContents()
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        /* Definició dels components */
        JLabel lUsername = new JLabel("Nom d'usuari");
        inputUsername = new JTextField();
        lPassword = new JLabel("Contrasenya");
        inputPassword = new JPasswordField();

        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
            		.addComponent(lUsername, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            		.addComponent(lPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            	)
            	.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            	.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
        			.addComponent(inputUsername, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
            		.addComponent(inputPassword, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
            	)
                .addContainerGap()
            )
        );

        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lUsername, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputUsername, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lPassword, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputPassword, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                )
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

        return pContents;
    }

    @Override
    protected boolean onAccept()
    {
        String strInputUsername = inputUsername.getText().trim();
        if(StringUtils.isBlank(strInputUsername)){
        	setMessage("Ha d'especificar un usuari", MSG_TYPE_ERROR);
            return false;
        }
        if(passwordRequired && inputPassword.getPassword().length==0){
        	setMessage("Ha d'especificar la contrasenya", MSG_TYPE_ERROR);
        	inputPassword.setText(null);
            return false;
        }
        	
        // S'especifiquen el valors de retorn
        username = strInputUsername;
        password = inputPassword.getPassword();           
            
        return true;
    }

    @Override
    protected boolean onCancel()
    {
        return true;
    }
    
    public String getUsername()
    {
    	return username;
    }
    
    public char[] getPassword()
    {
    	return password;
    }
}