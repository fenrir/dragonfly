package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.table.TableColumn;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.vespine.core.provider.IssueContentProvider;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;
import org.fenrir.dragonfly.ui.widget.ProviderDataTableModel;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140419
 */
@SuppressWarnings("serial")
public class ProviderDataDialog extends AbstractConfirmDialog
{
	// Datasource amb totes les dades corresponents a tots el proveidors
	private List<IProviderElementDTO> datasource;
	// Conjunt de valors inicialment sel.leccionats
	private List<IProviderElementDTO> initiallySelectedData;
	// Llista de dades que serà retornada en tancar la finestra
	private List<IProviderElementDTO> selectedData;
	
	private JComboBox comboProviders;
	private JTable tableMappedData;
    private ProviderDataTableModel<IProviderElementDTO> modelMappedData;    
	
    public ProviderDataDialog(List<IProviderElementDTO> data)
    {
    	this(data, Collections.<IProviderElementDTO>emptyList());
    }
    
	public ProviderDataDialog(List<IProviderElementDTO> data, 
			List<IProviderElementDTO> initiallySelectedData)
	{
		super(ApplicationWindowManager.getInstance().getMainWindow(), "Sel.lecció de registre associat", new Dimension(530, 620), true);
		
		this.datasource = data;
		this.initiallySelectedData = initiallySelectedData;
		
		// Es carreguen els proveidors disponibles
		IssueContentProvider contentProvider = (IssueContentProvider)ApplicationContext.getInstance().getRegisteredComponent(IssueContentProvider.class);
		Collection<String> providers = contentProvider.getProviders();
		for(String elem:providers){
			((DefaultComboBoxModel)comboProviders.getModel()).addElement(elem);
		}
		// Del proveidor sel.leccionat inicialment, es carreguen les dades disponibles
		loadSelectedProviderData();
	}
	
	@Override
    protected JComponent createContents() 
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        JLabel lProvider = new JLabel("Proveidor", SwingConstants.RIGHT);
        comboProviders = new JComboBox(new DefaultComboBoxModel());
        comboProviders.addItemListener(new ItemListener() 
        {
			@Override
			public void itemStateChanged(ItemEvent event) 
			{
				if(ItemEvent.SELECTED==event.getStateChange()){
					// TODO Alerta canvis perduts
					loadSelectedProviderData();
				}
			}
		});
        
        modelMappedData = new ProviderDataTableModel<IProviderElementDTO>(ProviderDataTableModel.MODE_CHECK_SELECTION_ENABLED);
        tableMappedData = new JTable(modelMappedData);
        // S'ajusta el tamany de la primera col.lumna corresponent al check sel.lector
        TableColumn col = tableMappedData.getColumnModel().getColumn(0);        
        col.setMaxWidth(20);
        JScrollPane scrollProviderData = new JScrollPane(tableMappedData);

        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                    	.addComponent(lProvider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE)
                    	.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    	.addComponent(comboProviders, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            		)
                    .addComponent(scrollProviderData)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
	        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	        .addGroup(layout.createSequentialGroup()
	            .addContainerGap()
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lProvider)
	                .addComponent(comboProviders, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	            .addComponent(scrollProviderData, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
	            .addContainerGap()
	        )
	    );
        
        return pContents;
    }
	
	@Override
    protected boolean onAccept() 
    {
		int selectedIndex = tableMappedData.getSelectedRow(); 
		if(selectedIndex<0){
			setMessage("Ha de sel.leccionar un registre", MSG_TYPE_WARN);
			return false;
		}
		selectedData = modelMappedData.getSelectedData();
		
		return true;
    }
	
	@Override
    protected boolean onCancel() 
    {
		return true;
    }
	
	public String getSelectedProvider()
	{
		return (String)comboProviders.getModel().getSelectedItem();
	}
	
	public List<IProviderElementDTO> getSelectedData()
    {
        return selectedData;
    }
	
	private void loadSelectedProviderData()
	{
		String selectedProvider = (String)comboProviders.getSelectedItem();
		if(selectedProvider!=null){
			modelMappedData.clearModelData();
			for(IProviderElementDTO elem:datasource){
				if(selectedProvider.equals(elem.getProvider())){
					boolean selected = initiallySelectedData.contains(elem);
					modelMappedData.addData(elem, selected);
				}
			}
		}
	}
}
