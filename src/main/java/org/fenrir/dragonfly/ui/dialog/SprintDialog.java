package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.dragonfly.core.dto.SprintFormDTO;
import org.fenrir.dragonfly.core.event.ISprintListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20141018
 */
@EventListener(definitions=ISprintListener.class)
public class SprintDialog extends AbstractConfirmDialog implements ISprintListener
{
	private static final long serialVersionUID = -4467197433251492281L;
	
	// Valor de retorn
    private SprintFormDTO sprint;
    // Valor original en cas de modificació
    private ISprintDTO originalSprint;
    
    private JComboBox comboProject;
    private JTextField inputStartDate;
    private JTextField inputEndDate;
    
    public SprintDialog() throws BusinessException
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear nou sprint", new Dimension(400, 250), true);
        
        loadData();
    }
    
    public SprintDialog(ISprintDTO sprint) throws BusinessException
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar sprint existent", new Dimension(400, 250), true);
    	this.originalSprint = sprint;
    	
    	loadData();
    	loadSprintData();
    }
    
    @Override
    protected JComponent createContents()
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        /* Definició dels components */
        JLabel lProject = new JLabel("Projecte", SwingConstants.RIGHT);
        comboProject = new JComboBox(new DefaultComboBoxModel());
        JLabel lStartDate = new JLabel("Data inici", SwingConstants.RIGHT);
        inputStartDate = new JTextField();                
        JLabel lEndDate = new JLabel("Data final", SwingConstants.RIGHT);
        inputEndDate = new JTextField();

        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
            		.addComponent(lProject, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
            		.addComponent(lStartDate, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
            		.addComponent(lEndDate, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
            		.addComponent(comboProject, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
            		.addComponent(inputStartDate, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
            		.addComponent(inputEndDate, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                )
                .addContainerGap()
            )
        );

        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lProject, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboProject, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)                    
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lStartDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputStartDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)                    
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lEndDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputEndDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)                    
                )
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

        return pContents;
    }

    private void loadData() throws BusinessException
    {
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
    	List<IProjectDTO> projects = administrationService.findAllProjects();
    	for(IProjectDTO elem:projects){
    		((DefaultComboBoxModel)comboProject.getModel()).addElement(elem);
    	}
    }
    
    private void loadSprintData()
    {
    	SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    	inputStartDate.setText(df.format(originalSprint.getStartDate()));
    	inputEndDate.setText(df.format(originalSprint.getEndDate()));
    }
    
    @Override
    protected boolean onAccept()
    {
    	IProjectDTO project = (IProjectDTO)comboProject.getSelectedItem();
    	String strStartDate = inputStartDate.getText().trim();
    	String strEndDate = inputEndDate.getText().trim();
    	Date startDate;
    	Date endDate;
    	try{
    		startDate = new SimpleDateFormat("dd/MM/yyyy").parse(strStartDate);
    	}
    	catch(ParseException e){
    		setMessage("El camp DATA INICI ha de contenir una data vàlida (DD/MM/YYYY)", MSG_TYPE_ERROR);
    		return false;
    	}
    	
    	try{
    		endDate = new SimpleDateFormat("dd/MM/yyyy").parse(strEndDate);
    	}
    	catch(ParseException e){
    		setMessage("El camp DATA FINAL ha de contenir una data vàlida (DD/MM/YYYY)", MSG_TYPE_ERROR);
    		return false;
    	}
    	
    	if(startDate.after(endDate)){
    		setMessage("La data final de l'sprint no pot ser posterior a la d'inici", MSG_TYPE_ERROR);
    		return false;
    	}
    	
    	sprint = new SprintFormDTO();
    	 // En cas de moficiació també es retorna la ID
        if(originalSprint!=null){
        	sprint.setSprintId(originalSprint.getSprintId());
        }
        sprint.setProject(project);
        sprint.setStartDate(startDate);
        sprint.setEndDate(endDate);
    	
    	return true;
    }

    @Override
    protected boolean onCancel()
    {
    	sprint = null;
    	
        return true;
    }
    
    public ISprintDTO getFormData()
    {
    	return sprint;
    }

	@Override
	public void sprintCreated(ISprintDTO sprint) 
	{
		// Res a fer...
	}

	@Override
	public void sprintUpdated(final ISprintDTO oldSprint, final ISprintDTO newSprint) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalSprint!=null && originalSprint.getSprintId().equals(newSprint.getSprintId())){
				this.originalSprint = (ISprintDTO)newSprint;
				JOptionPane.showMessageDialog(this, 
		                "Les dades de l'sprint han sigut modificades externament. Es procedirà a carregar-les de nou", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				loadSprintData();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	sprintUpdated(oldSprint, newSprint);
                }
            });
		}
	}

	@Override
	public void sprintDeleted(final ISprintDTO sprint) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalSprint!=null && originalSprint.getSprintId().equals(sprint.getSprintId())){
				JOptionPane.showMessageDialog(this, 
		                "L'sprint ha sigut eliminat.", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				close();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	sprintDeleted(sprint);
                }
            });
		}
	}
}
