package org.fenrir.dragonfly.ui.dialog;

import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.event.ISprintListener;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140818
 */
@EventListener(definitions=ISprintListener.class)
public class SprintManagementDialog extends AbstractEntityManagementDialog<ISprintDTO> implements ISprintListener
{
	private static final long serialVersionUID = -921300865003901735L;
	
	private final Logger log = LoggerFactory.getLogger(SprintManagementDialog.class);
	
	private static final String PARAMETER_NAME = "SPRINT";
	
    public SprintManagementDialog() throws Exception
    {
        super("Administrar Sprints", 450, 600);
    }

    @Override
    protected void loadData() throws BusinessException
    {
    	IExtendedIssueWorkFacade sprintService = (IExtendedIssueWorkFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedIssueWorkFacade.class);

        List<ISprintDTO> sprints = sprintService.findAllSprints();
        addElements(sprints);
    }

    @Override
    protected boolean openAddDialog(Map<String, Object> parameters) 
    {
    	try{
	        SprintDialog dialog = new SprintDialog();
	        boolean returnValue = dialog.open()==SprintDialog.DIALOG_OK;
	        ISprintDTO sprint = dialog.getFormData();
	        parameters.put(PARAMETER_NAME, sprint);
	        
	        return returnValue;
    	}
    	catch(BusinessException e){
    		log.error("Error obrint la finestra de creació d'sprints: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error obrint la finestra de creació d'sprints: " + e.getMessage(), e);
    	}
    	
    	return false;
    }
    
    @Override
    protected void doAdd(Map<String, Object> parameters)
    {        
    	try{
	    	IExtendedIssueWorkFacade sprintService = (IExtendedIssueWorkFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedIssueWorkFacade.class);
	        ISprintDTO sprint = (ISprintDTO)parameters.get(PARAMETER_NAME);
	        sprintService.createSprint(sprint.getProject().getProjectId(), sprint.getStartDate(), sprint.getEndDate());
    	}
    	catch(BusinessException e){
    		log.error("Error creant el nou sprint: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error creant el nou sprint: " + e.getMessage(), e);
    	}
    }

    @Override
    protected boolean openModifyDialog(Map<String, Object> parameters) 
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
    	ISprintDTO selectedSprint = (ISprintDTO)listEntities.getSelectedValue();
    	SprintDialog dialog = null;
    	try{
    		dialog = new SprintDialog(selectedSprint);
    		eventNotificationService.addListener(ISprintListener.class, dialog);
    		
    		boolean returnValue = dialog.open()==SprintDialog.DIALOG_OK;
            ISprintDTO sprint = dialog.getFormData();
            parameters.put(PARAMETER_NAME, sprint);
            
            return returnValue;
    	}
    	catch(BusinessException e){
    		log.error("Error obrint la finestra de modificació de l'sprint: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error obrint la finestra de modificació de l'sprint: " + e.getMessage(), e);
    	}
    	finally{
			if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
		}
    	
    	return false;
    }
    
    @Override
    protected void doModify(Map<String, Object> parameters)
    {        
    	try{
	    	IExtendedIssueWorkFacade sprintService = (IExtendedIssueWorkFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedIssueWorkFacade.class);
	    	ISprintDTO sprint = (ISprintDTO)parameters.get(PARAMETER_NAME);
	    	sprintService.updateSprint(sprint.getSprintId(), sprint.getProject().getProjectId(), sprint.getStartDate(), sprint.getEndDate());            
    	}
    	catch(BusinessException e){
    		log.error("Error modificant l'sprint: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error modificant l'sprint: " + e.getMessage(), e);
    	}
    }

    @Override
    protected void doDelete(Map<String, Object> parameters)
    {
    	try{
	    	ISprintDTO selectedSprint = (ISprintDTO)listEntities.getSelectedValue();
	    	IExtendedIssueWorkFacade sprintService = (IExtendedIssueWorkFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedIssueWorkFacade.class);
	        sprintService.deleteSprint(selectedSprint.getSprintId());
    	}
    	catch(BusinessException e){
    		log.error("Error eliminant l'sprint: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error eliminant l'sprint: " + e.getMessage(), e);
    	}
    }

	@Override
	public void sprintCreated(ISprintDTO sprint) 
	{
		reloadData();
	}

	@Override
	public void sprintUpdated(ISprintDTO oldSprint, ISprintDTO newSprint) 
	{
		reloadData();
	}

	@Override
	public void sprintDeleted(ISprintDTO sprint) 
	{
		reloadData();	
	}
    
	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
				log.error("Error recarregant els sprints: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant els sprints: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}