package org.fenrir.dragonfly.ui.dialog;

import java.util.List;
import java.util.Map;
import javax.swing.SwingUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.dragonfly.core.event.ITagListener;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.exception.BusinessException;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140825
 */
@EventListener(definitions=ITagListener.class)
public class TagManagementDialog extends AbstractEntityManagementDialog<ITagDTO> implements ITagListener
{
	private static final long serialVersionUID = -583451031542800458L;
	
	private final Logger log = LoggerFactory.getLogger(TagManagementDialog.class);
	
	private static final String PARAMETER_NAME = "TAG";
	
    public TagManagementDialog() throws Exception
    {
        super("Administrar Tags", 450, 600);
        
        setIconAsResource("/org/fenrir/dragonfly/ui/icons/tag_48.png");
    }

    @Override
    protected void loadData() throws BusinessException
    {
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

        List<ITagDTO> tags = administrationService.findAllTags();
        addElements(tags);
    }

    @Override
    protected boolean openAddDialog(Map<String, Object> parameters) 
    {
        TagDialog dialog = new TagDialog();
        boolean returnValue = dialog.open()==TagDialog.DIALOG_OK;
        ITagDTO tag = dialog.getFormData();
        parameters.put(PARAMETER_NAME, tag);
        
        return returnValue;
    }
    
    @Override
    protected void doAdd(Map<String, Object> parameters) throws Exception
    {        
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
        ITagDTO tag = (ITagDTO)parameters.get(PARAMETER_NAME);
        administrationService.createTag(tag.getName(), tag.isPreferred());
    }

    @Override
    protected boolean openModifyDialog(Map<String, Object> parameters) 
    {
    	IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
    	
        ITagDTO selectedTag = (ITagDTO)listEntities.getSelectedValue();
        TagDialog dialog = null;
        try{
        	dialog = new TagDialog(selectedTag);
        	eventNotificationService.addListener(ITagListener.class, dialog);
        	
        	boolean returnValue = dialog.open()==TagDialog.DIALOG_OK;
        	ITagDTO tag = dialog.getFormData();
        	parameters.put(PARAMETER_NAME, tag);
        	
        	return returnValue;
        }
        finally{
			if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
		}
    }
    
    @Override
    protected void doModify(Map<String, Object> parameters) throws Exception
    {        
    	IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
        ITagDTO tag = (ITagDTO)parameters.get(PARAMETER_NAME);
        administrationService.updateTag(tag.getTagId(), 
        		tag.getName(), 
        		tag.isPreferred());            
    }

    @Override
    protected void doDelete(Map<String, Object> parameters) throws Exception
    {
        ITagDTO selectedTag = (ITagDTO)listEntities.getSelectedValue();
        IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
        administrationService.deleteTag(selectedTag.getTagId());
    }

	@Override
	public void tagListUpdated() 
	{
		reloadData();
	}
    
	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
				log.error("Error recarregant les etiquetes: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant les etiquetes: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}