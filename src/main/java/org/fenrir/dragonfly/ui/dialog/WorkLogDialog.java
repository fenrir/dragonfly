package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractDialog;
import org.fenrir.yggdrasil.ui.widget.ElementSearchTextField;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.externalize.widget.TableTooltipCellRenderer;
import org.fenrir.dragonfly.ui.widget.IssueWorkRegistryTableModel;
import org.fenrir.dragonfly.core.provider.UserSearchProvider;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140722
 */
public class WorkLogDialog extends AbstractDialog implements ActionListener 
{
	private static final long serialVersionUID = -2191726416623636378L;
	
	private static final String ACTION_COMMAND_ADD_REGISTRY = "ADD_REGISTRY";
	private static final String ACTION_COMMAND_MODIFY_REGISTRY = "MODIFY_REGISTRY";
    private static final String ACTION_COMMAND_REMOVE_REGISTRY = "REMOVE_REGISTRY";
    private static final String ACTION_COMMAND_CANCEL_MODIFICATION = "CANCEL_MODIFICATION";
	
    private final Logger log = LoggerFactory.getLogger(WorkLogDialog.class);
    
	private IIssueDTO issue;
	
	private IWorkRegistryDTO selectedRegistry;
	
	private ElementSearchTextField<IUserDTO> inputUser;
	private JTextField inputWorkingDay;
	private JTextField inputAmount;
	private JTextField inputDescription;
	private JCheckBox checkSprint;
	private JButton bAddRegistry;
	private JButton bModifyRegistry;
    private JButton bRemoveRegistry;
    private JButton bCancelModification; 
    private JTable tableRegistries;
    private IssueWorkRegistryTableModel modelTableRegistries; 
	
    public WorkLogDialog(IIssueDTO issue)
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Registres de treball", new Dimension(720, 620), true);
    	this.issue = issue;
    	
    	try{
    		loadData();
    	}
    	catch(BusinessException e){
			log.error("Error carregant la llista de registres : {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error carregant la llista de registres: " + e.getMessage(), e);
            
            // Si no s'ha pogut carregar el contingut, es tanca la finestra després de mostrar l'error
            setVisible(false);
            dispose();
		}
    }
    
	@Override
	protected JComponent createContents() 
	{
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        JLabel lUser = new JLabel("Usuari"); 
        JLabel lWorkingDay = new JLabel("Data");
        JLabel lAmount = new JLabel("Temps");
        JLabel lMinutes = new JLabel("min.");
        JLabel lDescription = new JLabel("Descripció");
        inputUser = new ElementSearchTextField<IUserDTO>(UserSearchProvider.class);
        inputWorkingDay = new JTextField();
        inputAmount = new JTextField();
        inputDescription = new JTextField();
        checkSprint = new JCheckBox("Incloure a Sprint");
        // Per defecte s'inclou a l'sprint actiu pel rang de dates especificat
        checkSprint.setSelected(true);
        /* Opció afegir nou registre */
        bAddRegistry = new JButton("Afegir");
        bAddRegistry.addActionListener(this);
        bAddRegistry.setActionCommand(ACTION_COMMAND_ADD_REGISTRY);
        /* Opció modificat registre existent */
        bModifyRegistry = new JButton("Modificar");
        bModifyRegistry.addActionListener(this);
        bModifyRegistry.setActionCommand(ACTION_COMMAND_MODIFY_REGISTRY);
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bModifyRegistry.setVisible(false);
        /* Opció esborrar registre sel.leccionat */
        bRemoveRegistry = new JButton("Borrar");
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bRemoveRegistry.setVisible(false);
        bRemoveRegistry.setActionCommand(ACTION_COMMAND_REMOVE_REGISTRY);
        bRemoveRegistry.addActionListener(this);
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bRemoveRegistry.setVisible(false);
        /* Opció cancelar modificació en curs */
        bCancelModification = new JButton("Cancel.lar");
        bCancelModification.addActionListener(this);
        bCancelModification.setActionCommand(ACTION_COMMAND_CANCEL_MODIFICATION);
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bCancelModification.setVisible(false);
        /* Taula de regitres existents */
        modelTableRegistries = new IssueWorkRegistryTableModel();
        tableRegistries = new JTable(modelTableRegistries);
        tableRegistries.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        // S'afegeixen tooltips per mostrar els valors de les cel.les de la taula
        for(int i=0; i<tableRegistries.getColumnCount(); i++){
        	TableColumn column = tableRegistries.getColumnModel().getColumn(i);
        	column.setCellRenderer(new TableTooltipCellRenderer());
        }
        tableRegistries.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
        {
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				int index = tableRegistries.getSelectedRow();
				if(index>=0){
					IWorkRegistryDTO registry = modelTableRegistries.getModelData().get(index);
					setSelectedRegistry(registry);
				}
				else{
					setSelectedRegistry(null);
				}
			}
		});
        JScrollPane scrollSteps = new JScrollPane(tableRegistries);
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
            				.addComponent(lUser, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            				.addComponent(lDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            				.addComponent(lWorkingDay, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)            				
                    	)
                    	.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    	.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                			.addComponent(inputUser, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                			.addComponent(checkSprint, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                			.addComponent(inputDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                			.addGroup(layout.createSequentialGroup()
		                		.addComponent(inputWorkingDay, 100, 100, 100)
		                		.addGap(20)
		                		.addComponent(lAmount, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
		                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
		                		.addComponent(inputAmount, 100, 100, 100)
		                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
		                		.addComponent(lMinutes, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
		                		.addGap(20, 20, Short.MAX_VALUE)
		                		.addComponent(bModifyRegistry, 85, 85, 85)
		                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
		                		.addComponent(bRemoveRegistry, 85, 85, 85)
		                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
		                		.addComponent(bCancelModification, 85, 85, 85)
		                		.addComponent(bAddRegistry, 85, 85, 85)
		                	)
		                )
            		)
                    .addComponent(scrollSteps)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
	        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	        .addGroup(layout.createSequentialGroup()
	            .addContainerGap()
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
	                .addComponent(lUser)
	                .addComponent(inputUser, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
	                .addComponent(lDescription)
	                .addComponent(inputDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	            .addComponent(checkSprint)
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lWorkingDay)
	                .addComponent(inputWorkingDay)
	                .addComponent(lAmount)
	                .addComponent(inputAmount)
	                .addComponent(lMinutes)
	                .addComponent(bModifyRegistry)
	                .addComponent(bRemoveRegistry)
	                .addComponent(bCancelModification)
	                .addComponent(bAddRegistry)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)	            
	            .addComponent(scrollSteps, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
	            .addContainerGap()
	        )
	    );
        
        return pContents;
	}
	
	private void loadData() throws BusinessException
	{
		IExtendedIssueWorkFacade sprintService = (IExtendedIssueWorkFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedIssueWorkFacade.class);
		IExtendedUserFacade userService = (IExtendedUserFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedUserFacade.class);
		List<IWorkRegistryDTO> registries = sprintService.findAllIssueWorkRegistries(issue.getIssueId());
		/* S'afegeixen els elements a la taula. No es fa a través del mètode addElement del model perquè notificaria cada inserció.
		 * Al final del procés es farà una notificació indicant els canvis a la taula. D'aquesta manera es millora el rendiment.
		 */
		for(IWorkRegistryDTO elem:registries){
			modelTableRegistries.getModelData().add(elem);
		}
		// Es notifica els canvis a la taula ja que el mètode d'inserció utilitzat no ho fa
		modelTableRegistries.fireTableDataChanged();
		// Es carrega l'usuari actual per defecte
		IUserDTO defaultUser = userService.getLoggedUserAsDTO();
		inputUser.setSelectedElement(defaultUser);
	}
	
	@Override
	protected boolean onClose() 
	{
		return true;
	}
	
	private void setSelectedRegistry(IWorkRegistryDTO selectedRegistry)
	{
		this.selectedRegistry = selectedRegistry;
		
		IExtendedUserFacade userService = (IExtendedUserFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedUserFacade.class);
		IUserDTO selectedUser = null;
		try{
			userService.getLoggedUserAsDTO();
		}
		catch(BusinessException e){
			log.error("Error carregant les dades de l'usuari: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant les dades de l'usuari: " + e.getMessage(), e);
		}
		String strDescription = null;
		String strWorkingDay = null;
		String strAmount = null;
		boolean includeInSprint = true;
		if(selectedRegistry!=null){
			selectedUser = selectedRegistry.getUser();
			strDescription = selectedRegistry.getDescription();
			strWorkingDay = new SimpleDateFormat("dd/MM/yyyy").format(selectedRegistry.getWorkingDay());
			strAmount = selectedRegistry.getTime().toString();
			includeInSprint = selectedRegistry.getSprint()!=null;
		}
		inputUser.setSelectedElement(selectedUser);
		inputDescription.setText(strDescription);
		inputWorkingDay.setText(strWorkingDay);
		inputAmount.setText(strAmount);
		checkSprint.setSelected(includeInSprint);
		
		bAddRegistry.setVisible(selectedRegistry==null);
		bRemoveRegistry.setVisible(selectedRegistry!=null);
		bModifyRegistry.setVisible(selectedRegistry!=null);
		bCancelModification.setVisible(selectedRegistry!=null);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		IExtendedIssueWorkFacade issueWorkService = (IExtendedIssueWorkFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedIssueWorkFacade.class);
		
		if(ACTION_COMMAND_ADD_REGISTRY.equals(event.getActionCommand())
				|| ACTION_COMMAND_MODIFY_REGISTRY.equals(event.getActionCommand())){
			// Comprobació dels valors introduits
			String strDescription = inputDescription.getText();
			String strWorkingDay = inputWorkingDay.getText();
			String strAmount = inputAmount.getText();
			IUserDTO user = inputUser.getSelectedElement();
			boolean includeInSprint = checkSprint.isSelected();
			Date workingDay;
			try{
				workingDay = new SimpleDateFormat("dd/MM/yyyy").parse(strWorkingDay);
			}
			catch(ParseException e){
				setMessage("El format del camp DIA no és válid (DD/MM/YYYY)", MSG_TYPE_ERROR);
				return;
			}
			// Es permet introduir dates a passat però no a futur
//			if(!DateUtils.isSameDay(workingDay, issue.getSendDate())
//					&& workingDay.before(issue.getSendDate())){
//				setMessage("El valor del camp DIA ha de ser posterior a la data de creació de l'incidencia", MSG_TYPE_ERROR);
//				return;
//			}
			if(!DateUtils.isSameDay(workingDay, issue.getSendDate())
					&& workingDay.after(new Date())){
				setMessage("El valor del camp 'Dia' no ha de ser posterior a la data d'avui", MSG_TYPE_ERROR);
				return;
			}
			if(StringUtils.isBlank(strAmount) || !StringUtils.isNumeric(strAmount)){
				setMessage("El valor del camp TEMPS no és válid", MSG_TYPE_ERROR);
				return;
			}
			Integer amount = Integer.valueOf(strAmount);
			if(amount<=0){
				setMessage("El valor del camp TEMPS ha de ser més gran que 0", MSG_TYPE_ERROR);
				return;
			}
			if(user==null){
				setMessage("Ha d'introduir un valor en el camp USUARI", MSG_TYPE_ERROR);
				return;
			}

			if(ACTION_COMMAND_ADD_REGISTRY.equals(event.getActionCommand())){
				try{
					IWorkRegistryDTO registry = issueWorkService.createIssueWorkRegistry(issue.getIssueId(), workingDay, includeInSprint, strDescription, user.getUsername(), amount);
					modelTableRegistries.addData(registry);
				}
				catch(BusinessException e){
					log.error("Error creant el registre de treball: {}", e.getMessage(), e);
		            ApplicationWindowManager.getInstance()
		                    .displayErrorMessage("Error creant el registre de treball: " + e.getMessage(), e);
				}
			}
			else{
				try{
					IWorkRegistryDTO registry = issueWorkService.updateIssueWorkRegistry(selectedRegistry.getRegistryId(), issue.getIssueId(), workingDay, includeInSprint, strDescription, user.getUsername(), amount);
					int index = modelTableRegistries.getModelData().indexOf(selectedRegistry);
					modelTableRegistries.removeData(selectedRegistry);
					modelTableRegistries.getModelData().add(index, registry);
					modelTableRegistries.fireTableDataChanged();
					tableRegistries.getSelectionModel().clearSelection();
				}
				catch(BusinessException e){
					log.error("Error modificant el registre de treball: {}", e.getMessage(), e);
		            ApplicationWindowManager.getInstance()
		                    .displayErrorMessage("Error modificant el registre de treball: " + e.getMessage(), e);
				}
			}
		}
		else if(ACTION_COMMAND_REMOVE_REGISTRY.equals(event.getActionCommand())){
			try{
				int selectedIndex = tableRegistries.getSelectedRow();
				IWorkRegistryDTO registry = modelTableRegistries.getModelData().get(selectedIndex);
				issueWorkService.deleteIssueWorkRegistry(registry.getRegistryId(), registry.getIssue().getIssueId());
				modelTableRegistries.removeData(registry);
			}
			catch(BusinessException e){
				log.error("Error esborrant el registre de treball: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance()
	                    .displayErrorMessage("Error esborrant el registre de treball: " + e.getMessage(), e);
			}
		}
		else if(ACTION_COMMAND_CANCEL_MODIFICATION.equals(event.getActionCommand())){
			tableRegistries.getSelectionModel().clearSelection();
		}
	}		
}
