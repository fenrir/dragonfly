package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.widget.WorkflowStepTableModel;
import org.fenrir.dragonfly.core.dto.WorkflowStepFormDTO;
import org.fenrir.dragonfly.core.event.IWorkflowListener;
import org.fenrir.dragonfly.core.dto.WorkflowFormDTO;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20141018
 */
@EventListener(definitions=IWorkflowListener.class)
public class WorkflowDialog extends AbstractConfirmDialog implements ActionListener, IWorkflowListener
{
	private static final long serialVersionUID = -7080730559793451515L;
	
    private static final String ACTION_COMMAND_ADD_STEP = "ADD_STEP";
	private static final String ACTION_COMMAND_MODIFY_STEP = "MODIFY_STEP";
    private static final String ACTION_COMMAND_REMOVE_STEP = "REMOVE_STEP";
    private static final String ACTION_COMMAND_CANCEL_SELECTION = "CANCEL_SELECTION";

    private final Logger log = LoggerFactory.getLogger(WorkflowDialog.class);
    
	// Valor de retorn
    private WorkflowFormDTO workflow;
    // Valor original en cas de modificació
    private IWorkflowDTO originalWorkflow;
    
    private JTextField inputName;
    private JComboBox comboInitialStatus;
    private JButton bAddStep;
	private JButton bModifyStep;
    private JButton bRemoveStep;
    private JButton bCancelSelection; 
    private JTable tableSteps;
    private WorkflowStepTableModel modelTableSteps; 
    
    private WorkflowStepDialog stepDialog;
	
	public WorkflowDialog()
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear nou flux de treball", new Dimension(530, 620), true);
        
        loadData();
    }
	
	public WorkflowDialog(IWorkflowDTO workflow)
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar flux de treball existent", new Dimension(530, 620), true);
    	this.originalWorkflow = workflow;
    	
    	loadData();
    	loadWorkflowData();
    }
	
	@Override
	protected JComponent createContents() 
	{
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        /* Definició dels components */
        JLabel lName = new JLabel("Nom", SwingConstants.RIGHT);
        JLabel lInitialStatus = new JLabel("Estat inicial", SwingConstants.RIGHT);
        JLabel lTransitions = new JLabel("Transicions");
        
        inputName = new JTextField();
        comboInitialStatus = new JComboBox(new DefaultComboBoxModel());
        /* Opció afegir nou registre */
        bAddStep = new JButton("Afegir");
        bAddStep.addActionListener(this);
        bAddStep.setActionCommand(ACTION_COMMAND_ADD_STEP);
        /* Opció modificat registre existent */
        bModifyStep = new JButton("Modificar");
        bModifyStep.addActionListener(this);
        bModifyStep.setActionCommand(ACTION_COMMAND_MODIFY_STEP);
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bModifyStep.setVisible(false);
        /* Opció esborrar registre sel.leccionat */
        bRemoveStep = new JButton("Borrar");
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bRemoveStep.setVisible(false);
        bRemoveStep.setActionCommand(ACTION_COMMAND_REMOVE_STEP);
        bRemoveStep.addActionListener(this);
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bRemoveStep.setVisible(false);
        /* Opció cancelar modificació en curs */
        bCancelSelection = new JButton("Cancel.lar");
        bCancelSelection.addActionListener(this);
        bCancelSelection.setActionCommand(ACTION_COMMAND_CANCEL_SELECTION);
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bCancelSelection.setVisible(false);
        modelTableSteps = new WorkflowStepTableModel();
        tableSteps = new JTable(modelTableSteps);
        // S'ajusta el tamany de la primera columna
        TableColumn col = tableSteps.getColumnModel().getColumn(1);        
        col.setMaxWidth(20);
        tableSteps.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
        {
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
				int index = tableSteps.getSelectedRow();
				if(index>=0){
					IWorkflowStepDTO selectedStep = modelTableSteps.getModelData().get(index);
					stepSelected(selectedStep);
				}
				else{
					stepSelected(null);
				}
			}
		});
        JScrollPane scrollSteps = new JScrollPane(tableSteps);
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)            		
    				.addGroup(layout.createSequentialGroup()
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
    						.addComponent(lName, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    						.addComponent(lInitialStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
							.addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
							.addComponent(comboInitialStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						)
    				)
                    .addGroup(layout.createSequentialGroup()
                		.addComponent(lInitialStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboInitialStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    )
                    .addGroup(layout.createSequentialGroup()
                		.addComponent(lTransitions, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                    	.addGap(5, 5, Short.MAX_VALUE)
                		.addComponent(bModifyStep, 85, 85, 85)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(bRemoveStep, 85, 85, 85)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(bCancelSelection, 85, 85, 85)
                		.addComponent(bAddStep, 85, 85, 85)
            		)
                    .addComponent(scrollSteps)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
	        layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	        .addGroup(layout.createSequentialGroup()
	            .addContainerGap()
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lName)
	                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
	                .addComponent(lInitialStatus)
	                .addComponent(comboInitialStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
	            )
	            .addGap(20)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
            		.addComponent(lTransitions)
	                .addComponent(bModifyStep)
	                .addComponent(bRemoveStep)
	                .addComponent(bCancelSelection)
	                .addComponent(bAddStep)
	            )
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)	            
	            .addComponent(scrollSteps, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
	            .addContainerGap()
	        )
	    );
        
        return pContents;
	}
	
	private void loadWorkflowData()
	{
		inputName.setText(originalWorkflow.getName());
    	comboInitialStatus.setSelectedItem(originalWorkflow.getInitialStatus());
    	// Carregar les dades mapejades
    	for(IWorkflowStepDTO elem:originalWorkflow.getWorkflowSteps()){
    		modelTableSteps.addData(elem);
    	}
	}
	
	@Override
	protected boolean onAccept() 
	{
		String name = inputName.getText().trim();
		IStatusDTO initialStatus = (IStatusDTO)comboInitialStatus.getSelectedItem();
		List<IWorkflowStepDTO> steps = modelTableSteps.getModelData();
		if(StringUtils.isBlank(name)){
			setMessage("El camp NOM no pot ser buit", MSG_TYPE_ERROR);
			return false;
		}
		if(initialStatus==null){
			setMessage("El camp ESTAT INICIAL no pot ser buit", MSG_TYPE_ERROR);
			return false;
		}
		if(steps.isEmpty()){
			setMessage("Ha de definir almenys una transició", MSG_TYPE_ERROR);
			return false;
		}
    	
    	workflow = new WorkflowFormDTO();
    	// En cas de moficiació també es retorna la ID
        if(originalWorkflow!=null){
        	workflow.setWorkflowId(originalWorkflow.getWorkflowId());
        }
        workflow.setName(name);
        workflow.setInitialStatus(initialStatus);
        workflow.setWorkflowSteps(modelTableSteps.getModelData());
    	
    	return true;
	}

	@Override
	protected boolean onCancel() 
	{
		workflow = null;
        return true;
	}
	
	public IWorkflowDTO getFormData()
    {
        return workflow;
    }
	
	private void loadData()
	{
		try{
			IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
	        List<IStatusDTO> status = administrationService.findAllStatus();
	        for(IStatusDTO elem:status){
	        	((DefaultComboBoxModel)comboInitialStatus.getModel()).addElement(elem);
	        }
		}
		catch(BusinessException e){
			log.error("Error recuperant la llista d'estats: {}", e.getMessage(), e);
			ApplicationWindowManager.getInstance()
            		.displayErrorMessage("Error recuperant la llista d'estats: " + e.getMessage(), e);
		}
	}
	
	private void stepSelected(IWorkflowStepDTO selectedStep)
	{
		bAddStep.setVisible(selectedStep==null);
		bRemoveStep.setVisible(selectedStep!=null);
		bModifyStep.setVisible(selectedStep!=null);
		bCancelSelection.setVisible(selectedStep!=null);
	}
	
	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if(ACTION_COMMAND_ADD_STEP.equals(event.getActionCommand())){
			WorkflowStepDialog dialog = new WorkflowStepDialog(modelTableSteps.getModelData());
			if(dialog.open()==WorkflowDialog.DIALOG_OK){
				IWorkflowStepDTO workflowStep = dialog.getFormData();
				// Si s'està creant serà null. El procés de creació s'encarregarà d'assignar el correcte quan el crei.
				((WorkflowStepFormDTO)workflowStep).setWorkflow(originalWorkflow);
				modelTableSteps.addData(workflowStep);
			}
		}
		else if(ACTION_COMMAND_MODIFY_STEP.equals(event.getActionCommand())){
			WorkflowStepDialog dialog = new WorkflowStepDialog(modelTableSteps.getModelData());
			if(dialog.open()==WorkflowDialog.DIALOG_OK){
				IWorkflowStepDTO workflowStep = dialog.getFormData();
				// Si s'està creant serà null. El procés de creació s'encarregarà d'assignar el correcte quan el crei.
				((WorkflowStepFormDTO)workflowStep).setWorkflow(originalWorkflow);
				modelTableSteps.addData(workflowStep);
			}
		}
		else if(ACTION_COMMAND_REMOVE_STEP.equals(event.getActionCommand())){
			int selectedIndex = tableSteps.getSelectedRow();
			modelTableSteps.getModelData().remove(selectedIndex);
			// Es torna a desactiva la opció d'esborrar
			bRemoveStep.setEnabled(false);
		}
		else if(ACTION_COMMAND_CANCEL_SELECTION.equals(event.getActionCommand())){
			tableSteps.getSelectionModel().clearSelection();
		}
	}

	@Override
	public void workflowCreated(IWorkflowDTO workflow) 
	{
		// Res a fer...
	}

	@Override
	public void workflowUpdated(final IWorkflowDTO oldWorkflow, final IWorkflowDTO newWorkflow) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalWorkflow!=null && originalWorkflow.getWorkflowId().equals(newWorkflow.getWorkflowId())){
				this.originalWorkflow = newWorkflow;
				JOptionPane.showMessageDialog(this, 
		                "Les dades del flux de treball han sigut modificades externament. Es procedirà a carregar-les de nou", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				loadWorkflowData();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	workflowUpdated(oldWorkflow, newWorkflow);
                }
            });
		}
	}

	@Override
	public void workflowDeleted(final IWorkflowDTO workflow) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(originalWorkflow!=null && originalWorkflow.getWorkflowId().equals(workflow.getWorkflowId())){
				JOptionPane.showMessageDialog(stepDialog!=null ? stepDialog : this, 
		                "El flux de treball ha sigut eliminat.", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				if(stepDialog!=null){
					stepDialog.close();
				}
				close();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	workflowDeleted(workflow);
                }
            });
		}
	}
}
