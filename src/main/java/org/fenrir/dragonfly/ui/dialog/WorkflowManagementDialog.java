package org.fenrir.dragonfly.ui.dialog;

import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.dto.IWorkflowDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.event.IWorkflowListener;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140816
 */
@EventListener(definitions=IWorkflowListener.class)
public class WorkflowManagementDialog extends AbstractEntityManagementDialog<IWorkflowDTO> implements IWorkflowListener
{
	private static final long serialVersionUID = -8590274493225473942L;
	
	private static final String PARAMETER_NAME = "WORKFLOW";
	
	private final Logger log = LoggerFactory.getLogger(WorkflowManagementDialog.class);
	
	public WorkflowManagementDialog() throws Exception
    {
        super("Administrar Fluxes de Treball", 450, 600);
    }
	
	@Override
	protected void loadData() throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);

        List<IWorkflowDTO> elements = administrationService.findAllWorkflows();
        addElements(elements);
	}

	@Override
	protected boolean openAddDialog(Map<String, Object> parameters) 
	{
		WorkflowDialog dialog = new WorkflowDialog();
        boolean returnValue = dialog.open()==WorkflowDialog.DIALOG_OK;
        IWorkflowDTO workflow = dialog.getFormData();
        parameters.put(PARAMETER_NAME, workflow);
        
        return returnValue;
	}

	@Override
	protected void doAdd(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		
		IWorkflowDTO workflow = (IWorkflowDTO)parameters.get(PARAMETER_NAME);
		administrationService.createWorkflow(workflow.getName(), 
				workflow.getInitialStatus().getStatusId(), 
				workflow.getWorkflowSteps());		
	}

	@Override
	protected boolean openModifyDialog(Map<String, Object> parameters) 
	{
		IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
		
		IWorkflowDTO selectedWorkflow = (IWorkflowDTO)listEntities.getSelectedValue();
		WorkflowDialog dialog = null;
		try{
			dialog = new WorkflowDialog(selectedWorkflow);
			eventNotificationService.addListener(IWorkflowListener.class, dialog);
			
			boolean returnValue = dialog.open()==WorkflowDialog.DIALOG_OK;
			IWorkflowDTO workflow = dialog.getFormData();
			parameters.put(PARAMETER_NAME, workflow);
			
			return returnValue;
		}
		finally{
			if(dialog!=null){
        		eventNotificationService.removeListener(dialog);
        	}
		}
        
	}

	@Override
	protected void doModify(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
        IWorkflowDTO workflow = (IWorkflowDTO)parameters.get(PARAMETER_NAME);
        administrationService.updateWorkflow(workflow.getWorkflowId(), 
        		workflow.getName(), 
        		workflow.getInitialStatus().getStatusId(), 
        		workflow.getWorkflowSteps());
	}
	
	protected boolean openDeleteDialog(Map<String, Object> parameters)
    {
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IWorkflowDTO selectedWorkflow = (IWorkflowDTO)listEntities.getSelectedValue();
		try{
			if(administrationService.hasWorkflowRelatedProjects(selectedWorkflow.getWorkflowId())){
				JOptionPane.showMessageDialog(this, 
		                "No es pot eliminar el registre perquè té projectes relacionats", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				return false;
			}
			return super.openDeleteDialog(parameters);
		}
		catch(BusinessException e){
			log.error("Error esborrant registre: {}", e.getMessage(), e);
			ApplicationWindowManager.getInstance()
            		.displayErrorMessage("Error esborrant registre: " + e.getMessage(), e);
			
			return false;
		}
    }

	@Override
	protected void doDelete(Map<String, Object> parameters) throws Exception 
	{
		IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
		IWorkflowDTO selectedWorkflow = (IWorkflowDTO)listEntities.getSelectedValue();
        administrationService.deleteWorkflow(selectedWorkflow.getWorkflowId());
	}

	@Override
	public void workflowCreated(IWorkflowDTO workflow) 
	{
		reloadData();
	}

	@Override
	public void workflowUpdated(IWorkflowDTO oldWorkflow, IWorkflowDTO newWorkflow) 
	{
		reloadData();
	}

	@Override
	public void workflowDeleted(IWorkflowDTO workflow) 
	{
		reloadData();
	}
	
	private void reloadData()
	{
		if(SwingUtilities.isEventDispatchThread()){
			try{
				loadData();
			}
			catch(Exception e){
				log.error("Error recarregant els fluxos de treball: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error recarregant els fluxos de treball: " + e.getMessage(), e);
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	reloadData();
                }
            });
		}
	}
}
