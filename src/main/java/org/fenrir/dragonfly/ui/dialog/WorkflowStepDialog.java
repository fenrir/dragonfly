package org.fenrir.dragonfly.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.AbstractConfirmDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.core.dto.WorkflowStepFormDTO;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20141018
 */
public class WorkflowStepDialog extends AbstractConfirmDialog implements ActionListener
{
	private static final String ACTION_COMMAND_ADD_STEP = "ADD_STEP";
	private static final String ACTION_COMMAND_MODIFY_STEP = "MODIFY_STEP";
    private static final String ACTION_COMMAND_REMOVE_STEP = "REMOVE_STEP";
    private static final String ACTION_COMMAND_CANCEL_SELECTION = "CANCEL_SELECTION";
	
	private final Logger log = LoggerFactory.getLogger(WorkflowStepDialog.class);
	
	// Valor de retorn
    private WorkflowStepFormDTO workflowStep;
    // Valor original en cas de modificació
    private IWorkflowStepDTO originalWorkflowStep;
    // Valors per fer les comprovacions per si el workflow ja conté la transició
    private List<IWorkflowStepDTO> workflowCurrentSteps;
	
    private JComboBox comboSourceStatus;
    private JComboBox comboDestinationStatus;
    private JTable tableActions;
//    private WorkflowActionsTableModel modelTableActions;
    private JButton bAddAction;
	private JButton bModifyAction;
    private JButton bRemoveAction;
    private JButton bCancelSelection;
    
	public WorkflowStepDialog(List<IWorkflowStepDTO> workflowCurrentSteps)
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Crear nova transició", new Dimension(530, 620), true);
        this.workflowCurrentSteps = workflowCurrentSteps;
        
        loadData();
    }
	
	public WorkflowStepDialog(IWorkflowStepDTO workflowStep, List<IWorkflowStepDTO> workflowCurrentSteps)
    {
    	super(ApplicationWindowManager.getInstance().getMainWindow(), "Modificar transició existent", new Dimension(530, 620), true);
    	this.originalWorkflowStep = workflowStep;
    	this.workflowCurrentSteps = workflowCurrentSteps;
    	
    	loadData();
    	loadWorkflowStepData();
    }
	
	@Override
	protected JComponent createContents() 
	{
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        JLabel lSourceStatus = new JLabel("Estat origen");
        JLabel lDestinationStatus = new JLabel("Estat destí");
        comboSourceStatus = new JComboBox(new DefaultComboBoxModel());
        comboDestinationStatus = new JComboBox(new DefaultComboBoxModel());
        /* Opció afegir nou registre */
        bAddAction = new JButton("Afegir");
        bAddAction.addActionListener(this);
        bAddAction.setActionCommand(ACTION_COMMAND_ADD_STEP);
        /* Opció modificat registre existent */
        bModifyAction = new JButton("Modificar");
        bModifyAction.addActionListener(this);
        bModifyAction.setActionCommand(ACTION_COMMAND_MODIFY_STEP);
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bModifyAction.setVisible(false);
        /* Opció esborrar registre sel.leccionat */
        bRemoveAction = new JButton("Borrar");
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bRemoveAction.setVisible(false);
        bRemoveAction.setActionCommand(ACTION_COMMAND_REMOVE_STEP);
        bRemoveAction.addActionListener(this);
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bRemoveAction.setVisible(false);
        /* Opció cancelar modificació en curs */
        bCancelSelection = new JButton("Cancel.lar");
        bCancelSelection.addActionListener(this);
        bCancelSelection.setActionCommand(ACTION_COMMAND_CANCEL_SELECTION);
        // Inicialment invisible perquè no hi ha cap registre sel.leccionat
        bCancelSelection.setVisible(false);
        JLabel lActions = new JLabel("Accions");
//        modelTableActions = new WorkflowActionsTableModel();
//        tableActions = new JTable(modelTableActions);
        tableActions = new JTable();
        tableActions.getSelectionModel().addListSelectionListener(new ListSelectionListener() 
        {
			@Override
			public void valueChanged(ListSelectionEvent event) 
			{
//				int index = tableActions.getSelectedRow();
//				if(index>=0){
//					IWorkflowStepDTO selectedStep = modelTableSteps.getModelData().get(index);
//					stepSelected(selectedStep);
//				}
//				else{
//					stepSelected(null);
//				}
			}
		});
        JScrollPane scrollActions = new JScrollPane(tableActions);
        
        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)            		
				.addGroup(layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addComponent(lSourceStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lDestinationStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addComponent(comboSourceStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboDestinationStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					)
				)
				.addGroup(layout.createSequentialGroup()
            		.addComponent(lActions, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                	.addGap(5, 5, Short.MAX_VALUE)
            		.addComponent(bModifyAction, 85, 85, 85)
            		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            		.addComponent(bRemoveAction, 85, 85, 85)
            		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            		.addComponent(bCancelSelection, 85, 85, 85)
            		.addComponent(bAddAction, 85, 85, 85)
        		)
                .addComponent(scrollActions)
			)
			.addContainerGap()
		);
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lSourceStatus)
                .addComponent(comboSourceStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(lDestinationStatus)
                .addComponent(comboDestinationStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            )
            .addGap(20)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		.addComponent(lActions)
                .addComponent(bModifyAction)
                .addComponent(bRemoveAction)
                .addComponent(bCancelSelection)
                .addComponent(bAddAction)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)	            
            .addComponent(scrollActions, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
            .addContainerGap()
        );
        
        return pContents;
	}
	
	@Override
	protected boolean onAccept() 
	{
		IStatusDTO sourceStatus = (IStatusDTO)comboSourceStatus.getSelectedItem();
		IStatusDTO destinationStatus = (IStatusDTO)comboDestinationStatus.getSelectedItem();
		if(sourceStatus==null){
			setMessage("Ha d'indicar un ESTAT ORIGEN a la transició", MSG_TYPE_ERROR);
			return false;
		}
		if(destinationStatus==null){
			setMessage("Ha d'indicar un ESTAT DESTÍ a la transició", MSG_TYPE_ERROR);
			return false;
		}
		if(sourceStatus.equals(destinationStatus)){
			setMessage("L'estat ORIGEN no pot ser el mateix que l'estat DESTÍ", MSG_TYPE_ERROR);
			return false;
		}
		
		// Es comproba si ja existeix la transició. S'ha de fer a partir dels estats perquè l'equals del WorkflowStep té en compte el Workflow
		for(IWorkflowStepDTO elem:workflowCurrentSteps){
			if(elem.getSourceStatus().equals(sourceStatus) && elem.getDestinationStatus().equals(destinationStatus)){
				setMessage("La transició ja existeix", MSG_TYPE_ERROR);
				return false;
			}
		}
		
		// El workflow serà assignat per WorkflowDialog
		workflowStep = new WorkflowStepFormDTO();
		workflowStep.setSourceStatus(sourceStatus);
		workflowStep.setDestinationStatus(destinationStatus);
		
		return true;
	}
	
	@Override
	protected boolean onCancel() 
	{
		workflowStep = null;
		return true;
	}
	
	private void loadData()
	{
		try{
			IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
	        List<IStatusDTO> status = administrationService.findAllStatus();
	        for(IStatusDTO elem:status){
	        	((DefaultComboBoxModel)comboSourceStatus.getModel()).addElement(elem);
	        	((DefaultComboBoxModel)comboDestinationStatus.getModel()).addElement(elem);
	        }
		}
		catch(BusinessException e){
			log.error("Error recuperant la llista d'estats: {}", e.getMessage(), e);
			ApplicationWindowManager.getInstance()
            		.displayErrorMessage("Error recuperant la llista d'estats: " + e.getMessage(), e);
		}
	}
	
	private void loadWorkflowStepData()
	{
		comboSourceStatus.setSelectedItem(originalWorkflowStep.getSourceStatus());
		comboDestinationStatus.setSelectedItem(originalWorkflowStep.getDestinationStatus());
	}
	
	public IWorkflowStepDTO getFormData()
    {
        return workflowStep;
    }

	@Override
	public void actionPerformed(ActionEvent e) 
	{
		// TODO Auto-generated method stub
	}
}
