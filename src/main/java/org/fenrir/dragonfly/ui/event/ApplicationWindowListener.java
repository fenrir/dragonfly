package org.fenrir.dragonfly.ui.event;

import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import javax.swing.SwingUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.action.SwitchWorkspaceAction;
import org.fenrir.yggdrasil.ui.event.IWindowListener;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.IWorkspaceEventListener;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;
import org.fenrir.dragonfly.Application;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.ui.dialog.IssueTimerRegistryPublishingDialog;
import org.fenrir.dragonfly.core.event.ILoginEventListener;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140724
 */
@EventListener(definitions={IWindowListener.class,
		IWorkspaceEventListener.class, 
		ILoginEventListener.class})
public class ApplicationWindowListener implements IWindowListener, IWorkspaceEventListener, ILoginEventListener 
{
	private final Logger log = LoggerFactory.getLogger(ApplicationWindowListener.class);
	
    private Application applicationInstance;
	
    public void setApplicationInstance(Application application)
    {
        this.applicationInstance = application;
    }
	
    public void preWindowOpen()
    {

    }
		
    @Override
    public void windowOpened(WindowEvent event) 
    {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        IEventNotificationService eventNotificationService = (IEventNotificationService)applicationContext.getRegisteredComponent(IEventNotificationService.class);
        IWorkspaceAdministrationService workspaceService = (IWorkspaceAdministrationService)applicationContext.getRegisteredComponent(IWorkspaceAdministrationService.class);

        // Listener del procés de login
        eventNotificationService.addListener(ILoginEventListener.class, this);
        // Si no hi ha un workspace per defecte a la configuració, es mostra la finestra de dialeg per triar-lo    
        if(!workspaceService.isWorkspaceLoaded()){
        	eventNotificationService.addListener(IWorkspaceEventListener.class, this);
            // TODO v0.2 Canviar la manera de cridar les Actions
            ActionEvent actionEvent = new ActionEvent(event.getSource(), event.getID(), null);
            new SwitchWorkspaceAction().actionPerformed(actionEvent); 	
        }
        else{
        	/* Configuració inicial de l'aplicació */
        	if(applicationInstance.setupApplication()){
        		// Si la configuració inicial ha acabat correctament, es procedeix al login
        		applicationInstance.login();
        	}
        }
    }
	
    @Override
    public void windowActivated(WindowEvent event) 
    {

    }

    @Override
    public void windowClosed(WindowEvent event) 
    {	
        
    }

    @Override
    public void windowClosing(WindowEvent event) 
    {	

    }

    @Override
    public void windowDeactivated(WindowEvent event) 
    {	

    }

    @Override
    public void windowDeiconified(WindowEvent event) 
    {	

    }

    @Override
    public void windowIconified(WindowEvent event) 
    {	

    }

    private void loadViews() throws BusinessException
    {
        // Es carreguen totes les vistes
    	IExtendedIssueWorkFacade workService = (IExtendedIssueWorkFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedIssueWorkFacade.class);
    	long numNonPublishedWorkRegistries = workService.countNonPublishedTimerRegistries();
    	if(numNonPublishedWorkRegistries>0){
        	IssueTimerRegistryPublishingDialog publishDialog = new IssueTimerRegistryPublishingDialog();
        	publishDialog.open();
        	// S'esborren els registres no publicats
        	workService.deleteNonPublishedTimerRegistries();
    	}
    	
    	ApplicationWindowManager windowManager = ApplicationWindowManager.getInstance();
    	// Una vegada realitzat el login a l'aplicació és canvia a la perspectiva de treball
		windowManager.switchToDefaultPerspective();
    	windowManager.getCurrentPerspective().initializePerspective();
        ApplicationController applicationController = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);
        // TODO Carregar el filtre carregat a la sessió anterior
        applicationController.loadExplorerViews();
    }

	@Override
	public void workspaceLoaded() throws ApplicationException 
	{
		/* Configuració inicial de l'aplicació */
    	if(applicationInstance.setupApplication()){
    		// Si la configuració inicial ha acabat correctament, es procedeix al login
    		applicationInstance.login();
    	}
	}

	@Override
	public void workspaceUnloaded() throws ApplicationException 
	{
		// Res a fer...
	}
	
	@Override
	public void beforeWorkspaceLoad() throws ApplicationException 
	{
		// Res a fer...
	}

	@Override
	public void beforeWorkspaceUnload() throws ApplicationException 
	{
		// Res a fer...
	}

	@Override
	public void loginPerformed() 
	{
		// L'actualització de la UI s'ha de fer en un Thread a part per no bloquejar l'obertura de la finestra
    	if(SwingUtilities.isEventDispatchThread()){
    		try{
    			loadViews();
    		}
    		catch(BusinessException e){
    			log.error("Error carregant les vistes: {}", e.getMessage(), e);
                ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant les vistes: " + e.getMessage(), e);
    		}
    	}
    	else{
    		SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                	loginPerformed();
                }
            });
    	}
	}

	@Override
	public void logoutPerformed() throws ApplicationException 
	{
		// Res a fer...
	}

	@Override
	public void loginAborted() throws ApplicationException 
	{
		// Res a fer...
	}
}