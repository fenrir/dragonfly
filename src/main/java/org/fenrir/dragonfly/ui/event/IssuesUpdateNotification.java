package org.fenrir.dragonfly.ui.event;

import java.util.List;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.IApplicationNotification;
import org.fenrir.yggdrasil.ui.action.Action;
import org.fenrir.yggdrasil.ui.event.INotificationActionSupport;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.core.event.CoreEventConstants;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140505
 */
public class IssuesUpdateNotification implements IApplicationNotification, INotificationActionSupport
{
    public static final String NOTIFICATION_ID = CoreEventConstants.NOTIFICATION_ISSUES_UPDATED;    
    
    private String message;
    private List<String> issues;
        
    @Override
    public String getId()
    {
        return NOTIFICATION_ID;
    }
    
    @Override
    public String getCaption() 
    {
        return "Realitzada actualizació de les incidències";
    }

    @Override
    public final void setCaption(String caption) 
    {
        
    }

    @Override
    public String getMessage() 
    {
        return message;
    }

    @Override
    public void setMessage(String message) 
    {
        this.message = message;
    }

    @Override
    public String getIconPath() 
    {
        return "/org/fenrir/dragonfly/ui/icons/import_22.png";
    }

    @Override
    public final void setIconPath(String iconPath) 
    {
        
    }

    @Override
    public Action getAction()
    {
        // S'obren les incidències actualitzades a la llista de la vista principal
        Action action = new Action() 
        {
            @Override
            public void actionPerformed() 
            {
                if(!issues.isEmpty()){
                    ApplicationController controller = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);
                    controller.setIssueList(issues);
                }
            }
        };
        
        return action;
    }

    @Override
    public void setParameter(String id, Object value) 
    {
       if(!CoreEventConstants.NOTIFICATION_PARAMETER_ISSUES.equals(id)){
            throw new IllegalArgumentException("La ID especificada no es vàlida");
       }
       issues = (List<String>)value;
    }
}
