package org.fenrir.dragonfly.ui.externalize.dialog;

import java.awt.Dimension;
import org.fenrir.yggdrasil.ui.dialog.TaskProgressDialog;
import org.fenrir.yggdrasil.ui.worker.AbstractWorker;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140722
 */
@SuppressWarnings("serial")
public class TaskProgressDialogExt extends TaskProgressDialog 
{
	private boolean closeOnDone;
	
	public TaskProgressDialogExt(String taskTitle, AbstractWorker task) throws Exception
    {
       	super(taskTitle, task);			
    }
    
    public TaskProgressDialogExt(String taskTitle, AbstractWorker task, Dimension dimension) throws Exception
    {
        super(taskTitle, task, dimension);
    }
    
    public boolean isCloseOnDone()
    {
    	return closeOnDone;
    }
    
    public void setCloseOnDone(boolean closeOnDone)
    {
    	this.closeOnDone = closeOnDone;
    }
    
    @Override
    public void done() 
    {
    	super.done();
    	// Es tanca la finestra si s'ha indicat
    	if(closeOnDone){
    		setVisible(false);
    		dispose();
    	}
    }
}
