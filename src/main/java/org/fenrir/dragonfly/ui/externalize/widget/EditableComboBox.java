package org.fenrir.dragonfly.ui.externalize.widget;

import java.awt.CardLayout;
import java.awt.event.ItemListener;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140423
 */
@SuppressWarnings("serial")
public class EditableComboBox extends JPanel 
{
	protected static final String LAYOUT_COMBO_ID = "combo";
	protected static final String LAYOUT_LABEL_ID = "label";
	
	protected JLabel lValue;
	protected JComboBox comboValue;
	protected CardLayout layout;
	
	public EditableComboBox()
	{
		this(new DefaultComboBoxModel());
	}
	
	public EditableComboBox(ComboBoxModel comboModel)
	{
		createContents(comboModel);
	}
	
	protected void createContents(ComboBoxModel comboModel)
    {
		layout = new CardLayout();
		setLayout(layout);
		setOpaque(false);
		
		comboValue = new JComboBox(comboModel);
		add(comboValue, LAYOUT_COMBO_ID);
		lValue = new JLabel();
		add(lValue, LAYOUT_LABEL_ID);
    }
	
	public void setEditable(boolean editable)
	{
		if(editable){
			layout.show(this, LAYOUT_COMBO_ID);
		}
		else{
			String selectedText = null;
			if(comboValue.getSelectedItem()!=null){
				selectedText = comboValue.getSelectedItem().toString();
			}
			lValue.setText(selectedText);
			lValue.setToolTipText(selectedText);
			layout.show(this, LAYOUT_LABEL_ID);
		}
	}
	
	public ComboBoxModel getModel()
	{
		return comboValue.getModel();
	}
	
	public Object getSelectedItem()
	{
		return comboValue.getSelectedItem();
	}
	
	public void setSelectedItem(Object selectedItem)
	{
		comboValue.setSelectedItem(selectedItem);
		String selectedText = null;
		if(comboValue.getSelectedItem()!=null){
			selectedText = comboValue.getSelectedItem().toString();
		}
		lValue.setText(selectedText);
		lValue.setToolTipText(selectedText);
	}

	public void addItemListener(ItemListener listener) 
	{
		comboValue.addItemListener(listener);
	}

	public void removeItemListener(ItemListener listener) 
	{
		comboValue.removeItemListener(listener);
	}
}
