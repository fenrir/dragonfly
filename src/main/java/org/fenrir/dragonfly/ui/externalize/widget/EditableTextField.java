package org.fenrir.dragonfly.ui.externalize.widget;

import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.text.Document;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140407
 */
@SuppressWarnings("serial")
public class EditableTextField extends JTextField 
{
	
	
	public EditableTextField() 
	{
		super();
	}

	public EditableTextField(Document doc, String text, int columns) 
	{
		super(doc, text, columns);
	}

	public EditableTextField(int columns) 
	{
		super(columns);
	}

	public EditableTextField(String text, int columns) 
	{
		super(text, columns);
	}

	public EditableTextField(String text) 
	{
		super(text);
	}

	@Override
	public void setEditable(boolean editable)
	{
		String borderType;
		// Se li dóna l'aparença d'input
		if(editable){
			borderType = "TextField.border";
		}
		// Se li dóna l'aparença de Label
		else{
			borderType = "Label.border";
		}
		
		setBorder(UIManager.getBorder(borderType));            
        super.setEditable(editable);
        setOpaque(editable);		
	}
	
	@Override
	public void setText(String text)
	{
		super.setText(text);
		setToolTipText(text);
	}
}
