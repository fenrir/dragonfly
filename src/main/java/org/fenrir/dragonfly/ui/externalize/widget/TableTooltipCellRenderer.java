package org.fenrir.dragonfly.ui.externalize.widget;

import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultTableCellRenderer;

/**
 * TODO Javadoc
 * TODO Exportar a Yggdrasil.ui
 * @author Antonio Archilla
 * @version v0.3.20140208
 */
public class TableTooltipCellRenderer extends SubstanceDefaultTableCellRenderer 
{
	private static final long serialVersionUID = 3624800077931007722L;

	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) 
	{
		/* Com que la classe SubstanceDefaultTableCellRenderer exten de DefaultTableCellRenderer i aquesta de JLabel, 
         * es pot posar directament el text i la icona 
         */
		JLabel c = (JLabel)super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		c.setToolTipText(c.getText());
		return c;
	}
}
