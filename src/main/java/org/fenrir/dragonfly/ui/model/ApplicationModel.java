package org.fenrir.dragonfly.ui.model;

import java.util.List;
import org.fenrir.yggdrasil.ui.mvc.AbstractModel;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.dto.SearchQuery;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140504
 */
public class ApplicationModel extends AbstractModel
{
    public static final String PROPERTY_SELECTED_FILTER = "SELECTED_FILTER";
    public static final String PROPERTY_SELECTED_TAG = "SELECTED_TAG";
    public static final String PROPERTY_ISSUE_LIST = "ISSUE_LIST";
    public static final String PROPERTY_SEARCH_QUERY = "SEARCH_QUERY";
    public static final String PROPERTY_SELECTED_ISSUE = "SELECTED_ISSUE";
    public static final String PROPERTY_WORKING_ISSUE = "WORKING_ISSUE";

    private IViewFilterDTO selectedFilter;
    private ITagDTO selectedTag;
    private List<String> selectedIssues;
    private SearchQuery searchQuery;
    private IIssueDTO selectedIssue;
    private IIssueDTO workingIssue;

    private int loadedPage = 0;

    public IViewFilterDTO getSelectedFilter()
    {
        return selectedFilter;
    }
    
    public void setSelectedFilter(IViewFilterDTO selectedFilter)
    {
        selectedTag = null;
        selectedIssues = null;
        searchQuery = null;
        loadedPage = 0;
        
        IViewFilterDTO oldFilter = this.selectedFilter;
        this.selectedFilter = selectedFilter;
        firePropertyChange(PROPERTY_SELECTED_FILTER, oldFilter, selectedFilter);
    }
    
    public ITagDTO getSelectedTag()
    {
        return selectedTag;
    }
    
    public void setSelectedTag(ITagDTO selectedTag)
    {
        selectedFilter = null;
        selectedIssues = null;
        searchQuery = null;
        loadedPage = 0;
        
        ITagDTO oldTag = this.selectedTag;
        this.selectedTag = selectedTag;
        firePropertyChange(PROPERTY_SELECTED_TAG, oldTag, selectedTag);
    }

    public List<String> getSelectedIssues() 
    {
        return selectedIssues;
    }

    public void setIssueList(List<String> issueList) 
    {
        selectedFilter = null;
        selectedTag = null;
        searchQuery = null;
        loadedPage = 0;
        
        List<String> oldIssues = this.selectedIssues;
        this.selectedIssues = issueList;
        firePropertyChange(PROPERTY_ISSUE_LIST, oldIssues, issueList);
    }
    
    public SearchQuery getSearchQuery()
    {
        return searchQuery;
    }
    
    public void setSearchQuery(SearchQuery searchQuery)
    {
        selectedFilter = null;
        selectedIssues = null;
        selectedTag = null;
        loadedPage = 0;
        
        SearchQuery oldQuery = this.searchQuery;
        this.searchQuery = searchQuery;
        firePropertyChange(PROPERTY_SEARCH_QUERY, oldQuery, searchQuery);        
    }

    public IIssueDTO getSelectedIssue()
    {
    	return selectedIssue;
    }
    
    public void setSelectedIssue(IIssueDTO selectedIssue)
    {
    	IIssueDTO oldValue = selectedIssue;
        this.selectedIssue = selectedIssue;
        firePropertyChange(PROPERTY_SELECTED_ISSUE, oldValue, selectedIssue);
    }
    
    public IIssueDTO getWorkingIssue()
    {
    	return workingIssue;
    }
    
    public void setWorkingIssue(IIssueDTO workingIssue)
    {
    	IIssueDTO oldValue = this.workingIssue;
    	this.workingIssue = workingIssue;
    	firePropertyChange(PROPERTY_WORKING_ISSUE, oldValue, workingIssue);
    }
    
    public int getLoadedPage()
    {
        return loadedPage;
    }

    public void setLoadedPage(int loadedPage)
    {
        this.loadedPage = loadedPage;
    }
}
