package org.fenrir.dragonfly.ui.module;

import javax.swing.JMenuBar;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import org.fenrir.dragonfly.ui.MenuBar;

public class BasicUIModule extends AbstractModule
{
    @Override
    protected void configure()
    {
    	/* Menú principal */
        // No es pot lligar directament a una instancia perquè sino no actualitza bé el L&F
        bind(JMenuBar.class).to(MenuBar.class).in(Singleton.class);
    }
}
