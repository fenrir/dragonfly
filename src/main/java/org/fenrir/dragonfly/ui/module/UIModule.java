package org.fenrir.dragonfly.ui.module;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import org.fenrir.yggdrasil.ui.mvc.AbstractPerspective;
import org.fenrir.yggdrasil.core.event.IApplicationNotification;
import org.fenrir.dragonfly.ui.event.IssuesUpdateNotification;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.ui.model.ApplicationModel;
import org.fenrir.dragonfly.ui.perspective.IssueBrowserPerspective;
import org.fenrir.dragonfly.ui.view.IssueFilterExplorerView;
import org.fenrir.dragonfly.ui.view.IssueListView;
import org.fenrir.dragonfly.ui.view.IssueDetailView;
import org.fenrir.dragonfly.ui.view.IssueTimerView;
import org.fenrir.dragonfly.ui.view.TagExplorerView;
import org.fenrir.dragonfly.core.event.CoreEventConstants;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140208
 */
public class UIModule extends AbstractModule
{
    @Override
    protected void configure()
    {
        /* Notifications */
        bind(IApplicationNotification.class)
                .annotatedWith(Names.named(CoreEventConstants.NOTIFICATION_ISSUES_UPDATED))
                .to(IssuesUpdateNotification.class);
        /* Models */
        bind(ApplicationModel.class).in(Singleton.class);
        /* Controladors */
        bind(ApplicationController.class).in(Singleton.class);
        /* Perspectives */
        bind(AbstractPerspective.class)
        		.annotatedWith(Names.named(IssueBrowserPerspective.ID))
        		.to(IssueBrowserPerspective.class)
        		.in(Singleton.class);
        /* Vistes */
        bind(IssueTimerView.class).in(Singleton.class);
        bind(IssueFilterExplorerView.class).in(Singleton.class);
        bind(TagExplorerView.class).in(Singleton.class);
        bind(IssueListView.class).in(Singleton.class);
        bind(IssueDetailView.class);
    }
}
