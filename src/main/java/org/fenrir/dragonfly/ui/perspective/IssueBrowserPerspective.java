package org.fenrir.dragonfly.ui.perspective;

import java.awt.Dimension;
import javax.swing.Box;
import javax.swing.JComponent;
import javax.swing.JSplitPane;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.mvc.AbstractPerspective;
import org.fenrir.yggdrasil.ui.widget.AccordionPanel;
import org.fenrir.dragonfly.ui.view.IssueDetailView;
import org.fenrir.dragonfly.ui.view.IssueFilterExplorerView;
import org.fenrir.dragonfly.ui.view.IssueListView;
import org.fenrir.dragonfly.ui.view.IssueTimerView;
import org.fenrir.dragonfly.ui.view.TagExplorerView;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140508
 */
@SuppressWarnings("serial")
public class IssueBrowserPerspective extends AbstractPerspective 
{
	public static final String ID = "org.fenrir.dragonfly.ui.perspective.issueBrowserPerspective";
	
	private IssueTimerView timerView;
	
	private IssueFilterExplorerView filterExplorerView;
	
	private TagExplorerView tagExplorerView;
	
	private IssueListView issueListView;
	
	private JComponent leftSplit;
	
	@Override
	public String getId() 
	{
		return ID;
	}

	@Override
	protected JComponent createPerspectiveContents() 
	{
        JSplitPane splitHorizontal = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);    	
    	/* Navegador 200px */
        splitHorizontal.setDividerLocation(200);
        /* Split esquerra */
        leftSplit = Box.createVerticalBox();
        // Una mica de marge per que no estigui tan ajustat al menú
        leftSplit.add(Box.createVerticalStrut(10));
        
        /* Split dret */
        // Es composa la vista principal per poder mostrar tant la llista d'incidencies la de detall (multiple)
        ViewPlaceHolder mainArea = new ViewPlaceHolder();
        // TODO Això no hauria de fer falta per les vistes standalone
        setViewPlaceHolder(IssueListView.ID, mainArea);
        setViewPlaceHolder(IssueDetailView.ID, mainArea);

        splitHorizontal.setLeftComponent(leftSplit);
        splitHorizontal.setRightComponent(mainArea.getContentPanel());
        
        return splitHorizontal;
	}
	
	@Override
	public void initializePerspective()
	{
		ApplicationContext applicationContext = ApplicationContext.getInstance();
		timerView = (IssueTimerView)applicationContext.getRegisteredComponent(IssueTimerView.class);
		filterExplorerView = (IssueFilterExplorerView)applicationContext.getRegisteredComponent(IssueFilterExplorerView.class);
		tagExplorerView = (TagExplorerView)applicationContext.getRegisteredComponent(TagExplorerView.class);
		issueListView = (IssueListView)applicationContext.getRegisteredComponent(IssueListView.class);
		
		/* Inicialització de l'split esquerre */
		// Es redimension la vista del temporitzador per tal que ocupi un tamnay adecuat
        Dimension size = timerView.getPreferredSize();
        timerView.setPreferredSize(new Dimension(size.width, 90));
        size = timerView.getMaximumSize();
        timerView.setMaximumSize(new Dimension(size.width, 90));
        leftSplit.add(timerView);
        AccordionPanel accordionPanel = new AccordionPanel();
        // Vista explorador de filtres
        accordionPanel.addBar("Filtres", filterExplorerView);
        // Vista explorador tags
        accordionPanel.addBar("Etiquetes", tagExplorerView);
        leftSplit.add(accordionPanel);
        
        /* Inicialització de l'split dret */
        addView(IssueListView.ID, issueListView);
        
        updateUI();
	}

	@Override
	public boolean isViewAllowed(String viewId) 
	{
		return IssueDetailView.ID.equals(viewId) || IssueListView.ID.equals(viewId);
	}
}
