package org.fenrir.dragonfly.ui.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.BorderFactory;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Pattern;
import javax.inject.Inject;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.JCommandButton.CommandButtonKind;
import org.pushingpixels.flamingo.api.common.icon.ImageWrapperResizableIcon;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.lang.StringUtils;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;
import org.fenrir.yggdrasil.ui.widget.ElementSearchTextField;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.core.dto.ISprintDTO;
import org.fenrir.vespine.core.dto.IVespineIssueDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.spi.dto.ICategoryDTO;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.ICustomValueDTO;
import org.fenrir.vespine.spi.dto.IFieldType;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.vespine.spi.dto.IProjectDTO;
import org.fenrir.vespine.spi.dto.ISeverityDTO;
import org.fenrir.vespine.spi.dto.IStatusDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.dto.IUserDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.vespine.spi.util.IAttributeConverter;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.ui.externalize.widget.EditableComboBox;
import org.fenrir.dragonfly.ui.externalize.widget.EditableTextField;
import org.fenrir.dragonfly.ui.widget.CustomFieldWidgetFactory;
import org.fenrir.dragonfly.ui.widget.ICustomFieldWidget;
import org.fenrir.dragonfly.ui.widget.InputTag;
import org.fenrir.dragonfly.ui.widget.IssueNoteListWidget;
import org.fenrir.dragonfly.ui.widget.IssueToolbarWidget;
import org.fenrir.dragonfly.ui.widget.WorkReportWidget;
import org.fenrir.dragonfly.core.exception.ValidationException;
import org.fenrir.dragonfly.core.provider.SprintSearchProvider;
import org.fenrir.dragonfly.core.provider.UserSearchProvider;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140613
 */
public class IssueDetailView extends AbstractView<ApplicationController>
{
	private static final long serialVersionUID = 7036789719355246114L;

	public static final String ID = "org.fenrir.dragonfly.ui.view.issueDetailView";
	
	private final Logger log = LoggerFactory.getLogger(IssueDetailView.class);

	public static final int MODE_NOT_SET = 0;
	public static final int MODE_CREATION = 1;
	public static final int MODE_VISUALIZATION = 2;
	public static final int MODE_UPDATE = 3;
	
	private static final String ACTION_EDIT_ISSUE = "EDIT_ISSUE";
	private static final String ACTION_CREATE_ISSUE = "CREATE_ISSUE";
	private static final String ACTION_UPDATE_ISSUE = "UPDATE_ISSUE";
	private static final String ACTION_CANCEL_EDIT = "CANCEL_EDIT";
	
	private static final int FORM_FIRST_FIELD_WIDTH = 75;
	private static final Double HEADER_COLORIZATION_FACTOR = 0.45d;
	private static final Double BODY_COLORIZATION_FACTOR = 0.15d;

	@Inject
	private IIssueFacade issueService;
	@Inject
	private IAdministrationFacade administrationService;
	
	private int mode = MODE_NOT_SET;
	private IIssueDTO issue;
	
    /* Components de la capçalera */
	// TODO Integrar tota la botonera en un únic component
	private JCommandButton  bUpdateView;
    private IssueToolbarWidget pIssueToolbar;
    private ViewToolbar pViewToolbar;
    /* Components del detall */
    private JLabel lSummary;
    private JLabel lIssueId;
    private EditableTextField inputSummary;
    
    private JPanel pTags;
    
    private EditableComboBox comboProject;
    private EditableComboBox comboStatus;
    private EditableComboBox comboSeverity;
    private EditableComboBox comboCategory;
    private JTextArea inputDescription;
    private JLabel lReporter;
    private JLabel inputReporter;
    /* Camps custom */
    private List<ICustomFieldWidget<?>> customFieldWidgets;
    private GroupLayout layoutCustomFields;
    private JPanel pCustomFields;
    /* Dates */
    private JLabel lSendDate;
    private JLabel inputSendDate;
    private JLabel lModificationDate;
    private JLabel inputModificationDate;
    private JLabel lSlaDate;
    private EditableTextField inputSlaDate;
    private JToggleButton toggleSla;
    private JLabel lResolutionDate;
    private EditableTextField inputResolutionDate;
    /* Working data */
    private JLabel lAssignedUser;
    private ElementSearchTextField<IUserDTO> inputAssignedUser;
    private JLabel lSprint;
    private ElementSearchTextField<ISprintDTO> inputSprint;
    private JLabel lEstimatedHours;
    private EditableTextField inputEstimatedTime;
    private WorkReportWidget workReportWidget;
    /* Contenidor de les notes */
    private IssueNoteListWidget pNotes;
    
    @Inject
    public void setApplicationController(ApplicationController applicationController)
    {
        setController(applicationController);
    }
    
    public void setIssueService(IIssueFacade issueService)
    {
    	this.issueService = issueService;
    }
    
    public void setAdministrationService(IAdministrationFacade administrationService)
    {
    	this.administrationService = administrationService;
    }
    
    public static String createViewId(String issueId)
    {
    	return new StringBuilder(ID).append("@").append(issueId).toString();
    }
    
    @Override
    public String getId()
    {
    	return ID;
    }
    
    @Override
    public String getComposedId()
    {
    	if(issue==null){
    		return ID;
    	}
    	else{
    		return createViewId(issue.getIssueId());
    	}
    }

    @Override
    protected final JComponent createViewContents()
    {
        JPanel pContents = new JPanel();
        pContents.setLayout(new BorderLayout());
        JComponent headerPanel = createHeaderPanel();
        pContents.add(headerPanel, BorderLayout.NORTH);
        JComponent contentsPanel = createContentsPanel();
        pContents.add(contentsPanel, BorderLayout.CENTER);
        
        // Es carreguen les dades mestres
        try{
	        loadProjects();
	    	loadCategories();
	    	loadSeverities();
	    	IProjectDTO selectedProject = (IProjectDTO)comboProject.getSelectedItem();
	    	if(selectedProject!=null){
	    		loadStatus(selectedProject);
	    		loadCustomFields(selectedProject);
	    	}
        }
        catch(BusinessException e){
        	log.error("Error carregant les dades mestres de la vista: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error carregant les dades mestres de la vista: " + e.getMessage(), e);
        }
        
        return pContents;
    }
    
    private JComponent createHeaderPanel()
    {
    	JCommandButton bReturn = new JCommandButton(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/go_previous_22.png"), new Dimension(22, 22)));
    	bReturn.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
    	bReturn.setDisplayState(CommandButtonDisplayState.TILE);
    	bReturn.setActionRichTooltip(new RichTooltip("Tornar", "Tornar a la llista d'incidencies"));
    	bReturn.setFlat(false);
    	bReturn.addActionListener(new ActionListener() 
    	{
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				controller.leaveIssueDetailView(getComposedId());
			}
		});
    	bReturn.getActionModel().setActionCommand(ACTION_UPDATE_ISSUE);
    	
    	// Actualitzar
    	bUpdateView = new JCommandButton(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/reload_22.png"), new Dimension(22, 22)));
        bUpdateView.setDisplayState(CommandButtonDisplayState.TILE);
        bUpdateView.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
        bUpdateView.setActionRichTooltip(new RichTooltip("Refrescar", "Refrescar les dades de la incidència"));
        bUpdateView.setFlat(false);
        bUpdateView.addActionListener(new ActionListener() 
    	{
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				controller.reloadIssueDetailView();
			}
		});
        // Deshabilitat i amagat fins que no s'indiqui una incidencia
        bUpdateView.setEnabled(false);
    	
        pIssueToolbar = new IssueToolbarWidget();
        pViewToolbar = new ViewToolbar();
        
        JPanel pHeader = new JPanel();        
        GroupLayout layout = new GroupLayout(pHeader);
        pHeader.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createSequentialGroup()    
            .addContainerGap()
            .addComponent(bReturn)
            .addGap(20)
            .addComponent(bUpdateView)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
        	.addComponent(pIssueToolbar, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        	.addGap(20, 20, Short.MAX_VALUE)
        	.addComponent(pViewToolbar, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addContainerGap()
        );                            
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		.addComponent(bReturn, 30, 30, 30)
        		.addComponent(bUpdateView, 30, 30, 30)
        		.addComponent(pIssueToolbar, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
        		.addComponent(pViewToolbar, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
    		)
            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        
        return pHeader;
    }
    
    private JComponent createContentsPanel()
    {
    	/* Summary de l'incidencia */
    	JPanel pSummary = new JPanel();
    	pSummary.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK),                
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    	pSummary.setBackground(Color.BLACK);
    	pSummary.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, BODY_COLORIZATION_FACTOR);
    	
    	lSummary = new JLabel("Resum", SwingConstants.RIGHT);
    	lSummary.setFont(new Font("Sans-Serif", Font.BOLD, 12));
    	lIssueId = new JLabel();
        inputSummary = new EditableTextField();
        
        // Tags
        pTags = new JPanel();
        pTags.setOpaque(false);
        // Per defecte un FlowLayout permet disposar els components en varies linies si no té prou espai
        pTags.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
        pTags.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
    	
    	GroupLayout summaryLayout = new GroupLayout(pSummary);
    	pSummary.setLayout(summaryLayout);
        /* Grup horitzontal */
    	summaryLayout.setHorizontalGroup(summaryLayout.createSequentialGroup()
			.addContainerGap()
    		.addGroup(summaryLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
    			.addGroup(summaryLayout.createSequentialGroup()
					.addComponent(lSummary, FORM_FIRST_FIELD_WIDTH, FORM_FIRST_FIELD_WIDTH, FORM_FIRST_FIELD_WIDTH)
		    		.addGap(5)
		    		.addComponent(lIssueId, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)			
		    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
		    		.addComponent(inputSummary, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
	            )
	            .addComponent(pTags, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
			)
			.addContainerGap()
        );                            
        /* Grup vertical */
    	summaryLayout.setVerticalGroup(summaryLayout.createSequentialGroup()
            .addContainerGap()
            .addGroup(summaryLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		.addComponent(lSummary)
        		.addComponent(lIssueId)
        		.addComponent(inputSummary)
    		)        
    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    		.addComponent(pTags)
            .addContainerGap()
        );
    	
    	/* Dades de l'incidencia */
    	JLabel lIssueDataHeader = new JLabel("Dades incidencia");
    	lIssueDataHeader.setFont(new Font("Sans-Serif", Font.BOLD, 12));
    	lIssueDataHeader.setOpaque(true);
    	lIssueDataHeader.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.BLACK),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    	lIssueDataHeader.setBackground(Color.BLACK);
    	lIssueDataHeader.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, HEADER_COLORIZATION_FACTOR);
    	JComponent pIssueData = createIssueDataPanel();
    	
    	/* Camps custom */
    	pCustomFields = new JPanel();
    	pCustomFields.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createMatteBorder(0, 1, 1, 1, Color.BLACK),                
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    	pCustomFields.setBackground(Color.BLACK);
    	pCustomFields.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, BODY_COLORIZATION_FACTOR);
    	layoutCustomFields = new GroupLayout(pCustomFields);
    	pCustomFields.setLayout(layoutCustomFields);
    	customFieldWidgets = new ArrayList<ICustomFieldWidget<?>>();
        
    	/* Dates */
    	JLabel lDatesHeader = new JLabel("Dates");
    	lDatesHeader.setFont(new Font("Sans-Serif", Font.BOLD, 12));
    	lDatesHeader.setOpaque(true);
    	lDatesHeader.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.BLACK),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    	lDatesHeader.setBackground(Color.BLACK);
    	lDatesHeader.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, HEADER_COLORIZATION_FACTOR);
    	JComponent pDates = createDatesPanel();
    	
    	/* Working Data */
    	JLabel lWorkingDataHeader = new JLabel("Progrés");
    	lWorkingDataHeader.setFont(new Font("Sans-Serif", Font.BOLD, 12));
    	lWorkingDataHeader.setOpaque(true);
    	lWorkingDataHeader.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.BLACK),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
    	lWorkingDataHeader.setBackground(Color.BLACK);
    	lWorkingDataHeader.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, HEADER_COLORIZATION_FACTOR);
    	JComponent pWorkingData = createWorkingDataPanel();
    	
    	/* Descripció */
        JLabel lDescriptionHeader = new JLabel("Descripció");
        lDescriptionHeader.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        lDescriptionHeader.setOpaque(true);
        lDescriptionHeader.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.BLACK),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        lDescriptionHeader.setBackground(Color.BLACK);
        lDescriptionHeader.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, HEADER_COLORIZATION_FACTOR);
        
        inputDescription = new JTextArea();
        inputDescription.setRows(3);
        inputDescription.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createMatteBorder(0, 1, 1, 1, Color.BLACK),                
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        inputDescription.setLineWrap(true);
        inputDescription.setWrapStyleWord(true);
        
        /* Notes */
        pNotes = new IssueNoteListWidget();
        
        JPanel pContents = new JPanel();
        JScrollPane scrollPane = new JScrollPane(pContents);
        // S'augmenta la velocitat de l'scroll
        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createSequentialGroup()    
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            		.addComponent(pSummary, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
            		.addGroup(layout.createSequentialGroup()
        				.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
    						.addComponent(lIssueDataHeader, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
    						.addComponent(pIssueData, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
    						.addComponent(pCustomFields, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
    						.addComponent(lDescriptionHeader, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
    						.addComponent(inputDescription, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)                    
    						.addComponent(pNotes, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
						)
						.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
						.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
							.addComponent(lDatesHeader, GroupLayout.PREFERRED_SIZE, 300, 300)
    						.addComponent(pDates, GroupLayout.PREFERRED_SIZE, 300, 300)
    						.addComponent(lWorkingDataHeader, GroupLayout.PREFERRED_SIZE,  300, 300)           
    						.addComponent(pWorkingData, GroupLayout.PREFERRED_SIZE, 300, 300)
						)
            		)
                )
                .addContainerGap()
        );                            
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addComponent(pSummary, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        		.addGroup(layout.createSequentialGroup()
    				.addComponent(lIssueDataHeader)
    				.addComponent(pIssueData, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, 130)
    				.addComponent(pCustomFields, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    				.addComponent(lDescriptionHeader)                
					.addComponent(inputDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(pNotes)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        		)
        		.addGroup(layout.createSequentialGroup()
    				.addComponent(lDatesHeader)
    				.addComponent(pDates, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, 130)
    				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    				.addComponent(lWorkingDataHeader)
    				.addComponent(pWorkingData)
    				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        		)
            )
        );                
        
        return scrollPane;
    }

    private JComponent createIssueDataPanel()
    {
    	JPanel pData = new JPanel();
    	pData.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createMatteBorder(0, 1, 1, 1, Color.BLACK),                
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        pData.setBackground(Color.BLACK);
        pData.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, BODY_COLORIZATION_FACTOR);
    	
        // Projecte
        JLabel lProject = new JLabel("Projecte", SwingConstants.RIGHT);
        lProject.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        comboProject = new EditableComboBox();
        comboProject.addItemListener(new ItemListener() 
        {
			@Override
			public void itemStateChanged(ItemEvent event) 
			{
				if(ItemEvent.SELECTED==event.getStateChange())
				{
					try{
						IProjectDTO project = (IProjectDTO)event.getItem();
						loadStatus(project);
						loadCustomFields(project);
					}
					catch(BusinessException e){
			        	log.error("Error carregant les dades mestres de la vista: {}", e.getMessage(), e);
			            ApplicationWindowManager.getInstance()
			                    .displayErrorMessage("Error carregant les dades mestres de la vista: " + e.getMessage(), e);
			        }
				}
			}
		});
        // Estat
        JLabel lStatus = new JLabel("Estat");
        lStatus.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        comboStatus = new EditableComboBox();
        // Severitat
        JLabel lSeverity = new JLabel("Severitat");
        lSeverity.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        comboSeverity = new EditableComboBox();
        comboSeverity.addItemListener(new ItemListener() 
        {
			@Override
			public void itemStateChanged(ItemEvent event) 
			{
				if(ItemEvent.SELECTED==event.getStateChange())
				{
					setSlaDate();
				}
			}
		});
        // Categoria
        JLabel lCategory = new JLabel("Categoria", SwingConstants.RIGHT);
        lCategory.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        comboCategory = new EditableComboBox();
        // Usuari informador
        lReporter = new JLabel("Informador", SwingConstants.RIGHT);
    	lReporter.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        inputReporter = new JLabel();
    	
        GroupLayout layout = new GroupLayout(pData);
        pData.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createSequentialGroup()
    		.addContainerGap()
    		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
				.addComponent(lProject, FORM_FIRST_FIELD_WIDTH, FORM_FIRST_FIELD_WIDTH, FORM_FIRST_FIELD_WIDTH)
				.addComponent(lCategory, FORM_FIRST_FIELD_WIDTH, FORM_FIRST_FIELD_WIDTH, FORM_FIRST_FIELD_WIDTH)
				.addComponent(lReporter, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			)
			.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
			.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)    
				.addComponent(comboProject, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addGroup(layout.createSequentialGroup()
					.addComponent(comboCategory, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(20)
					.addComponent(lSeverity, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(comboSeverity, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(20)
					.addComponent(lStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(comboStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				)
				.addComponent(inputReporter, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			)
            .addContainerGap()
        );                            
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addContainerGap()
            // L'alineament d'aquest grup ha de ser CENTER perquè BASELINE no funciona bé amb els camps de tipus EditableComboBox
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
        		.addComponent(lProject, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(comboProject, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    		// L'alineament d'aquest grup ha de ser CENTER perquè BASELINE no funciona bé amb els camps de tipus EditableComboBox
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
        		.addComponent(lCategory, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(comboCategory, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(lSeverity, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(comboSeverity, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(lStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(comboStatus, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
				.addComponent(lReporter, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
	    		.addComponent(inputReporter, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );                
        
    	return pData;
    }
    
    private JComponent createDatesPanel()
    {
    	JPanel pData = new JPanel();
    	pData.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createMatteBorder(0, 1, 1, 1, Color.BLACK),                
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        pData.setBackground(Color.BLACK);
        pData.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, BODY_COLORIZATION_FACTOR);
        
        /* Dates */
        lSendDate = new JLabel("Data enviament", SwingConstants.RIGHT);
        lSendDate.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        // La data d'enviament mai serà editable
        inputSendDate = new JLabel();
        lModificationDate = new JLabel("Data modificació", SwingConstants.RIGHT);
        lModificationDate.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        // La data de modificació mai serà editable
        inputModificationDate = new JLabel();
        toggleSla = new JToggleButton("SLA");
        toggleSla.addActionListener(new ActionListener() 
        {
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				setSlaDate();
			}
		});
        lSlaDate = new JLabel("Data SLA", SwingConstants.RIGHT);
        lSlaDate.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        inputSlaDate = new EditableTextField();
        lResolutionDate = new JLabel("Data resolució", SwingConstants.RIGHT);
        lResolutionDate.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        inputResolutionDate = new EditableTextField();
    	
        GroupLayout layout = new GroupLayout(pData);
        pData.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createSequentialGroup()
    		.addContainerGap()
			.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
				.addComponent(lSlaDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(lSendDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(lModificationDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(lResolutionDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
			)
			.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
			.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(inputSlaDate, 75, 75, 75)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addComponent(toggleSla)
				)
				.addComponent(inputSendDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(inputModificationDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
				.addComponent(inputResolutionDate, 75, 75, 75)
			)
            .addContainerGap()
        );                            
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		.addComponent(lSlaDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(inputSlaDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(toggleSla, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		.addComponent(lSendDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(inputSendDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		.addComponent(lModificationDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(inputModificationDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		.addComponent(lResolutionDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(inputResolutionDate, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
            .addContainerGap()
        );
        
        return pData;
    }
    
    private JComponent createWorkingDataPanel()
    {
    	JPanel pData = new JPanel();
    	pData.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createMatteBorder(0, 1, 1, 1, Color.BLACK),                
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        pData.setBackground(Color.BLACK);
        pData.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, BODY_COLORIZATION_FACTOR);
    	
        lAssignedUser = new JLabel("Usuari assignat", SwingConstants.RIGHT);
        lAssignedUser.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        inputAssignedUser = new ElementSearchTextField<IUserDTO>(UserSearchProvider.class);
        lSprint = new JLabel("Sprint", SwingConstants.RIGHT);
        lSprint.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        inputSprint = new ElementSearchTextField<ISprintDTO>(SprintSearchProvider.class);
        lEstimatedHours = new JLabel("Hores estimades", SwingConstants.RIGHT);
        lEstimatedHours.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        inputEstimatedTime = new EditableTextField();
        workReportWidget = new WorkReportWidget();        
        
        GroupLayout layout = new GroupLayout(pData);
        pData.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createSequentialGroup()
    		.addContainerGap()
    		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
						.addComponent(lAssignedUser, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lSprint, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lEstimatedHours, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
					)
					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
					.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
						.addComponent(inputAssignedUser, 150, 150, 150)
						.addComponent(inputSprint, 150, 150, 150)
						.addComponent(inputEstimatedTime, 150, 150, 150)
					)			
				)
				.addComponent(workReportWidget, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
			)
            .addContainerGap()
        );                            
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addContainerGap()
    		// L'alineament d'aquest grup ha de ser CENTER perquè BASELINE no funciona bé amb els camps de tipus AdvancedTextField
    		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
        		.addComponent(lAssignedUser, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(inputAssignedUser, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    		// L'alineament d'aquest grup ha de ser CENTER perquè BASELINE no funciona bé amb els camps de tipus AdvancedTextField
    		.addGroup(layout.createParallelGroup(GroupLayout.Alignment.CENTER)
        		.addComponent(lSprint, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(inputSprint, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
        		.addComponent(lEstimatedHours, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
        		.addComponent(inputEstimatedTime, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
    		)
    		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    		.addComponent(workReportWidget)
            .addContainerGap()
        );
        
        return pData;
    }
    
    public int getMode()
    {
    	return mode;
    }
    
    /**
     * Mètode que permet especificar el mode de treball de la pantalla (Visualització, creació o modificació).
     * @param mode int - Mode en el que treballa la vista. 
     */
    public void setMode(int mode)
    {
    	if(mode!=MODE_CREATION 
    			&& mode!=MODE_UPDATE
    			&& mode!=MODE_VISUALIZATION){
    		throw new IllegalArgumentException(String.format("Mode %1$d incorrecte", mode));
    	}
    	
    	this.mode = mode;
    	
    	// Summary
    	lIssueId.setVisible(mode!=MODE_VISUALIZATION);
    	inputSummary.setEditable(mode!=MODE_VISUALIZATION);
    	
    	// Dades de la incidencia
        lSummary.setVisible(mode!=MODE_VISUALIZATION);
        lIssueId.setVisible(mode!=MODE_CREATION);
		comboProject.setEditable(mode!=MODE_VISUALIZATION);
		comboCategory.setEditable(mode!=MODE_VISUALIZATION);
		comboSeverity.setEditable(mode!=MODE_VISUALIZATION);
		comboStatus.setEditable(mode!=MODE_VISUALIZATION);
		lReporter.setVisible(mode!=MODE_CREATION);
		inputReporter.setVisible(mode!=MODE_CREATION);
		// Dates
		lSendDate.setVisible(mode!=MODE_CREATION);
		inputSendDate.setVisible(mode!=MODE_CREATION);		
		lModificationDate.setVisible(mode!=MODE_CREATION);
		inputModificationDate.setVisible(mode!=MODE_CREATION);
		inputSlaDate.setEditable(mode!=MODE_VISUALIZATION);
        toggleSla.setVisible(mode!=MODE_VISUALIZATION);
		lResolutionDate.setVisible(mode!=MODE_CREATION);
		inputResolutionDate.setVisible(mode!=MODE_CREATION);
		if(mode!=MODE_CREATION){
			inputResolutionDate.setVisible(mode!=MODE_VISUALIZATION);
		}
		// Work data
		inputAssignedUser.setEditable(mode!=MODE_VISUALIZATION);
		inputSprint.setEditable(mode!=MODE_VISUALIZATION);
		inputEstimatedTime.setEditable(mode!=MODE_VISUALIZATION);		
		
		// Descripció
		inputDescription.setEditable(mode!=MODE_VISUALIZATION);
    	if(mode==MODE_VISUALIZATION){    		
    		inputDescription.setBackground(Color.BLACK);
    		inputDescription.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, new Double(0.25d));
    	}
    	else{
    		inputDescription.setBackground(UIManager.getColor("TextArea.background"));
    	}
    	
    	// Camps custom
    	for(ICustomFieldWidget<?> widget:customFieldWidgets){
    		widget.setEditable(mode!=MODE_VISUALIZATION);
    	}
    	
    	// Actualització de les opcions visibles a la barra d'eines de la vista
    	pViewToolbar.setMode(mode);
    	
    	// Actualització de les opcions visibles al widget de les notes
    	pNotes.setEnabled(mode!=MODE_CREATION);
    }
    
    public void setIssueDetails(int mode, IIssueDTO issue) throws BusinessException
    {
    	if(mode!=MODE_VISUALIZATION && mode!=MODE_UPDATE){
    		throw new IllegalArgumentException("Mode no vàlid: " + mode);
    	}
    	
        this.issue = issue;
        setMode(mode);
        /* Actualització de la barra d'eines */
    	bUpdateView.setEnabled(true);
        pIssueToolbar.updateToolbar(issue);
        /* Actualització de la capçalera */ 
        lIssueId.setText(issue.getVisibleId());
		inputSummary.setText(issue.getSummary());
		/* Tags associats */
		pTags.removeAll();
        Collection<ITagDTO> tags = issue.getTags();            
        for(ITagDTO tag:tags){                
            pTags.add(new InputTag(issue.getIssueId(), tag));
        }
        pTags.revalidate();
        /* Actualització del contingut de la incidència */
        comboProject.setSelectedItem(issue.getProject());
        comboCategory.setSelectedItem(issue.getCategory());
        comboSeverity.setSelectedItem(issue.getSeverity());
        // Es carreguen els estats segons workflow
        loadStatus(issue.getProject());
        comboStatus.setSelectedItem(issue.getStatus());
        inputReporter.setText(issue.getReporter().toString());
        inputDescription.setText(issue.getDescription());
        // Es carreguen els camps custom
        loadCustomFieldValues();
        // Dates
        inputSendDate.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(issue.getSendDate()));
        inputModificationDate.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(issue.getModifiedDate()));
        if(issue.getResolutionDate()!=null){
        	inputResolutionDate.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(issue.getResolutionDate()));
        }
        if(issue.getSlaDate()!=null){
        	inputSlaDate.setEditable(true);
        	inputSlaDate.setText(new SimpleDateFormat("dd/MM/yyyy").format(issue.getSlaDate()));
        	toggleSla.setSelected(true);
        }
        else{
        	inputSlaDate.setText(null);
        	inputSlaDate.setEditable(false);
        }
        /* Working data */
        inputAssignedUser.setSelectedElement(issue.getAssignedUser());
    	inputEstimatedTime.setText(issue.getEstimatedTimeFormated());
    	if(issue instanceof IVespineIssueDTO){
    		inputSprint.setSelectedElement(((IVespineIssueDTO)issue).getSprint());
    	}
    	else{
    		inputSprint.setSelectedElement(null);
    	}
        workReportWidget.setIssue(issue);
        /* Actualització de les notes de la incidència */
        List<IIssueNoteDTO> notes = new ArrayList<IIssueNoteDTO>(issue.getNotes());
        pNotes.setNotes(notes);
    }
    
    private void loadProjects() throws BusinessException
    {
    	List<IProjectDTO> projects = administrationService.findAllProjects();
    	((DefaultComboBoxModel)comboProject.getModel()).removeAllElements();
    	for(IProjectDTO elem:projects){
    		((DefaultComboBoxModel)comboProject.getModel()).addElement(elem);
    	}
    }
    
    private void loadCategories() throws BusinessException
    {
    	List<ICategoryDTO> categories = administrationService.findAllCategories();
    	((DefaultComboBoxModel)comboCategory.getModel()).removeAllElements();
    	for(ICategoryDTO elem:categories){
    		((DefaultComboBoxModel)comboCategory.getModel()).addElement(elem);
    	}
    }
    
    private void loadSeverities() throws BusinessException
    {
    	List<ISeverityDTO> severities = administrationService.findAllSeverities();
    	((DefaultComboBoxModel)comboSeverity.getModel()).removeAllElements();
    	for(ISeverityDTO elem:severities){
    		((DefaultComboBoxModel)comboSeverity.getModel()).addElement(elem);
    	}
    }
    
    private void loadStatus(IProjectDTO project) throws BusinessException
    {
    	// Estats
    	String status = issue==null ? null : issue.getStatus().getStatusId();
    	List<IStatusDTO> statusList = controller.getIssueNextStatus(project.getProjectId(), status);
    	((DefaultComboBoxModel)comboStatus.getModel()).removeAllElements();
    	for(IStatusDTO elem:statusList){
    		((DefaultComboBoxModel)comboStatus.getModel()).addElement(elem);
    	}
    }
    
    private void loadCustomFields(IProjectDTO project) throws BusinessException
    {
    	pCustomFields.removeAll();
    	List<ICustomFieldDTO> customFields = administrationService.findCustomFieldsByProject(project.getProjectId());
    	// Si no hi ha camps per mostrar, s'amaga la secció corresponent
    	if(customFields.isEmpty()){
    		pCustomFields.setVisible(false);
    		return;
    	}
    	// En cas contrari es mostra la secció si no estava visible
    	else if(!pCustomFields.isVisible()){
    		pCustomFields.setVisible(true);
    	}
    	    	
    	CustomFieldWidgetFactory widgetFactory = CustomFieldWidgetFactory.getInstance(); 
    	GroupLayout.ParallelGroup hGroupLabels = layoutCustomFields.createParallelGroup(GroupLayout.Alignment.TRAILING);
    	GroupLayout.ParallelGroup hGroupFields = layoutCustomFields.createParallelGroup(GroupLayout.Alignment.LEADING);
    	GroupLayout.SequentialGroup vGroup = layoutCustomFields.createSequentialGroup();
    	vGroup.addContainerGap();
    	for(int i=0; i<customFields.size(); i++){
    		ICustomFieldDTO field = customFields.get(i);    		
    		ICustomFieldWidget<?> widget = widgetFactory.createWidget(field);
    		customFieldWidgets.add(widget);
    		JLabel label = widget.createLabel();
    		JComponent component = widget.createComponent();    		
    		if(i>0){
    			vGroup.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED);
    		}
    		GroupLayout.ParallelGroup vWidgetGroup = layoutCustomFields.createParallelGroup(GroupLayout.Alignment.BASELINE);
    		if(label!=null){    			
    			hGroupLabels.addComponent(label, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE);
    			vWidgetGroup.addComponent(label);
    		}
    		hGroupFields.addComponent(component, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
    		vWidgetGroup.addComponent(component);
    		vGroup.addGroup(vWidgetGroup);
    	}
    	layoutCustomFields.setHorizontalGroup(layoutCustomFields.createSequentialGroup()
    			.addContainerGap()
    			.addGroup(hGroupLabels)
    			.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    			.addGroup(hGroupFields)
    			.addContainerGap()
    	);
    	vGroup.addContainerGap();
    	layoutCustomFields.setVerticalGroup(vGroup);
    }
    
    private void loadCustomFieldValues() throws BusinessException
    {
    	IIssueFacade issueService = (IIssueFacade)ApplicationContext.getInstance().getRegisteredComponent(IIssueFacade.class);
    	List<ICustomValueDTO> values = issueService.findIssueCustomValues(issue.getIssueId());
    	// S'organitzen els valors rebuts per poder recuperar-los més ràpidament
    	HashMap<ICustomFieldDTO, ICustomValueDTO> valuesMap = new HashMap<ICustomFieldDTO, ICustomValueDTO>();
    	for(ICustomValueDTO elem:values){
    		valuesMap.put(elem.getField(), elem);
    	}
    	// S'emplenen els camps amb els valors recuperats
    	for(ICustomFieldWidget widget:customFieldWidgets){
    		ICustomValueDTO issueCustomValue = valuesMap.get(widget.getCustomField());
    		if(issueCustomValue!=null && issueCustomValue.getValue()!=null){
    			// Es converteix el valor
    			IFieldType fieldType = widget.getCustomField().getFieldType(); 
	        	IAttributeConverter converter = (IAttributeConverter)ApplicationContext.getInstance().getRegisteredComponent(fieldType.getConverter());
	        	Map<String, String> params = fieldType.getConverterParameters();
	        	if(params!=null){
					for(String paramName:params.keySet()){
						String paramValue = params.get(paramName);
						converter.setParameter(paramName, paramValue);
					}
				}
    			widget.setValue(converter.convertToEntityAttribute(issueCustomValue.getValue()));
    		}
    	}
    }
    
    private void setSlaDate()
    {
    	inputSlaDate.setEditable(toggleSla.isSelected());
    	if(toggleSla.isSelected()){
			Date sendDate = issue==null ? new Date() : issue.getSendDate();
			ISeverityDTO severity = (ISeverityDTO)comboSeverity.getSelectedItem();
			if(severity!=null){
				try{
					Date slaDate = issueService.computeSLADate(sendDate, severity.getSeverityId());
					inputSlaDate.setText(new SimpleDateFormat("dd/MM/yyyy").format(slaDate));
				}
				catch(Exception e){
		            log.error("Error calculant la data SLA de l'incidència {}: {}", new Object[]{issue.getIssueId(), e.getMessage(), e});
		            ApplicationWindowManager.getInstance()
		                    .displayErrorMessage("Error calculant la data SLA de l'incidència: " + e.getMessage(), e);            
		        }
			}
		}
		else{
			inputSlaDate.setText(null);
		}
    }
    
    private class ViewToolbar extends JPanel implements ActionListener
    {
		private static final long serialVersionUID = -3391633369645757618L;
		
		private JCommandButton bEdit;
    	private JCommandButton bCreate;
    	private JCommandButton bUpdate;
    	private JCommandButton bCancel;
    	
    	public ViewToolbar()
    	{
    		createContents();
    	}
    	
    	public void createContents()
    	{
    		 // Panell d'opcions. No es pot fer servir un Box perquè el L&F Substance el no coloritza correctament
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
            putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, new Double(0.25d));
            /* Opcions associades */
            // Editar
            bEdit = new JCommandButton("Editar");
            bEdit.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
            bEdit.setDisplayState(CommandButtonDisplayState.MEDIUM);
            bEdit.setActionRichTooltip(new RichTooltip("Editar incidencia", "Editar les dades de la incidencia"));
            bEdit.setFlat(false);
            bEdit.getActionModel().setActionCommand(ACTION_EDIT_ISSUE);
            bEdit.addActionListener(this);            
            add(bEdit);
            // Crear
            bCreate = new JCommandButton("Crear");
            bCreate.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
            bCreate.setDisplayState(CommandButtonDisplayState.MEDIUM);
            bCreate.setActionRichTooltip(new RichTooltip("Crear incidencia", "Crear una nova incidencia"));
            bCreate.setFlat(false);
            bCreate.getActionModel().setActionCommand(ACTION_CREATE_ISSUE);
            bCreate.addActionListener(this);            
            add(bCreate);
            // Actualitzar
            bUpdate = new JCommandButton("Actualitzar");
            bUpdate.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
            bUpdate.setDisplayState(CommandButtonDisplayState.MEDIUM);
            bUpdate.setActionRichTooltip(new RichTooltip("Actualitzar incidencia", "Actualitzar les dades de la incidencia"));
            bUpdate.setFlat(false);
            bUpdate.getActionModel().setActionCommand(ACTION_UPDATE_ISSUE);
            bUpdate.addActionListener(this);            
            add(bUpdate);            
            // Cancel.lar
            add(Box.createHorizontalStrut(5));
            bCancel = new JCommandButton("Cancel.lar");
            bCancel.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
            bCancel.setDisplayState(CommandButtonDisplayState.MEDIUM);
            bCancel.setActionRichTooltip(new RichTooltip("Cancel.lar", "Cancel.lar l'edició de la incidencia"));
            bCancel.setFlat(false);
            bCancel.getActionModel().setActionCommand(ACTION_CANCEL_EDIT);
            bCancel.addActionListener(this);            
            add(bCancel);
    	}

    	public void setMode(int mode)
    	{
    		IExtendedUserFacade userManagementService = (IExtendedUserFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedUserFacade.class);
    		
    		// Control de permisos
    		boolean reporterRole = userManagementService.checkRole(SecurityConstants.AUTHORIZATION_ROLE_REPORTER);
    		
    		// Només visible en el mode visualització i si es tenen permisos
    		bEdit.setVisible(mode==MODE_VISUALIZATION && reporterRole);
    		// Només visible en el mode creació
    		bCreate.setVisible(mode==MODE_CREATION);
    		// Només visible en el mode actualització
    		bUpdate.setVisible(mode==MODE_UPDATE);
    		// Només visible en el mode creació o actualització
    		bCancel.setVisible(mode!=MODE_VISUALIZATION);
    	}
    	
		@Override
		public void actionPerformed(ActionEvent event) 
		{
			if(ACTION_EDIT_ISSUE.equals(event.getActionCommand())){
				IssueDetailView.this.setMode(MODE_UPDATE);
			}
			else if(ACTION_CREATE_ISSUE.equals(event.getActionCommand())){
				if(!validateFields(ACTION_CREATE_ISSUE)){
					return;
				}
				
				// Es recuperen els camps custom
				HashMap<ICustomFieldDTO, String> customFields = new HashMap<ICustomFieldDTO, String>();
				for(ICustomFieldWidget<?> widget:customFieldWidgets){
					ICustomFieldDTO field = widget.getCustomField();
					String value = widget.getValueAsString();
					customFields.put(field, value);
				}
				controller.createIssue((IProjectDTO)comboProject.getSelectedItem(), 
						(ICategoryDTO)comboCategory.getSelectedItem(), 
						(ISeverityDTO)comboSeverity.getSelectedItem(), 
						(IStatusDTO)comboStatus.getSelectedItem(), 
						inputSummary.getText(), 
						inputDescription.getText(),
						inputAssignedUser.getSelectedElement(),
						inputSprint.getSelectedElement(),
						inputEstimatedTime.getText(),
						inputSlaDate.getText(), 
						customFields);
			}
			else if(ACTION_UPDATE_ISSUE.equals(event.getActionCommand())){
				if(!validateFields(ACTION_UPDATE_ISSUE)){
					return;
				}
				
				// Es recuperen els camps custom
				HashMap<ICustomFieldDTO, String> customFields = new HashMap<ICustomFieldDTO, String>();
				for(ICustomFieldWidget<?> widget:customFieldWidgets){
					ICustomFieldDTO field = widget.getCustomField();
					String value = widget.getValueAsString();
					customFields.put(field, value);
				}
				controller.updateIssue(issue.getIssueId(), 
						(IProjectDTO)comboProject.getSelectedItem(), 
						(ICategoryDTO)comboCategory.getSelectedItem(), 
						(ISeverityDTO)comboSeverity.getSelectedItem(), 
						(IStatusDTO)comboStatus.getSelectedItem(), 
						inputSummary.getText(), 
						inputDescription.getText(),
						inputAssignedUser.getSelectedElement(),
						inputSprint.getSelectedElement(),
						inputEstimatedTime.getText(),
						inputSlaDate.getText(),
						inputResolutionDate.getText(),
						customFields);
			}
			else if(ACTION_CANCEL_EDIT.equals(event.getActionCommand())){
				if(issue!=null){
					controller.openIssueDetailsView(issue.getIssueId());
				}
				else{
					controller.leaveIssueDetailView(getComposedId());
				}
			}
		}
		
		private boolean validateFields(String actionCommand)
		{
			IProjectDTO project = (IProjectDTO)comboProject.getSelectedItem();
			ICategoryDTO category = (ICategoryDTO)comboCategory.getSelectedItem();
			ISeverityDTO severity = (ISeverityDTO)comboSeverity.getSelectedItem();
			IStatusDTO status = (IStatusDTO)comboStatus.getSelectedItem();
			String summary = inputSummary.getText();
			String description = inputDescription.getText();
			String slaDate = inputSlaDate.getText();
			String resolutionDate = inputResolutionDate.getText();
			String estimatedTime = inputEstimatedTime.getText();
			
			if(project==null){
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
	                    "Cal especificar el projecte al que pertany la incidencia",
	                    "Advertència",
	                    JOptionPane.WARNING_MESSAGE);
				
				return false;
			}
			if(category==null){
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
	                    "Cal especificar la categoria a la que pertany la incidencia",
	                    "Advertència",
	                    JOptionPane.WARNING_MESSAGE);
				
				return false;
			}
			if(severity==null){
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
	                    "Cal especificar la severitat a la que pertany la incidencia",
	                    "Advertència",
	                    JOptionPane.WARNING_MESSAGE);
				
				return false;
			}
			if(status==null){
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
	                    "Cal especificar l'estat en que es troba la incidencia",
	                    "Advertència",
	                    JOptionPane.WARNING_MESSAGE);
				
				return false;
			}
			if(summary==null){
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
	                    "Cal especificar un títol per la incidencia",
	                    "Advertència",
	                    JOptionPane.WARNING_MESSAGE);
				
				return false;
			}
			if(description==null){
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
	                    "Cal especificar una descripció per la incidencia",
	                    "Advertència",
	                    JOptionPane.WARNING_MESSAGE);
				
				return false;
			}
			if(StringUtils.isNotBlank(slaDate)){
				try{
					new SimpleDateFormat("dd/MM/yyyy").parse(slaDate);
				}
				catch(ParseException e){
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
		                    "El format de la data de periode SLA no és vàlid (DD/MM/YYYY)",
		                    "Advertència",
		                    JOptionPane.WARNING_MESSAGE);
					
					return false;
				}
			}
			if(toggleSla.isSelected() && StringUtils.isBlank(slaDate)){
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
	                    "Ha d'introduir una data de periode SLA vàlida",
	                    "Advertència",
	                    JOptionPane.WARNING_MESSAGE);
				
				return false;
			}
			if(ACTION_UPDATE_ISSUE.equals(actionCommand) && StringUtils.isNotBlank(resolutionDate)){
				try{
					new SimpleDateFormat("dd/MM/yyyy").parse(resolutionDate);
				}
				catch(ParseException e){
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
		                    "El format de la data de resolució no és vàlid (DD/MM/YYYY)",
		                    "Advertència",
		                    JOptionPane.WARNING_MESSAGE);
					
					return false;
				}
			}
			if(StringUtils.isNotBlank(estimatedTime) && !Pattern.matches("^(?:\\d+h){0,1}\\W*(?:\\d+m){0,1}$", estimatedTime)){
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
	                    "El format del camp TEMPS ESTIMAT no és vàlid (##h ##m)",
	                    "Advertència",
	                    JOptionPane.WARNING_MESSAGE);
				
				return false;
			}
			// Validació dels camps custom
			try{
				for(ICustomFieldWidget<?> customField:customFieldWidgets){
					customField.validate();
				}
			}
			catch(ValidationException e){
				JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
	                    e.getMessage(),
	                    "Advertència",
	                    JOptionPane.WARNING_MESSAGE);
				
				return false;
			}
			
			return true;
		}
    }
}
