package org.fenrir.dragonfly.ui.view;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.util.List;
import javax.inject.Inject;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.dragonfly.ui.controller.ApplicationController;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140505
 */
public class IssueFilterExplorerView extends AbstractView<ApplicationController>
{
	public static final String ID = "org.fenrir.dragonfly.ui.view.issueFilterView";
	
    private JList listApplicationFilters;
    private JList listUserFilters;
    private boolean fireChangeEvent = true;

    @Inject
    public void setApplicationController(ApplicationController applicationController)
    {
        setController(applicationController);
    }

    public String getId()
    {
    	return ID;
    }
    
    @Override
    protected final JComponent createViewContents()
    {
        // Filtres de l'aplicació
        listApplicationFilters = new JList(new DefaultListModel());        
        listApplicationFilters.addListSelectionListener(new ListSelectionListener()
        {
            @Override
            public void valueChanged(ListSelectionEvent event)
            {
                if(fireChangeEvent){
                    // Es desel.lecciona un possible valor de la llista de filtres d'usuari
                    fireChangeEvent = false;
                    listUserFilters.clearSelection();
                    fireChangeEvent = true;
                    IViewFilterDTO selectedFilter = (IViewFilterDTO)listApplicationFilters.getSelectedValue();
                    controller.setSelectedFilter(selectedFilter);
                }
            }
        });
        // Filtres de l'usuari
        listUserFilters = new JList(new DefaultListModel());        
        listUserFilters.addListSelectionListener(new ListSelectionListener()
        {
            @Override
            public void valueChanged(ListSelectionEvent event)
            {
                if(fireChangeEvent){
                    // Es desel.lecciona un possible valor de la llista de filtres d'usuari
                    fireChangeEvent = false;
                    listApplicationFilters.clearSelection();
                    fireChangeEvent = true;
                    IViewFilterDTO selectedFilter = (IViewFilterDTO)listUserFilters.getSelectedValue();
                    controller.setSelectedFilter(selectedFilter);
                }
            }
        });
        
        JPanel contentPane = new JPanel();
        GroupLayout layout = new GroupLayout(contentPane);
        contentPane.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()                
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(listApplicationFilters, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                    .addComponent(listUserFilters, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)                    
                )                
            )
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(listApplicationFilters)
                .addGap(10)
                .addComponent(listUserFilters)                
                .addContainerGap()
            )
        );
        
        JScrollPane scrollFilters = new JScrollPane();
        scrollFilters.getVerticalScrollBar().setUnitIncrement(10);
        scrollFilters.setViewportView(contentPane);

        return scrollFilters;
    }

    public void setApplicationFilterList(List<IViewFilterDTO> filters)
    {
        fireChangeEvent = false;
        // S'esborren tots els elements de la llista
        ((DefaultListModel)listApplicationFilters.getModel()).removeAllElements();
        for(IViewFilterDTO filter:filters){
            ((DefaultListModel)listApplicationFilters.getModel()).addElement(filter);
        }
        fireChangeEvent = true;
    }
    
    public void setUserFilterList(List<IViewFilterDTO> filters)
    {
        fireChangeEvent = false;
        // S'esborren tots els elements de la llista
        ((DefaultListModel)listUserFilters.getModel()).removeAllElements();
        for(IViewFilterDTO filter:filters){
            ((DefaultListModel)listUserFilters.getModel()).addElement(filter);
        }
        fireChangeEvent = true;
    }

    public void clearSelection()
    {
        fireChangeEvent = false;
        listUserFilters.clearSelection();
        fireChangeEvent = true;
    }
}
