package org.fenrir.dragonfly.ui.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import javax.inject.Inject;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.ui.widget.IssueWidget;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140504
 */
public class IssueListView extends AbstractView<ApplicationController> implements ActionListener
{
	private static final long serialVersionUID = 2290364809862985317L;

	public static final String ID = "org.fenrir.dragonfly.ui.view.issueListView";
    
    private final String ACTION_REFRESH_FILTER = "REFRESH_FILTER";
    private final String ACTION_CREATE_FILTER = "CREATE_FILTER";
    private final String ACTION_MODIFY_FILTER = "MODIFY_FILTER";

    private final Logger log = LoggerFactory.getLogger(IssueListView.class);

    private JLabel lTitleName;
    // Necessari mantenir la referència per poder refrescar el contingut una vegada afegides o eliminades incidències
    private JScrollPane scrollIssues;
    private Box pIssues;
    // Components indexats per IssueID
    private Map<String, JComponent> indexedIssueComponents = new HashMap<String, JComponent>();
    // Relació VisibleID -> IssueID
    private Map<String, String> indexedIssueIds = new HashMap<String, String>();
    // Component de la incidència sel.leccionada. Necessari per poder expandir / contraure més ràpidament les seves opcions avançades
    private IssueWidget selectedIssueWidget;

    @Inject
    public void setApplicationController(ApplicationController applicationController)
    {
        setController(applicationController);
    }

    @Override
    public String getId()
    {
    	return ID;
    }
    
    @Override
    protected final JComponent createViewContents()
    {
        JPanel pContents = new JPanel();
        pContents.setLayout(new BorderLayout());
		
        /* Títol */
        JComponent pTitle = createTitlePanel();		
        pContents.add(pTitle, BorderLayout.NORTH);
        
        /* Llista d'incidències */
        pIssues = Box.createVerticalBox();        
        scrollIssues = new JScrollPane(pIssues);
        // S'augmenta la velocitat de l'scroll
        scrollIssues.getVerticalScrollBar().setUnitIncrement(10);
        scrollIssues.getViewport().setBackground(new Color(0, 0, 0, 100));
        pContents.add(scrollIssues, BorderLayout.CENTER);                
        
        return pContents;
    }
    
    private JComponent createTitlePanel()
    {
        Box mainBox = Box.createVerticalBox();
        // Marge
        mainBox.add(Box.createVerticalStrut(5));
        // Barra de títol
        Box titleBox = Box.createHorizontalBox();
        lTitleName = new JLabel();                
        titleBox.add(lTitleName);
        // S'assegura el tamany mínim
        titleBox.add(Box.createVerticalStrut(25));
        titleBox.add(Box.createHorizontalGlue());
        mainBox.add(titleBox);
        // Marge intermig
        mainBox.add(Box.createVerticalStrut(5));
        // Botonera de la vista
        Box toolbarBox = Box.createHorizontalBox();
        // Merge esquerra
        toolbarBox.add(Box.createHorizontalStrut(5));
        JButton bRefreshFilter = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/reload_22.png")));
        bRefreshFilter.setToolTipText("Recarregar la vista");
        bRefreshFilter.setActionCommand(ACTION_REFRESH_FILTER);
        bRefreshFilter.addActionListener(this);
        bRefreshFilter.setPreferredSize(new Dimension(30, 30));
        toolbarBox.add(bRefreshFilter);
        // Marge intermedi
        toolbarBox.add(Box.createHorizontalStrut(5));
        JButton bCreateFilter = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/filter_new_22.png")));
        bCreateFilter.setToolTipText("Crear un nou filtre");
        bCreateFilter.setActionCommand(ACTION_CREATE_FILTER);
        bCreateFilter.addActionListener(this);
        bCreateFilter.setPreferredSize(new Dimension(30, 30));        
        toolbarBox.add(bCreateFilter);
        // Marge intermedi
        toolbarBox.add(Box.createHorizontalStrut(5));
        JButton bModifyFilter = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/filter_modify_22.png")));
        bModifyFilter.setToolTipText("Modificar el filtre actual");
        bModifyFilter.setActionCommand(ACTION_MODIFY_FILTER);
        bModifyFilter.addActionListener(this);
        bModifyFilter.setPreferredSize(new Dimension(30, 30));        
        toolbarBox.add(bModifyFilter);        
        // Espai extra
        toolbarBox.add(Box.createHorizontalGlue());                
        // Cerca d'incidències per issueId
        JTextField inputSearchIssueId = new JTextField("Localitzar incidencia...");
        inputSearchIssueId.setForeground(UIManager.getColor("TextField.inactiveForeground"));  
        inputSearchIssueId.addKeyListener(new KeyAdapter() 
        {
            @Override
            public void keyPressed(KeyEvent event) 
            {
                if(event.getKeyCode()==KeyEvent.VK_ENTER){                    
                    try{
                        String value = ((JTextField)event.getSource()).getText();
                        controller.seekIssueInView(value);
                    }
                    catch(NumberFormatException e){
                        log.warn("Error localitzant incidencia amb id {} dins la vista: Format incorrecte");
                        JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                                "El número d'incidència especificat no es correcte",
                                "Advertència",
                                JOptionPane.WARNING_MESSAGE);
                    }
                    scrollIssues.requestFocusInWindow();
                }
            }            
        });
        inputSearchIssueId.addFocusListener(new FocusAdapter() 
        {
            @Override
            public void focusGained(FocusEvent event) 
            {
                ((JTextField)event.getSource()).setForeground(UIManager.getColor("TextField.foreground"));
                ((JTextField)event.getSource()).setText(null);                
            }

            @Override
            public void focusLost(FocusEvent event) 
            {
                ((JTextField)event.getSource()).setForeground(UIManager.getColor("TextField.inactiveForeground"));  
                ((JTextField)event.getSource()).setText("Localitzar incidencia...");
            }            
        });
        inputSearchIssueId.setMinimumSize(new Dimension(250, inputSearchIssueId.getPreferredSize().height));
        inputSearchIssueId.setPreferredSize(new Dimension(250, inputSearchIssueId.getPreferredSize().height));
        inputSearchIssueId.setMaximumSize(new Dimension(250, inputSearchIssueId.getPreferredSize().height));
        toolbarBox.add(inputSearchIssueId);
        toolbarBox.add(Box.createHorizontalStrut(5));
        
        mainBox.add(toolbarBox);
        // Marge inferior
        mainBox.add(Box.createVerticalStrut(5));
        
        // Permisos
        IExtendedUserFacade userService = (IExtendedUserFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedUserFacade.class);
        boolean administratorRole = userService.checkRole(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR);
        bCreateFilter.setVisible(administratorRole);
        bModifyFilter.setVisible(administratorRole);
        
        return mainBox;
    }

    public void clearIssues() throws Exception
    {
        // L'actualització de la UI s'ha de fer en un Thread a part si cal
        if(SwingUtilities.isEventDispatchThread()){
            pIssues.removeAll();
            indexedIssueComponents.clear();
            indexedIssueIds.clear();
            selectedIssueWidget = null;

            scrollIssues.validate();
        }
        else{
            SwingUtilities.invokeAndWait(new Runnable()
            {
                @Override
                public void run()
                {
                    pIssues.removeAll();
                    indexedIssueComponents.clear();
                    indexedIssueIds.clear();
                    selectedIssueWidget = null;

                    scrollIssues.validate();
                }
            });
        }
    }

    public void addIssues(final List<IIssueDTO> issues, final boolean hasMoreIssues) throws Exception
    {
        // L'actualització de la UI s'ha de fer en un Thread a part si cal
        if(SwingUtilities.isEventDispatchThread()){
            addIssuesPrivate(issues, hasMoreIssues);
        }
        else{
            SwingUtilities.invokeAndWait(new Runnable()
            {
                @Override
                public void run()
                {                                
                    addIssuesPrivate(issues, hasMoreIssues);
                }
            });
        }
    }

    private void addIssuesPrivate(final List<IIssueDTO> issues, boolean hasMoreIssues)
    {
        if(pIssues.getComponentCount()>0){
            pIssues.remove(pIssues.getComponentCount()-1);
        }

        for(IIssueDTO issue:issues){
            Color backgroundColor = controller.getStatusColor(issue.getStatus().getStatusId());
            final IssueWidget issueWidget = new IssueWidget(issue, backgroundColor);            
            // Necessari per alinear correctament el botó de carregar més incidències
            issueWidget.setAlignmentX(LEFT_ALIGNMENT);
            pIssues.add(issueWidget);
            issueWidget.addMouseListener(new MouseInputAdapter()
            {
                @Override
                public void mouseClicked(MouseEvent event)
                {
                    if(event.getClickCount()==2){
                        controller.openIssueDetailsView(issueWidget.getIssueId());
                    }
                    else{
//                      issueWidget.setHighlighted(!visible);
						if(selectedIssueWidget!=null && selectedIssueWidget!=issueWidget){
                        	selectedIssueWidget.setOptionsVisible(false);
	                    }
    	                issueWidget.setOptionsVisible(!issueWidget.isOptionsVisible());
        	            selectedIssueWidget = issueWidget.isOptionsVisible() ? issueWidget : null;
                    }
                }
            });
            // S'afegeix al map d'incidències indexades
            indexedIssueComponents.put(issue.getIssueId(), issueWidget);
            indexedIssueIds.put(issue.getVisibleId(), issue.getIssueId());
        }
        if(hasMoreIssues){
            JButton bLoadMore = new JButton("Carregar més incidències");
            bLoadMore.setPreferredSize(new Dimension(400, 40));
            bLoadMore.setMaximumSize(new Dimension(Short.MAX_VALUE, 40));
            bLoadMore.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent event)
                {
                    controller.loadMoreIssues();
                }
            });
            // Necessari per alinear correctament el botó
            bLoadMore.setAlignmentX(LEFT_ALIGNMENT);
            pIssues.add(bLoadMore);
        }
        else{
            // Espai extra
            pIssues.add(Box.createVerticalGlue());
        }

        /* Necessari per refrescar el panell i que es mostrin els components afegits
         * en cas que es crein les vistes després que s'hagi creat la finestra principal,
         * per exemple, durant la primera execució quan no hi ha workspace per defecte.
         */
        scrollIssues.validate();                
    }
    
    public boolean seekIssue(String visibleId)
    {
    	String issueId = indexedIssueIds.get(visibleId.toUpperCase());
        JComponent widget = indexedIssueComponents.get(issueId);
        // En el cas que estigui present a la pàgina actual no cal buscar més
        if(widget!=null){
            scrollIssues.getViewport().setViewPosition(widget.getBounds().getLocation());
            // TODO Resaltar widget
            
            return true;
        }        
        
        return false;
    }
    
    public void updateIssueWidget(IIssueDTO issue)
    {
        IssueWidget widget = (IssueWidget)indexedIssueComponents.get(issue.getIssueId());
        /* Pot ser que el widget no existeixi a la vista 
         * si el tag s'ha assigant desde la pantalla de detall
         */
        if(widget!=null){
        	widget.updateWidget(issue);
        }
    }
    
    public void setTitle(String title)
    {
        lTitleName.setText(title);
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
        if(ACTION_REFRESH_FILTER.equals(event.getActionCommand())){
            controller.reloadIssuesView();
        }
        else if(ACTION_CREATE_FILTER.equals(event.getActionCommand())){
            controller.openNewFilterDialog();
        }
        else if(ACTION_MODIFY_FILTER.equals(event.getActionCommand())){
            controller.openModifyFilterDialog();
        }        
    }    
}
