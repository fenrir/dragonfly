package org.fenrir.dragonfly.ui.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import java.util.Date;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.Timer;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.core.entity.IssueTimerRegistry;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140720
 */
@SuppressWarnings("serial")
public class IssueTimerView extends AbstractView<ApplicationController> implements ActionListener 
{
	public static final String ID = "org.fenrir.dragonfly.ui.view.issueTimerView";
	
	private final Logger log = LoggerFactory.getLogger(IssueTimerView.class);

	private static final String ACTION_COMMAND_PLAY = "PLAY";
	private static final String ACTION_COMMAND_CLEAR = "CLEAR";
	private static final String ACTION_COMMAND_RESET = "RESET";
	private static final String ACTION_COMMAND_PUBLISH = "PUBLISH";
	private static final String ACTION_COMMAND_TIMER_ACTION = "TIMER_ACTION";
	
	// 1 minut
	private int TIMER_MILLIS_DELAY = 60 * 1000;

	private static final ImageIcon iconStart = new ImageIcon(IssueTimerView.class.getResource("/org/fenrir/dragonfly/ui/icons/work_registry_play_48.png"));
	private static final ImageIcon iconPause = new ImageIcon(IssueTimerView.class.getResource("/org/fenrir/dragonfly/ui/icons/work_registry_pause_48.png"));
	private static final String NO_ISSUE_DESCRIPTION_MESSAGE = "[No s'està treballant en cap incidència]";
	private static final String NO_ISSUE_WORK_TIME_MESSAGE = "--:--";
	private static final String ISSUE_WORK_TIME_MESSAGE_SIMPLE = "{0, number, integer}h. {1, number, integer}min.";
	private static final String ISSUE_WORK_TIME_MESSAGE_MULTIPLE = "{0, number, integer}h. {1, number, integer}min. / {2, number, integer}h. {3, number, integer}min.";
	
	private IssueTimerRegistry registry;
	private IIssueDTO issue;
	private int elapsedTime = 0;
	
	private Timer timer;

	private JButton bPlay;
	private JButton bReset;
	private JButton bClear;
	private JButton bPublish;
	private JLabel lIssueSummary;
	private JLabel lElapsedTime;
	
	private IExtendedIssueWorkFacade workService;
	private IIssueFacade issueService;
	
	@Inject
    public void setApplicationController(ApplicationController applicationController)
    {
        setController(applicationController);
    }
	
	@Inject
	public void setWorkService(IExtendedIssueWorkFacade workservice)
	{
		this.workService = workservice;
	}
	
	@Inject
	public void setIssueSearchService(IIssueFacade issueService)
	{
		this.issueService = issueService;
	}
    
    @Override
    public String getId()
    {
    	return ID;
    }
    
    @Override
    protected final JComponent createViewContents()
    {
    	JPanel pContents = new JPanel();

    	lIssueSummary = new JLabel(NO_ISSUE_DESCRIPTION_MESSAGE);
    	lIssueSummary.setToolTipText(NO_ISSUE_DESCRIPTION_MESSAGE);
    	bPlay = new JButton(iconStart);
    	bPlay.setActionCommand(ACTION_COMMAND_PLAY);
    	bPlay.addActionListener(this);
    	bPlay.setEnabled(false);
    	bReset = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/reload_22.png")));    	
    	bReset.setToolTipText("Reiniciar");
    	bReset.setActionCommand(ACTION_COMMAND_RESET);
    	bReset.addActionListener(this);
    	bReset.setEnabled(false);
    	bClear = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/work_registry_remove_22.png")));    	
    	bClear.setToolTipText("Descartar");
    	bClear.setActionCommand(ACTION_COMMAND_CLEAR);
    	bClear.addActionListener(this);
    	bClear.setEnabled(false);
    	bPublish = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/ok_22.png")));
    	bPublish.setToolTipText("Registrar");
    	bPublish.setActionCommand(ACTION_COMMAND_PUBLISH);
    	bPublish.addActionListener(this);
    	bPublish.setEnabled(false);
    	lElapsedTime = new JLabel(NO_ISSUE_WORK_TIME_MESSAGE);
    	lElapsedTime.setToolTipText(NO_ISSUE_WORK_TIME_MESSAGE);
    	
    	GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
    		.addGroup(layout.createSequentialGroup()
    			.addContainerGap()        		
    	        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)            	            
    	            .addComponent(lIssueSummary, 200, 200, Short.MAX_VALUE)
    	            .addGroup(layout.createSequentialGroup()
    	            	.addComponent(bPlay, 50, 50, 50)
    	            	.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    	            	.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	            			.addComponent(lElapsedTime, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
	            			.addGroup(layout.createSequentialGroup()
            					.addComponent(bReset, 22, 22, 22)	
            					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            					.addComponent(bClear, 22, 22, 22)
            					.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            					.addComponent(bPublish, 22, 22, 22)
	            			)
    	            	)
    	            )	
    	        )
    	        .addContainerGap()
    		)
		);
        /* Grup vertical */
        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
	        .addGroup(layout.createSequentialGroup()
	            .addContainerGap()
	            .addComponent(lIssueSummary)
	            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
	            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
	            	.addComponent(bPlay, 50, 50, 50)
	            	.addGroup(layout.createSequentialGroup()
            			.addComponent(lElapsedTime)
            			.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            			.addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        					.addComponent(bReset, 22, 22, 22)	
        					.addComponent(bClear, 22, 22, 22)
        					.addComponent(bPublish, 22, 22, 22)
            			)
	            	)
        		)
	            .addContainerGap()
	        )
        );
        
    	return pContents;
    }
    
    public void setWorkingIssue(IssueTimerRegistry registry)
    {
    	if(registry==null || !registry.equals(this.registry)){
    		elapsedTime = 0;
    		setTimerMode(false);
    	}
    	
    	try{
	    	this.registry = registry;
	    	if(registry!=null){
	    		issue = issueService.findIssueById(registry.getIssueId().toString());
		    		
	    		elapsedTime = registry.getElapsedTime();
	    		lIssueSummary.setText(issue.getSummary());
	    		lIssueSummary.setToolTipText(issue.getSummary());
	    		displayElapsedTime();
	    	}
	    	else{
	    		issue = null;
	    		lIssueSummary.setText(NO_ISSUE_DESCRIPTION_MESSAGE);
	    		lIssueSummary.setToolTipText(NO_ISSUE_DESCRIPTION_MESSAGE);
	    		lElapsedTime.setText(NO_ISSUE_WORK_TIME_MESSAGE);
	    		lElapsedTime.setToolTipText(NO_ISSUE_WORK_TIME_MESSAGE);
	    	}
	    	bPlay.setEnabled(registry!=null);
	    	bClear.setEnabled(registry!=null);
	    	bReset.setEnabled(registry!=null);
	    	bPublish.setEnabled(registry!=null);
    	}
    	catch(BusinessException e){
    		log.error("Error inicialitzant la vista de registre de temps: {}", e.getMessage(), e);
    		// Reset de la vista
    		setWorkingIssue(null);
            ApplicationWindowManager.getInstance()
                    .displayErrorMessage("Error inicialitzant la vista de registre de temps: " + e.getMessage(), e);
    	}
    }

	@Override
	public void actionPerformed(ActionEvent event) 
	{
		if(ACTION_COMMAND_PLAY.equals(event.getActionCommand())){
			if(timer==null){
				timer = new Timer(TIMER_MILLIS_DELAY, this);
				timer.setActionCommand(ACTION_COMMAND_TIMER_ACTION);
			}
			// Necessari si s'inicia per primera vegada el timer
			displayElapsedTime();
			if(!timer.isRunning()){
				setTimerMode(true);
			}
			else{
				setTimerMode(false);
			}
		}
		else if(ACTION_COMMAND_CLEAR.equals(event.getActionCommand())){
			setTimerMode(false);
			try{
				workService.deleteWorkInProgressRegistry(registry.getId().toString(), registry.getIssueId().toString());
				controller.setWorkingIssue(null);
			}
			catch(BusinessException e){
	    		log.error("Error esborrant el timer: {}", e.getMessage(), e);
	            ApplicationWindowManager.getInstance().displayErrorMessage("Error esborrant el timer: " + e.getMessage(), e);
	    	}
		}
		else if(ACTION_COMMAND_RESET.equals(event.getActionCommand())){
			setTimerMode(false);
			elapsedTime = 0;
			displayElapsedTime();
		}
		else if(ACTION_COMMAND_PUBLISH.equals(event.getActionCommand())){
			setTimerMode(false);
			controller.publishIssueWork(issue, new Date(), elapsedTime);
			workService.updateTimerRegistry(registry.getId(), 0);
			elapsedTime = 0;
			displayElapsedTime();
		}
		else if(ACTION_COMMAND_TIMER_ACTION.equals(event.getActionCommand())){
			elapsedTime++;
			displayElapsedTime();
			workService.updateTimerRegistry(registry.getId(), elapsedTime);
		}
	}
	
	private void setTimerMode(boolean running)
	{
		if(!running){
			bPlay.setIcon(iconStart);
			if(timer!=null && timer.isRunning()){
				timer.stop();
			}
		}
		else if(running){
			bPlay.setIcon(iconPause);
			if(timer!=null && !timer.isRunning()){
				timer.start();
			}
		}
	}
	
	private void displayElapsedTime()
	{
		Integer elapsedHours = elapsedTime / 60;
		Integer elapsedMinutes = elapsedTime % 60;
		String message = MessageFormat.format(ISSUE_WORK_TIME_MESSAGE_SIMPLE, new Object[]{elapsedHours, elapsedMinutes});
		lElapsedTime.setText(message);
		lElapsedTime.setToolTipText(message);
	}
}
