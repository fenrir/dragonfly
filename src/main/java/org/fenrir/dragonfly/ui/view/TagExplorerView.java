package org.fenrir.dragonfly.ui.view;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.inject.Inject;
import java.util.List;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.ui.widget.TagListCellRenderer;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140502
 */
@SuppressWarnings("serial")
public class TagExplorerView extends AbstractView<ApplicationController>
{
	public static final String ID = "org.fenrir.dragonfly.ui.view.tagExplorerView";
	
    private JList listTags;
    private boolean fireChangeEvent = true;

    @Inject
    public void setApplicationController(ApplicationController applicationController)
    {
        setController(applicationController);
    }
    
    @Override
    public String getId()
    {
    	return ID;
    }
    
    @Override
    protected final JComponent createViewContents()
    {
        JScrollPane scrollTags = new JScrollPane();
        scrollTags.getVerticalScrollBar().setUnitIncrement(10);		
        listTags = new JList(new DefaultListModel());
        listTags.setCellRenderer(new TagListCellRenderer());
        listTags.addListSelectionListener(new ListSelectionListener() 
        {			
            @Override
            public void valueChanged(ListSelectionEvent event) 
            {
                if(fireChangeEvent){
                	ITagDTO selectedTag = (ITagDTO)listTags.getSelectedValue();
                    controller.setSelectedTag(selectedTag);
                }
            }
        });
        scrollTags.setViewportView(listTags);
		
        return scrollTags;
    }
    
    public void setTagList(List<ITagDTO> tags)
    {
        fireChangeEvent = false;
        // S'esborren tots els elements de la llista
        ((DefaultListModel)listTags.getModel()).removeAllElements();
        for(ITagDTO tag:tags){
            ((DefaultListModel)listTags.getModel()).addElement(tag);
        }
        fireChangeEvent = true;
    }		
	
    public void clearSelection()
    {
        fireChangeEvent = false;
        listTags.clearSelection();
        fireChangeEvent = true;
    }
}
