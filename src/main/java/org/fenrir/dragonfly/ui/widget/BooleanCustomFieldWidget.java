package org.fenrir.dragonfly.ui.widget;

import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140116
 */
public class BooleanCustomFieldWidget implements ICustomFieldWidget<Boolean>
{
	private ICustomFieldDTO customField;
	private JCheckBox checkValue;
	
	public BooleanCustomFieldWidget(ICustomFieldDTO customField)
	{
		if(!FieldType.TYPE_BOOLEAN.equals(customField.getFieldType().getType())){
			throw new IllegalArgumentException("El camp custom indicat no està suportat per aquest widget [" + customField.getName() + "]");
		}
		this.customField = customField;
	}
	
	@Override
	public JLabel createLabel()
	{
		return null;
	}
	
	@Override
	public JComponent createComponent()
	{
		checkValue = new JCheckBox(customField.getName());
		return checkValue;
	}

	@Override
	public ICustomFieldDTO getCustomField()
	{
		return customField;
	}
	
	@Override
	public Boolean getValue()
	{
		return checkValue.isSelected();
	}
	
	@Override
	public String getValueAsString()
	{
		return Boolean.toString(checkValue.isSelected());
	}
	
	@Override
	public void setValue(Boolean value)
	{
		checkValue.setSelected(value);
	}
	
	@Override
	public void validate()
	{
		// Res a validar per un camp de tipus booleà...
	}
	
	@Override
	public void setEditable(boolean editable)
	{
		checkValue.setEnabled(editable);
	}
}
