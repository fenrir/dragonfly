package org.fenrir.dragonfly.ui.widget;

import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.dto.IFieldType;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140503
 */
public class CustomFieldWidgetFactory 
{
	private static CustomFieldWidgetFactory instance;
	
	private CustomFieldWidgetFactory()
	{
		
	}
	
	public static CustomFieldWidgetFactory getInstance()
	{
		if(instance==null){
			instance = new CustomFieldWidgetFactory();
		}
		
		return instance;
	}
	
	public ICustomFieldWidget<?> createWidget(ICustomFieldDTO field)
	{
		if(IFieldType.TYPE_NUMERIC.equals(field.getFieldType().getType())){
			return new NumericCustomFieldWidget(field);
		}
		else if(IFieldType.TYPE_BOOLEAN.equals(field.getFieldType().getType())){
			return new BooleanCustomFieldWidget(field);
		}
		else if(IFieldType.TYPE_STRING.equals(field.getFieldType().getType())){
			return new StringCustomFieldWidget(field);
		}
		else if(IFieldType.TYPE_DATE.equals(field.getFieldType().getType())){
			return new DateCustomFieldWidget(field, DateCustomFieldWidget.DATE_FORMAT);
		}
		else if(IFieldType.TYPE_TIMESTAMP.equals(field.getFieldType().getType())){
			return new DateCustomFieldWidget(field, DateCustomFieldWidget.TIMESTAMP_FORMAT);
		}
		else if(IFieldType.TYPE_DICTIONARY.equals(field.getFieldType().getType())){
			return new DictionaryCustomFieldWidget(field);
		}
		
		// No s'hauria de donar mai...
		return null;
	}
}
