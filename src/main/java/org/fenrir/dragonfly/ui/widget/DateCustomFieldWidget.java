package org.fenrir.dragonfly.ui.widget;

import java.awt.Font;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.dragonfly.core.exception.ValidationException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140503
 */
public class DateCustomFieldWidget implements ICustomFieldWidget<Date>
{
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	public static final String TIMESTAMP_FORMAT = "dd/MM/yyyy HH:mm:ss";

	private final Logger log = LoggerFactory.getLogger(DateCustomFieldWidget.class);
	
	private String format;
	private ICustomFieldDTO customField;
	private JTextField inputValue;
	
	public DateCustomFieldWidget(ICustomFieldDTO customField, String format)
	{
		if(!FieldType.TYPE_DATE.equals(customField.getFieldType().getType())
				&& !FieldType.TYPE_TIMESTAMP.equals(customField.getFieldType().getType())){
			throw new IllegalArgumentException("El camp custom indicat no està suportat per aquest widget [" + customField.getName() + "]");
		}
		if(FieldType.TYPE_DATE.equals(customField.getFieldType().getType()) && !DATE_FORMAT.equals(format)
				|| FieldType.TYPE_TIMESTAMP.equals(customField.getFieldType().getType()) && !TIMESTAMP_FORMAT.equals(format)){
			throw new IllegalArgumentException("El format indicat no està suportat per aquest camp [" + customField.getName() + "]");
		}
		this.customField = customField;
		this.format = format;
	}
	
	@Override
	public JLabel createLabel()
	{
		JLabel label = new JLabel(customField.getName());
		label.setFont(new Font("Sans-Serif", Font.BOLD, 12));
		return label;
	}
	
	@Override
	public JComponent createComponent()
	{
		inputValue = new JTextField();
		return inputValue;
	}
	
	@Override
	public ICustomFieldDTO getCustomField()
	{
		return customField;
	}
	
	@Override
	public Date getValue()
	{
		if(StringUtils.isBlank(inputValue.getText())){
			return null;
		}
		
		try{
			String strDate = inputValue.getText();
			return new SimpleDateFormat(format).parse(strDate);
		}
		catch(ParseException e){
			log.error("Error recuperant el valor del camp {}: {}", new Object[]{customField.getName(), e.getMessage(), e});
			throw new RuntimeException("Error recueperant el valor del camp " + customField.getName());
		}
	}
	
	@Override
	public String getValueAsString()
	{
		if(StringUtils.isBlank(inputValue.getText())){
			return null;
		}
		
		return inputValue.getText();
	}
	
	@Override
	public void setValue(Date value)
	{
		String strDate = new SimpleDateFormat(format).format(value);
		inputValue.setText(strDate);
	}
	
	@Override
	public void validate() throws ValidationException
	{
		String strDate = inputValue.getText();
		if(customField.isMandatory() && StringUtils.isBlank(strDate)){
			throw new ValidationException("El camp '" + customField.getName() + "' és obligatori");
		}
		if(StringUtils.isNotBlank(strDate)){
			try{
				new SimpleDateFormat(format).parse(strDate);
			}
			catch(ParseException e){
				throw new ValidationException("El format de la data del camp '" + customField.getName() + "' no és vàlid (" + format + ")");
			}
		}
	}
	
	@Override
	public void setEditable(boolean editable)
	{
		inputValue.setEditable(editable);
	}	
}
