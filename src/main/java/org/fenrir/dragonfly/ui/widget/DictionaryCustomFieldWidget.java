package org.fenrir.dragonfly.ui.widget;

import java.awt.Font;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JLabel;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;
import org.fenrir.vespine.core.dto.IVespineCustomFieldDTO;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.externalize.widget.EditableComboBox;
import org.fenrir.dragonfly.core.exception.ValidationException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140625
 */
public class DictionaryCustomFieldWidget implements ICustomFieldWidget<IDictionaryItemDTO> 
{
	private final Logger log = LoggerFactory.getLogger(DictionaryCustomFieldWidget.class);
	
	private IVespineCustomFieldDTO customField;
	private EditableComboBox comboValues;
	
	public DictionaryCustomFieldWidget(ICustomFieldDTO customField)
	{
		if(!FieldType.TYPE_DICTIONARY.equals(customField.getFieldType().getType())){
			throw new IllegalArgumentException("El camp custom indicat no està suportat per aquest widget [" + customField.getName() + "]");
		}
		this.customField = (IVespineCustomFieldDTO)customField;
	}
	
	@Override
	public JLabel createLabel() 
	{
		JLabel label = new JLabel(customField.getName());
		label.setFont(new Font("Sans-Serif", Font.BOLD, 12));
		return label;
	}

	@Override
	public JComponent createComponent() 
	{
        comboValues = new EditableComboBox();
        // Es carreguen les dades
        try{
	        IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
	        List<IDictionaryItemDTO> items = administrationService.findDictionaryItems(customField.getDataProviderId());
	        DefaultComboBoxModel comboModel = (DefaultComboBoxModel)comboValues.getModel();
	        // El primer item serà buit
	        comboModel.addElement(null);
	        for(IDictionaryItemDTO item:items){
	        	comboModel.addElement(item);
	        }
        }
        catch(BusinessException e){
        	log.error("Error carregant les dades del camp: {}", e.getMessage(), e);
        }
        
        return comboValues;
	}

	@Override
	public ICustomFieldDTO getCustomField() 
	{
		return customField;
	}

	@Override
	public IDictionaryItemDTO getValue() 
	{
		return (IDictionaryItemDTO)comboValues.getSelectedItem();
	}
	
	@Override
	public String getValueAsString() 
	{
		IDictionaryItemDTO selectedItem = getValue();
		if(selectedItem!=null){
			return selectedItem.getItemId();
		}
		return null;
	}

	@Override
	public void setValue(IDictionaryItemDTO value) 
	{
		comboValues.setSelectedItem(value);
	}

	@Override
	public void validate() throws ValidationException 
	{
		if(customField.isMandatory() && comboValues.getSelectedItem()!=null){
			throw new ValidationException("El camp '" + customField.getName() + "' és obligatori");
		}
	}

	@Override
	public void setEditable(boolean editable) 
	{
		comboValues.setEditable(editable);
	}
}
