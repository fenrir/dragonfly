package org.fenrir.dragonfly.ui.widget;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;
import org.fenrir.vespine.core.dto.IDictionaryItemDTO;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140420
 */
public class DictionaryItemTableModel extends AbstractTableModel
{
	private static final long serialVersionUID = -2004494248977523720L;

	private static final String[] COLUMN_NAMES = new String[]{"Valor", "Descripció"};

	private static final int COLUMN_INDEX_VALUE = 0;
	private static final int COLUMN_INDEX_DESCRIPTION = 1;
	
	private List<IDictionaryItemDTO> modelData = new ArrayList<IDictionaryItemDTO>();
	
	@Override
	public int getRowCount() 
	{
		return modelData.size();
	}

	@Override
	public int getColumnCount() 
	{
		return COLUMN_NAMES.length;
	}
	
	@Override
	public String getColumnName(int column) 
	{
		return COLUMN_NAMES[column];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) 
	{
		IDictionaryItemDTO item = modelData.get(rowIndex);
		if(COLUMN_INDEX_VALUE==columnIndex){
			return item.getValue();
		}
		else if(COLUMN_INDEX_DESCRIPTION==columnIndex){
			return item.getDescription();
		}
		
		throw new IllegalArgumentException("Índex de columna no vàlid " + columnIndex);
	}
	
	public List<IDictionaryItemDTO> getModelData()
    {
        return modelData;
    }
    
    public void addData(IDictionaryItemDTO data)
    {
        this.modelData.add(data);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    
    public void removeData(IDictionaryItemDTO data)
    {
    	int index = this.modelData.indexOf(data);
    	if(this.modelData.remove(data)){
    		fireTableRowsDeleted(index, index);
    	}
    }
}
