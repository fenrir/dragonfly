package org.fenrir.dragonfly.ui.widget;

import javax.swing.JComponent;
import javax.swing.JLabel;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.dragonfly.core.exception.ValidationException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140405
 */
public interface ICustomFieldWidget<T> 
{
	public JLabel createLabel();
	public JComponent createComponent();
	public ICustomFieldDTO getCustomField();
	public T getValue();
	public String getValueAsString();
	public void setValue(T value);
	public void validate() throws ValidationException;
	public void setEditable(boolean editable);	
}
