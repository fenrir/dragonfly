package org.fenrir.dragonfly.ui.widget;

import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MouseInputAdapter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.controller.ApplicationController;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140503
 */
public class InputTag extends JPanel
{
	private final Logger log = LoggerFactory.getLogger(InputTag.class);
	
    private String issueId;
    private ITagDTO tag;
    private boolean emptyTag;
    private JTextField inputDescription;
    private JDialog popup;
    private JList listTags;
    private boolean fireChangeEvent = true;

    public InputTag(String issueId)
    {
        this.issueId = issueId;
        emptyTag = true;
        createContents();
    }
    
    public InputTag(String issueId, ITagDTO tag)
    {
        this.issueId = issueId;
        this.tag = tag;
        emptyTag = false;
        createContents();
    }
    
    private void createContents()
    {
        setOpaque(false);
        
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(Box.createVerticalStrut(20));
        JLabel lIcon;        
        JComponent tagDescriptionComponent;
        if(emptyTag){
            lIcon = new JLabel(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/tag_new_16.png")));
            inputDescription = new JTextField("Crear un nou tag");
            inputDescription.setToolTipText("Doble click per crear un nou tag");
            inputDescription.addMouseListener(new MouseInputAdapter() 
            {
                @Override
                public void mouseClicked(MouseEvent event)
                {                
                    // Doble click habilita l'edició del camp
                    if(event.getClickCount()>1){
                        setDescriptionFieldEditable(true);
                        inputDescription.setText(null);
                    }
                }
            });        
            inputDescription.addKeyListener(new KeyAdapter() 
            {      
                @Override
                public void keyReleased(KeyEvent event) 
                {
                    if(KeyEvent.VK_ENTER==event.getKeyCode() && inputDescription.isEditable()){
                        popup.setVisible(false);
                        setDescriptionFieldEditable(false);
                        if(StringUtils.isNotEmpty(inputDescription.getText())){
                            ApplicationController controller = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);
                            controller.tagIssueByDescription(issueId, inputDescription.getText());
                        }
                        else{
                            inputDescription.setText("Crear un nou tag");
                        }
                    }
                    else if(KeyEvent.VK_ESCAPE==event.getKeyCode() && inputDescription.isEditable()){
                        popup.setVisible(false);
                        setDescriptionFieldEditable(false);
                        inputDescription.setText("Crear un nou tag");
                    }
                    else if(inputDescription.isEditable() && !event.isActionKey()){
                    	try{
	                        // Càrrega inicial de tots els tags disponibles        
	                        loadTagList(inputDescription.getText());
	                        // Només es mostra si hi ha tags creats
	                        if(!popup.isVisible() && listTags.getModel().getSize()>0){
	                            Point location = new Point(InputTag.this.getLocation());
	                            SwingUtilities.convertPointToScreen(location, InputTag.this);
	                            location.translate(0, 20);
	                            popup.setLocation(location);
	                            popup.setVisible(true);
	                            // Es retorn el focus al camp input per poder-hi escriure                            
	                            inputDescription.requestFocusInWindow();
	                        }
	                        else if(popup.isVisible() && listTags.getModel().getSize()==0){
	                            popup.setVisible(false);
	                        }                        
                    	}
                    	catch(BusinessException e){
                        	log.error("Error carregant les etiquetes existents: {}", e.getMessage(), e);
                            ApplicationWindowManager.getInstance().displayErrorMessage("Error carregant les etiquetes existents: " + e.getMessage(), e);
                        }
                    }
                }
            });
            setDescriptionFieldEditable(false);
            
            tagDescriptionComponent = inputDescription;
        }
        else{
            lIcon = new JLabel(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/tag_16.png")));
            tagDescriptionComponent = new JLabel(tag.getName());            
        }        
        add(lIcon);
        add(Box.createHorizontalStrut(5));                
        add(tagDescriptionComponent);
        // Botó per esborrar
        if(!emptyTag){
            JButton bDelete = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/delete_16.png")));
            bDelete.setFocusPainted(false);
            bDelete.setMargin(new Insets(0, 0, 0, 0));
            bDelete.setContentAreaFilled(false);
            bDelete.setBorderPainted(false);
            bDelete.setOpaque(false);
            bDelete.addActionListener(new ActionListener() 
            {
                @Override
                public void actionPerformed(ActionEvent event) 
                {
                   ApplicationController controller = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);
                   controller.dettachTag(issueId, tag.getTagId());
                }
            });
            add(bDelete);
        }
        
        // Es crea el popup que contindrà els tags
        popup = new JDialog(ApplicationWindowManager.getInstance().getMainWindow(), false);
        popup.setUndecorated(true);
        popup.setAlwaysOnTop(true);
        // Per tal de no perdre el focus del camp de text en mostrar el popup
        popup.setFocusableWindowState(false);
        popup.setVisible(false);         
        popup.setLayout(new BoxLayout(popup.getContentPane(), BoxLayout.Y_AXIS));        
        JScrollPane scrollTags = new JScrollPane();
        scrollTags.getVerticalScrollBar().setUnitIncrement(10);		
        listTags = new JList(new DefaultListModel());
        listTags.setMaximumSize(new Dimension(Short.MAX_VALUE, listTags.getPreferredSize().height));
        listTags.addListSelectionListener(new ListSelectionListener() 
        {			
            @Override
            public void valueChanged(ListSelectionEvent event) 
            {
                if(fireChangeEvent){
                    ITagDTO selectedTag = (ITagDTO)listTags.getSelectedValue();
                    inputDescription.setText(selectedTag.getName());
                    popup.setVisible(false);
                }
            }
        });
        scrollTags.setViewportView(listTags);                
        popup.add(scrollTags);
        popup.setSize(200, 300);
    }
    
    private void setDescriptionFieldEditable(boolean editable)
    {
        if(editable){
            // Se li dóna l'aparença d'input
            inputDescription.setBorder(UIManager.getBorder("TextField.border"));            
            inputDescription.setEditable(true);
            inputDescription.setOpaque(true);
            inputDescription.getCaret().setVisible(true);
        }
        else{
            // Se li dóna l'aparença de Label
            inputDescription.setBorder(UIManager.getBorder("Label.border"));
            inputDescription.setEditable(false);
            inputDescription.setOpaque(false);
            inputDescription.getCaret().setVisible(false);
        }
    }    
    
    private void loadTagList(String input) throws BusinessException
    {
        fireChangeEvent = false;
        
        ApplicationContext context = ApplicationContext.getInstance();
        IAdministrationFacade administrationService = (IAdministrationFacade)context.getRegisteredComponent(IAdministrationFacade.class);

        List<ITagDTO> tags;
        if(StringUtils.isEmpty(input)){
            tags = administrationService.findAllTags();
        }
        else{
            tags = administrationService.findTagsFiltered(input);
        }
        // S'esborren tots els elements de la llista
        ((DefaultListModel)listTags.getModel()).removeAllElements();
        for(ITagDTO elem:tags){
            ((DefaultListModel)listTags.getModel()).addElement(elem);
        }
        
        fireChangeEvent = true;
    }
}
