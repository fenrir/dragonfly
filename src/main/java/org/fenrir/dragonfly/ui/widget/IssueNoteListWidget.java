package org.fenrir.dragonfly.ui.widget;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.apache.commons.lang.StringUtils;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.spi.dto.IIssueNoteDTO;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;
import org.fenrir.dragonfly.ui.controller.ApplicationController;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140823
 */
public class IssueNoteListWidget extends JPanel
{
	private static final long serialVersionUID = 8709615660652751464L;

	private static final String HEADER_FORMAT = "Notes ({0, number, integer})";
	private static final String NOTE_REFERENCE_TEXT_FORMAT = "<html><b>{0}</b> enviat el {1}</html>";
	
	private static final Double HEADER_COLORIZATION_FACTOR = 0.45d;
	private static final Double BODY_ODD_COLORIZATION_FACTOR = 0.15d;
	private static final Double BODY_EVEN_COLORIZATION_FACTOR = 0.25d;
	
	private JLabel lNoteHeader;
	private Box pNotes;
	
	public IssueNoteListWidget()
	{
		createContents();
		lNoteHeader.setText(MessageFormat.format(HEADER_FORMAT, 0));
	}
	
	public void createContents()
	{
		/* Header */
		JPanel pHeader = new JPanel();
		BoxLayout layoutHeader = new BoxLayout(pHeader, BoxLayout.X_AXIS);
		pHeader.setLayout(layoutHeader);
		pHeader.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.BLACK),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
		pHeader.setBackground(Color.BLACK);
		pHeader.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, HEADER_COLORIZATION_FACTOR);
		lNoteHeader = new JLabel();
        lNoteHeader.setFont(new Font("Sans-Serif", Font.BOLD, 12));
        pHeader.add(lNoteHeader);
        pHeader.add(Box.createHorizontalGlue());
        /* Contents */
        pNotes = Box.createVerticalBox();
        
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createSequentialGroup()    
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(pHeader, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)                    
                .addComponent(pNotes, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)                    
            )
        );                            
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addComponent(pHeader, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addComponent(pNotes)
        );
	}	
	
	public void setNotes(List<IIssueNoteDTO> notes)
	{
		lNoteHeader.setText(MessageFormat.format(HEADER_FORMAT, notes.size()));
        pNotes.removeAll();           
        // S'ordenen les notes per data ascendent
        Collections.sort(notes, new Comparator<IIssueNoteDTO>()
        {
            @Override
            public int compare(IIssueNoteDTO o1, IIssueNoteDTO o2) 
            {
                Date sendDate1 = o1.getSendDate();
                Date sendDate2 = o2.getSendDate();
                
                return sendDate1.compareTo(sendDate2);
            }            
        });
        for(int i=0; i<notes.size(); i++){
        	IssueNoteWidget pNote = new IssueNoteWidget(i%2==0);
        	pNote.setIssueNote(notes.get(i));
            pNotes.add(pNote);
        }
        // S'afegeix una nova nota per la seva creació
		IssueNoteWidget pNote = new IssueNoteWidget(notes.size()%2==0);
		pNotes.add(pNote);
        updateUI();
	}
		
	private class IssueNoteWidget extends JPanel implements ActionListener
	{
		private static final long serialVersionUID = -8857529868202780942L;
		
		private static final String ACTION_COMMAND_CANCEL = "CANCEL";
		private static final String ACTION_COMMAND_SAVE = "SAVE";
		private static final String ACTION_COMMAND_DELETE = "DELETE";
		private static final String ACTION_COMMAND_EDIT = "EDIT";
		
		private IIssueNoteDTO note;
		
		private JLabel lAvatar;
		private JLabel lReference;
        private JTextArea inputContents;
        private JButton bCancel;
        private JButton bSave;
        private JButton bEdit;
        private JButton bDelete;
        private Component separator;
        private boolean oddRow;
        
        public IssueNoteWidget(boolean oddRow)
        {
        	this.oddRow = oddRow;
        	IssueNoteWidget.this.createContents();
        }
        
        private void createContents()
        {
        	lAvatar = new JLabel();
            lReference = new JLabel();
            inputContents = new JTextArea();
            inputContents.setRows(3);
            inputContents.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK),                
                    BorderFactory.createEmptyBorder(10, 10, 10, 10)));
            inputContents.setLineWrap(true);
            inputContents.setWrapStyleWord(true);
            // Barra de botons
            Box pButtons = Box.createHorizontalBox();
            // Opció cancel.lar creació / edició
            bCancel = new JButton("Cancel.lar");
            // Inicialment invisible
            bCancel.setVisible(false);
            bCancel.setActionCommand(ACTION_COMMAND_CANCEL);
            bCancel.addActionListener(IssueNoteWidget.this);
            pButtons.add(bCancel);
            // Opció esborrar nota existent
            bDelete = new JButton("Esborrar");
            // Inicialment serà invisible
            bDelete.setVisible(false);
            bDelete.setActionCommand(ACTION_COMMAND_DELETE);
            bDelete.addActionListener(IssueNoteWidget.this);
            pButtons.add(bDelete, 0);
            separator = Box.createHorizontalStrut(5);
            // Inicialment invisible
            separator.setVisible(false);
            pButtons.add(separator, 0);
            // Opció crear / guardar
            bSave = new JButton("Crear");
            bSave.setActionCommand(ACTION_COMMAND_SAVE);
            bSave.addActionListener(IssueNoteWidget.this);
            pButtons.add(bSave, 0);
            // Opció editar nota existent
            bEdit = new JButton("Editar");
            // Inicialment serà invisible
            bEdit.setVisible(false);
            bEdit.setActionCommand(ACTION_COMMAND_EDIT);
            bEdit.addActionListener(IssueNoteWidget.this);
            pButtons.add(bEdit, 0);
            pButtons.add(Box.createHorizontalGlue(), 0);
            
            GroupLayout layout = new GroupLayout(IssueNoteWidget.this);
            IssueNoteWidget.this.setLayout(layout);
            /* Grup horitzontal */
            layout.setHorizontalGroup(layout.createSequentialGroup()    
                .addContainerGap()
                .addComponent(lAvatar, 64, 64, 64)
                .addGap(20)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            		.addComponent(lReference)
            		.addComponent(inputContents, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
            		.addComponent(pButtons, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
        		)
                .addContainerGap()
            );                            
            /* Grup vertical */
            layout.setVerticalGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            		.addComponent(lAvatar)
                    .addGroup(layout.createSequentialGroup()
                		.addComponent(lReference)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(inputContents, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
                		.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                		.addComponent(pButtons, 30, 30, 30)
            		)
                )                
                .addContainerGap()
            );
            
            IssueNoteWidget.this.setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createMatteBorder(0, 1, 1, 1, Color.BLACK),                
                    BorderFactory.createEmptyBorder(10, 10, 10, 10)));
            IssueNoteWidget.this.setBackground(Color.BLACK);
            Double factor = oddRow ? BODY_ODD_COLORIZATION_FACTOR : BODY_EVEN_COLORIZATION_FACTOR;
            IssueNoteWidget.this.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, factor);
            
            // Inicialment ja porta l'imatge de l'avatar
            lAvatar.setIcon(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/avatar_64.png")));
        }
        
        public void setIssueNote(IIssueNoteDTO note)
        {
        	IExtendedUserFacade userService = (IExtendedUserFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedUserFacade.class);
        	boolean editNotePermission = userService.checkRole(SecurityConstants.AUTHORIZATION_ROLE_ADMINISTRATOR) 
        			|| userService.getLoggedUsername().equals(note.getReporter());
        	
        	this.note = note;
        	
        	String strSendDate = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(note.getSendDate());
            lReference.setText(MessageFormat.format(NOTE_REFERENCE_TEXT_FORMAT, note.getReporter(), strSendDate));
            inputContents.setText(note.getText());
            inputContents.setEditable(false);
            
            bCancel.setVisible(false);
            bSave.setVisible(false);
            bSave.setText("Guardar");
            // Només serà editable si se n'és el propietari o per un administrador 
            bDelete.setVisible(editNotePermission);
            bEdit.setVisible(editNotePermission);
            separator.setVisible(true);
            inputContents.setBackground(Color.BLACK);
            Double factor = oddRow ? BODY_ODD_COLORIZATION_FACTOR : BODY_EVEN_COLORIZATION_FACTOR;
    		inputContents.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, factor);
        }

		@Override
		public void actionPerformed(ActionEvent event) 
		{
			ApplicationController controller = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);
			
			if(ACTION_COMMAND_CANCEL.equals(event.getActionCommand())){
				bSave.setVisible(false);
				bCancel.setVisible(false);
				bDelete.setVisible(true);
				bEdit.setVisible(true);
					
				inputContents.setEditable(false);
				inputContents.setBackground(Color.BLACK);
				Double factor = oddRow ? BODY_ODD_COLORIZATION_FACTOR : BODY_EVEN_COLORIZATION_FACTOR;
	    		inputContents.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, factor);
			}
			else if(ACTION_COMMAND_SAVE.equals(event.getActionCommand())){
				String noteContents = inputContents.getText();
				if(StringUtils.isBlank(noteContents)){
					JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
		                    "És obligatori introduir el contingut de la nota",
		                    "Advertència",
		                    JOptionPane.WARNING_MESSAGE);
					
					return;
				}
				// Crear
				if(note==null){
					controller.createIssueNote(noteContents);
				}
				// Actualitzar
				else{
					controller.updateIssueNote(note.getIssue().getIssueId(), note.getNoteId(), noteContents);
				}
				// Es recarreguen les notes
				setNotes(controller.getSelectedIssueNotes());				
			}
			else if(ACTION_COMMAND_DELETE.equals(event.getActionCommand())){
				int option = JOptionPane.showConfirmDialog(ApplicationWindowManager.getInstance().getMainWindow(),
						"Vol esborrar la nota?",
	                    "Advertència",
	                    JOptionPane.YES_NO_OPTION);
	            if(option==JOptionPane.YES_OPTION){
	            	controller.deleteIssueNote(note.getIssue().getIssueId(), note.getNoteId());
	            	// Es recarreguen les notes
	            	setNotes(controller.getSelectedIssueNotes());
	            }
			}
			else if(ACTION_COMMAND_EDIT.equals(event.getActionCommand())){
				// S'habiliten les opcions disponibles durant l'edició
				bDelete.setVisible(false);
				bEdit.setVisible(false);
				bSave.setVisible(true);
				bCancel.setVisible(true);
				// S'habilita el camp
				inputContents.setEditable(true);
				inputContents.setBackground(Color.WHITE);
	    		inputContents.putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, 1.0d);
	    		// Focus
	    		inputContents.requestFocusInWindow();
			}
		}
	}
}
