package org.fenrir.dragonfly.ui.widget;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.spi.dto.ISeverityDTO;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.0.20121111
 */
public class IssueSeverityTableModel extends AbstractTableModel
{
    private List<ModelData> modelData = new ArrayList<ModelData>();
    
    @Override
    public String getColumnName(int col) 
    {
        return null;
    }
	
    @Override
    public int getColumnCount() 
    {		
        return 2;
    }

    @Override
    public int getRowCount() 
    {
        return modelData.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) 
    {
        if(columnIndex==0){
            return modelData.get(rowIndex).getSeverity().getName();
        }
        else{
            return modelData.get(rowIndex).getSlaTerm();
        }
    }
	
    /*
     * Necessari perque la col.lumna slaTerm es pot editar
     */
    @Override
    public void setValueAt(Object value, int row, int col) 
    {
	if(col==1){
            modelData.get(row).setSlaTerm((String)value);
	}
				
        fireTableCellUpdated(row, col);
    }                
	
    @Override
    public boolean isCellEditable(int row, int column) 
    {		
        return column==1;
    }
	
    @Override
    public Class<?> getColumnClass(int column) 
    {		
        return String.class;
    }

    public List<ModelData> getModelData()
    {
        return modelData;
    }
    
    public void addData(ModelData data)
    {
        this.modelData.add(data);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }

    public void clearModelData()
    {
        modelData.clear();
        fireTableDataChanged();
    }

    public boolean containsSeverity(ISeverityDTO severityDTO)
    {
        for(ModelData elem:modelData){
            if(elem.getSeverity().equals(severityDTO)){
                return true;
            }
        }

        return false;
    }
    
    public static class ModelData implements ISeverityDTO
    {
        private ISeverityDTO severity;
        private String slaTerm;
        
        public ModelData(ISeverityDTO severity)
        {
            this.severity = severity;
            this.slaTerm = severity.getSlaTerm();
        }

        public ISeverityDTO getSeverity()
        {
            return severity;
        }

        public void setSeverity(ISeverityDTO severity)
        {
            this.severity = severity;
        }

        @Override
        public String getProvider()
        {
        	return severity.getProvider();
        }
        
        @Override
        public String getSlaTerm()
        {
            return slaTerm;
        }

        public void setSlaTerm(String slaTerm)
        {
            this.slaTerm = slaTerm;
        }

        @Override
        public String getSeverityId() 
        {
            return severity.getSeverityId();
        }

        @Override
        public String getName() 
        {
            return severity.getName();
        }
        
        @Override
        public Date getLastUpdated() 
        {
        	return severity.getLastUpdated();
        }
        
        @Override
        public int hashCode()
        {
            return severity.hashCode();
        }

        @Override
        public boolean equals(Object obj)
        {
            return severity.equals(obj);
        }
    }
}
