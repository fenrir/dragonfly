package org.fenrir.dragonfly.ui.widget;

import java.awt.Color;
import java.text.MessageFormat;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.fenrir.vespine.spi.dto.IStatusDTO;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.0.20121124
 */
public class IssueStatusTableModel extends AbstractTableModel
{
    private List<ModelData> modelData = new ArrayList<ModelData>();
    
    @Override
    public String getColumnName(int col) 
    {
        return null;
    }
	
    @Override
    public int getColumnCount() 
    {		
        return 2;
    }
    
    @Override
    public int getRowCount() 
    {
        return modelData.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) 
    {
        if(columnIndex==0){
            return modelData.get(rowIndex).getName();
        }
        else{
            return modelData.get(rowIndex).getColor();
        }
    }
	
    /*
     * Necessari perque les col.lumnes es puguin editar
     */
    @Override
    public void setValueAt(Object value, int row, int col) 
    {
        if(col==0){
            modelData.get(row).setName((String)value);
        }
        else if(col==1){
            modelData.get(row).setColor((Color)value);
	}
				
        fireTableCellUpdated(row, col);
    }                
	
    @Override
    public boolean isCellEditable(int row, int column) 
    {		
        return true;
    }
	
    @Override
    public Class getColumnClass(int column) 
    {		
        if(column==1){
            return Color.class;
        }
        else{
            return String.class;
        }
    }

    public List<ModelData> getModelData()
    {
        return modelData;
    }    

    public void addData(ModelData data)
    {
        this.modelData.add(data);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }

    public void clearModelData()
    {
        modelData.clear();
        fireTableDataChanged();
    }

    public boolean containsStatus(IStatusDTO statusDTO)
    {
        for(ModelData elem:modelData){
            if(elem.equals(statusDTO)){
                return true;
            }
        }

        return false;
    }

    public static class ModelData implements IStatusDTO
    {
        private IStatusDTO status;
        private String name;
        private Color color;

        public ModelData(IStatusDTO status, Color color)
        {
            this.status = status;
            this.color = color;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public Color getColor()
        {
            return color;
        }

        public void setColor(Color color)
        {
            this.color = color;
        }

        @Override
        public String getProvider()
        {
        	return status.getProvider();
        }
        
        @Override
        public String getStatusId()
        {
            return status.getStatusId();
        }

        @Override
        public String getName()
        {
            if(name!=null){
                return name;
            }
            
            return status.getName();
        }

        @Override
        public Date getLastUpdated()
        {
        	return status.getLastUpdated();
        }
        
        @Override
        public String getColorRGB()
        {
            Object[] msgArguments = new Object[]{color.getRed(), color.getGreen(), color.getBlue()};
            return MessageFormat.format("{0, number, integer},{1, number, integer},{2, number, integer}", msgArguments);            
        }

        @Override
        public int hashCode()
        {
            return status.getStatusId().hashCode();
        }

        @Override
        public boolean equals(Object obj)
        {
            return status.getStatusId().equals(((IStatusDTO)obj).getStatusId());
        }
    }
}
