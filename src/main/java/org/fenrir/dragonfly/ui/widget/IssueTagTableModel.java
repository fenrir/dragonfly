package org.fenrir.dragonfly.ui.widget;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import org.fenrir.vespine.spi.dto.ITagDTO;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140503
 */
public class IssueTagTableModel extends AbstractTableModel
{
    private static final long serialVersionUID = 1L;

    private List<DataElement> data;

    public IssueTagTableModel()
    {
        data = new ArrayList<DataElement>();
    }

    @Override
    public String getColumnName(int col) 
    {
        return null;
    }
	
    @Override
    public int getColumnCount() 
    {		
        return 2;
    }

    @Override
    public int getRowCount() 
    {
        return data.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) 
    {
        if(columnIndex==0){
            return data.get(rowIndex).isChecked();
        }
        else{
            return data.get(rowIndex).getTag().getName();
        }
    }
	
    /**
     * No es necessari implementar aquest mètode a no ser que 
     * les dades de la taula canviin     
     */
    @Override
    public void setValueAt(Object value, int row, int col) 
    {
        if(col==0){
            data.get(row).setChecked((Boolean)value);
        }
				
        fireTableCellUpdated(row, col);
    }
    
    public ITagDTO getTagAt(int row)
    {
    	return (ITagDTO)data.get(row).getTag();
    }
    
    public boolean isTagChecked(int row)
    {
    	return (Boolean)data.get(row).isChecked();
    }
	
    @Override
    public boolean isCellEditable(int row, int column) 
    {		
        return column==0;
    }
		
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Override
    public Class getColumnClass(int column) 
    {		
        if(column==0){
            return Boolean.class;
        }
        else{
            return String.class;
        }
    }

    public void addTag(ITagDTO tag, boolean checked)
    {
        DataElement element = new DataElement(tag, checked);		
        if(!data.contains(element)){
            data.add(new DataElement(tag, checked));
            fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
        }
        // En cas que estigui present s'actualitza l'estat
        else{
            int index = data.indexOf(element);
            data.get(index).setChecked(checked);
            fireTableDataChanged();
        }
    }

    public void addTags(Collection<ITagDTO> tags, boolean checked)
    {		
        for(ITagDTO tag:tags){
            DataElement element = new DataElement(tag, checked);
            if(!data.contains(element)){
                data.add(new DataElement(tag, checked));					
            }
        }
        fireTableDataChanged();
    }		

    public void removeAllUncheckedTags()
    {
        Iterator<DataElement> iterator = data.iterator();
        while(iterator.hasNext()){
            DataElement elem = iterator.next();
            if(!elem.checked){
                iterator.remove();
            }
        }
        fireTableDataChanged();
    }

    public void removeAllTags()
    {
        data.clear();
        fireTableDataChanged();
    }

    public int countUncheckedTags()
    {
        int count = 0;
        Iterator<DataElement> iterator = data.iterator();
        while(iterator.hasNext()){
            DataElement elem = iterator.next();
            if(!elem.checked){
                count++;
            }
        }

        return count;
    }

    private class DataElement
    {
        private ITagDTO tag;
        private boolean checked;

        public DataElement(ITagDTO tag, boolean checked)
        {
            this.tag = tag;
            this.checked = checked;
        }

        public ITagDTO getTag() 
        {
            return tag;
        }

        public void setTag(ITagDTO tag) 
        {
            this.tag = tag;
        }

        public boolean isChecked() 
        {
            return checked;
        }

        public void setChecked(boolean checked) 
        {
            this.checked = checked;
        }

        @Override
        public int hashCode() 
        {
            final int prime = 31;

            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + ((tag == null) ? 0 : tag.hashCode());

            return result;
        }

        @Override
        public boolean equals(Object obj) 
        {
            if(this==obj){
                return true;
            }
            if (obj==null){
                return false;
            }
            if(!(obj instanceof DataElement)){
                return false;
            }

            DataElement other = (DataElement)obj;
            if(!getOuterType().equals(other.getOuterType())){
                return false;
            }
            if(tag==null){
                if(other.tag!=null){
                    return false;
                }
            } 
            else if(!tag.equals(other.tag)){
                return false;
            }

            return true;
        }

        private IssueTagTableModel getOuterType() 
        {
            return IssueTagTableModel.this;
        }
    }
}
