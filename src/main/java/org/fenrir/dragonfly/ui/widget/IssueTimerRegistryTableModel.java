package org.fenrir.dragonfly.ui.widget;

import javax.swing.table.AbstractTableModel;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.fenrir.dragonfly.core.entity.IssueTimerRegistry;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140326
 */
public class IssueTimerRegistryTableModel extends AbstractTableModel
{
private static final long serialVersionUID = 1L;
	
	public static final int COLUMN_SELECTOR = 0;
	public static final int COLUMN_ISSUE = 1;
	public static final int COLUMN_DATE = 2;
	public static final int COLUMN_AMOUNT = 3;
	
	private String[] columnNames = new String[]{"", "Incidència", "Data", "Temps"}; 
	private List<ModelData> data;	
	
	public IssueTimerRegistryTableModel()
	{
		data = new ArrayList<ModelData>();
	}
	
	public IssueTimerRegistryTableModel(List<ModelData> data)
	{
		this.data = data;
	}
	
	@Override
	public String getColumnName(int col) 
	{
        return columnNames[col];
    }
	
	@Override
	public int getColumnCount() 
	{		
		return columnNames.length;
	}

	@Override
	public int getRowCount() 
	{
		return data.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) 
	{
		if(columnIndex==COLUMN_SELECTOR){
			return data.get(rowIndex).isChecked();
		}
		else if(columnIndex==COLUMN_ISSUE){
			return data.get(rowIndex).getIssueId();
		}		
		else if(columnIndex==COLUMN_DATE){
			return data.get(rowIndex).getWorkingDate();
		}
		else if(columnIndex==COLUMN_AMOUNT){
			return data.get(rowIndex).getAmountAsString();
		}

		return null;
	}
	
	/**
	 * Es necessari implementar aquest mètode perquè les dades de la taula poden canviar 
     */
    public void setValueAt(Object value, int row, int col) 
	{
		if(col==COLUMN_SELECTOR){
			data.get(row).setChecked((Boolean)value);
		}
        fireTableCellUpdated(row, col);
    }
    
    @Override
	public boolean isCellEditable(int row, int column) 
	{
    	// El check només serà editable quan el registre no hagi estat processat
    	if(column==COLUMN_SELECTOR){
    		if(data.get(row).isProcessed()){
    			return false;
    		}
    		
    		return true;
    	}

    	return false;
	}
		
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Class getColumnClass(int column) 
	{
		if(column==0){
			return Boolean.class;
		}
		else{
			return String.class;
		}		
	}
	
	public List<ModelData> getData()
	{
		return data;
	}
	
	public void addData(ModelData data)
	{
		this.data.add(data);
		fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
	}
	
	public void clearData()
	{
		data.clear();
		fireTableDataChanged();
	}
	
	public void selectAll()
	{
		for(ModelData elem:data){
			if(!elem.isProcessed()){
				elem.setChecked(true);
			}
		}
		fireTableDataChanged();
	}
	
	public void selectNone()
	{
		for(ModelData elem:data){
			elem.setChecked(false);
		}
		fireTableDataChanged();
	}
	
	public static class ModelData
	{
		private Boolean checked;
		private Boolean processed;
		private IssueTimerRegistry registry;
		
		public ModelData(IssueTimerRegistry registry)
		{
			this.checked = true;
			this.processed = false;
			this.registry = registry;		
		}

		public Long getRegistryId()
		{
			return registry.getId();
		}
		
		public Boolean isChecked() 
		{
			return checked;
		}

		/**
		 * Indica si el registre corresponent de la taula serà publicat o no. 
		 * Especificant null es deshabilita l'edició del camp
		 * @param checked Boolean - 
		 */
		public void setChecked(Boolean checked) 
		{
			this.checked = checked;
		}
		
		public String getIssueId() 
		{
			return registry.getIssueId().toString();
		}
		
		public String getWorkingDate()
		{
			return new SimpleDateFormat("dd/MM/yyyy").format(registry.getWorkingDate());
		}
		
		public Integer getAmount()
		{
			return registry.getElapsedTime();
		}
		
		public String getAmountAsString()
		{
			Integer elapsedHours = registry.getElapsedTime() / 60;
			Integer elapsedMinutes = registry.getElapsedTime() % 60;
			return MessageFormat.format("{0, number, integer}h. {1, number, integer}min.", new Object[]{elapsedHours, elapsedMinutes});
		}
		
		public boolean isProcessed()
		{
			return processed;
		}
		
		public void setProcessed(boolean processed)
		{
			this.checked = !processed;
			this.processed = processed;
		}
		
		@Override
		public int hashCode() 
		{
			final int prime = 31;
			
			int result = 1;
			result = prime * result + ((registry == null) ? 0 : registry.hashCode());
			
			return result;
		}

		@Override
		public boolean equals(Object obj) 
		{
			if(this==obj){
				return true;
			}
			if (obj==null){
				return false;
			}
			if(!(obj instanceof ModelData)){
				return false;
			}
			
			ModelData other = (ModelData)obj;
			if(registry==null){
				if(other.registry!=null){
					return false;
				}
			} 
			else if(!registry.equals(other.registry)){
				return false;
			}
			
			return true;
		}					
	}
}
