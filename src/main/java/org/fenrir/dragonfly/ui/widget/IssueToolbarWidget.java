package org.fenrir.dragonfly.ui.widget;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.pushingpixels.flamingo.api.common.CommandButtonDisplayState;
import org.pushingpixels.flamingo.api.common.JCommandButton;
import org.pushingpixels.flamingo.api.common.JCommandButton.CommandButtonKind;
import org.pushingpixels.flamingo.api.common.JCommandMenuButton;
import org.pushingpixels.flamingo.api.common.RichTooltip;
import org.pushingpixels.flamingo.api.common.popup.JCommandPopupMenu;
import org.pushingpixels.flamingo.api.common.popup.JPopupPanel;
import org.pushingpixels.flamingo.api.common.popup.PopupPanelCallback;
import org.pushingpixels.flamingo.api.common.icon.ImageWrapperResizableIcon;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.core.dto.IIssueWipRegistryDTO;
import org.fenrir.vespine.core.dto.IVespineIssueDTO;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.core.service.IExtendedIssueWorkFacade;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140822
 */
public class IssueToolbarWidget extends JPanel implements ActionListener
{
	private static final long serialVersionUID = -4122826108050621578L;
	
	private static final String ACTION_SYNC_ISSUE = "SYNC_ISSUE";
    private static final String ACTION_SEND_TO_TRASH = "SEND_TO_TRASH";
    private static final String ACTION_RESTORE_FROM_TRASH = "RESTORE_FROM_TRASH";
    private static final String ACTION_DELETE_ISSUE = "DELETE_ISSUE";
    private static final String ACTION_MANAGE_TAGS = "MANAGE_TAGS";
    private static final String ACTION_ADD_TIMER_REGISTRY = "ADD_TIMER_REGISTRY";
    private static final String ACTION_REMOVE_TIMER_REGISTRY = "REMOVE_TIMER_REGISTRY";
    private static final String ACTION_WORK_ON_TIMER_REGISTRY = "WORK_ON_TIMER_REGISTRY";
    private static final String ACTION_CHANGE_ISSUE_STATUS = "CHANGE_ISSUE_STATUS";
    
    private final Logger log = LoggerFactory.getLogger(IssueToolbarWidget.class);
    
    private IIssueDTO issue;

//    private JCommandButton bSync;
    private JCommandButton bTrash;
    private JCommandButton bRestore;
    private JCommandButton bTags;
    private JCommandButton bAddToWorkQueue; 
    private JCommandButton bWorkOnIssue;
    private JCommandButton bRemoveFromWorkQueue;
    private JCommandButton bChangeIssueStatus;
    private Component separator;
    
    public IssueToolbarWidget()
    {
        createContents();
    }
    
    public IssueToolbarWidget(IIssueDTO issue)
    {
        createContents();
        updateToolbar(issue);
    }
    
    public final void updateToolbar(IIssueDTO issue)
    {
        this.issue = issue;
        reloadToolbar();
    }
     
    public final void reloadToolbar()
    {
    	IExtendedIssueWorkFacade workService = (IExtendedIssueWorkFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedIssueWorkFacade.class);
        IExtendedUserFacade userManagementService = (IExtendedUserFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedUserFacade.class);
    	
    	/* Permisos sobre les accions */
        boolean reporterRole = userManagementService.checkRole(SecurityConstants.AUTHORIZATION_ROLE_REPORTER);
    	
        // Esborrar (trash)        
        if(issue instanceof IVespineIssueDTO && !((IVespineIssueDTO)issue).isDeleted()){
            bRestore.setVisible(false);
            separator.setVisible(false);
            
            bTrash.setIcon(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/trash_in_22.png"), new Dimension(22, 22)));
            bTrash.setActionRichTooltip(new RichTooltip("Esborrar incidencia", "Enviar l'incidencia a la paperera"));
            bTrash.getActionModel().setActionCommand(ACTION_SEND_TO_TRASH);                        
        }
        // Esborrar i restaurar
        else{
            bRestore.setVisible(true);
            separator.setVisible(true);
            
            bTrash.setIcon(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/yggdrasil/ui/icons/remove_22.png"), new Dimension(22, 22)));
            bTrash.setActionRichTooltip(new RichTooltip("Esborrar incidencia", "Enviar l'incidencia definitivament"));
            bTrash.getActionModel().setActionCommand(ACTION_DELETE_ISSUE);            
        }
        
        // S'habiliten tots els botons deshabilitats en cas de no haber-se indicat una incidencia
//        bSync.setEnabled(true);
        bRestore.setEnabled(true);
        bTrash.setEnabled(true);
        bTags.setEnabled(true);
        bChangeIssueStatus.setEnabled(true);
        
        IIssueWipRegistryDTO wipRegistry = null;
        try{
	        wipRegistry = workService.findIssueWorkInProgressRegistry(issue.getIssueId(), userManagementService.getLoggedUsername());
        }
        catch(BusinessException e){
    		log.warn("Error recuperant registres de treball en procés: {}", e.getMessage());
    	}
        bAddToWorkQueue.setVisible(wipRegistry==null);
        // Pels casos que es passa de la creació a la visualització
        bAddToWorkQueue.setEnabled(true);
        bWorkOnIssue.setVisible(wipRegistry!=null);
        bRemoveFromWorkQueue.setVisible(wipRegistry!=null);
        
        // Permisos
        bRestore.setVisible(bRestore.isVisible() && reporterRole);
        separator.setVisible(separator.isVisible() && reporterRole);
        bTrash.setVisible(reporterRole);
        bTags.setVisible(reporterRole);
        bChangeIssueStatus.setVisible(reporterRole);
    }
    
    private void createContents()
    {
        // Panell d'opcions. No es pot fer servir un Box perquè el L&F Substance el no coloritza correctament
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        putClientProperty(SubstanceLookAndFeel.COLORIZATION_FACTOR, new Double(0.25d));
        /* Opcions associades */
        // Sincronitzar 
//        bSync = new JCommandButton(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/synchronize_22.png"), new Dimension(22, 22)));
//        bSync.setCommandButtonKind(CommandButtonKind.ACTION_AND_POPUP_MAIN_ACTION);
//        bSync.setDisplayState(CommandButtonDisplayState.TILE);
//        bSync.setActionRichTooltip(new RichTooltip("Sincronitzar incidencia", "Sincronitza amb el sistema XXXX"));
//        bSync.setFlat(false);
//        bSync.setPopupCallback(new PopupPanelCallback()
//        {
//            @Override
//            public JPopupPanel getPopupPanel(JCommandButton commandButton) 
//            {
//        		JCommandPopupMenu bUpdatePopup = new JCommandPopupMenu();
//        		bUpdatePopup.addMenuButton(new JCommandMenuButton("Sincronitzar amb YYY", ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/synchronize_22.png"), new Dimension(30, 30))));
//        		bUpdatePopup.addMenuButton(new JCommandMenuButton("Sincronitzar amb ZZZ", ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/synchronize_22.png"), new Dimension(30, 30))));
//        		
//        		return bUpdatePopup;
//            }
//        });
//        bSync.addActionListener(this);
//        bSync.getActionModel().setActionCommand(ACTION_SYNC_ISSUE);
//        // Deshabilitat fins que no s'indiqui una incidencia
//        bSync.setEnabled(false);
//        add(bSync);
        add(Box.createHorizontalStrut(5));
        /* Restaurar */
        bRestore = new JCommandButton(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/trash_out_22.png"), new Dimension(22, 22)));
        bRestore.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
        bRestore.setDisplayState(CommandButtonDisplayState.TILE);
        bRestore.setActionRichTooltip(new RichTooltip("Restaurar incidencia", "Restaurar l'incidencia desde la paperera"));
        bRestore.setFlat(false);
        bRestore.getActionModel().setActionCommand(ACTION_RESTORE_FROM_TRASH);
        bRestore.addActionListener(this);
        // Deshabilitat i amagat fins que no s'indiqui una incidencia
        bRestore.setEnabled(false);
        bRestore.setVisible(false);
        add(bRestore);
        separator = Box.createHorizontalStrut(5);
        separator.setVisible(false);
        add(separator);
        /* Esborrar (trash) */
        bTrash = new JCommandButton(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/trash_in_22.png"), new Dimension(22, 22)));
        bTrash.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
        bTrash.setDisplayState(CommandButtonDisplayState.TILE);
        bTrash.setActionRichTooltip(new RichTooltip("Esborrar incidencia", "Enviar l'incidencia a la paperera"));
        bTrash.setFlat(false);
        bTrash.getActionModel().setActionCommand(ACTION_SEND_TO_TRASH);
        bTrash.addActionListener(this);
        // Deshabilitat fins que no s'indiqui una incidencia
        bTrash.setEnabled(false);
        add(bTrash);                 
        add(Box.createHorizontalStrut(5));
        /* Tags */
        bTags = new JCommandButton(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/tag_22.png"), new Dimension(22, 22)));
        bTags.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
        bTags.setDisplayState(CommandButtonDisplayState.TILE);
        bTags.setActionRichTooltip(new RichTooltip("Gestionar etiquetes", "Gestionar les etiquetes de la incidencia"));
        bTags.setFlat(false);
        bTags.getActionModel().setActionCommand(ACTION_MANAGE_TAGS);
        bTags.addActionListener(this);
        // Deshabilitat fins que no s'indiqui una incidencia
        bTags.setEnabled(false);
        add(bTags);        
        add(Box.createHorizontalStrut(5));
        /* Afegir a la llista d'incidències sel.leccionades per treballar */
        bAddToWorkQueue = new JCommandButton(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/work_registry_new_22.png"), new Dimension(22, 22)));
        bAddToWorkQueue.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
        bAddToWorkQueue.setDisplayState(CommandButtonDisplayState.TILE);
        bAddToWorkQueue.setActionRichTooltip(new RichTooltip("Afegir a la llista de treball", "Afegir l'incidència a la llista de planificades per treballar"));
        bAddToWorkQueue.setFlat(false);
        bAddToWorkQueue.getActionModel().setActionCommand(ACTION_ADD_TIMER_REGISTRY);
        bAddToWorkQueue.addActionListener(this);
        // Deshabilitat fins que no s'indiqui una incidencia
        bAddToWorkQueue.setEnabled(false);
        add(bAddToWorkQueue);
        /* Treballar a l'incidència */
        bWorkOnIssue = new JCommandButton(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/work_registry_working_22.png"), new Dimension(22, 22)));
        bWorkOnIssue.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
        bWorkOnIssue.setDisplayState(CommandButtonDisplayState.TILE);
        bWorkOnIssue.setActionRichTooltip(new RichTooltip("Registrar temps de treball", "Afegir l'incidència al temporitzador de treball fet"));
        bWorkOnIssue.setFlat(false);
        bWorkOnIssue.getActionModel().setActionCommand(ACTION_WORK_ON_TIMER_REGISTRY);
        bWorkOnIssue.addActionListener(this);
        // Invisible fins que no s'indiqui una incidencia en la que s'estigui treballant
        bWorkOnIssue.setVisible(false);
        add(bWorkOnIssue);
        add(Box.createHorizontalStrut(5));
        /* Treure de la llista d'incidències sel.leccionades per treballar */
        bRemoveFromWorkQueue = new JCommandButton(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/work_registry_remove_22.png"), new Dimension(22, 22)));
        bRemoveFromWorkQueue.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
        bRemoveFromWorkQueue.setDisplayState(CommandButtonDisplayState.TILE);
        bRemoveFromWorkQueue.setActionRichTooltip(new RichTooltip("Treure de la llista de treball", "Eliminar l'incidència de la llista de planificades per treballar"));
        bRemoveFromWorkQueue.setFlat(false);
        bRemoveFromWorkQueue.getActionModel().setActionCommand(ACTION_REMOVE_TIMER_REGISTRY);
        bRemoveFromWorkQueue.addActionListener(this);
        // Invisible fins que no s'indiqui una incidencia en la que s'estigui treballant
        bRemoveFromWorkQueue.setVisible(false);
        add(bRemoveFromWorkQueue);
        add(Box.createHorizontalStrut(5));
        /* Canvi d'estat i assignació d'usuari */
        bChangeIssueStatus = new JCommandButton(ImageWrapperResizableIcon.getIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/edit_status_22.png"), new Dimension(22, 22)));
        bChangeIssueStatus.setCommandButtonKind(CommandButtonKind.ACTION_ONLY);
        bChangeIssueStatus.setDisplayState(CommandButtonDisplayState.TILE);
        bChangeIssueStatus.setActionRichTooltip(new RichTooltip("Canviar estat", "Canviar l'estat i l'usuari assignat"));
        bChangeIssueStatus.setFlat(false);
        bChangeIssueStatus.getActionModel().setActionCommand(ACTION_CHANGE_ISSUE_STATUS);
        bChangeIssueStatus.addActionListener(this);
        // Deshabilitat fins que no s'indiqui una incidencia
        bChangeIssueStatus.setEnabled(false);
        add(bChangeIssueStatus);        
        // Espai addicional de la botonera        
        add(Box.createHorizontalGlue());
    }
    
    @Override
    public void actionPerformed(ActionEvent event)
    {
        ApplicationController controller = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);

        if(ACTION_SYNC_ISSUE.equals(event.getActionCommand())){
        	System.out.println("It works!!!");
//            controller.updateIssue(Long.parseLong(issueDTO.getProviderId()));
        }
        else if(ACTION_SEND_TO_TRASH.equals(event.getActionCommand()) 
                || ACTION_DELETE_ISSUE.equals(event.getActionCommand())){
			String msg = ACTION_SEND_TO_TRASH.equals(event.getActionCommand()) ? "Vol enviar la incidència a la paperera?"
					: "Vol esborrar la incidència?";
            int option = JOptionPane.showConfirmDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                    msg,
                    "Advertència",
                    JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_OPTION){
                controller.deleteIssue(issue.getIssueId());
            }
        }
        else if(ACTION_RESTORE_FROM_TRASH.equals(event.getActionCommand())){
            int option = JOptionPane.showConfirmDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                    "Vol restaurar la incidència?",
                    "Advertència",
                    JOptionPane.YES_NO_OPTION);
            if(option==JOptionPane.YES_OPTION){
                controller.restoreIssue(issue.getIssueId());
            }                        
        }        
        else if(ACTION_MANAGE_TAGS.equals(event.getActionCommand())){
            controller.manageIssueTags(issue.getIssueId());
        }
        else if(ACTION_ADD_TIMER_REGISTRY.equals(event.getActionCommand())){
        	controller.addIssueToWorkQueue(issue);
        }
        else if(ACTION_REMOVE_TIMER_REGISTRY.equals(event.getActionCommand())){
        	controller.removeIssueFromWorkQueue(issue);
        }
        else if(ACTION_WORK_ON_TIMER_REGISTRY.equals(event.getActionCommand())){
        	controller.setWorkingIssue(issue);
        }
        else if(ACTION_CHANGE_ISSUE_STATUS.equals(event.getActionCommand())){
        	controller.changeIssueStatus(issue);
        }
    }
}
