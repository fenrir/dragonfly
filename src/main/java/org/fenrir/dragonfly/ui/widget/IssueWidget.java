package org.fenrir.dragonfly.ui.widget;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Box;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.ImageIcon;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.io.File;
import java.util.Collection;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.SecurityConstants;
import org.fenrir.vespine.core.dto.IAlertTypeDTO;
import org.fenrir.vespine.core.dto.IIssueAlertDTO;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.vespine.spi.dto.ITagDTO;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.core.service.IExtendedUserFacade;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140828
 */
@SuppressWarnings("serial")
public class IssueWidget extends JComponent implements ActionListener, MouseListener
{
    private final String ACTION_TOGGLE_SLA = "TOGGLE_SLA";
//    private final String ACTION_OPEN_BROWSER = "OPEN_BROWSER";    

    private final int OPTIONS_PANEL_HEIGHT = 35;

    private final Logger log = LoggerFactory.getLogger(IssueWidget.class);
    
    private IIssueDTO issue;

    private JPanel pIssue;
    private JTextField inputIssueId;
    private JTextField inputIssueSummary;
    private JTextField inputProject;
    private JTextField inputIssueStatus;
    private JToggleButton toggleSLA;
//    private JButton bOpenBrowser;
    private JTextField inputSlaDate;
    private JTextField inputSendDate;
    private JPanel pTags;
    private JPanel pAlerts;
    private JSeparator separator;
    private IssueToolbarWidget optionsPanel;    
    // Al començament es veurà normalment
    private boolean highlighted = true;
    
    public IssueWidget(IIssueDTO issue)
    {
        this(issue, null);
    }
    
    public IssueWidget(IIssueDTO issue, Color color)
    {
        this.issue = issue;
        createContents(color);
    }
    
    private void createContents(Color backgroundColor)
    {
        inputIssueId = new JTextField();
        inputIssueId.setHorizontalAlignment(JTextField.RIGHT);
        inputIssueId.setBorder(UIManager.getBorder("Label.border"));
        inputIssueId.setEditable(false);
        inputIssueId.setOpaque(false);
        
        inputIssueSummary = new JTextField(issue.getSummary());
        inputIssueSummary.setBorder(UIManager.getBorder("Label.border"));
        inputIssueSummary.setEditable(false);
        inputIssueSummary.setOpaque(false);
                
        String projectMessage = MessageFormat.format("{0} - {1}",
                issue.getProject(),
                issue.getCategory());
        inputProject = new JTextField(projectMessage);
        inputProject.setBorder(UIManager.getBorder("Label.border"));
        inputProject.setEditable(false);
        inputProject.setOpaque(false);
                
        String statusMessage = MessageFormat.format("{0} - {1} [{2}]",
                issue.getSeverity(),
                issue.getStatus(),
                issue.getAssignedUser()!=null ? issue.getAssignedUser() : "Ningú");
        inputIssueStatus = new JTextField(statusMessage);
        inputIssueStatus.setBorder(UIManager.getBorder("Label.border"));
        inputIssueStatus.setEditable(false);
        inputIssueStatus.setOpaque(false);
        
        toggleSLA = new JToggleButton("SLA");
        toggleSLA.setSelected(issue.isSla());
        toggleSLA.setActionCommand(ACTION_TOGGLE_SLA);
        toggleSLA.addActionListener(this);
//        bOpenBrowser = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/browser_16.png")));
//        bOpenBrowser.setActionCommand(ACTION_OPEN_BROWSER);
//        bOpenBrowser.addActionListener(this);
        
        inputSlaDate = new JTextField();
        inputSlaDate.setToolTipText("Doble click per modificar la data");
        // Listener per permetre l'edició
        inputSlaDate.addMouseListener(this);
        inputSlaDate.addFocusListener(new FocusAdapter()
        {
            @Override
            public void focusLost(FocusEvent event)
            {
                if(inputSlaDate.isEnabled())
                {
                    updateSlaDate();
                }
            }
        });
        inputSlaDate.addKeyListener(new KeyAdapter() 
        {
            @Override
            public void keyPressed(KeyEvent event) 
            {
                if(KeyEvent.VK_ENTER==event.getKeyCode() && inputSlaDate.isEnabled()){
                    updateSlaDate();
                }
            }
        });                
        if(issue.isSla()){
            String strSLADate = new SimpleDateFormat("dd/MM/yyyy").format(issue.getSlaDate());
            inputSlaDate.setText(strSLADate);
        }
        setSlaDateFieldEditable(false);
                
        String strSendDate = new SimpleDateFormat("dd/MM/yyyy").format(issue.getSendDate());
        inputSendDate = new JTextField(strSendDate);
        inputSendDate.setBorder(UIManager.getBorder("Label.border"));
        inputSendDate.setEditable(false);
        inputSendDate.setOpaque(false);       
        
        // Tags
        pTags = new JPanel();
        pTags.setOpaque(false);
        // Per defecte un FlowLayout permet disposar els components en varies linies si no té prou espai
        pTags.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));
        pTags.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);        
                
        pAlerts = new JPanel(new FlowLayout(FlowLayout.LEFT));
        // Mateix color del fons
        pAlerts.setOpaque(false);
        separator = new JSeparator();

        // Panell d'opcions. No es pot fer servir un Box perquè el L&F Substance el no coloritza correctament
        optionsPanel = new IssueToolbarWidget(issue);    
        // Marge esquerra
        optionsPanel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 0));
//        optionsPanel.setOpaque(false);
        // Mida de proves
        optionsPanel.setPreferredSize(new Dimension(100, OPTIONS_PANEL_HEIGHT));
        // Incialment estarà amagat
        optionsPanel.setVisible(false);        
        
        updateWidget(issue);
        
        /* Panell dades incidència */
        pIssue = new JPanel();
        // Color de fons si s'indica
        if(backgroundColor!=null){
            pIssue.setBackground(backgroundColor);
        }
        
        // Separador entre el label del projecte i les dates
        Component hGlue = Box.createHorizontalGlue();
        // Layout del panell de dades
        GroupLayout pIssueLayout = new GroupLayout(pIssue);
        pIssue.setLayout(pIssueLayout);
        pIssueLayout.setHorizontalGroup(
            pIssueLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pIssueLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(inputIssueId, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pIssueLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(inputIssueStatus, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputIssueSummary, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(pIssueLayout.createSequentialGroup()
                        .addComponent(inputProject, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(hGlue)
                        .addGap(3, 3, 3)
                        .addComponent(inputSendDate, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(inputSlaDate, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(toggleSLA)
//                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
//                        .addComponent(bOpenBrowser, 23, 23, 23)
                        .addGap(3, 3, 3)
                    )
                    .addComponent(pTags, GroupLayout.PREFERRED_SIZE, inputIssueStatus.getPreferredSize().width, inputIssueStatus.getMaximumSize().width)
                    .addComponent(pAlerts, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addContainerGap()
            )
        );
        pIssueLayout.setVerticalGroup(
            pIssueLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(pIssueLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pIssueLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(inputIssueId, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(inputIssueSummary, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pIssueLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(inputSendDate)
                    .addComponent(toggleSLA)
//                    .addComponent(bOpenBrowser, 23, 23, 23)
                    .addComponent(inputProject)
                    .addComponent(hGlue)
                    .addComponent(inputSlaDate, 22, 22, 22)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(inputIssueStatus)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pTags)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pAlerts)
                .addContainerGap()
            )
        );
        JXLayer layer = new JXLayer(pIssue);          
        AbstractLayerUI layerUI = new AbstractLayerUI() 
        {
            @Override
            public void installUI(JComponent component) 
            {
                super.installUI(component);
                // Fa que tots els event passin al component inferior, incloent els events de tipus Mouse Wheel
                ((JXLayer)component).setLayerEventMask(0);
            }
            
            @Override  
            protected void paintLayer(Graphics2D g2, JXLayer layer) 
            {
                // Es pinta la capa tal qual és
                super.paintLayer(g2, layer);
                // Efectes
//              if(!highlighted){
//                  g2.setColor(new Color(0, 0, 0, 100));
//                  g2.fillRect(0, 0, layer.getWidth(), layer.getHeight());
//              }
            }            
        };
        layer.setUI(layerUI);

        /* Layout general */                
        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(separator)
            .addComponent(layer, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(optionsPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)            
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(layer, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)                
                .addComponent(optionsPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)                
                .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            )
        );
        
        // Permisos
        IExtendedUserFacade userService = (IExtendedUserFacade)ApplicationContext.getInstance().getRegisteredComponent(IExtendedUserFacade.class);
        boolean reporterRole = userService.checkRole(SecurityConstants.AUTHORIZATION_ROLE_REPORTER);
        toggleSLA.setVisible(reporterRole);

        // Es pinten les alertes
        paintAlerts();        
    }

    public void updateWidget(IIssueDTO issue)
    {
        inputIssueId.setText(issue.getVisibleId());
        inputIssueSummary.setText(issue.getSummary());        
        String projectMessage = MessageFormat.format("{0} - {1}",
                issue.getProject(),
                issue.getCategory());
        inputProject.setText(projectMessage);
        String statusMessage = MessageFormat.format("{0} - {1} [{2}]",
                issue.getSeverity(),
                issue.getStatus(),
                issue.getAssignedUser()!=null ? issue.getAssignedUser() : "Ningú");
        inputIssueStatus.setText(statusMessage);        
        if(issue.isSla()){
            String strSLADate = new SimpleDateFormat("dd/MM/yyyy").format(issue.getSlaDate());
            inputSlaDate.setText(strSLADate);
        }
        String strSendDate = new SimpleDateFormat("dd/MM/yyyy").format(issue.getSendDate());
        inputSendDate.setText(strSendDate);
        // Color de fons si s'indica
        if(issue.getStatus()!=null){
            String strColor = issue.getStatus().getColorRGB();
            Color objColor = null;
            if(strColor!=null){
            	String[] rgb = strColor.split(",");
	            objColor = new Color(Integer.parseInt(rgb[0].trim()),
	                   Integer.parseInt(rgb[1].trim()),
	                   Integer.parseInt(rgb[2].trim()));
            }
            else{
            	objColor = UIManager.getColor("Panel.background");
            }
            if(pIssue!=null){
            	pIssue.setBackground(objColor);
            }
        }
        
        pTags.removeAll();
        // Tags associats
        Collection<ITagDTO> tags = issue.getTags();            
        for(ITagDTO tag:tags){                
            pTags.add(new InputTag(issue.getIssueId(), tag));
        }
        pTags.revalidate();
        
        optionsPanel.updateToolbar(issue);
        
        if(pIssue!=null){
            resizeWidget();
        }
    }
    
    public String getIssueId()
    {
        return issue.getIssueId();
    }
    
    public boolean isHighlighted()
    {
        return highlighted;
    }
    
    public void setHighlighted(boolean highlighted)
    {
        this.highlighted = highlighted;
    }
        
    @Override
    public void addMouseListener(MouseListener listener)
    {
        pIssue.addMouseListener(listener);
    }
    
    @Override
    public void removeMouseListener(MouseListener listener)
    {
        pIssue.removeMouseListener(listener);
    }
    
    public boolean isOptionsVisible()
    {
        return optionsPanel.isVisible();
    }
    
    public void setOptionsVisible(boolean visible)
    {        
        optionsPanel.setVisible(visible);
        resizeWidget();
    }        
    
    private void resizeWidget()
    {
        Dimension minimumSize = pIssue.getMinimumSize();
        Dimension preferredSize = pIssue.getPreferredSize();
        Dimension maximumSize = pIssue.getMaximumSize();        
        int extraHeight = 0;
        if(isOptionsVisible()){                        
            // S'iguala el tamany del panell base amb el contingut          
            extraHeight += OPTIONS_PANEL_HEIGHT;                                    
        }        
        extraHeight += separator.getPreferredSize().height;
        
        this.setMinimumSize(new Dimension(minimumSize.width, minimumSize.height + extraHeight));
        this.setPreferredSize(new Dimension(preferredSize.width, preferredSize.height + extraHeight));
        this.setMaximumSize(new Dimension(maximumSize.width, preferredSize.height + extraHeight));
    }
        
    private void paintAlerts()
    {
        pAlerts.removeAll();
        
        final IIssueFacade issueService = (IIssueFacade)ApplicationContext.getInstance().getRegisteredComponent(IIssueFacade.class);
        try{
	        List<IIssueAlertDTO> issueAlerts = issueService.findIssueAlertsByIssueId(issue.getIssueId());
	        for(IIssueAlertDTO alert:issueAlerts){
	            IAlertTypeDTO type = alert.getType();
	            String strIcon = type.getIcon();
	            JLabel lAlert = new JLabel();       
	            String tooltip = MessageFormat.format("<html><b>{0}</b><br/>"
	                    + "Creada el {1,date,dd/MM/yyyy}<br/>"
	                    + "(Click per esborrar)</html>",
	                    new Object[]{type.getName(), alert.getCreationDate()});
	            lAlert.setToolTipText(tooltip);
	            if(new File(strIcon).exists()){
	                lAlert.setIcon(new ImageIcon(strIcon));
	            }
	            else{
	                lAlert.setIcon(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/image_24.png")));
	            }
	            // En fer click sobre l'etiqueta s'esborra l'alerta
	            final String alertId = alert.getAlertId();
	            final String issueId = alert.getIssue().getIssueId();
	            lAlert.addMouseListener(new MouseAdapter() 
	            {
	                @Override
	                public void mouseClicked(MouseEvent event) 
	                {                 
	                	try{
	                		issueService.deleteIssueAlert(alertId, issueId);
	                	}
	                	catch(BusinessException e){
	                		log.error("Error esborrant alerta: {}", e.getMessage(), e);
	            			ApplicationWindowManager.getInstance()
	                        		.displayErrorMessage("Error esborrant alerta: " + e.getMessage(), e);
	                	}
	                    paintAlerts();
	                }
	            });
	            pAlerts.add(lAlert);
	        }
	        
	        pAlerts.repaint();
        }
        catch(BusinessException e){
    		log.error("Error pintant alertes: {}", e.getMessage(), e);
    	}
    }

    private void setSlaDateFieldEditable(boolean editable)
    {
        if(editable){
            // Se li dóna l'aparença d'input
            inputSlaDate.setBorder(UIManager.getBorder("TextField.border"));            
            inputSlaDate.setEditable(true);
            inputSlaDate.setOpaque(true);
        }
        else{
            // Se li dóna l'aparença de Label
            inputSlaDate.setBorder(UIManager.getBorder("Label.border"));
            inputSlaDate.setEditable(false);
            inputSlaDate.setOpaque(false);
        }
    }
    
    private void updateSlaDate()
    {
        setSlaDateFieldEditable(false);
                                        
        String strNewSlaDate = inputSlaDate.getText();
        String strOldSLADate = issue.getSlaDate()!=null ? new SimpleDateFormat("dd/MM/yyyy").format(issue.getSlaDate()) : null;
        try{
	        if(StringUtils.isNotBlank(strNewSlaDate) && !strNewSlaDate.equals(strOldSLADate)){
	            ApplicationController controller = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);
	            issue = controller.updateIssueSLADate(issue.getIssueId(), strNewSlaDate);
	        }
	        // S'actualitza el camp amb el valor. Si l'actualització falla s'haurà de tornar al valor inicial
	        if(issue.isSla()){
	            String strSLADate = new SimpleDateFormat("dd/MM/yyyy").format(issue.getSlaDate());
	            inputSlaDate.setText(strSLADate);
	        }
        }
        catch(BusinessException e){
        	// El controlador ja s'encarrega de tractar l'error. Només es posa el try/catch perquè no intenti fer l'actualització del camp
        }
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
        ApplicationController controller = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);

        if(ACTION_TOGGLE_SLA.equals(event.getActionCommand())){
        	try{
	            issue = controller.updateIssueSLADate(issue.getIssueId(), toggleSLA.isSelected());
	            if(issue.isSla()){
	                String strSLADate = new SimpleDateFormat("dd/MM/yyyy").format(issue.getSlaDate());
	                inputSlaDate.setText(strSLADate);
	            }
	            else{
	                inputSlaDate.setText(null);
	            }
        	}
        	catch(BusinessException e){
            	// El controlador ja s'encarrega de tractar l'error. Només es posa el try/catch perquè no intenti fer l'actualització del camp
            }
        }
//        else if(ACTION_OPEN_BROWSER.equals(event.getActionCommand())){
//            controller.openBrowser(issue.getIssueId());
//        }        
    }

    @Override
    public void mouseClicked(MouseEvent event)
    {
        if(event.getSource()==inputSlaDate){
            // En cas que la incidència no sigui un SLA s'avisa
            if(!issue.isSla()){
                JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(),
                    "Cal especificar la incidència com a SLA abas de poder modificar la data",
                    "Advertència",
                    JOptionPane.WARNING_MESSAGE);
            }
            // En cas contrari s'habilita l'edició
            else{
                // Doble click habilita l'edició del camp
                if(event.getClickCount()==2){
                    setSlaDateFieldEditable(true);
                }
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent event)
    {
        // Res a fer
    }

    @Override
    public void mouseReleased(MouseEvent event)
    {
        // Res a fer
    }

    @Override
    public void mouseEntered(MouseEvent event)
    {
        // Res a fer
    }

    @Override
    public void mouseExited(MouseEvent event)
    {
        // Res a fer
    }
}
