package org.fenrir.dragonfly.ui.widget;

import javax.swing.table.AbstractTableModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.fenrir.vespine.core.dto.IWorkRegistryDTO;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140504
 */
public class IssueWorkRegistryTableModel extends AbstractTableModel
{
	private static final long serialVersionUID = 6810202346351576675L;

	private static final String[] COLUMN_NAMES = new String[]{"Sprint", "Usuari", "Data realització", "Temps (en minuts)", "Descripció"};

	private static final int COLUMN_INDEX_SPRINT = 0;
	private static final int COLUMN_INDEX_USER = 1;
	private static final int COLUMN_INDEX_WORKING_DAY = 2;
	private static final int COLUMN_INDEX_TIME = 3;
	private static final int COLUMN_INDEX_DESCRIPTION = 4;
	
	private List<IWorkRegistryDTO> modelData = new ArrayList<IWorkRegistryDTO>();
	
	@Override
	public int getRowCount() 
	{
		return modelData.size();
	}

	@Override
	public int getColumnCount() 
	{
		return COLUMN_NAMES.length;
	}
	
	@Override
	public String getColumnName(int column) 
	{
		return COLUMN_NAMES[column];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) 
	{
		IWorkRegistryDTO registry = modelData.get(rowIndex);
		if(COLUMN_INDEX_SPRINT==columnIndex){
			if(registry.getSprint()!=null){
				return registry.getSprint().toString();
			}
			else{
				return "";
			}
		}
		else if(COLUMN_INDEX_USER==columnIndex){
			if(registry.getUser()!=null){
				return registry.getUser().toString();
			}
			else{
				return "";
			}
		}
		else if(COLUMN_INDEX_WORKING_DAY==columnIndex){
			return new SimpleDateFormat("dd/MM/yyyy").format(registry.getWorkingDay());
		}
		else if(COLUMN_INDEX_TIME==columnIndex){
			return Integer.toString(registry.getTime());
		}
		else if(COLUMN_INDEX_DESCRIPTION==columnIndex){
			return registry.getDescription();
		}
		
		throw new IllegalArgumentException("Índex de columna no vàlid " + columnIndex);
	}
	
	public List<IWorkRegistryDTO> getModelData()
    {
        return modelData;
    }
    
    public void addData(IWorkRegistryDTO data)
    {
        this.modelData.add(data);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    
    public void removeData(IWorkRegistryDTO data)
    {
    	int index = this.modelData.indexOf(data);
    	if(this.modelData.remove(data)){
    		fireTableRowsDeleted(index, index);
    	}
    }
}
