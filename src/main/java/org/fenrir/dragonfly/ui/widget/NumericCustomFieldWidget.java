package org.fenrir.dragonfly.ui.widget;

import java.awt.Font;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTextField;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.dragonfly.core.exception.ValidationException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140503
 */
public class NumericCustomFieldWidget implements ICustomFieldWidget<Integer>
{
	private final Logger log = LoggerFactory.getLogger(DateCustomFieldWidget.class);
	
	private ICustomFieldDTO customField;
	private JTextField inputValue;
	
	public NumericCustomFieldWidget(ICustomFieldDTO customField)
	{
		if(!FieldType.TYPE_NUMERIC.equals(customField.getFieldType().getType())){
			throw new IllegalArgumentException("El camp custom indicat no està suportat per aquest widget [" + customField.getName() + "]");
		}
		this.customField = customField;
	}
	
	@Override
	public JLabel createLabel()
	{
		JLabel label = new JLabel(customField.getName());
		label.setFont(new Font("Sans-Serif", Font.BOLD, 12));
		return label;
	}
	
	@Override
	public JComponent createComponent()
	{
		inputValue = new JTextField();
		return inputValue;
	}
	
	@Override
	public ICustomFieldDTO getCustomField()
	{
		return customField;
	}
	
	@Override
	public Integer getValue()
	{
		if(StringUtils.isBlank(inputValue.getText())){
			return null;
		}
		
		try{
			return Integer.parseInt(inputValue.getText());
		}
		catch(NumberFormatException e){
			log.error("Error recuperant el valor del camp {}: {}", new Object[]{customField.getName(), e.getMessage(), e});
			throw new RuntimeException("Error recuperant el valor del camp: '" + customField.getName() + "'");
		}
	}
	
	@Override
	public String getValueAsString()
	{
		if(StringUtils.isBlank(inputValue.getText())){
			return null;
		}
		
		try{
			Integer.parseInt(inputValue.getText());
			return inputValue.getText();
		}
		catch(NumberFormatException e){
			log.error("Error recuperant el valor del camp {}: {}", new Object[]{customField.getName(), e.getMessage(), e});
			throw new RuntimeException("Error recuperant el valor del camp: '" + customField.getName() + "'");
		}
	}
	
	@Override
	public void setValue(Integer value)
	{
		inputValue.setText(value.toString());
	}
	
	@Override
	public void validate() throws ValidationException
	{
		String strDate = inputValue.getText();
		if(customField.isMandatory() && StringUtils.isBlank(strDate)){
			throw new ValidationException("El camp '" + customField.getName() + "' és obligatori");
		}
		if(StringUtils.isNotBlank(strDate)){
			try{
				Integer.parseInt(strDate);
			}
			catch(NumberFormatException e){
				throw new ValidationException("El format de la data del camp '" + customField.getName() + "' no és vàlid");
			}
		}
	}
	
	@Override
	public void setEditable(boolean editable)
	{
		inputValue.setEditable(editable);
	}	
}

