package org.fenrir.dragonfly.ui.widget;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.fenrir.vespine.spi.dto.IProviderElementDTO;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140419
 */
public class ProviderDataTableModel<T extends IProviderElementDTO> extends AbstractTableModel
{
	private static final long serialVersionUID = 1L;

	public static final int MODE_CHECK_SELECTION_ENABLED = 1;
	public static final int MODE_CHECK_SELECTION_DISABLED = 2;
	public static final int MODE_DEFAULT = MODE_CHECK_SELECTION_DISABLED;
	
	private static final String[] COLUMN_NAMES = new String[]{"", "Proveïdor", "Id.", "Nom"};
	
	private static final int COLUMN_INDEX_SELECTOR = 0;
	private static final int COLUMN_INDEX_PROVIDER = 1;
	private static final int COLUMN_INDEX_PROVIDER_ID = 2;
	private static final int COLUMN_INDEX_NAME = 3;

	private int mode = 0;
	
    private List<T> modelData = new ArrayList<T>();
    private Set<T> selectedElements = new HashSet<T>();
    
    public ProviderDataTableModel()
    {
    	this(MODE_DEFAULT);
    }
    
    public ProviderDataTableModel(int mode)
    {
    	this.mode = mode;
    }
    
    @Override
    public String getColumnName(int columnIndex) 
    {
    	if(columnIndex>COLUMN_NAMES.length){
    		throw new IllegalArgumentException("Índex de columna no vàlid " + columnIndex);
    	}
    	return COLUMN_NAMES[columnIndex];
    }
	
    @Override
    public int getColumnCount() 
    {		
        return COLUMN_NAMES.length;
    }

    @Override
    public int getRowCount() 
    {
        return modelData.size();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) 
    {
    	// Si estan habilitats els checks es mostra marcat o desmarcat en funció de si es troba sel.leccionat
    	if(mode==MODE_CHECK_SELECTION_ENABLED && columnIndex==COLUMN_INDEX_SELECTOR){
    		return selectedElements.contains(modelData.get(rowIndex));
    	}
    	// En cas de no presentar els checks de sel.lecció el la columna estarà en "blanc"
    	else if(columnIndex==COLUMN_INDEX_SELECTOR){
    		return "";
    	}
    	else if(columnIndex==COLUMN_INDEX_PROVIDER){
    		return modelData.get(rowIndex).getProvider();
    	}
    	else if(columnIndex==COLUMN_INDEX_PROVIDER_ID){
            return modelData.get(rowIndex).getProviderId();
        }
        else if(columnIndex==COLUMN_INDEX_NAME){
            return modelData.get(rowIndex).getName();
        }
    	
    	throw new IllegalArgumentException("Índex de columna no vàlid " + columnIndex);
    }
	
    /**
	 * Es necessari implementar aquest mètode perquè les dades de la taula poden canviar 
     */
    public void setValueAt(Object value, int row, int col) 
	{
    	// Check només editable si el mode de funcionament així ho indica
		if(mode==MODE_CHECK_SELECTION_ENABLED && col==COLUMN_INDEX_SELECTOR){
			// Registre sel.leccionat. S'afegeix a la llista
			if((Boolean)value){
				selectedElements.add(modelData.get(row));
			}
			else{
				selectedElements.remove(modelData.get(row));
			}
		}
				
        fireTableCellUpdated(row, col);
    }
    
    @Override
	public boolean isCellEditable(int row, int column) 
	{
    	// El check serà editable en cas d'haver-se indicat. La resta no
    	return mode==MODE_CHECK_SELECTION_ENABLED && column==COLUMN_INDEX_SELECTOR;
	}
		
    @Override
    public Class<?> getColumnClass(int column) 
    {		
    	// Es mostra el check només en cas de que s'hagi indicat el mode de funcionament adient
    	if(mode==MODE_CHECK_SELECTION_ENABLED && column==COLUMN_INDEX_SELECTOR){
			return Boolean.class;
		}
		else{
			return String.class;
		}
    }

    public List<T> getModelData()
    {
        return modelData;
    }
    
    public void addData(T data)
    {
        this.modelData.add(data);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
    
    public void addData(T data, boolean selected)
    {
    	if(mode!=MODE_CHECK_SELECTION_ENABLED){
    		throw new IllegalStateException("No està permès afegir elements sel.leccionats si el mode de funcionament no és MODE_CHECK_SELECTION_ENABLED");
    	}
        this.modelData.add(data);
        if(selected){
        	selectedElements.add(data);
        }
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }

    public void clearModelData()
    {
        modelData.clear();
        fireTableDataChanged();
    }
    
    public List<T> getSelectedData()
    {
    	if(mode!=MODE_CHECK_SELECTION_ENABLED){
    		throw new IllegalStateException("No està permès recuperar els elements sel.leccionats si el mode de funcionament no és MODE_CHECK_SELECTION_ENABLED");
    	}
    	return new ArrayList<T>(selectedElements);
    }
}
