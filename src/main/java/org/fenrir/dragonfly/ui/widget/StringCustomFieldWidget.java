package org.fenrir.dragonfly.ui.widget;

import java.awt.Font;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.apache.commons.lang.StringUtils;
import org.fenrir.vespine.core.entity.FieldType;
import org.fenrir.vespine.spi.dto.ICustomFieldDTO;
import org.fenrir.dragonfly.ui.externalize.widget.EditableTextField;
import org.fenrir.dragonfly.core.exception.ValidationException;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140503
 */
public class StringCustomFieldWidget implements ICustomFieldWidget<String>
{
	private ICustomFieldDTO customField;
	private EditableTextField inputValue;
	
	public StringCustomFieldWidget(ICustomFieldDTO customField)
	{
		if(!FieldType.TYPE_STRING.equals(customField.getFieldType().getType())){
			throw new IllegalArgumentException("El camp custom indicat no està suportat per aquest widget [" + customField.getName() + "]");
		}
		this.customField = customField;
	}
	
	@Override
	public JLabel createLabel()
	{
		JLabel label = new JLabel(customField.getName());
		label.setFont(new Font("Sans-Serif", Font.BOLD, 12));
		return label;
	}
	
	@Override
	public JComponent createComponent()
	{
		inputValue = new EditableTextField();
		return inputValue;
	}
	
	@Override
	public ICustomFieldDTO getCustomField()
	{
		return customField;
	}
	
	@Override
	public String getValue()
	{
		return inputValue.getText();
	}
	
	@Override
	public String getValueAsString()
	{
		return getValue();
	}
	
	@Override
	public void setValue(String value)
	{
		inputValue.setText(value);
	}
	
	@Override
	public void validate() throws ValidationException
	{
		if(customField.isMandatory() && StringUtils.isBlank(inputValue.getText())){
			throw new ValidationException("El camp '" + customField.getName() + "' és obligatori");
		}
	}
	
	@Override
	public void setEditable(boolean editable)
	{
		inputValue.setEditable(editable);
	}
}