package org.fenrir.dragonfly.ui.widget;

import java.awt.Component;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import org.pushingpixels.substance.api.renderers.SubstanceDefaultListCellRenderer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.0.20121214
 */
public class TagListCellRenderer extends SubstanceDefaultListCellRenderer
{
    private final Logger log = LoggerFactory.getLogger(TagListCellRenderer.class);
    
    public TagListCellRenderer()
    {
        setOpaque(true);
    }
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
    {        
        /* Com que la classe SubstanceDefaultListCellRenderer exten de DefaultListCellRenderer i aquesta de JLabel, 
         * es pot posar directament el text i la icona 
         */
        JLabel label = (JLabel)super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        // TODO Afegir el nombre d'incidències al tag
//      setText(value.toString() + " (" + issueCount + ")");
        setText(value.toString());
        URL icon = getClass().getResource("/org/fenrir/dragonfly/ui/icons/tag_16.png");
        label.setIcon(new ImageIcon(icon));
    
        return label;
    }
}
