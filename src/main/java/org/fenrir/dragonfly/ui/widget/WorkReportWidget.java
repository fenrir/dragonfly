package org.fenrir.dragonfly.ui.widget;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.core.dto.IVespineIssueDTO;
import org.fenrir.vespine.core.service.IIssueFacade;
import org.fenrir.vespine.spi.dto.IIssueDTO;
import org.fenrir.dragonfly.ui.dialog.WorkLogDialog;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20140503
 */
public class WorkReportWidget extends JComponent 
{
	private static final long serialVersionUID = 285731159074333870L;

	private final Logger log = LoggerFactory.getLogger(WorkReportWidget.class);
	
	private IIssueDTO issue;
	
	private JButton bReport;
	private ProgressBar progressBar;
	private JLabel lWorkDoneTitle;
	private JLabel lWorkDone;
	
	public WorkReportWidget()
	{
		this(null);
	}
	
	public WorkReportWidget(IIssueDTO issue)
	{
		createContents();
		setIssue(issue);
	}
	
	private void createContents()
	{
		bReport = new JButton("Add");
		bReport.addActionListener(new ActionListener() 
		{
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				new WorkLogDialog(issue).open();
				// Es refresca el contingut del widget
				updateContents();
			}
		});
		lWorkDoneTitle = new JLabel("Temps consumit");
		lWorkDoneTitle.setFont(new Font("Sans-Serif", Font.BOLD, 12));
		lWorkDone = new JLabel();
		progressBar = new ProgressBar();
		
		GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        /* Grup horitzontal */
        layout.setHorizontalGroup(layout.createSequentialGroup()    
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        		.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, Short.MAX_VALUE)
        		.addGroup(layout.createSequentialGroup()
    				.addComponent(lWorkDoneTitle)
    				.addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
    				.addComponent(lWorkDone)
        		)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(bReport, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
            .addContainerGap()
        );                            
        /* Grup vertical */
        layout.setVerticalGroup(layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        		.addComponent(progressBar, 20, 20, 20)
        		.addComponent(bReport)
            )
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
        		.addComponent(lWorkDoneTitle)
        		.addComponent(lWorkDone)
            )
            .addContainerGap()
        );
	}
	
	public void setIssue(IIssueDTO issue)
	{
		this.issue = issue;
		
		bReport.setEnabled(issue!=null);
		lWorkDoneTitle.setVisible(issue!=null);
		lWorkDone.setVisible(issue!=null);
		updateContents();
	}
	
	public void updateContents()
	{
		if(issue!=null){
			try{
				IIssueFacade issueService = (IIssueFacade)ApplicationContext.getInstance().getRegisteredComponent(IIssueFacade.class);
				// Es refresca l'entitat a l'entityManager.
				issue = issueService.findIssueById(issue.getIssueId());
				if(issue instanceof IVespineIssueDTO){
					lWorkDone.setText(((IVespineIssueDTO)issue).getWorkTimeFormated());
				}
				else{
					lWorkDone.setText(null);
				}
			}
			catch(Exception e){
				log.error("Error recuperant les dades de l'incidència: {}", e.getMessage(), e);
				lWorkDone.setText(null);
			}
		}
		
		progressBar.repaint();
	}
	
	private class ProgressBar extends JPanel
	{
		private static final long serialVersionUID = -5845989621074874325L;

		@Override
		public void paint(Graphics g) 
		{
			// Color de fons
			g.setColor(new Color(230, 230, 230));
			g.fillRect(0, 0, this.getWidth()-1, this.getHeight()-1);
			
			try{
				if(issue!=null){
					/* Es refresca l'objecte. 
					 * En un entorn CLIENT causa que es facin peticions al servidor cada vegada que es fa scroll sobre la pantalla. 
					 * Es comenta aquesta acció però s'ha de tenir en compte per si apareixen errors
					 */
//					IIssueFacade issueService = (IIssueFacade)ApplicationContext.getInstance().getRegisteredComponent(IIssueFacade.class);
//					issue = issueService.findIssueById(issue.getIssueId());
					
					// Completat
					Integer estimatedTime = issue.getEstimatedTime();
					if(issue instanceof IVespineIssueDTO){
						Integer workTime = ((IVespineIssueDTO)issue).getWorkTime();
						if(estimatedTime!=null && workTime!=null && estimatedTime>0 && workTime>0){
							double factor = (double)workTime / (double)estimatedTime;
							// La mida de la barra serà el més petit entre la mida total i el treball realitzat
							int width = Math.min((int)Math.floor((this.getWidth()-1)*factor), this.getWidth()-1);
							g.setColor(Color.GREEN);
							g.fillRect(0, 0, width, this.getHeight()-1);
							// Overflow de l'estimació
							if(factor>1){
								double overflowFactor = factor - 1;
								width = Math.min((int)Math.floor((this.getWidth()-1)*overflowFactor), this.getWidth()-1);
								g.setColor(Color.RED);
								g.fillRect(this.getWidth()-width, 0, width, this.getHeight()-1);
							}
						}
					}
				}
			}
			catch(Exception e){
				log.error("Error recuperant les dades de l'incidència per pintar la barra de progrés: {}", e.getMessage(), e);
			}
			
			// Border
			g.setColor(Color.BLACK);
			g.drawRect(0, 0, this.getWidth()-1, this.getHeight()-1);
		}
	}
}
