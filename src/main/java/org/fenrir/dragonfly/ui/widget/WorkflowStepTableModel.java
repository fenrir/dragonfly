package org.fenrir.dragonfly.ui.widget;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;
import org.fenrir.vespine.core.dto.IWorkflowStepDTO;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140419
 */
public class WorkflowStepTableModel extends AbstractTableModel
{
	private static final long serialVersionUID = 2957825675277435730L;

	private static final String[] COLUMN_NAMES = new String[]{"Estat origen", "", "Estat destí"};
	
	private static final int COLUMN_INDEX_SOURCE_STATUS = 0;
	private static final int COLUMN_INDEX_ARROW = 1;
	private static final int COLUMN_INDEX_DESTINATION_STATUS = 2;
	
	private List<IWorkflowStepDTO> modelData = new ArrayList<IWorkflowStepDTO>();
	
	@Override
	public int getRowCount() 
	{
		return modelData.size();
	}

	@Override
	public int getColumnCount() 
	{
		return COLUMN_NAMES.length;
	}
	
	@Override
	public String getColumnName(int column) 
	{
		return COLUMN_NAMES[column];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) 
	{
		if(COLUMN_INDEX_SOURCE_STATUS==columnIndex){
			return modelData.get(rowIndex).getSourceStatus();
		}
		else if(COLUMN_INDEX_ARROW==columnIndex){
			return "->";
		}
		else if(COLUMN_INDEX_DESTINATION_STATUS==columnIndex){
			return modelData.get(rowIndex).getDestinationStatus();
		}
		
		throw new IllegalArgumentException("Índex de columna no vàlid " + columnIndex);
	}
	
	public List<IWorkflowStepDTO> getModelData()
    {
        return modelData;
    }
    
    public void addData(IWorkflowStepDTO data)
    {
        this.modelData.add(data);
        fireTableRowsInserted(getRowCount()-1, getRowCount()-1);
    }
}
