package org.fenrir.dragonfly.ui.wizard;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.vespine.core.dto.IViewFilterDTO;
import org.fenrir.vespine.core.service.IAdministrationFacade;
import org.fenrir.dragonfly.ui.controller.ApplicationController;
import org.fenrir.dragonfly.core.dto.ViewFilterFormDTO;
import org.fenrir.dragonfly.core.event.IViewFilterListener;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20140818
 */
@EventListener(definitions=IViewFilterListener.class)
public class IssueViewFilterAdministrationWizard extends AbstractWizard implements IViewFilterListener
{
    private final Logger log = LoggerFactory.getLogger(IssueViewFilterAdministrationWizard.class);

    public static final int MODE_CREATION = 0;
    public static final int MODE_ADMINISTRATION = 1;

    private int mode;

    private IViewFilterDTO filter;

    private IssueViewFilterQueryWizardPage<IssueViewFilterAdministrationWizard> filterQueryWizardPage;
    private IssueViewFilterOrderWizardPage<IssueViewFilterAdministrationWizard> filterOrderWizardPage;
    private IssueViewFilterInfoWizardPage<IssueViewFilterAdministrationWizard> filterInfoWizardPage;

    public IssueViewFilterAdministrationWizard() throws Exception
    {
        this.mode = MODE_CREATION;
        setTitle("Creació d'un filtre");
        addPages();
    }

    public IssueViewFilterAdministrationWizard(IViewFilterDTO filter) throws Exception
    {
        this.mode = MODE_ADMINISTRATION;
        this.filter = filter;
        setTitle("Edició de les dades del filtre");
        addPages();
    }

    protected void addPages() throws Exception
    {
        // Query
        filterQueryWizardPage = new IssueViewFilterQueryWizardPage("Edició de la consulta del filtre",
                "Modifiqui la consulta d'obtenció de dades del filtre",
                this);
        addPage(filterQueryWizardPage);
        // Ordre
//        filterOrderWizardPage = new IssueViewFilterOrderWizardPage("Edició de la ordernació de l filtre",
//                "Modifiqui la ordenació de les dades del filtre",
//                this);
//        addPage(filterOrderWizardPage);
        // Info
        filterInfoWizardPage = new IssueViewFilterInfoWizardPage("Opcions del filtre",
                "Indiqui les dades per guardar el filtre",
                this);
        addPage(filterInfoWizardPage);
        // Inicialització de les dades
        if(mode==MODE_ADMINISTRATION){
            filterQueryWizardPage.setIssueViewFilterQuery(filter.getFilterQuery());
            filterQueryWizardPage.setIssueViewOrderbyClause(filter.getOrderClause());
            filterInfoWizardPage.setFilterSaveOptions(filter.getName(), filter.getDescription());
        }
    }

    @Override
    protected boolean canPerformPrevious()
    {
        // Varies pàgines i no es troba a la primera
        return !IssueViewFilterQueryWizardPage.ID.equals(getCurrentPage().getId());
    }

    @Override
    protected boolean canPerformNext()
    {
        // Varies pàgines i no es troba a l'última
        return !IssueViewFilterInfoWizardPage.ID.equals(getCurrentPage().getId());
    }

    @Override
    protected boolean canPerformFinalize()
    {
        // Varies pàgines i es troba a l'última
//        return IssueViewFilterInfoWizardPage.ID.equals(getCurrentPage().getId())
//                || IssueViewFilterOrderWizardPage.ID.equals(getCurrentPage().getId());
        return IssueViewFilterInfoWizardPage.ID.equals(getCurrentPage().getId());
    }

    @Override
    public boolean performPrevious()
    {
        return true;
    }

    @Override
    public boolean performNext()
    {
        return true;
    }

    @Override
    public boolean performFinalize()
    {
        IAdministrationFacade administrationService = (IAdministrationFacade)ApplicationContext.getInstance().getRegisteredComponent(IAdministrationFacade.class);
        IEventNotificationService eventNotificationService = (IEventNotificationService)ApplicationContext.getInstance().getRegisteredComponent(IEventNotificationService.class);
        ApplicationController applicationController = (ApplicationController)ApplicationContext.getInstance().getRegisteredComponent(ApplicationController.class);

        String filterName = filterInfoWizardPage.getFilterName();
        String filterDescription = filterInfoWizardPage.getFilterDescription();
        String filterQuery = filterQueryWizardPage.getIssueViewFilterQuery();
        String filterOrderByClause = filterQueryWizardPage.getIssueViewOrderByClause();

        // Si es un filtre temporal s'especifica directament
        if(!filterInfoWizardPage.saveRequired()){
        	ViewFilterFormDTO tempFilter = new ViewFilterFormDTO();
            tempFilter.setName("Filtre temporal");
            tempFilter.setDescription("Filtre temporal");
            tempFilter.setFilterQuery(filterQuery);
            applicationController.setSelectedFilter(tempFilter);
        }
        else{
            // Mode creació o NO sobreescriure filtre existent
            if(mode==MODE_CREATION || !filterInfoWizardPage.overideExistingFilter()){
                try{
                	filter = administrationService.createViewFilter(filterName, filterDescription, filterQuery, filterOrderByClause);
                    eventNotificationService.notifyNamedEvent(IViewFilterListener.class, IViewFilterListener.EVENT_VIEW_FILTER_CREATED_ID, filter);
                }
                catch(Exception e){
                    log.error("Error a creació del nou filtre: {}", e.getMessage(), e);
                    JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error a creació del nou filtre",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
            // Mode administració
            else{
            	IViewFilterDTO oldFilter = filter;
                try{
                	filter = administrationService.updateViewFilter(filter.getFilterId(), filterName, filterDescription, filterQuery, filterOrderByClause);
                    eventNotificationService.notifyNamedEvent(IViewFilterListener.class, IViewFilterListener.EVENT_VIEW_FILTER_UPDATED_ID, oldFilter, filter);
                }
                catch(Exception e){
                    log.error("Error a la modificació del filtre: {}", e.getMessage(), e);
                    JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), "Error a la modificació del filtre",
                            "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }

        return true;
    }

    @Override
    public boolean performCancel()
    {
        return true;
    }

    public IViewFilterDTO getFilter()
    {
        return filter;
    }

	@Override
	public void viewFilterCreated(IViewFilterDTO filter) 
	{
		// Res a fer...
	}

	@Override
	public void viewFilterModified(final IViewFilterDTO oldFilter, final IViewFilterDTO newFilter) 
	{
		if(SwingUtilities.isEventDispatchThread()){
			if(filter!=null && filter.getFilterId().equals(newFilter.getFilterId())){
				this.filter = (IViewFilterDTO)newFilter;
				JOptionPane.showMessageDialog(dialog, 
		                "Les dades del filtre han sigut modificades externament.", 
		                "Advertència", 
		                JOptionPane.WARNING_MESSAGE);
				
				dialog.close();
			}
		}
		else{
			SwingUtilities.invokeLater(new Runnable() 
            {
                @Override
                public void run() 
                {
                	viewFilterModified(oldFilter, newFilter);
                }
            });
		}
	}

	@Override
	public void viewFilterDeleted(IViewFilterDTO filter) 
	{
		// Res a fer...
	}

	@Override
	public void viewFilterOrdered() 
	{
		// Res a fer...
	}
}
