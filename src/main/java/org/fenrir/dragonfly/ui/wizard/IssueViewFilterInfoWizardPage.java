package org.fenrir.dragonfly.ui.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import org.apache.commons.lang.StringUtils;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.0.20121111
 */
public class IssueViewFilterInfoWizardPage<T extends AbstractWizard> extends AbstractWizardPage<T> implements KeyListener
{
    public static final String ID = "org.fenrir.dragonfly.ui.wizard.issueViewFilterInfoWizardPage";

    private JCheckBox checkSave;
    private JTextField inputName;
    private JTextField inputDescription;
    private ButtonGroup buttonGroup;
    private JRadioButton radioNewFilter;
    private JRadioButton radioOverrideFilter;

    private boolean firePageValidation = true;
    private boolean filterEdit = false;

    public IssueViewFilterInfoWizardPage(String title, String description, T wizard)throws Exception
    {
        super(title, description, wizard);
    }

    @Override
    public String getId()
    {
        return ID;
    }

    @Override
    protected JComponent createContents()
    {
        JPanel pContent = new JPanel();

        buttonGroup = new ButtonGroup();
        JLabel lName = new JLabel("Nom", JLabel.RIGHT);
        JLabel lDescription = new JLabel("Descripció", JLabel.RIGHT);
        inputName = new JTextField();
        inputName.addKeyListener(this);
        inputDescription = new JTextField();
        inputDescription.addKeyListener(this);
        checkSave = new JCheckBox("Guardar filtre");
        checkSave.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent event)
            {
                setSaveOptionsEnabled(checkSave.isSelected());
                wizard.validateCurrentPage();
            }
        });
        radioNewFilter = new JRadioButton("Crear un nou filtre", true);
        buttonGroup.add(radioNewFilter);
        radioOverrideFilter = new JRadioButton("Sobreescriure el filtre", false);
        buttonGroup.add(radioOverrideFilter);

        // Es deshabiliten inicialment les opcions de guardat
        setSaveOptionsEnabled(false);

        GroupLayout layout = new GroupLayout(pContent);
        pContent.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(checkSave, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                            .addComponent(lDescription, GroupLayout.DEFAULT_SIZE, 83, Short.MAX_VALUE)
                            .addComponent(lName, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        )
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                            .addComponent(radioOverrideFilter, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(radioNewFilter, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(inputName)
                            .addComponent(inputDescription, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
                        )
                    )
                )
                .addContainerGap()
            )
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(checkSave)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lName)
                    .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lDescription)
                    .addComponent(inputDescription, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioNewFilter)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioOverrideFilter)
                .addContainerGap(144, Short.MAX_VALUE)
            )
        );

        return pContent;
    }

    @Override
    public boolean validatePage()
    {
        if(checkSave.isSelected()){
            if(StringUtils.isBlank(inputName.getText())){
                wizard.showErrorMessage("S'ha d'especificar el nom del filtre");
                return false;
            }
            if(StringUtils.isBlank(inputDescription.getText())){
                wizard.showErrorMessage("S'ha d'especificar la descripció del filtre");
                return false;
            }
        }

        return true;
    }

    private void setSaveOptionsEnabled(boolean enabled)
    {
        inputName.setEnabled(enabled);
        inputDescription.setEnabled(enabled);

        radioNewFilter.setEnabled(enabled);
        if(filterEdit){
            radioOverrideFilter.setEnabled(enabled);
        }        
    }

    public boolean saveRequired()
    {
        return checkSave.isSelected();
    }

    public boolean overideExistingFilter()
    {
        return radioOverrideFilter.isSelected();
    }

    public String getFilterName()
    {
        return inputName.getText();
    }

    public String getFilterDescription()
    {
        return inputDescription.getText();
    }

    public void setFilterSaveOptions(String filterName, String filterDescription)
    {
        firePageValidation = false;
        
        inputName.setText(filterName);
        inputDescription.setText(filterDescription);
        radioOverrideFilter.setSelected(true);
        // Per deixar l'aspecte del radioButton deshabilitat s'ha de tornar a fer setEnabled(false)
        radioOverrideFilter.setEnabled(false);
        filterEdit = true;

        firePageValidation = true;
    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        if(firePageValidation){
            wizard.validateCurrentPage();
        }
    }

    @Override
    public void keyTyped(KeyEvent e)
    {

    }

    @Override
    public void keyPressed(KeyEvent e)
    {

    }
}
