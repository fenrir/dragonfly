package org.fenrir.dragonfly.ui.wizard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.DefaultListModel;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.0.20121103
 */
public class IssueViewFilterOrderWizardPage<T extends AbstractWizard> extends AbstractWizardPage<T> implements ActionListener
{
    private final String ACTION_LEFT2RIGHT = "left2right";
    private final String ACTION_RIGHT2LEFT = "right2left";
    private final String ACTION_UP = "up";
    private final String ACTION_DOWN = "down";

    public static final String ID = "org.fenrir.dragonfly.ui.wizard.issueViewFilterOrderWizardPage";

    private final Logger log = LoggerFactory.getLogger(IssueViewFilterOrderWizardPage.class);

    private JList listOrigin;
    private JList listDestination;

    public IssueViewFilterOrderWizardPage(String title, String description, T wizard)throws Exception
    {
        super(title, description, wizard);

        loadData();
    }

    @Override
    public String getId()
    {
        return ID;
    }

    @Override
    protected JComponent createContents()
    {
        JPanel pContent = new JPanel();

        listOrigin = new JList(new DefaultListModel());
        JScrollPane scrollOrigin = new JScrollPane(listOrigin);
        listDestination = new JList(new DefaultListModel());
        JScrollPane scrollDestination = new JScrollPane(listDestination);
        JButton bRight = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/arrow_right_16.png")));
        bRight.setActionCommand(ACTION_LEFT2RIGHT);
        bRight.addActionListener(this);
        JButton bLeft = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/arrow_left_16.png")));
        bLeft.setActionCommand(ACTION_RIGHT2LEFT);
        bLeft.addActionListener(this);
        JButton bUp = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/arrow_up_16.png")));
        bUp.setActionCommand(ACTION_UP);
        bUp.addActionListener(this);
        JButton bDown = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/dragonfly/ui/icons/arrow_down_16.png")));
        bDown.setActionCommand(ACTION_DOWN);
        bDown.addActionListener(this);

        GroupLayout layout = new GroupLayout(pContent);
        pContent.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollOrigin, GroupLayout.DEFAULT_SIZE, 212, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(bRight, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                    .addComponent(bLeft, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                    .addComponent(bUp, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                    .addComponent(bDown, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollDestination, GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE)
                .addContainerGap()
            )
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(scrollOrigin, GroupLayout.DEFAULT_SIZE, 398, Short.MAX_VALUE)
                    .addComponent(scrollDestination)
                )
                .addContainerGap()
            )
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(bRight)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bLeft)
                .addGap(34, 34, 34)
                .addComponent(bUp)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bDown)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

        return pContent;
    }

    @Override
    public boolean validatePage()
    {
        DefaultListModel listDestinationModel = (DefaultListModel)listDestination.getModel();
        return listDestinationModel.size()>0;
    }

    private void loadData()
    {
        DefaultListModel listOriginModel = (DefaultListModel)listOrigin.getModel();
        listOriginModel.addElement("id");
        listOriginModel.addElement("issueId");
        listOriginModel.addElement("summary");
        listOriginModel.addElement("projectId");
        listOriginModel.addElement("projectName");
        listOriginModel.addElement("statusId");
        listOriginModel.addElement("statusName");
        listOriginModel.addElement("sendDate");
        listOriginModel.addElement("slaDate");
        listOriginModel.addElement("modifiedDate");
    }

    public List<String> getFilterOrderColumns()
    {
        List<String> columns = new ArrayList<String>();

        Enumeration elements = ((DefaultListModel)listDestination.getModel()).elements();
        while(elements.hasMoreElements()){
            String column = (String)elements.nextElement();
            columns.add(column);
        }

        return columns;
    }

    public void setFilterOrderColumns(List<String> columns)
    {
        DefaultListModel listOriginModel = (DefaultListModel)listOrigin.getModel();
        DefaultListModel listDestinationModel = (DefaultListModel)listDestination.getModel();
        for(String column:columns){
            if(listOriginModel.contains(column)){
                listOriginModel.removeElement(column);
            }
            listDestinationModel.addElement(column);
        }
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
        DefaultListModel listOriginModel = (DefaultListModel)listOrigin.getModel();
        DefaultListModel listDestinationModel = (DefaultListModel)listDestination.getModel();
        // Afegir columna d'ordenació
        if(ACTION_LEFT2RIGHT.equals(event.getActionCommand())){
            int selectedIndex = listOrigin.getSelectedIndex();
            if(selectedIndex>=0){
                Object selectedItem = listOriginModel.get(selectedIndex);
                listOriginModel.remove(selectedIndex);
                listDestinationModel.addElement(selectedItem);
            }
            else{
                // TODO Mostrar avís
            }

            // Es valida la pàgina
            wizard.validateCurrentPage();
        }
        // Treure columna d'ordenació
        else if(ACTION_RIGHT2LEFT.equals(event.getActionCommand())){
            int selectedIndex = listDestination.getSelectedIndex();
            if(selectedIndex>=0){
                Object selectedItem = listDestinationModel.get(selectedIndex);
                listDestinationModel.remove(selectedIndex);
                listOriginModel.addElement(selectedItem);
            }
            else{
                // TODO Mostrar avís
            }

            // Es valida la pàgina
            wizard.validateCurrentPage();
        }
        // Ascendir columna d'ordenació
        else if(ACTION_UP.equals(event.getActionCommand())){
            int selectedIndex = listDestination.getSelectedIndex();
            if(selectedIndex>0){
                Object selectedItem = listDestinationModel.get(selectedIndex);
                listDestinationModel.remove(selectedIndex);
                listDestinationModel.add(selectedIndex-1, selectedItem);
            }
            else{
                // TODO Mostrar avís
            }
        }
        // Ascendir columna d'ordenació
        else if(ACTION_DOWN.equals(event.getActionCommand())){
            int selectedIndex = listDestination.getSelectedIndex();
            if(selectedIndex>=0 && selectedIndex<listDestinationModel.size()-1){
                Object selectedItem = listDestinationModel.get(selectedIndex);
                listDestinationModel.remove(selectedIndex);
                listDestinationModel.add(selectedIndex+1, selectedItem);
            }
            else{
                // TODO Mostrar avís
            }
        }
    }
}
