package org.fenrir.dragonfly.ui.wizard;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.widget.AdvancedTextField;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20131207
 */
public class IssueViewFilterQueryWizardPage<T extends AbstractWizard> extends AbstractWizardPage<T> implements DocumentListener
{
	private static final long serialVersionUID = 4892049761246878660L;

	public static final String ID = "org.fenrir.dragonfly.ui.wizard.issueViewFilterQueryWizardPage";

    private final Logger log = LoggerFactory.getLogger(IssueViewFilterQueryWizardPage.class);

    private JEditorPane editorFilterQuery;
    private JEditorPane editorFilterOrderByClause;

    private boolean firePageValidation = true;

    public IssueViewFilterQueryWizardPage(String title, String description, T wizard)throws Exception
    {
        super(title, description, wizard);
    }

    @Override
    public String getId()
    {
        return ID;
    }

    @Override
    protected JComponent createContents()
    {
        JPanel pContent = new JPanel();

        JLabel lQuery = new JLabel("Consulta");
        editorFilterQuery = new JEditorPane();
        editorFilterQuery.getDocument().addDocumentListener(this);
        JScrollPane scrollFilterQuery = new JScrollPane();
        scrollFilterQuery.setViewportView(editorFilterQuery);
        JLabel lOrder = new JLabel("Ordre");
        editorFilterOrderByClause = new JEditorPane();
        editorFilterOrderByClause.getDocument().addDocumentListener(this);
        JScrollPane scrollFilterOrderByClause = new JScrollPane();
        scrollFilterOrderByClause.setViewportView(editorFilterOrderByClause);

        GroupLayout layout = new GroupLayout(pContent);
        pContent.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(lQuery)
                    .addComponent(scrollFilterQuery)
                    .addComponent(lOrder)
                    .addComponent(scrollFilterOrderByClause)
                )
                .addContainerGap()
            )
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()                
                .addComponent(lQuery)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollFilterQuery, GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lOrder)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollFilterOrderByClause, GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                .addContainerGap()
            )
        );

        return pContent;
    }

    @Override
    public boolean validatePage()
    {
        if(StringUtils.isBlank(editorFilterQuery.getText())){
            wizard.showErrorMessage("S'ha d'especificar la consulta del filtre");
            return false;
        }
        // TODO Validació de l'expressió

        return true;
    }

    public String getIssueViewFilterQuery()
    {
        return editorFilterQuery.getText();
    }

    public void setIssueViewFilterQuery(String issueViewFilterQuery)
    {
        // Per tal que no llenci els events de canvi dels camps i activi les validacions
        firePageValidation = false;

        editorFilterQuery.setText(issueViewFilterQuery);

        // Es torna a activar les validacions
        firePageValidation = true;
    }

    public String getIssueViewOrderByClause()
    {
        return editorFilterOrderByClause.getText();
    }

    public void setIssueViewOrderbyClause(String orderByClause)
    {
        // Per tal que no llenci els events de canvi dels camps i activi les validacions
        firePageValidation = false;

        editorFilterOrderByClause.setText(orderByClause);

        // Es torna a activar les validacions
        firePageValidation = true;
    }

    @Override
    public void insertUpdate(DocumentEvent event)
    {
        if(firePageValidation){
            wizard.validateCurrentPage();
        }
    }

    @Override
    public void removeUpdate(DocumentEvent event)
    {
        if(log.isDebugEnabled()){
            log.debug("Rebut event d'esborrat de l'editor de la query");
        }

        if(firePageValidation){
            wizard.validateCurrentPage();
        }
    }

    @Override
    public void changedUpdate(DocumentEvent event)
    {
        if(log.isDebugEnabled()){
            log.debug("Rebut event de modificació de l'editor de la query");
        }

        if(firePageValidation){
            wizard.validateCurrentPage();
        }
    }
}
