package org.fenrir.dragonfly.ui.wizard;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import javax.swing.JScrollPane;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.dragonfly.core.dto.ReportParameterDTO;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.0.20121104
 */
public class ReportParametersWizardPage<T extends AbstractWizard> extends AbstractWizardPage<T> implements DocumentListener
{
    public static final String ID = "org.fenrir.dragonfly.ui.wizard.reportParametersWizardPage";

    private final Logger log = LoggerFactory.getLogger(ReportParametersWizardPage.class);

    private JPanel pContent;
    private Map<ReportParameterDTO, JTextField> fields = new HashMap<ReportParameterDTO, JTextField>();

    public ReportParametersWizardPage(String title, String description, T wizard)throws Exception
    {
        super(title, description, wizard);
    }

    @Override
    public String getId()
    {
        return ID;
    }

    @Override
    protected JComponent createContents()
    {
        pContent = new JPanel();

        pContent.setLayout(new GridBagLayout());
        JScrollPane scroll = new JScrollPane(pContent);        

        return scroll;
    }

    @Override
    public boolean validatePage()
    {
        for(ReportParameterDTO param:fields.keySet()){
            JTextField field = fields.get(param);
            if(StringUtils.isBlank(field.getText())){
                wizard.showErrorMessage("S'ha d'especificar un valor pel camp " + param.getDescription());
                return false;
            }            
        }

        return true;
    }    

    @Override
    public void insertUpdate(DocumentEvent e) 
    {
        wizard.validateCurrentPage();
    }

    @Override
    public void removeUpdate(DocumentEvent e) 
    {
        wizard.validateCurrentPage();
    }

    @Override
    public void changedUpdate(DocumentEvent e) 
    {
        wizard.validateCurrentPage();
    }

    public List<ReportParameterDTO> getReportParameters()
    {
        List<ReportParameterDTO> parameters = new ArrayList<ReportParameterDTO>();

        for(ReportParameterDTO param:fields.keySet()){
            JTextField field = fields.get(param);
            // Tipus Date
            if(Date.class.getName().equals(param.getType())){
                try{
                    Date date = new SimpleDateFormat("dd/MM/yyyy").parse(field.getText());
                    param.setValue(date);
                }
                catch(ParseException e){
                    log.error("Error tractant data {}: {}", new Object[]{field.getText(), e.getMessage(), e});
                }
            }
            // Tipus String per defecte
            else{
                param.setValue(field.getText());
            }

            parameters.add(param);
        }

        return parameters;
    }

    public void setReportParameters(List<ReportParameterDTO> parameters)
    {
        int row = 0;
        for(ReportParameterDTO param:parameters){
            /* Etiqueta */
            JLabel lName = new JLabel(param.getDescription(), JLabel.RIGHT);
            GridBagConstraints constraints = new GridBagConstraints();
            if(row==0){
                constraints.insets = new Insets(20, 10, 0, 10);
            }
            else{
                constraints.insets = new Insets(10, 10, 0, 10);
            }
            constraints.weightx = 0;
            constraints.weighty = 0;
            constraints.gridx = 0;
            constraints.gridy = row;
            constraints.fill = GridBagConstraints.NONE;
            pContent.add(lName, constraints);

            /* Camp */
            JTextField inputValue;
            // Tipus Date
            if(Date.class.getName().equals(param.getType())){
                inputValue = new JFormattedTextField(new SimpleDateFormat("dd/MM/yyyy"));
            }
            else{
                inputValue = new JTextField();
            }
            inputValue.getDocument().addDocumentListener(this);
            constraints = new GridBagConstraints();
            if(row==0){
                constraints.insets = new Insets(20, 10, 0, 10);
            }
            else{
                constraints.insets = new Insets(10, 10, 0, 10);
            }
            constraints.weightx = 1;
            constraints.weighty = 0;
            constraints.gridx = 1;
            constraints.gridy = row;
            constraints.fill = GridBagConstraints.HORIZONTAL;
            pContent.add(inputValue, constraints);

            fields.put(param, inputValue);
            
            row++;
        }
        // S'afegeix un component extra perque la última fila agafi l'espai restant de la vista
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.weightx = 0;
        constraints.weighty = 1;
        constraints.gridx = 0;
        constraints.gridy = row;
        pContent.add(new JPanel(), constraints);
    }
}
