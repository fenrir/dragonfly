package org.fenrir.dragonfly.ui.wizard;

import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.dragonfly.core.dto.ReportParameterDTO;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20131202
 */
public class ReportPrintWizard extends AbstractWizard
{
    private final Logger log = LoggerFactory.getLogger(ReportPrintWizardPage.class);
    
    private String reportPath;
    private Rectangle oldDialogSize = null;

    private ReportParametersWizardPage parametersWizardPage;
    private ReportPrintWizardPage printWizardPage;

    public ReportPrintWizard(String reportPath) throws Exception
    {
        this.reportPath = reportPath;

        addPages();        
    }

    protected void addPages() throws Exception
    {
        List<ReportParameterDTO> parameters = readReportParameters(reportPath);
        if(!parameters.isEmpty()){
            parametersWizardPage = new ReportParametersWizardPage("Introducció dels paràmetres de l'informe",
                    "Introdueixi els valors dels paràmetres d'entrada de l'informe",
                    this);
            addPage(parametersWizardPage);
            // Es carreguen els paràmetres del report a la pàgina inicial
            parametersWizardPage.setReportParameters(parameters);
        }
        
        printWizardPage = new ReportPrintWizardPage("Informe resultant", 
                "", 
                this);
        addPage(printWizardPage);

        // Si no hi ha paràmetres es presenta el report directament
        if(parameters.isEmpty()){
            printWizardPage.printReport(reportPath);
        }
    }

    @Override
    protected boolean canPerformPrevious()
    {
        return false;
    }

    @Override
    protected boolean canPerformNext()
    {
        return ReportParametersWizardPage.ID.equals(getCurrentPage().getId());
    }

    @Override
    protected boolean canPerformFinalize()
    {
        return ReportPrintWizardPage.ID.equals(getCurrentPage().getId());
    }

    @Override
    public boolean performPrevious()
    {
        return true;
    }

    @Override
    public boolean performNext()
    {
        // Parametres -> Report
        if(ReportParametersWizardPage.ID.equals(getCurrentPage().getId())){
            try{
                List<ReportParameterDTO> parameters = parametersWizardPage.getReportParameters();
                printWizardPage.printReport(reportPath, parameters);
            
                // Es maximitza la finestra per presentar l'informe
                if(oldDialogSize==null){
                    oldDialogSize = dialog.getBounds();
                    Rectangle bounds = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();
                    dialog.setBounds(bounds);
                }

                return true;
            }
            catch(ApplicationException e){
                log.error("Error en crear el report {}: {}", new Object[]{reportPath, e.getMessage(), e});
                JOptionPane.showMessageDialog(ApplicationWindowManager.getInstance().getMainWindow(), e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }

        return false;
    }

    @Override
    public boolean performFinalize()
    {
        // No s'ha de fer res, només tancar la finestra
        return true;
    }

    @Override
    public boolean performCancel()
    {
        return true;
    }

    private List<ReportParameterDTO> readReportParameters(String reportPath) throws Exception
    {
        List<ReportParameterDTO> parameterDTOs = new ArrayList<ReportParameterDTO>();

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(getClass().getResourceAsStream(reportPath));

        XPath xpath = XPathFactory.newInstance().newXPath();
        XPathExpression exprParameter = xpath.compile("//parameter");
        NodeList parameters = (NodeList)exprParameter.evaluate(document, XPathConstants.NODESET);
        for(int i=0; i<parameters.getLength(); i++){
            Node parameterNode = parameters.item(i);
            String parameterName = parameterNode.getAttributes().getNamedItem("name").getNodeValue();
            String parameterClass = parameterNode.getAttributes().getNamedItem("class").getNodeValue();
            XPathExpression expreDescription = xpath.compile("//parameter[@name='" + parameterName + "']/parameterDescription/text()");
            String parameterDescription = (String)expreDescription.evaluate(document, XPathConstants.STRING);
            if(parameterDescription==null){
                parameterDescription = parameterName;
            }

            ReportParameterDTO dto = new ReportParameterDTO();
            dto.setName(parameterName);
            dto.setDescription(StringUtils.defaultString(parameterDescription, parameterName));
            dto.setType(parameterClass);
            parameterDTOs.add(dto);
        }

        return parameterDTOs;
    }
}
