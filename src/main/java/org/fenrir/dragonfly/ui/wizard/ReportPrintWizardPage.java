package org.fenrir.dragonfly.ui.wizard;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JPanel;

import java.sql.Connection;
import java.sql.SQLException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.jdbc.Work;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.swing.JRViewer;

import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;
import org.fenrir.dragonfly.core.dto.ReportParameterDTO;
import org.fenrir.dragonfly.util.ConnectionWrapper;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.3.20131202
 */
public class ReportPrintWizardPage<T extends AbstractWizard> extends AbstractWizardPage<T>
{
    public static final String ID = "org.fenrir.dragonfly.ui.wizard.reportPrintWizardPage";
    
    private JPanel pContents;
    
    public ReportPrintWizardPage(String title, String description, T wizard)throws Exception
    {
        super(title, description, wizard);
    }
    
    @Override
    public String getId()
    {
        return ID;
    }

    @Override
    protected JComponent createContents()
    {
        pContents = new JPanel();
        pContents.setLayout(new BorderLayout());     
        
        return pContents;
    }
    
    public void printReport(String reportPath) throws ApplicationException
    {
        List<ReportParameterDTO> parameters = Collections.emptyList();
        printReport(reportPath, parameters);
    }
    
    public void printReport(final String reportPath, List<ReportParameterDTO> parameters) throws ApplicationException
    {
        final Map<String, Object> reportParameters = new HashMap<String, Object>();
        for(ReportParameterDTO param:parameters){
            reportParameters.put(param.getName(), param.getValue());
        }
        
        try{            
            ConnectionWrapper connectionWrapper = (ConnectionWrapper)ApplicationContext.getInstance().getRegisteredComponent(ConnectionWrapper.class);
            connectionWrapper.doWork(new Work() 
            {
                @Override
                public void execute(Connection connection) throws SQLException 
                {
                    try{
                        InputStream fileStream = getClass().getResourceAsStream(reportPath);
                        JasperReport compiledReport = JasperCompileManager.compileReport(fileStream);
                        JasperPrint jasperPrint = JasperFillManager.fillReport(compiledReport, reportParameters, connection);
                        JRViewer jasperViewer = new JRViewer(jasperPrint);
                        pContents.add(jasperViewer, BorderLayout.CENTER);
                    }
                    catch(JRException e){
                        throw new RuntimeException(e);
                    }
                }
            });                                                
        }
        catch(Exception e){            
            throw new ApplicationException("Error en crear el report", e);
        }
    }
    
    @Override
    public boolean validatePage()
    {
        // Res a validar
        return true;
    }
}
