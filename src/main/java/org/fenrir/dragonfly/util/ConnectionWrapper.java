package org.fenrir.dragonfly.util;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import com.google.inject.Provider;
import org.hibernate.Session;
import org.hibernate.ejb.HibernateEntityManager;
import org.hibernate.jdbc.Work;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.0.20121105
 */
public class ConnectionWrapper 
{
    /* Al tractar-se el DAO com a Singleton s'ha d'injectar el provider en comptes del manager
     */
    @Inject 
    private Provider<EntityManager> entityManagerProvider;
	
    public void setEntityManagerProvider(Provider<EntityManager> entityManagerProvider)
    {
        this.entityManagerProvider = entityManagerProvider;
    }
    
    public Session getSession()
    {
        HibernateEntityManager em = (HibernateEntityManager)entityManagerProvider.get();
        return em.getSession();
    }
    
    public void doWork(Work work)
    {
        getSession().doWork(work);
    }
}
