package org.fenrir.dragonfly.util;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.fenrir.vespine.spi.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20131202
 */
public class HtmlPageScrapper 
{
    private static final String DEFAULT_USER_AGENT = "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13";
    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final String DEFAULT_SCHEME = "http";
    
    private final Logger log = LoggerFactory.getLogger(HtmlPageScrapper.class);
    
    private HttpClient httpclient;
    private HttpContext localContext;
    
    private String scheme;
    private String host;
    private int port = -1;
    
    private List<String> patterns = new ArrayList<String>();
    
    public HtmlPageScrapper()
    {
        CookieStore cookieStore = new BasicCookieStore();
        localContext = new BasicHttpContext();
        localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
        httpclient = new DefaultHttpClient();
    }
    
    public String getScheme()
    {
        return scheme!=null ? scheme : DEFAULT_SCHEME;
    }
    
    public HtmlPageScrapper setScheme(String scheme)
    {
        this.scheme = scheme;
        return this;
    }
    
    public String getHost()
    {
        return host;
    }
    
    public HtmlPageScrapper setHost(String host)
    {
        this.host = host;
        return this;
    }
    
    public int getPort()
    {
        return port;
    }
    
    public HtmlPageScrapper setPort(int port)
    {
        this.port = port;
        return this;
    }
    
    public List<String> getPatterns()
    {
        return patterns;
    }
    
    public HtmlPageScrapper addPattern(String pattern)
    {
        patterns.add(pattern);
        return this;
    }

    public void login(String loginPath, Map<String, String> params, String loginFailedPage) throws BusinessException
    {
        HttpResponse response = doPostRequest(loginPath, params);
        HttpEntity entity = response.getEntity();        
        if(entity==null){
            throw new BusinessException("No s'ha pogut contactar amb el servidor " + host + ":" + port + "/" + loginPath);
        }
        // Tractar resposta login incorrecte
        String responseLocation = response.getFirstHeader("Location").getValue();
        if(responseLocation.contains(loginFailedPage)){
            log.info("Tractant resposta de login incorrecte");
            throw new BusinessException("Trancant resposta de login incorrecte");
        }
        // S'ha de consumir la reposta obligatoriament
        try{
            EntityUtils.consume(entity);
        }
        catch(IOException e){
            log.error("Error tractant resposta del login: {}", e.getMessage(), e);
            throw new BusinessException("Error realitzant login: " + e.getMessage(), e);
        }
    }
    
    public ResultSet scrapData(String path, Map<String, String> params) throws BusinessException
    {
        ResultSet results = new ResultSet();
        
        HttpResponse response = doGetRequest(path, params);
        HttpEntity entity = response.getEntity();
        if(entity==null && response.getStatusLine().getStatusCode()!=200){
            throw new BusinessException("No s'ha pogut contactar amb el servidor " + host + ":" + port + "/" + path);
        }
        try{
            String pageCode = EntityUtils.toString(entity, DEFAULT_ENCODING);            
            if(pageCode!=null && pageCode.length()>0){
                for(String strPattern:patterns){
                    // S'indica que el caràcter . sigui també per salts de linia
                    Pattern pattern = Pattern.compile(strPattern, Pattern.DOTALL);
                    Matcher matcher = pattern.matcher(pageCode);
                    while(matcher.find()){
                        ResultMatch match = new ResultMatch(matcher.group(0));
                        /* S'itera sobre els grups per recuperar les diferents parts de l'expressió
                         * Es descarta el grup 0 perquè és l'expressió regular sencera
                         */
                        for(int i=1; i<matcher.groupCount(); i++){
                            match.addResultPart(matcher.group(i));
                        }
                        results.putValue(strPattern, match);
                    }
                }
            }
            else{
                throw new BusinessException("No s'ha pogut rebre contingut del servidor per la url " + host + ":" + port + "/" + path);
            }
        }
        catch(IOException e){
            log.error("Error obtenint les dades de la url {}:{}/{}", new Object[]{host, port, path, e});
            throw new BusinessException("Error obtenint les dades de la url " + host + ":" + port + "/" + path, e);
        }
        
        return results;
    }
    
    private HttpResponse doGetRequest(String path, Map<String, String> params) throws BusinessException
    {
        List<NameValuePair> qparams = new ArrayList<NameValuePair>();
        for(String key:params.keySet()){
            qparams.add(new BasicNameValuePair(key, params.get(key)));
        }
        try{
            String requestScheme = scheme!=null ? scheme : DEFAULT_SCHEME;
            URI uri = URIUtils.createURI(requestScheme, host, port, path, URLEncodedUtils.format(qparams, DEFAULT_ENCODING), null);
            return doGetRequest(uri);
        }
        catch(URISyntaxException e){
            log.error("Error descarregant desde el path {}: {}", new Object[]{path, e.getMessage(), e});
            throw new BusinessException("Error descarregant desde el path " + path + ": " + e.getMessage(), e);
        }
    }
    
    private HttpResponse doGetRequest(URI uri) throws BusinessException
    {
        try{            
            HttpGet httpget = new HttpGet(uri);
            httpget.setHeader("User-Agent", DEFAULT_USER_AGENT);

            HttpResponse response = httpclient.execute(httpget, localContext);
            return response;            
        }
        catch(IOException e){
            log.error("Error descarregant desde la url {}: {}", new Object[]{uri, e.getMessage(), e});
            throw new BusinessException("Error descarregant desde la url " + uri + ": " + e.getMessage(), e);
        }
    }
    
    private HttpResponse doPostRequest(String path, Map<String, String> params) throws BusinessException
    {
        List<NameValuePair> qparams = new ArrayList<NameValuePair>();
        for(String key:params.keySet()){
            qparams.add(new BasicNameValuePair(key, params.get(key)));
        }
        URI uri = null;
        try{
            String requestScheme = scheme!=null ? scheme : DEFAULT_SCHEME;
            uri = URIUtils.createURI(requestScheme, host, port, path, null, null);

            HttpPost httpPost = new HttpPost(uri);
            httpPost.setHeader("User-Agent", DEFAULT_USER_AGENT);
            httpPost.setEntity(new UrlEncodedFormEntity(qparams));
            HttpResponse response = httpclient.execute(httpPost, localContext);            
            
            return response;            
        }
        catch(URISyntaxException e){
            log.error("Error descarregant desde el path {}: {}", new Object[]{path, e.getMessage(), e});
            throw new BusinessException("Error descarregant desde el path " + path + ": " + e.getMessage(), e);
        }
        catch(IOException e){
            log.error("Error descarregant desde la url {}: {}", new Object[]{uri, e.getMessage(), e});
            throw new BusinessException("Error descarregant desde la url " + uri + ": " + e.getMessage(), e);
        }
    }
        
    public static class ResultSet
    {
        private Map<String, List<ResultMatch>> results = new HashMap<String, List<ResultMatch>>();

        public Set<String> keySet()
        {
            return results.keySet();
        }
        
        public boolean containsKey(String key)
        {
            return results.containsKey(key);
        }
        
        public List<ResultMatch> getValues(String key)
        {
            return results.get(key);
        }
        
        public ResultMatch getFirstValue(String key)
        {
            if(results.containsKey(key) && results.get(key).size()>0){
                return results.get(key).get(0);
            }
            
            return null;
        }
        
        public void putValue(String key, ResultMatch value)
        {
            if(results.containsKey(key)){
                results.get(key).add(value);
            }
            else{
                List values = new ArrayList<String>();
                values.add(value);
                results.put(key, values);
            }
        }                
    }
    
    public static class ResultMatch
    {
        private String fullResult;
        private List<String> resultParts = new ArrayList<String>();
        
        public ResultMatch(String fullResult)
        {
            this.fullResult = fullResult;
        }
        
        public String getFullResult()
        {
            return fullResult;
        }
        
        public boolean hasResultParts()
        {
            return !resultParts.isEmpty();
        }
        
        public List<String> getResultParts()
        {
            return resultParts;
        }
        
        public void addResultPart(String part)
        {
            resultParts.add(part);
        }        
    }
}
