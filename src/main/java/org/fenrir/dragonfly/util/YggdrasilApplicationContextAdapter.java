package org.fenrir.dragonfly.util;

import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.vespine.spi.IApplicationContext;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20140329
 */
public class YggdrasilApplicationContextAdapter implements IApplicationContext 
{
	private ApplicationContext applicationContext;
	
	public YggdrasilApplicationContextAdapter(ApplicationContext applicationContext)
	{
		this.applicationContext = applicationContext;
	}
	
	@Override
	public Object getRegisteredComponent(Class<?> type) 
	{
		return applicationContext.getRegisteredComponent(type);
	}

	@Override
	public Object getRegisteredComponent(Class<?> type, String name) 
	{
		return applicationContext.getRegisteredComponent(type, name);
	}

	@Override
	public void injectMembers(Object object) 
	{
		applicationContext.injectMembers(object);
	}
}