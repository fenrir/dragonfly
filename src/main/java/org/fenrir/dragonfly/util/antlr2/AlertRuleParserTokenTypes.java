// $ANTLR 2.7.7 (20060906): "AlertRuleGrammar.g" -> "AlertRuleLexer.java"$

    package org.fenrir.dragonfly.util.antlr2;

public interface AlertRuleParserTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int LITERAL_var = 4;
	int EQUALS = 5;
	int SEMI = 6;
	int IDENT = 7;
	int COMMA = 8;
	int INTLIT = 9;
	int STRING_LITERAL = 10;
	int LSQUAREBRACKET = 11;
	int RSQUAREBRACKET = 12;
	int LITERAL_function = 13;
	int LPAREN = 14;
	int RPAREN = 15;
	int LITERAL_return = 16;
	int LITERAL_if = 17;
	int LCURLYBRACKET = 18;
	// "else if" = 19
	int LITERAL_else = 20;
	int RCURLYBRACKET = 21;
	int LITERAL_while = 22;
	int DOT = 23;
	// "!" = 24
	int PLUS = 25;
	int MINUS = 26;
	int TIMES = 27;
	int DIV = 28;
	int LITERAL_mod = 29;
	int NOT_EQUALS = 30;
	int GT = 31;
	int GTE = 32;
	int LT = 33;
	int LTE = 34;
	int LITERAL_and = 35;
	int LITERAL_or = 36;
	int COMMENT = 37;
	int DIGIT = 38;
	int CHARLIT = 39;
	int WS = 40;
}
