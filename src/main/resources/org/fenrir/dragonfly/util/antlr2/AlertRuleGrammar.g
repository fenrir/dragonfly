header{
    package org.fenrir.dragonfly.util.antlr2;
}

//-----------------------------------------------------------------------------
// Definició del Parser
//-----------------------------------------------------------------------------
class AlertRuleParser extends Parser;
options {
    // No generar handlers d'errors de parseig. S'utilitzaran els que hi ha per defecte
    defaultErrorHandler = true; 
    // Construir arbre AST
    buildAST = true;
}

{
    public void validate() throws RecognitionException, TokenStreamException
    {
        program();
    }
}

//-----------------------------------------------------------------------------
// Regles del parser
//-----------------------------------------------------------------------------
protected program :    
    subprogramBody
;

protected subprogramBody : 
    statementList    
;

protected varDecl :
    "var" identList
    (EQUALS constantValue)?
    SEMI
;

protected identList :
    IDENT (COMMA IDENT)*
;

protected constantValue :
    INTLIT
        | STRING_LITERAL
        | IDENT
;

protected arrayDecl :
    "var" IDENT EQUALS LSQUAREBRACKET RSQUAREBRACKET
;

protected integerConstant :
    INTLIT
        | IDENT
;

protected functionDecl :
    "function" IDENT (formalParameters)? EQUALS
        subprogramBody
    SEMI
;

protected formalParameters :
    LPAREN parameterSpec (COMMA parameterSpec)* RPAREN
;

protected parameterSpec : 
    identList
;

protected statement :
    returnStatement
        | ifStatement
        | loopStatement
        | (IDENT (LPAREN|SEMI))=> procedureCallStatement
        | assignmentStatement
;

protected statementList :
    statement  statementList        
        | // Res
;

protected assignmentStatement : 
    ("var")? variableReference EQUALS expression SEMI
;

protected procedureCallStatement :
    IDENT (actualParameters)? SEMI
;

protected actualParameters : 
    LPAREN (expression (COMMA expression)*)? RPAREN
;

protected returnStatement : 
    "return" SEMI
;

protected ifStatement :
    "if" ifPart
;

protected ifPart : 
    LPAREN expression RPAREN 
    LCURLYBRACKET
    statementList
    ( "else if" ifPart
        | "else" statementList
    )?
    RCURLYBRACKET
;

protected loopStatement : 
    "while" LPAREN expression RPAREN
    LCURLYBRACKET
        statementList
    RCURLYBRACKET    
;

protected variableReference :
    IDENT
    ( LSQUAREBRACKET expression RSQUAREBRACKET
        | DOT IDENT
    )*
;

protected primitiveElement : 
    variableReference
        | constantValue
        | LPAREN expression RPAREN
;

protected booleanNegationExpression : 
    ("!")* primitiveElement
;

protected signExpression : 
    ((PLUS|MINUS))* booleanNegationExpression
;


protected multiplyingExpression : 
    signExpression ((TIMES|DIV|"mod") signExpression)*
;

protected addingExpression : 
    multiplyingExpression ((PLUS|MINUS) multiplyingExpression)*
;

protected relationalExpression : 
    addingExpression ((EQUALS|NOT_EQUALS|GT|GTE|LT|LTE) addingExpression)*
;

protected expression : 
    relationalExpression (("and"|"or") relationalExpression)*
;

//----------------------------------------------------------------------------
// Definició de l'scanner
//----------------------------------------------------------------------------
class AlertRuleLexer extends Lexer;

options{
    // Caràcters admesos
    charVocabulary = '\0'..'\377';
    // No comprobar automàticament els literals
    testLiterals = false;
    // 2 caràcters de lookahead
    k = 2;    
}

// Comentaris simples; S'ignoren
protected COMMENT : "//" (~('\n'|'\r'))*
    { $setType(Token.SKIP); }
;

// Literals
protected DIGIT : 
    '0'..'9'
;

protected INTLIT :
    (DIGIT)+
;

protected CHARLIT :
    '\''! . '\''!
;

protected STRING_LITERAL :
    '"'!
    ( '"' '"'!
        | ~('"'|'\n'|'\r')
    )*
    ( '"'!
        | // Res a fer
    )
;

// Espais en blanc
protected WS : 
    ( ' '
        | '\t'
        | '\f'

        // Gestionar canvis de linia
        | ( "\r\n"  // DOS/Windows
            | '\r'    // Macintosh
            | '\n'    // Unix
        )
        // Increamentar el comptador de linies a l'scanner
        { newline(); }
    )
    // S'ignoren els tokens d'aquest tipus
    { $setType(Token.SKIP); }
;

// Identificadors. S'indica testLiterals = true el que vol dir que després de fer match de la regla
// es comproba la taula de literals per mirar si es un literal o un identificador
protected IDENT
    options {testLiterals=true;}
    : ('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9')*
;
  
// Operadors
protected DOT            : '.'   ;
protected SEMI           : ';'   ;
protected COMMA          : ','   ;
protected EQUALS         : '='   ;
protected LSQUAREBRACKET : '['   ;
protected RSQUAREBRACKET : ']'   ;
protected LCURLYBRACKET  : '{'   ;
protected RCURLYBRACKET  : '}'   ;
protected LPAREN         : '('   ;
protected RPAREN         : ')'   ;
protected NOT_EQUALS     : "!="  ;
protected LT             : '<'   ;
protected LTE            : "<="  ;
protected GT             : '>'   ;
protected GTE            : ">="  ;
protected PLUS           : '+'   ;
protected MINUS          : '-'   ;
protected TIMES          : '*'   ;
protected DIV            : '/'   ;
