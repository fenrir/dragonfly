package org.fenrir.dragonfly.test;

import antlr.Token;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import org.fenrir.dragonfly.util.antlr2.AlertRuleLexer;
import org.fenrir.dragonfly.util.antlr2.AlertRuleParser;
import org.junit.Test;

/**
 * TODO v1.0 Documentació
 * @author Anatonio Archilla Nava
 * @version v0.0.20121221
 */
public class AntlrGrammarTest 
{
    @Test
    public void alertTypeRuleGrammarTest() throws Exception
    {
        File file = new File(getClass().getResource("/org/fenrir/dragonfly/test/antlr2/sample1.txt").toURI());
        BufferedReader reader = new BufferedReader(new FileReader(file));
        AlertRuleLexer lexer = new AlertRuleLexer(reader);        
        AlertRuleParser parser = new AlertRuleParser(lexer);
    }
}
