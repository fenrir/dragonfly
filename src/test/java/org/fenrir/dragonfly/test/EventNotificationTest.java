package org.fenrir.dragonfly.test;

import com.google.inject.Guice;
import com.google.inject.Injector;
import junit.framework.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.fenrir.dragonfly.test.event.Event;
import org.fenrir.dragonfly.test.event.EventNotificationTestModule;
import org.fenrir.dragonfly.test.event.ITestListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;

/**
 * TODO v1.0 Documentació
 * @author Anatonio Archilla Nava
 * @version v0.0.20121031
 */
public class EventNotificationTest 
{
    private static Injector mainInjector;
    
    @BeforeClass
    public static void testCaseSetup()
    {
        mainInjector = Guice.createInjector(new EventNotificationTestModule());
    }
    
    @Test
    public void simpleListenerTest() throws Exception
    {
        IEventNotificationService eventNotificationService = mainInjector.getInstance(IEventNotificationService.class);
        
        // Event per defecte
        Event event = new Event();
        eventNotificationService.notifyEvent(ITestListener.class, event);
        Assert.assertEquals(ITestListener.ID_DEFAULT, event.getId());

        // 1st event
        event = new Event();
        eventNotificationService.notifyNamedEvent(ITestListener.class, ITestListener.ID_FIRST, event);
        Assert.assertEquals(ITestListener.ID_FIRST, event.getId());                
    }
    
    @Test
    public void voidEventTest() throws Exception
    {
        IEventNotificationService eventNotificationService = mainInjector.getInstance(IEventNotificationService.class);
        eventNotificationService.notifyNamedEvent(ITestListener.class, ITestListener.ID_VOID);
    }
}
