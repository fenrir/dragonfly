package org.fenrir.dragonfly.test;

import javax.script.Bindings;
import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import junit.framework.Assert;
import org.junit.Test;
import org.fenrir.vespine.core.entity.Issue;
import org.fenrir.vespine.core.entity.IssueProject;
import org.fenrir.vespine.core.entity.IssueStatus;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.3.20131127
 */
public class ScriptEngineTest 
{
    @Test
    public void rhinoTest() throws Exception
    {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("js");
        Compilable compileEngine = (Compilable)engine;
        
        IssueStatus status = new IssueStatus();
        status.setName("Estat de prova");
        
        IssueProject project = new IssueProject();
        project.setName("Projecte de proves");
        
        Issue issue = new Issue();
        issue.setSummary("Incidència de proves");
        issue.setProject(project);
        issue.setStatus(status);
        
        Bindings bindings = engine.createBindings();
        bindings.put("issue", issue);
        
        String script = "var accepted = false;"
                + "accepted = issue.getProject().getProjectId()==1;";
        CompiledScript compiledScript = compileEngine.compile(script);        
        
        Object result = compiledScript.eval(bindings);
        Assert.assertEquals(true, result);
    }
}
