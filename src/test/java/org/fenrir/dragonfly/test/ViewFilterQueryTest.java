package org.fenrir.dragonfly.test;

import java.util.regex.Pattern;
import junit.framework.Assert;
import org.junit.Test;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.0.20121102
 */
public class ViewFilterQueryTest
{
    @Test
    public void querySyntaxPortionRegexTest()
    {
        String issueRegex = "(?:ISSUE)\\.(?:id|name)";
        String input = "issue.id";
        Pattern pattern = Pattern.compile(issueRegex, Pattern.CASE_INSENSITIVE);
        Assert.assertTrue("q1: Sintaxi invàlida", pattern.matcher(input).matches());

        String projectRegex = "(?:PROJECT)\\.(?:id|name)";
        input = "project.id";
        pattern = Pattern.compile(projectRegex, Pattern.CASE_INSENSITIVE);
        Assert.assertTrue("q2: Sintaxi invàlida", pattern.matcher(input).matches());

        String expressionRegex = "(" + issueRegex + ")"
                + "(?:\\W*(?:=|!<>)\\W*|\\W+(in|not in)\\W+)"
                + "(" + projectRegex + ")";
        input = "issue.id = project.id";
        pattern = Pattern.compile(expressionRegex, Pattern.CASE_INSENSITIVE);
        Assert.assertTrue("q3: Sintaxi invàlida", pattern.matcher(input).matches());

        String completeExpression = "(" + expressionRegex +")"
                + "(?:"
                    + "(?:\\W+(AND|OR)\\W+)"
                    + "\\1"
                + ")*";
        input = "issue.id = project.id and issue.id = project.id";
        pattern = Pattern.compile(completeExpression, Pattern.CASE_INSENSITIVE);
        Assert.assertTrue("q4: Sintaxi invàlida", pattern.matcher(input).matches());

        String parenthesesExpression = "((?:ISSUE)\\.(?:id))"
                + "(?:\\W*(?:=)\\W*)"
                + "([(]*(?:PROJECT)\\.(?:id)(?:,\\W*\\3)*[)]*)";
        input = "(issue.id = (project.id))";
        pattern = Pattern.compile(parenthesesExpression, Pattern.CASE_INSENSITIVE);
        Assert.assertTrue("q5: Sintaxi invàlida", pattern.matcher(input).matches());
    }

    @Test
    public void querySyntaxRegexTest()
    {
        String input = "issue.id=project.name    and    issue.id=project.name";
        Assert.assertTrue("q1: Sintaxis invàlida", checkRegexSyntax(input));

        input = "issue.id=project.name and issue.id=(project.name) or issue.id=project.name";
        Assert.assertTrue("q2: Sintaxis invàlida", checkRegexSyntax(input));
    }

    private boolean checkRegexSyntax(String query)
    {
        String issueRegex = "(?:ISSUE)\\.(?:id|name|issueId|project|status|sendDate|slaDate|resolutionDate)";
        String projectRegex = "(?:PROJECT)\\.(?:id|name|projectId)";
        String operatorsRegex = "(?:\\W*(?:=|<>)\\W*|\\W+(?:in|not in)\\W+)";

        String expressionRegex = "[(]*"
                + "(" + issueRegex + ")"
                + operatorsRegex
                + "(" + projectRegex + ")"
                + "[)]*";

        String completeExpression = "^(?:\\W*(" + expressionRegex +")\\W*"
                + "(?:"
                    + "(?:\\W+(AND|OR)\\W+)"
                    + "\\1"
                + ")*)$";

        Pattern pattern = Pattern.compile(completeExpression, Pattern.CASE_INSENSITIVE);
        return pattern.matcher(query).matches();
    }
}
