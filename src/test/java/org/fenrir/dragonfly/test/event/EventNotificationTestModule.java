package org.fenrir.dragonfly.test.event;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.AbstractMatcher;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;
import org.fenrir.yggdrasil.core.service.impl.EventNotificationServiceImpl;

/**
 * TODO v1.0 Documentació
 * @author Anatonio Archilla Nava
 * @version v0.3.20131202
 */
public class EventNotificationTestModule extends AbstractModule
{
    private final Logger log = LoggerFactory.getLogger(EventNotificationServiceImpl.class);
    
    @Override
    protected void configure()
    {
        bind(IEventNotificationService.class).to(EventNotificationServiceImpl.class).in(Singleton.class);
        bind(TestListenerImpl.class).toInstance(new TestListenerImpl());
        // Listener per inicialitzar els listeners després de la seva injecció
        EventNotificationTestModule.ObserverTypeListener typeListener = new EventNotificationTestModule.ObserverTypeListener();
        bindListener(new AbstractMatcher<TypeLiteral<?>>() 
        {
            @Override
            public boolean matches(TypeLiteral<?> typeLiteral) 
            {
                return typeLiteral.getRawType().getAnnotation(EventListener.class)!=null
                        || IEventNotificationService.class.isAssignableFrom(typeLiteral.getRawType());
            }
        }, typeListener);
    }
    
    class ObserverTypeListener implements TypeListener
    {        
        private IEventNotificationService eventNotificationService;
        private Map<Class, List<Object>> listenerMap = new HashMap<Class, List<Object>>();
        
        @Override
        public <I> void hear(final TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) 
        {
            typeEncounter.register(new InjectionListener<I>() 
            {
                /**
                 * Mètode executat una vegada Guice ha injectat tots els membres a l'objecte
                 * S'han de tractar 2 casos: 1) S'està tractant el service de gestió d'events, cas
                 * en que es guardarà la referència a aquest per poder especificar els listeners que han de
                 * ser tractats i afegir els que ja s'han tractat anteriorment si n'hi ha.
                 * 2) S'està tractant un listener, cas en que es guardarà la seva referència si encara 
                 * no s'ha pogut recuperar el service o bé s'especificarà directament a aquest si ja s'ha pogut recuperar
                 * @param i I Objecte del que s'han injectat els membres.
                 */
                @Override
                public void afterInjection(I i) 
                {
                    // Service
                    if(IEventNotificationService.class.isAssignableFrom(i.getClass())){
                        if(log.isDebugEnabled()){
                            log.debug("Executant post inicialització del service de gestió d'events {}", i.getClass().getName());
                        }
                        eventNotificationService = (IEventNotificationService)i;
                        for(Class key:listenerMap.keySet()){
                            List<Object> listeners = listenerMap.get(key);
                            eventNotificationService.addListeners(key, listeners);
                        }
                    }
                    // Listener
                    else{
                        if(log.isDebugEnabled()){
                            log.debug("Executant post inicialització del listener {}", i.getClass().getName());
                        }
                        EventListener listenerAnnotation = i.getClass().getAnnotation(EventListener.class);
                        Class[] definitions = listenerAnnotation.definitions();
                        for(Class definition:definitions){
                            if(eventNotificationService!=null){
                                eventNotificationService.addListener(definition, i);
                            }
                            else{
                                List<Object> listeners = listenerMap.get(definition);
                                if(listeners==null){
                                    listeners = new ArrayList<Object>();
                                    listenerMap.put(definition, listeners);
                                }
                                listeners.add(i);
                            }
                        }                        
                    }
                }
            });
        }
    }
}
