package org.fenrir.dragonfly.test.event;

import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.3.20131202
 */
public interface ITestListener 
{
    public static final String ID_DEFAULT = "ITestListener.default";
    public static final String ID_FIRST = "ITestListener.first";
    public static final String ID_SECOND = "ITestListener.second";
    public static final String ID_VOID = "ITestListener.void";
    
    public void performDefaultEvent(Event event);

    @EventMethod(eventName=ID_FIRST)
    public void performFirstEvent(Event event);

    @EventMethod(eventName=ID_SECOND)
    public void performSecondEvent(Event event);
    
    @EventMethod(eventName=ID_VOID)
    public void performVoidEvent();
}
