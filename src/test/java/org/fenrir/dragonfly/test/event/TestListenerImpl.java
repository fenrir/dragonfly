package org.fenrir.dragonfly.test.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.event.annotation.EventListener;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131202
 */
@EventListener(definitions=ITestListener.class)
public class TestListenerImpl implements ITestListener 
{	
    private final Logger log = LoggerFactory.getLogger(TestListenerImpl.class);
    
    @Override
    public void performDefaultEvent(Event event)
    {
        event.setId(ID_DEFAULT);
    }

    @Override
    public void performFirstEvent(Event event)
    {
        event.setId(ID_FIRST);
    }

    @Override
    public void performSecondEvent(Event event)
    {
        event.setId(ID_SECOND);
    }

    @Override
    public void performVoidEvent() 
    {
        log.info("Executat event VOID");
    }        
}
